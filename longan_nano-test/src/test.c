#include "inc.h"
//
uint16_t test_func_c_001(uint16_t in)
{
	uint16_t temp_loc_1 = (in & 0xff) << 8;
	uint16_t temp_loc_2 = (in & 0xff00) >> 8;
	return (temp_loc_1|temp_loc_2);
}
