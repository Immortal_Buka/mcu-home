#include "std_system.hpp"
//
#define WA	__attribute__ ((weak, alias ("_Z15Default_Handlerv")))
//
void isr_jumps(void);
void call_ctors(const func_ptr* start, const func_ptr* end);
void Default_Handler(void);
extern "C"
{
	void Reset_Handler(void);
	void eclic_msip_handler(void) WA;
	void eclic_mtip_handler(void) WA;
	void eclic_bwei_handler(void) WA;
	void eclic_pmovi_handler(void) WA;
	void WWDGT_IRQHandler(void) WA;
	void LVD_IRQHandler(void) WA;
	void TAMPER_IRQHandler(void) WA;
	void RTC_IRQHandler(void) WA;
	void FMC_IRQHandler(void) WA;
	void RCU_IRQHandler(void) WA;
	void EXTI0_IRQHandler(void) WA;
	void EXTI1_IRQHandler(void) WA;
	void EXTI2_IRQHandler(void) WA;
	void EXTI3_IRQHandler(void) WA;
	void EXTI4_IRQHandler(void) WA;
	void DMA0_Channel0_IRQHandler(void) WA;
	void DMA0_Channel1_IRQHandler(void) WA;
	void DMA0_Channel2_IRQHandler(void) WA;
	void DMA0_Channel3_IRQHandler(void) WA;
	void DMA0_Channel4_IRQHandler(void) WA;
	void DMA0_Channel5_IRQHandler(void) WA;
	void DMA0_Channel6_IRQHandler(void) WA;
	void ADC0_1_IRQHandler(void) WA;
	void CAN0_TX_IRQHandler(void) WA;
	void CAN0_RX0_IRQHandler(void) WA;
	void CAN0_RX1_IRQHandler(void) WA;
	void CAN0_EWMC_IRQHandler(void) WA;
	void EXTI5_9_IRQHandler(void) WA;
	void TIMER0_BRK_IRQHandler(void) WA;
	void TIMER0_UP_IRQHandler(void) WA;
	void TIMER0_TRG_CMT_IRQHandler(void) WA;
	void TIMER0_Channel_IRQHandler(void) WA;
	void TIMER1_IRQHandler(void) WA;
	void TIMER2_IRQHandler(void) WA;
	void TIMER3_IRQHandler(void) WA;
	void I2C0_EV_IRQHandler(void) WA;
	void I2C0_ER_IRQHandler(void) WA;
	void I2C1_EV_IRQHandler(void) WA;
	void I2C1_ER_IRQHandler(void) WA;
	void SPI0_IRQHandler(void) WA;
	void SPI1_IRQHandler(void) WA;
	void USART0_IRQHandler(void) WA;
	void USART1_IRQHandler(void) WA;
	void USART2_IRQHandler(void) WA;
	void EXTI10_15_IRQHandler(void) WA;
	void RTC_Alarm_IRQHandler(void) WA;
	void USBFS_WKUP_IRQHandler(void) WA;
	void EXMC_IRQHandler(void) WA;
	void TIMER4_IRQHandler(void) WA;
	void SPI2_IRQHandler(void) WA;
	void UART3_IRQHandler(void) WA;
	void UART4_IRQHandler(void) WA;
	void TIMER5_IRQHandler(void) WA;
	void TIMER6_IRQHandler(void) WA;
	void DMA1_Channel0_IRQHandler(void) WA;
	void DMA1_Channel1_IRQHandler(void) WA;
	void DMA1_Channel2_IRQHandler(void) WA;
	void DMA1_Channel3_IRQHandler(void) WA;
	void DMA1_Channel4_IRQHandler(void) WA;
	void CAN1_TX_IRQHandler(void) WA;
	void CAN1_RX0_IRQHandler(void) WA;
	void CAN1_RX1_IRQHandler(void) WA;
	void CAN1_EWMC_IRQHandler(void) WA;
	void USBFS_IRQHandler(void) WA;
}
//
#if __clang__
volatile const uint8_t compiler_clang[4] __attribute__((section (".version"))) = {'c', __clang_major__, __clang_minor__, __clang_patchlevel__};
#else
	#if defined __GNUC__
volatile const uint8_t compiler_gcc[4] __attribute__((section (".version"))) = {'g', __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__};
	#endif
#endif
//
__attribute__((noreturn, section(".isr_vector"))) void Reset_Handler(void)
{
	asm("la sp, __StackTop");
	extern void sys_init(void);
	extern void main(void);
	extern uint32_t __etext;
	extern uint32_t __data_start__;
	extern uint32_t __data_end__;
	extern uint32_t __bss_start__;
	extern uint32_t __bss_end__;
	extern func_ptr __cpp_begin[];
	extern func_ptr __cpp_end[];
	uint32_t* pSrc;
	uint32_t* pDest;
	sys_init();
	pSrc = &__etext;
	pDest = &__data_start__;
	while(pDest < &__data_end__) *pDest++ = *pSrc++;
	pDest = &__bss_start__;
	while(pDest < &__bss_end__) *pDest++ = 0;
	call_ctors(__cpp_begin, __cpp_end);
	main();

}
void call_ctors(const func_ptr* start, const func_ptr* end)
{
	for(; start < end; ++start)
	{
		if(*start) (*start)();
	}
}
void delay(uint32_t data)
{
	volatile uint32_t temp1 = data;
	while (temp1--) __asm("NOP");
}
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size)
{
	for(uint32_t i=0; i<(size>>2); i++) ptr[i] = value;
}
__attribute__((interrupt)) void Default_Handler(void)
{
	while(1){}
}
__attribute__ ((section(".isr_vectors"),naked,noreturn)) void isr_jumps(void)
{
	asm("j Reset_Handler");				//0
	asm("j _Z15Default_Handlerv");		//1
	asm("j _Z15Default_Handlerv");		//2
	asm("j eclic_msip_handler");		//3
	asm("j _Z15Default_Handlerv");		//4
	asm("j _Z15Default_Handlerv");		//5
	asm("j _Z15Default_Handlerv");		//6
	asm("j eclic_mtip_handler");		//7
	asm("j _Z15Default_Handlerv");		//8
	asm("j _Z15Default_Handlerv"); 		//9
	asm("j _Z15Default_Handlerv");		//10
	asm("j _Z15Default_Handlerv");		//11
	asm("j _Z15Default_Handlerv");		//12
	asm("j _Z15Default_Handlerv");		//13
	asm("j _Z15Default_Handlerv");		//14
	asm("j _Z15Default_Handlerv");		//15
	asm("j _Z15Default_Handlerv");		//16
	asm("j eclic_bwei_handler"); 		//17
	asm("j eclic_pmovi_handler");		//18
	asm("j WWDGT_IRQHandler"); 			//19
	asm("j LVD_IRQHandler");			//20
	asm("j TAMPER_IRQHandler");			//21
	asm("j RTC_IRQHandler");			//22
	asm("j FMC_IRQHandler");			//23
	asm("j RCU_IRQHandler");			//24
	asm("j EXTI0_IRQHandler");			//25
	asm("j EXTI1_IRQHandler");			//26
	asm("j EXTI2_IRQHandler");			//27
	asm("j EXTI3_IRQHandler");			//28
	asm("j EXTI4_IRQHandler");			//29
	asm("j DMA0_Channel0_IRQHandler");	//30
	asm("j DMA0_Channel1_IRQHandler");	//31
	asm("j DMA0_Channel2_IRQHandler");	//32
	asm("j DMA0_Channel3_IRQHandler");	//33
	asm("j DMA0_Channel4_IRQHandler");	//34
	asm("j DMA0_Channel5_IRQHandler");	//35
	asm("j DMA0_Channel6_IRQHandler");	//36
	asm("j ADC0_1_IRQHandler");			//37
	asm("j CAN0_TX_IRQHandler");		//38
	asm("j CAN0_RX0_IRQHandler");		//39
	asm("j CAN0_RX1_IRQHandler");		//40
	asm("j CAN0_EWMC_IRQHandler");		//41
	asm("j EXTI5_9_IRQHandler");		//42
	asm("j TIMER0_BRK_IRQHandler");		//43
	asm("j TIMER0_UP_IRQHandler");		//44
	asm("j TIMER0_TRG_CMT_IRQHandler");	//45
	asm("j TIMER0_Channel_IRQHandler");	//46
	asm("j TIMER1_IRQHandler");			//47
	asm("j TIMER2_IRQHandler");			//48
	asm("j TIMER3_IRQHandler");			//49
	asm("j I2C0_EV_IRQHandler");		//50
	asm("j I2C0_ER_IRQHandler");		//51
	asm("j I2C1_EV_IRQHandler");		//52
	asm("j I2C1_ER_IRQHandler");		//53
	asm("j SPI0_IRQHandler");			//54
	asm("j SPI1_IRQHandler");			//55
	asm("j USART0_IRQHandler");			//56
	asm("j USART1_IRQHandler");			//57
	asm("j USART2_IRQHandler");			//58
	asm("j EXTI10_15_IRQHandler");		//59
	asm("j RTC_Alarm_IRQHandler");		//60
	asm("j USBFS_WKUP_IRQHandler");		//61
	asm("j _Z15Default_Handlerv");		//62
	asm("j _Z15Default_Handlerv");		//63
	asm("j _Z15Default_Handlerv");		//64
	asm("j _Z15Default_Handlerv");		//65
	asm("j _Z15Default_Handlerv");		//66
	asm("j EXMC_IRQHandler");			//67
	asm("j _Z15Default_Handlerv");		//68
	asm("j TIMER4_IRQHandler");			//69
	asm("j SPI2_IRQHandler");			//70
	asm("j UART3_IRQHandler");			//71
	asm("j UART4_IRQHandler");			//72
	asm("j TIMER5_IRQHandler");			//73
	asm("j TIMER6_IRQHandler");			//74
	asm("j DMA1_Channel0_IRQHandler");	//75
	asm("j DMA1_Channel1_IRQHandler");	//76
	asm("j DMA1_Channel2_IRQHandler");	//77
	asm("j DMA1_Channel3_IRQHandler");	//78
	asm("j DMA1_Channel4_IRQHandler");	//79
	asm("j _Z15Default_Handlerv");		//80
	asm("j _Z15Default_Handlerv");		//81
	asm("j CAN1_TX_IRQHandler");		//82
	asm("j CAN1_RX0_IRQHandler");		//83
	asm("j CAN1_RX1_IRQHandler");		//84
	asm("j CAN1_EWMC_IRQHandler");		//85
	asm("j USBFS_IRQHandler");			//86
}
