#ifndef _STD_SYSTEM_H_
#define _STD_SYSTEM_H_
#include <stdint.h>
#include <stdbool.h>
//
typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t number;
	uint8_t day;
	uint8_t month;
	uint8_t year;
} version_t;
typedef void(*func_ptr)();
//
void delay(uint32_t data);
void memset_word(uint32_t* ptr, uint32_t value, uint32_t size);
//
#endif//_STD_SYSTEM_H_
