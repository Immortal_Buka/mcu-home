#include <project.h>
//
CY_ISR(sw1_irq_handler);
CY_ISR(qe_irq_handler);
int main(void);
void uint32_to_string(uint32_t data, uint8_t* string_ptr);
//
uint32_t cnt, cnt_prev;
uint8_t string[11] = {0};
//
CY_ISR(sw1_irq_handler)
{
    sw1_in_ClearInterrupt();
    cnt = QuadDec_1_ReadCounter();
    uint32_to_string(cnt, string);
    UART_1_UartPutString(string);
    UART_1_UartPutString("\r\n");
}
CY_ISR(qe_irq_handler)
{
    CyDelay(1000); 
    QuadDec_1_ClearInterrupt(QuadDec_1_INTR_MASK_CC_MATCH);
    cnt = QuadDec_1_ReadCounter();
    //cnt_prev = cnt;
    uint32_to_string(cnt, string);
    UART_1_UartPutString(string);
    UART_1_UartPutString("\r\n");
}
int main(void)
{
    CyGlobalIntEnable;
    irq_2_StartEx(sw1_irq_handler);
    irq_1_StartEx(qe_irq_handler);
    UART_1_Start();
    QuadDec_1_Start();
    QuadDec_1_TriggerCommand(QuadDec_1_MASK, QuadDec_1_CMD_RELOAD);
    led_Write(1);
    while(1)
    {
    }
}
void uint32_to_string(uint32_t data, uint8_t* string_ptr)
{
	uint8_t temp_buf[10];
	uint8_t index = 0;
	temp_buf[0] = data % 10;
	for(int8_t j=1; j<10; j++)
	{
		data /= 10;
		temp_buf[j] = data % 10;
	}
	for(int8_t j=0; j<10; j++)
	{
		if(temp_buf[j] > 0) index = j;
	}
	//*size = index+1;
	for(int8_t j=index; j>(-1); j--)
	{
		*string_ptr = temp_buf[j] + 0x30;
		string_ptr++;
	}
	*string_ptr = 0;
}
