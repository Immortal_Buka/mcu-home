#include "system.h"
#include "usbd_core.h"
#include "stm32f4xx_ll_usb.h"
#include "usbd_hid.h"
//
#define DEQ								15
#define HEA								0x81
#define HES								0x04
#define USBD_MAX_NUM_CONFIGURATION     	1
#define USBD_MAX_STR_DESC_SIZ          	0x100
#define USBD_SUPPORT_USER_STRING        1
#define USBD_SELF_POWERED               1
#define USBD_DEBUG_LEVEL                0
//
void main(void);
USBD_StatusTypeDef USBD_LL_Init(USBD_HandleTypeDef* pdev);
HAL_StatusTypeDef PCDEx_SetRxFiFo(uint16_t size);
HAL_StatusTypeDef PCDEx_SetTxFiFo(uint8_t fifo, uint16_t size);
USBD_StatusTypeDef USBD_LL_Start(USBD_HandleTypeDef* pdev);
uint8_t* get_device_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_lang_id_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_manufacturer_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_product_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_serial_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_configuration_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t* get_interface_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length);
uint8_t hid_init(USBD_HandleTypeDef* pdev, uint8_t cfgidx);
uint8_t hid_deinit(USBD_HandleTypeDef* pdev, uint8_t cfgidx);
uint8_t hid_control_setup(USBD_HandleTypeDef* pdev, USBD_SetupReqTypedef  *req);
uint8_t hid_ep0_tx_sent(USBD_HandleTypeDef* pdev);
uint8_t hid_ep0_rx_ready(USBD_HandleTypeDef* pdev);
uint8_t hid_data_in(USBD_HandleTypeDef* pdev, uint8_t epnum);
uint8_t hid_data_out(USBD_HandleTypeDef* pdev, uint8_t epnum);
uint8_t hid_sof(USBD_HandleTypeDef* pdev);
uint8_t* get_config_descriptor(uint16_t *length);
uint8_t* get_dDevice_qualifier_descriptor(uint16_t *length);
uint8_t* get_user_str_descriptor(USBD_HandleTypeDef* pdev,uint8_t index,  uint16_t *length);
USBD_StatusTypeDef USBD_LL_OpenEP(USBD_HandleTypeDef* pdev, uint8_t ep_addr, uint8_t ep_type, uint16_t ep_mps);
//
struct
{
	USB_OTG_EPTypeDef IN_ep[16U];   /*!< IN endpoint parameters             */
	USB_OTG_EPTypeDef OUT_ep[16U];  /*!< OUT endpoint parameters            */
	uint32_t Setup[12U];   /*!< Setup packet buffer                */
	void* pData;       /*!< Pointer to upper stack Handler */
} dev_hpcd;
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x23,
	.month = 0x10,
	.year = 0x18
};
USBD_HandleTypeDef usb_dev;
USB_OTG_CfgTypeDef init_cfg;
USBD_DescriptorsTypeDef descriptors =
{
	get_device_descriptor,
	get_lang_id_str_descriptor,
	get_manufacturer_str_descriptor,
	get_product_str_descriptor,
	get_serial_str_descriptor,
	get_configuration_str_descriptor,
	get_interface_str_descriptor,
};
USBD_ClassTypeDef class_functions =
{
	hid_init,
	hid_deinit,
	hid_control_setup,
	hid_ep0_tx_sent,
	hid_ep0_rx_ready,
	hid_data_in,
	hid_data_out,
	hid_sof,
	NULL,
	NULL,
	get_config_descriptor,
	get_config_descriptor,
	get_config_descriptor,
	get_dDevice_qualifier_descriptor,
	get_user_str_descriptor,
};
__ALIGN_BEGIN uint8_t dev_desc[] __ALIGN_END =
{
	0x12,//bLength
	0x01,//bDescriptorType
	0x00,//bcdUSB
	0x02,
	0x00,//bDeviceClass
	0x00,//bDeviceSubClass
	0x00,//bDeviceProtocol
	0x40,//bMaxPacketSize0
	0xaa,//idVendor
	0xbb,
	0xcc,//idProduct
	0xdd,
	0x02,//bcdDevice
	0x01,
	0x01,//iManufacturer
	0x02,//iProduct
	0x03,//iSerialNumber
	0x01,//bNumConfigurations
};
__ALIGN_BEGIN uint8_t lang_id_desc[] __ALIGN_END =
{
	0x04,//bLength
	0x03,//bDescriptorType
	0x09,//wLangID[0]
	0x04,
};
__ALIGN_BEGIN uint8_t str_desc[256] __ALIGN_END ;
USBD_HID_HandleTypeDef usb_hid_struct;
__ALIGN_BEGIN uint8_t cfg_desc[] __ALIGN_END =
{
};
//
void main(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_OTGHSEN;
	USBD_Init(&usb_dev, &descriptors, 0);
	USBD_RegisterClass(&usb_dev, &class_functions);
	USBD_Start(&usb_dev);
	while (1)
	{}
}
USBD_StatusTypeDef USBD_LL_Init(USBD_HandleTypeDef* pdev)
{
	init_cfg.dev_endpoints = DEQ;
	init_cfg.use_dedicated_ep1 = 0;
	init_cfg.ep0_mps = 0x40;
	init_cfg.dma_enable = 0;
	init_cfg.low_power_enable = 0;
	init_cfg.phy_itface = 2;
	init_cfg.speed = 1;
	init_cfg.Sof_enable = 0;
	init_cfg.vbus_sensing_enable = 0;
	NVIC_EnableIRQ(OTG_HS_IRQn);
	USB_DisableGlobalInt(USB_OTG_HS);
	USB_CoreInit(USB_OTG_HS, init_cfg);
	USB_SetCurrentMode(USB_OTG_HS, USB_OTG_DEVICE_MODE);
 	for (uint8_t i = 0; i < 15; i++)
 	{
 		dev_hpcd.IN_ep[i].is_in = 1U;
 		dev_hpcd.IN_ep[i].num = i;
 		dev_hpcd.IN_ep[i].tx_fifo_num = i;
 		dev_hpcd.IN_ep[i].type = EP_TYPE_CTRL;
 		dev_hpcd.IN_ep[i].maxpacket = 0U;
 		dev_hpcd.IN_ep[i].xfer_buff = 0U;
 		dev_hpcd.IN_ep[i].xfer_len = 0U;
 		dev_hpcd.OUT_ep[i].is_in = 0U;
 		dev_hpcd.OUT_ep[i].num = i;
 		dev_hpcd.IN_ep[i].tx_fifo_num = i;
 		dev_hpcd.OUT_ep[i].type = EP_TYPE_CTRL;
 		dev_hpcd.OUT_ep[i].maxpacket = 0U;
 		dev_hpcd.OUT_ep[i].xfer_buff = 0U;
 		dev_hpcd.OUT_ep[i].xfer_len = 0U;
 		USB_OTG_HS->DIEPTXF[i] = 0U;
 	}
 	USB_DevInit(USB_OTG_HS, init_cfg);
 	USB_DevDisconnect(USB_OTG_HS);
 	NVIC_SetPriority(OTG_HS_IRQn, 0);
 	NVIC_EnableIRQ(OTG_HS_IRQn);
  	PCDEx_SetRxFiFo(0x200);
  	PCDEx_SetTxFiFo(0, 0x100);
  	PCDEx_SetTxFiFo(1, 0x100);
  	PCDEx_SetTxFiFo(2, 0x100);
  	return USBD_OK;
}
HAL_StatusTypeDef PCDEx_SetRxFiFo(uint16_t size)
{
	USB_OTG_HS->GRXFSIZ = size;
	return HAL_OK;
}
HAL_StatusTypeDef PCDEx_SetTxFiFo(uint8_t fifo, uint16_t size)
{
	uint32_t Tx_Offset = USB_OTG_HS->GRXFSIZ;
	if(fifo == 0) USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = (uint32_t)(((uint32_t)size << 16U) | Tx_Offset);
	else
	{
		Tx_Offset += (USB_OTG_HS->DIEPTXF0_HNPTXFSIZ) >> 16U;
		for (uint8_t i = 0; i < (fifo - 1); i++) Tx_Offset += (USB_OTG_HS->DIEPTXF[i] >> 16U);
		USB_OTG_HS->DIEPTXF[fifo - 1] = (uint32_t)(((uint32_t)size << 16U) | Tx_Offset);
	}
	return HAL_OK;
}
USBD_StatusTypeDef USBD_LL_Start(USBD_HandleTypeDef* pdev)
{
	USB_DevConnect(USB_OTG_HS);
	USB_EnableGlobalInt(USB_OTG_HS);
	return USBD_OK;
}
uint8_t* get_device_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	*length = sizeof(dev_desc);
	return dev_desc;
}
uint8_t* get_lang_id_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	*length = sizeof(lang_id_desc);
	return lang_id_desc;
}
uint8_t* get_manufacturer_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	USBD_GetString("Buka", str_desc, length);
	return str_desc;
}
uint8_t* get_product_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	USBD_GetString("HID device", str_desc, length);
	return str_desc;
}
uint8_t* get_serial_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	USBD_GetString("UH0001_23102018", str_desc, length);
	return str_desc;
}
uint8_t* get_configuration_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	USBD_GetString("conf_string", str_desc, length);
	return str_desc;
}
uint8_t* get_interface_str_descriptor(USBD_SpeedTypeDef speed, uint16_t* length)
{
	USBD_GetString("interface_string", str_desc, length);
	return str_desc;
}
uint8_t hid_init(USBD_HandleTypeDef* pdev, uint8_t cfgidx)
{
}
uint8_t hid_deinit(USBD_HandleTypeDef* pdev, uint8_t cfgidx)
{
}
uint8_t hid_control_setup(USBD_HandleTypeDef* pdev, USBD_SetupReqTypedef  *req)
{
}
uint8_t hid_ep0_tx_sent(USBD_HandleTypeDef* pdev)
{
}
uint8_t hid_ep0_rx_ready(USBD_HandleTypeDef* pdev)
{
}
uint8_t hid_data_in(USBD_HandleTypeDef* pdev, uint8_t epnum)
{
}
uint8_t hid_data_out(USBD_HandleTypeDef* pdev, uint8_t epnum)
{
}
uint8_t hid_sof(USBD_HandleTypeDef* pdev)
{
}
uint8_t* get_config_descriptor(uint16_t *length)
{
	*length = sizeof(cfg_desc);
	return cfg_desc;
}
uint8_t* get_dDevice_qualifier_descriptor(uint16_t *length)
{
}
uint8_t* get_user_str_descriptor(USBD_HandleTypeDef* pdev,uint8_t index,  uint16_t *length)
{
}
USBD_StatusTypeDef USBD_LL_OpenEP(USBD_HandleTypeDef* pdev, uint8_t ep_addr, uint8_t ep_type, uint16_t ep_mps)
{
	USB_OTG_EPTypeDef *ep;
	if ((ep_addr & 0x80) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr & 0x7F];
	ep->num   = ep_addr & 0x7F;
	ep->is_in = (0x80 & ep_addr) != 0;
	ep->maxpacket = ep_mps;
	ep->type = ep_type;
	if (ep->is_in) ep->tx_fifo_num = ep->num;
	if (ep_type == EP_TYPE_BULK) ep->data_pid_start = 0U;
	USB_ActivateEndpoint(USB_OTG_HS, ep);
	return USBD_OK;
}
USBD_StatusTypeDef USBD_LL_CloseEP(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	USB_OTG_EPTypeDef* ep;
	if ((ep_addr & 0x80) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr & 0x7F];
	ep->num   = ep_addr & 0x7F;
	ep->is_in = (0x80 & ep_addr) != 0;
	USB_DeactivateEndpoint(USB_OTG_HS, ep);
	return USBD_OK;
}
