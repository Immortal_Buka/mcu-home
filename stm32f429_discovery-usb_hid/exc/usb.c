#ifdef AA
 #define RX_FIFO_HS_SIZE                          512
 #define TX0_FIFO_HS_SIZE                         512
 #define TX1_FIFO_HS_SIZE                         512
 #define TX2_FIFO_HS_SIZE                          0
 #define TX3_FIFO_HS_SIZE                          0
 #define TX4_FIFO_HS_SIZE                          0
 #define TX5_FIFO_HS_SIZE                          0
 #define TXH_NP_HS_FIFOSIZ                         96
 #define TXH_P_HS_FIFOSIZ                          96
#else
 #define RX_FIFO_FS_SIZE                          128
 #define TX0_FIFO_FS_SIZE                          64
 #define TX1_FIFO_FS_SIZE                         128
 #define TX2_FIFO_FS_SIZE                          0
 #define TX3_FIFO_FS_SIZE                          0
 #define TXH_NP_HS_FIFOSIZ                         96
 #define TXH_P_HS_FIFOSIZ                          96
#endif
#define HID_DESCRIPTOR_TYPE           0x21
#define HID_REPORT_DESC               0x22
#define HID_REQ_SET_PROTOCOL          0x0B
#define HID_REQ_GET_PROTOCOL          0x03
#define HID_REQ_SET_IDLE              0x0A
#define HID_REQ_GET_IDLE              0x02
#define HID_REQ_SET_REPORT            0x09
#define HID_REQ_GET_REPORT            0x01
#define  USB_LEN_DEV_QUALIFIER_DESC                     0x0A
#define  USB_LEN_DEV_DESC                               0x12
#define  USB_LEN_CFG_DESC                               0x09
#define  USB_LEN_IF_DESC                                0x09
#define  USB_LEN_EP_DESC                                0x07
#define  USB_LEN_OTG_DESC                               0x03
#define  USBD_IDX_LANGID_STR                            0x00
#define  USBD_IDX_MFC_STR                               0x01
#define  USBD_IDX_PRODUCT_STR                           0x02
#define  USBD_IDX_SERIAL_STR                            0x03
#define  USBD_IDX_CONFIG_STR                            0x04
#define  USBD_IDX_INTERFACE_STR                         0x05
#define  USB_REQ_TYPE_STANDARD                          0x00
#define  USB_REQ_TYPE_CLASS                             0x20
#define  USB_REQ_TYPE_VENDOR                            0x40
#define  USB_REQ_TYPE_MASK                              0x60
#define  USB_REQ_RECIPIENT_DEVICE                       0x00
#define  USB_REQ_RECIPIENT_INTERFACE                    0x01
#define  USB_REQ_RECIPIENT_ENDPOINT                     0x02
#define  USB_REQ_RECIPIENT_MASK                         0x03
#define  USB_REQ_GET_STATUS                             0x00
#define  USB_REQ_CLEAR_FEATURE                          0x01
#define  USB_REQ_SET_FEATURE                            0x03
#define  USB_REQ_SET_ADDRESS                            0x05
#define  USB_REQ_GET_DESCRIPTOR                         0x06
#define  USB_REQ_SET_DESCRIPTOR                         0x07
#define  USB_REQ_GET_CONFIGURATION                      0x08
#define  USB_REQ_SET_CONFIGURATION                      0x09
#define  USB_REQ_GET_INTERFACE                          0x0A
#define  USB_REQ_SET_INTERFACE                          0x0B
#define  USB_REQ_SYNCH_FRAME                            0x0C
#define  USB_DESC_TYPE_DEVICE                              1
#define  USB_DESC_TYPE_CONFIGURATION                       2
#define  USB_DESC_TYPE_STRING                              3
#define  USB_DESC_TYPE_INTERFACE                           4
#define  USB_DESC_TYPE_ENDPOINT                            5
#define  USB_DESC_TYPE_DEVICE_QUALIFIER                    6
#define  USB_DESC_TYPE_OTHER_SPEED_CONFIGURATION           7
#define USB_CONFIG_REMOTE_WAKEUP                           2
#define USB_CONFIG_SELF_POWERED                            1
#define USB_FEATURE_EP_HALT                                0
#define USB_FEATURE_REMOTE_WAKEUP                          1
#define USB_FEATURE_TEST_MODE                              2
#define  SWAPBYTE(addr)        							(((uint16_t)(*((uint8_t *)(addr)))) + (((uint16_t)(*(((uint8_t *)(addr)) + 1))) << 8))
#define LOBYTE(x)  										((uint8_t)(x & 0x00FF))
#define HIBYTE(x)  										((uint8_t)((x & 0xFF00) >>8))
#define USB_OTG_EP0_SETUP                         1
#define USB_OTG_EP0_DATA_IN                       2
#define USB_OTG_EP0_DATA_OUT                      3
#define USB_OTG_EP0_STATUS_IN                     4
#define USB_OTG_EP0_STATUS_OUT                    5
#define USB_OTG_EP0_STALL                         6
#define USB_OTG_EP_CONTROL                       0
#define USB_OTG_EP_ISOC                          1
#define USB_OTG_EP_BULK                          2
#define USB_OTG_EP_INT                           3
#define USB_OTG_EP_MASK                          3
#define USB_OTG_DEFAULT                          1
#define USB_OTG_ADDRESSED                        2
#define USB_OTG_CONFIGURED                       3
#define USB_OTG_SUSPENDED                        4
#define GAHBCFG_TXFEMPTYLVL_EMPTY              1
#define GAHBCFG_TXFEMPTYLVL_HALFEMPTY          0
#define GAHBCFG_GLBINT_ENABLE                  1
#define GAHBCFG_INT_DMA_BURST_SINGLE           0
#define GAHBCFG_INT_DMA_BURST_INCR             1
#define GAHBCFG_INT_DMA_BURST_INCR4            3
#define GAHBCFG_INT_DMA_BURST_INCR8            5
#define GAHBCFG_INT_DMA_BURST_INCR16           7
#define GAHBCFG_DMAENABLE                      1
#define GAHBCFG_TXFEMPTYLVL_EMPTY              1
#define GAHBCFG_TXFEMPTYLVL_HALFEMPTY          0
#define GRXSTS_PKTSTS_IN                       2
#define GRXSTS_PKTSTS_IN_XFER_COMP             3
#define GRXSTS_PKTSTS_DATA_TOGGLE_ERR          5
#define GRXSTS_PKTSTS_CH_HALTED                7
#define MODE_HNP_SRP_CAPABLE                   0
#define MODE_SRP_ONLY_CAPABLE                  1
#define MODE_NO_HNP_SRP_CAPABLE                2
#define MODE_SRP_CAPABLE_DEVICE                3
#define MODE_NO_SRP_CAPABLE_DEVICE             4
#define MODE_SRP_CAPABLE_HOST                  5
#define MODE_NO_SRP_CAPABLE_HOST               6
#define A_HOST                                 1
#define A_SUSPEND                              2
#define A_PERIPHERAL                           3
#define B_PERIPHERAL                           4
#define B_HOST                                 5
#define DEVICE_MODE                            0
#define HOST_MODE                              1
#define OTG_MODE                               2
#define DSTS_ENUMSPD_HS_PHY_30MHZ_OR_60MHZ     0
#define DSTS_ENUMSPD_FS_PHY_30MHZ_OR_60MHZ     1
#define DSTS_ENUMSPD_LS_PHY_6MHZ               2
#define DSTS_ENUMSPD_FS_PHY_48MHZ              3
#define DCFG_FRAME_INTERVAL_80                 0
#define DCFG_FRAME_INTERVAL_85                 1
#define DCFG_FRAME_INTERVAL_90                 2
#define DCFG_FRAME_INTERVAL_95                 3
#define DEP0CTL_MPS_64                         0
#define DEP0CTL_MPS_32                         1
#define DEP0CTL_MPS_16                         2
#define DEP0CTL_MPS_8                          3
#define EP_SPEED_LOW                           0
#define EP_SPEED_FULL                          1
#define EP_SPEED_HIGH                          2
#define EP_TYPE_CTRL                           0
#define EP_TYPE_ISOC                           1
#define EP_TYPE_BULK                           2
#define EP_TYPE_INTR                           3
#define EP_TYPE_MSK                            3
#define STS_GOUT_NAK                           1
#define STS_DATA_UPDT                          2
#define STS_XFER_COMP                          3
#define STS_SETUP_COMP                         4
#define STS_SETUP_UPDT                         6
#define HC_PID_DATA0                           0
#define HC_PID_DATA2                           1
#define HC_PID_DATA1                           2
#define HC_PID_SETUP                           3
#define HPRT0_PRTSPD_HIGH_SPEED                0
#define HPRT0_PRTSPD_FULL_SPEED                1
#define HPRT0_PRTSPD_LOW_SPEED                 2
#define HCFG_30_60_MHZ                         0
#define HCFG_48_MHZ                            1
#define HCFG_6_MHZ                             2
#define HCCHAR_CTRL                            0
#define HCCHAR_ISOC                            1
#define HCCHAR_BULK                            2
#define HCCHAR_INTR                            3
#define  MIN(a, b)      (((a) < (b)) ? (a) : (b))
#define USB_OTG_READ_REG32(reg)  (*(__IO uint32_t *)reg)
#define USB_OTG_WRITE_REG32(reg,value) (*(__IO uint32_t *)reg = value)
#define USB_OTG_MODIFY_REG32(reg,clear_mask,set_mask) USB_OTG_WRITE_REG32(reg, (((USB_OTG_READ_REG32(reg)) & ~clear_mask) | set_mask ) )
#define USB_OTG_FS_MAX_PACKET_SIZE           64

typedef struct _USBD_DCD_INT
{
  uint8_t (* DataOutStage) (USB_OTG_CORE_HANDLE *pdev , uint8_t epnum);
  uint8_t (* DataInStage)  (USB_OTG_CORE_HANDLE *pdev , uint8_t epnum);
  uint8_t (* SetupStage) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* SOF) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* Reset) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* Suspend) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* Resume) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* IsoINIncomplete) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* IsoOUTIncomplete) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* DevConnected) (USB_OTG_CORE_HANDLE *pdev);
  uint8_t (* DevDisconnected) (USB_OTG_CORE_HANDLE *pdev);

}USBD_DCD_INT_cb_TypeDef;
typedef enum {
  USBD_OK   = 0,
  USBD_BUSY,
  USBD_FAIL,
}USBD_Status;
typedef enum {
  USB_OTG_OK = 0,
  USB_OTG_FAIL
}USB_OTG_STS;
typedef struct
{
  uint8_t  bLength;
  uint8_t  bDescriptorType;
  uint8_t  bEndpointAddress;
  uint8_t  bmAttributes;
  uint16_t wMaxPacketSize;
  uint8_t  bInterval;
}
EP_DESCRIPTOR , *PEP_DESCRIPTOR;

typedef enum {
  CTRL_START = 0,
  CTRL_XFRC,
  CTRL_HALTED,
  CTRL_NAK,
  CTRL_STALL,
  CTRL_XACTERR,
  CTRL_BBLERR,
  CTRL_DATATGLERR,
  CTRL_FAIL
}CTRL_STATUS;
typedef struct USB_OTG_ep
{
  uint8_t        num;
  uint8_t        is_in;
  uint8_t        is_stall;
  uint8_t        type;
  uint8_t        data_pid_start;
  uint8_t        even_odd_frame;
  uint16_t       tx_fifo_num;
  uint32_t       maxpacket;
  /* transaction level variables*/
  uint8_t        *xfer_buff;
  uint32_t       dma_addr;
  uint32_t       xfer_len;
  uint32_t       xfer_count;
  /* Transfer level variables*/
  uint32_t       rem_data_len;
  uint32_t       total_data_len;
  uint32_t       ctl_data_len;
};
typedef struct USB_OTG_core_cfg
{
  uint8_t       host_channels;
  uint8_t       dev_endpoints;
  uint8_t       speed;
  uint8_t       dma_enable;
  uint16_t      mps;
  uint16_t      TotalFifoSize;
  uint8_t       phy_itface;
  uint8_t       Sof_output;
  uint8_t       low_power;
  uint8_t       coreID;
};
typedef  struct  usb_setup_req {

    uint8_t   bmRequest;
    uint8_t   bRequest;
    uint16_t  wValue;
    uint16_t  wIndex;
    uint16_t  wLength;
} USB_SETUP_REQ;
typedef struct _Device_TypeDef
{
  uint8_t  *(*GetDeviceDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetLangIDStrDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetManufacturerStrDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetProductStrDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetSerialStrDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetConfigurationStrDescriptor)( uint8_t speed , uint16_t *length);
  uint8_t  *(*GetInterfaceStrDescriptor)( uint8_t speed , uint16_t *length);
} USBD_DEVICE, *pUSBD_DEVICE;
typedef struct _USBD_USR_PROP
{
  void (*Init)(void);
  void (*DeviceReset)(uint8_t speed);
  void (*DeviceConfigured)(void);
  void (*DeviceSuspended)(void);
  void (*DeviceResumed)(void);
  void (*DeviceConnected)(void);
  void (*DeviceDisconnected)(void);
}
USBD_Usr_cb_TypeDef;
typedef struct _DCD
{
  uint8_t        device_config;
  uint8_t        device_state;
  uint8_t        device_status;
  uint8_t        device_old_status;
  uint8_t        device_address;
  uint8_t        connection_status;
  uint8_t        test_mode;
  uint32_t       DevRemoteWakeup;
  USB_OTG_EP     in_ep   [15];
  USB_OTG_EP     out_ep  [15];
  uint8_t        setup_packet [8*3];
  USBD_DEVICE                   *usr_device;
  uint8_t        *pConfig_descriptor;
 }
DCD_DEV , *DCD_PDEV;
USBD_DCD_INT_cb_TypeDef USBD_DCD_INT_cb =
{
  USBD_DataOutStage,
  USBD_DataInStage,
  USBD_SetupStage,
  USBD_Reset,
  USBD_Suspend,
  USBD_Resume
};
uint32_t USBD_ep_status __attribute__ ((aligned (4)))  = 0;
uint32_t  USBD_default_cfg __attribute__ ((aligned (4)))  = 0;
uint32_t  USBD_cfg_status __attribute__ ((aligned (4)))  = 0;
uint8_t USBD_StrDesc[64] __attribute__ ((aligned (4))) ;
USBD_DCD_INT_cb_TypeDef  *USBD_DCD_INT_fops = &USBD_DCD_INT_cb;
static uint32_t  USBD_HID_AltSet  __attribute__ ((aligned (4))) = 0;
static uint32_t  USBD_HID_Protocol  __attribute__ ((aligned (4))) = 0;
static uint32_t  USBD_HID_IdleState __attribute__ ((aligned (4))) = 0;
static uint8_t USBD_HID_CfgDesc[34] __attribute__ ((aligned (4))) =
{
  0x09, /* bLength: Configuration Descriptor size */
  USB_CONFIGURATION_DESCRIPTOR_TYPE, /* bDescriptorType: Configuration */
  34, /* wTotalLength: Bytes returned */
  0x00,
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing the configuration*/
  0xE0,         /*bmAttributes: bus powered and Support Remote Wake-up */
  0x32,         /*MaxPower 100 mA: this current is used for detecting Vbus*/
  /************** Descriptor of Joystick Mouse interface ****************/
  /* 09 */
  0x09,         /*bLength: Interface Descriptor size*/
  USB_INTERFACE_DESCRIPTOR_TYPE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x01,         /*bNumEndpoints*/
  0x03,         /*bInterfaceClass: HID*/
  0x01,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0x02,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/
  /******************** Descriptor of Joystick Mouse HID ********************/
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  74,/*wItemLength: Total length of Report descriptor*/
  0x00,
  /******************** Descriptor of Mouse endpoint ********************/
  /* 27 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_ENDPOINT_DESCRIPTOR_TYPE, /*bDescriptorType:*/
  HID_IN_EP,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_IN_PACKET, /*wMaxPacketSize: 4 Byte max */
  0x00,
  0x0A         /*bInterval: Polling Interval (10 ms)*/
};
static uint8_t USBD_HID_Desc[9] __attribute__ ((aligned (4)))=
{
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  74,/*wItemLength: Total length of Report descriptor*/
  0x00,
};
static uint8_t HID_MOUSE_ReportDesc[74] __attribute__ ((aligned (4))) =
{
  0x05,   0x01,
  0x09,   0x02,
  0xA1,   0x01,
  0x09,   0x01,
  0xA1,   0x00,
  0x05,   0x09,
  0x19,   0x01,
  0x29,   0x03,
  0x15,   0x00,
  0x25,   0x01,
  0x95,   0x03,
  0x75,   0x01,
  0x81,   0x02,
  0x95,   0x01,
  0x75,   0x05,
  0x81,   0x01,
  0x05,   0x01,
  0x09,   0x30,
  0x09,   0x31,
  0x09,   0x38,
  0x15,   0x81,
  0x25,   0x7F,
  0x75,   0x08,
  0x95,   0x03,
  0x81,   0x06,
  0xC0,   0x09,
  0x3c,   0x05,
  0xff,   0x09,
  0x01,   0x15,
  0x00,   0x25,
  0x01,   0x75,
  0x01,   0x95,
  0x02,   0xb1,
  0x22,   0x75,
  0x06,   0x95,
  0x01,   0xb1,
  0x01,   0xc0
};

void USB_OTG_FlushTxFifo (uint8_t num)
{
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|(num << 6);
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
	delay_us(3);
}
void USB_OTG_FlushRxFifo(void)
{
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
	delay_us(3);
}
void USBD_Init(USB_OTG_CORE_HANDLE *pdev, USB_OTG_CORE_ID_TypeDef coreID, USBD_DEVICE *pDevice)
{
	pdev->dev.usr_device = pDevice;
	pdev->cfg.mps              = 64;
	pdev->cfg.dev_endpoints    = 6 ;
	pdev->cfg.TotalFifoSize    = 1280;/* in 32-bits */
	pdev->cfg.dma_enable       = 1;
	pdev->dev.device_status = USB_OTG_DEFAULT;
	pdev->dev.device_address = 0;
	for (i = 0; i < 6; i++)
	{
		ep = &pdev->dev.in_ep[i];
		ep->is_in = 1;
		ep->num = i;
		ep->tx_fifo_num = i;
		ep->type = EP_TYPE_CTRL;
		ep->maxpacket =  64;
		ep->xfer_buff = 0;
		ep->xfer_len = 0;
		ep = &pdev->dev.out_ep[i];
		ep->is_in = 0;
		ep->num = i;
		ep->tx_fifo_num = i;
		ep->type = EP_TYPE_CTRL;
		ep->maxpacket = 64;
		ep->xfer_buff = 0;
		ep->xfer_len = 0;
	}
	USB_OTG_HS->GAHBCFG &= ~USB_OTG_GAHBCFG_GINT;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_AHBIDL) == 0){};
	USB_OTG_HS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_CSRST){};
	USB_OTG_BSP_uDelay(3);
	USB_OTG_HS->GCCFG = USB_OTG_GCCFG_PWRDWN|USB_OTG_GCCFG_VBUSASEN|USB_OTG_GCCFG_VBUSBSEN;
	USB_OTG_BSP_mDelay(20);
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_DMAEN|(5<<1);/* 64 x 32-bits*/
	USB_OTG_HS->GUSBCFG &= ~USB_OTG_GUSBCFG_FHMOD;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_FDMOD;
	USB_OTG_BSP_mDelay(50);
	USB_OTG_HS_PCGCCTL = 0;
	USB_OTG_HS_DEVICE->DCFG |= 3;//DCFG_FRAME_INTERVAL_80|USB_OTG_SPEED_PARAM_HIGH_IN_FULL;
	USB_OTG_HS->GRXFSIZ = RX_FIFO_HS_SIZE;
	USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = (TX0_FIFO_HS_SIZE << 16)|RX_FIFO_HS_SIZE;//Set Tx EP0 FIFO size
	USB_OTG_HS->DIEPTXF[0] = (TX1_FIFO_HS_SIZE << 16)|(RX_FIFO_HS_SIZERX_FIFO_HS_SIZE + TX0_FIFO_HS_SIZE);
	USB_OTG_HS->DIEPTXF[1] = (TX2_FIFO_HS_SIZE << 16)|(RX_FIFO_HS_SIZERX_FIFO_HS_SIZE + TX0_FIFO_HS_SIZE + TX1_FIFO_HS_SIZE);
	USB_OTG_HS->DIEPTXF[2] = (TX3_FIFO_HS_SIZE << 16)|(RX_FIFO_HS_SIZERX_FIFO_HS_SIZE + TX0_FIFO_HS_SIZE + TX1_FIFO_HS_SIZE +
			TX2_FIFO_HS_SIZE);
	USB_OTG_HS->DIEPTXF[3] = (TX4_FIFO_HS_SIZE << 16)|(RX_FIFO_HS_SIZERX_FIFO_HS_SIZE + TX0_FIFO_HS_SIZE + TX1_FIFO_HS_SIZE +
			TX2_FIFO_HS_SIZE + TX3_FIFO_HS_SIZE);
	USB_OTG_HS->DIEPTXF[4] = (TX5_FIFO_HS_SIZE << 16)|(RX_FIFO_HS_SIZERX_FIFO_HS_SIZE + TX0_FIFO_HS_SIZE + TX1_FIFO_HS_SIZE +
			TX2_FIFO_HS_SIZE + TX3_FIFO_HS_SIZE + TX4_FIFO_HS_SIZE);
	USB_OTG_FlushTxFifo(0x10);
	USB_OTG_FlushRxFifo();
	USB_OTG_HS_DEVICE->DIEPMSK  = 0;
	USB_OTG_HS_DEVICE->DOEPMSK  = 0;
	USB_OTG_HS_DEVICE->DAINT = 0xFFFFFFFF;
	USB_OTG_HS_DEVICE->DAINTMSK = 0;
	for (uint8_t i = 0; i < 6; i++)
	{
    	USB_OTG_HS_INEP(i)->DIEPCTL = 0;
    	USB_OTG_HS_INEP(i)->DIEPTSIZ = 0;
    	USB_OTG_HS_INEP(i)->DIEPINT = 0xFF;
    	USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
    	USB_OTG_HS_OUTEP(i)->DOEPTSIZ = 0;
    	USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFF;
    }
    USB_OTG_HS_DEVICE->DIEPMSK = USB_OTG_DIEPMSK_TXFURM;
    USB_OTG_HS_DEVICE->DTHRCTL = USB_OTG_DTHRCTL_NONISOTHREN|USB_OTG_DTHRCTL_ISOTHREN|USB_OTG_DTHRCTL_RXTHREN|(64<<17)|(64<<2);
    USB_OTG_HS->GINTMSK = 0;
    USB_OTG_HS->GINTSTS = 0xBFFFFFFF;
    USB_OTG_HS->GINTMSK = USB_OTG_GINTMSK_WUIM|USB_OTG_GINTMSK_USBSUSPM|USB_OTG_GINTMSK_USBRST|USB_OTG_GINTMSK_ENUMDNEM|
    		USB_OTG_GINTMSK_IEPINT|USB_OTG_GINTMSK_OEPINT|USB_OTG_GINTMSK_SOFM|USB_OTG_GINTMSK_IISOIXFRM|
			USB_OTG_GINTMSK_PXFRM_IISOOXFRM;
    USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
    NVIC_ClearPendingIRQ(OTG_HS_IRQn);
    NVIC_EnableIRQ(OTG_HS_IRQn);
}
//
void USBD_CtlError( USB_OTG_CORE_HANDLE  *pdev, USB_SETUP_REQ *req)
{
  DCD_EP_Stall(pdev, 0x80);
  DCD_EP_Stall(pdev, 0);
  USB_OTG_EP0_OutStart(pdev);  
}
void USBD_GetString(uint8_t *desc, uint8_t *unicode, uint16_t *len)
{
	uint8_t idx = 0;
	if (desc != NULL)
	{
		*len =  USBD_GetLen(desc) * 2 + 2;
		unicode[idx++] = *len;
		unicode[idx++] =  USB_DESC_TYPE_STRING;
		while (*desc != NULL)
		{
			unicode[idx++] = *desc++;
			unicode[idx++] =  0x00;
		}
	}
}
static uint8_t USBD_GetLen(uint8_t *buf)
{
    uint8_t  len = 0;
    while (*buf != NULL) 
    {
        len++;
        buf++;
    }
    return len;
}
void  USBD_CtlSendData (USB_OTG_CORE_HANDLE  *pdev, uint8_t *pbuf, uint16_t len)
{
	pdev->dev.in_ep[0].total_data_len = len;
	pdev->dev.in_ep[0].rem_data_len   = len;
	pdev->dev.device_state = USB_OTG_EP0_DATA_IN;
	DCD_EP_Tx (pdev, 0, pbuf, len);
}
void  USBD_CtlSendStatus (USB_OTG_CORE_HANDLE  *pdev)
{
  pdev->dev.device_state = USB_OTG_EP0_STATUS_IN;
  DCD_EP_Tx (pdev, 0, NULL, 0); 
  USB_OTG_EP0_OutStart(pdev);  
}
void USBD_SetupStage(USB_OTG_CORE_HANDLE *pdev)
{
	USB_SETUP_REQ req;
	req->bmRequest = *(uint8_t *)(pdev->dev.setup_packet);
	req->bRequest = *(uint8_t *)(pdev->dev.setup_packet +  1);
	req->wValue = SWAPBYTE(pdev->dev.setup_packet +  2);
	req->wIndex = SWAPBYTE(pdev->dev.setup_packet +  4);
	req->wLength = SWAPBYTE(pdev->dev.setup_packet +  6);
	pdev->dev.in_ep[0].ctl_data_len = req->wLength  ;
	pdev->dev.device_state = USB_OTG_EP0_SETUP;
	switch (req.bmRequest & 0x1F)
	{
		case USB_REQ_RECIPIENT_DEVICE:
			switch (req->bRequest)
			{
				case USB_REQ_GET_DESCRIPTOR:
					uint16_t len;
					uint8_t *pbuf;
					switch (req->wValue >> 8)
					{
						case USB_DESC_TYPE_DEVICE:
							pbuf = pdev->dev.usr_device->GetDeviceDescriptor(pdev->cfg.speed, &len);
							if ((req->wLength == 64) ||( pdev->dev.device_status == USB_OTG_DEFAULT)) len = 8;
							break;
						case USB_DESC_TYPE_CONFIGURATION:
							pbuf   = USBD_HID_GetCfgDesc(&len);
							pbuf[1] = USB_DESC_TYPE_CONFIGURATION;
							pdev->dev.pConfig_descriptor = pbuf;
							break;
						case USB_DESC_TYPE_STRING:
							switch ((uint8_t)(req->wValue))
							{
								case USBD_IDX_LANGID_STR:
									pbuf = pdev->dev.usr_device->GetLangIDStrDescriptor(pdev->cfg.speed, &len);
									break;
								case USBD_IDX_MFC_STR:
									pbuf = pdev->dev.usr_device->GetManufacturerStrDescriptor(pdev->cfg.speed, &len);
									break;
								case USBD_IDX_PRODUCT_STR:
									pbuf = pdev->dev.usr_device->GetProductStrDescriptor(pdev->cfg.speed, &len);
									break;
								case USBD_IDX_SERIAL_STR:
									pbuf = pdev->dev.usr_device->GetSerialStrDescriptor(pdev->cfg.speed, &len);
									break;
								case USBD_IDX_CONFIG_STR:
									pbuf = pdev->dev.usr_device->GetConfigurationStrDescriptor(pdev->cfg.speed, &len);
									break;
								case USBD_IDX_INTERFACE_STR:
									pbuf = pdev->dev.usr_device->GetInterfaceStrDescriptor(pdev->cfg.speed, &len);
									break;
								default:
									pbuf = 0;
									break;
							}
							break;
						default:
							USBD_CtlError(pdev , req);
							return;
					}
					if((len != 0)&& (req->wLength != 0))
					{
						len = MIN(len , req->wLength);
					  	USBD_CtlSendData (pdev, pbuf, len);
					}
					break;
				case USB_REQ_SET_ADDRESS:
					uint8_t  dev_addr;
					if ((req->wIndex == 0) && (req->wLength == 0))
					{
						dev_addr = (uint8_t)(req->wValue) & 0x7F;
						if (pdev->dev.device_status == USB_OTG_CONFIGURED) USBD_CtlError(pdev , req);
						else
						{
							pdev->dev.device_address = dev_addr;
							dcfg.b.devaddr = dev_addr;
							USB_OTG_MODIFY_REG32( &pdev->regs.DREGS->DCFG, 0, dcfg.d32);
							USBD_CtlSendStatus(pdev);
							if (dev_addr != 0) pdev->dev.device_status  = USB_OTG_ADDRESSED;
							else pdev->dev.device_status  = USB_OTG_DEFAULT;
						}
					}
					else USBD_CtlError(pdev , req);
					break;
				case USB_REQ_SET_CONFIGURATION:
					static uint8_t  cfgidx;
					cfgidx = (uint8_t)(req->wValue);
					if (cfgidx > 1 ) USBD_CtlError(pdev , req);
					else
					{
						switch (pdev->dev.device_status)
						{
							case USB_OTG_ADDRESSED:
								if (cfgidx)
								{
									pdev->dev.device_config = cfgidx;
									pdev->dev.device_status = USB_OTG_CONFIGURED;
									DCD_EP_Open(pdev, HID_IN_EP, HID_IN_PACKET, USB_OTG_EP_INT);
									DCD_EP_Open(pdev, HID_OUT_EP, HID_OUT_PACKET, USB_OTG_EP_INT);
									USBD_CtlSendStatus(pdev);
								}
								else USBD_CtlSendStatus(pdev);
								break;
							case USB_OTG_CONFIGURED:
								if (cfgidx == 0)
								{
									pdev->dev.device_status = USB_OTG_ADDRESSED;
									pdev->dev.device_config = cfgidx;
									DCD_EP_Close (pdev , HID_IN_EP);
									DCD_EP_Close (pdev , HID_OUT_EP);
									USBD_CtlSendStatus(pdev);
								}
								else if (cfgidx != pdev->dev.device_config)
								{
									DCD_EP_Close (pdev , HID_IN_EP);
									DCD_EP_Close (pdev , HID_OUT_EP);
									pdev->dev.device_config = cfgidx;
									DCD_EP_Open(pdev, HID_IN_EP, HID_IN_PACKET, USB_OTG_EP_INT);
									DCD_EP_Open(pdev, HID_OUT_EP, HID_OUT_PACKET, USB_OTG_EP_INT);
									USBD_CtlSendStatus(pdev);
								}
								else USBD_CtlSendStatus(pdev);
								break;
							default:
								USBD_CtlError(pdev , req);
								break;
						}
					}
					break;
				case USB_REQ_GET_CONFIGURATION:
					if (req->wLength != 1) USBD_CtlError(pdev , req);
					else
					{
						switch (pdev->dev.device_status )
						{
						case USB_OTG_ADDRESSED:
							USBD_CtlSendData (pdev, (uint8_t *)&USBD_default_cfg, 1);
							break;
						case USB_OTG_CONFIGURED:
							USBD_CtlSendData (pdev, &pdev->dev.device_config, 1);
							break;
						default:
							USBD_CtlError(pdev , req);
							break;
						}
					}
					break;
				case USB_REQ_GET_STATUS:
					switch (pdev->dev.device_status)
					{
						case USB_OTG_ADDRESSED:
						case USB_OTG_CONFIGURED:
#ifdef USBD_SELF_POWERED
							USBD_cfg_status = USB_CONFIG_SELF_POWERED;
#else
							USBD_cfg_status = 0x00;
#endif
							if (pdev->dev.DevRemoteWakeup) USBD_cfg_status |= USB_CONFIG_REMOTE_WAKEUP;
							USBD_CtlSendData (pdev, (uint8_t *)&USBD_cfg_status, 2);
							break;
						default :
							USBD_CtlError(pdev , req);
							break;
					}
					break;
				case USB_REQ_SET_FEATURE:
					USB_OTG_DCTL_TypeDef     dctl;
					uint8_t test_mode = 0;
					if (req->wValue == USB_FEATURE_REMOTE_WAKEUP)
					{
						pdev->dev.DevRemoteWakeup = 1;
						USBD_HID_Setup(pdev, req);
						USBD_CtlSendStatus(pdev);
					}
					else if ((req->wValue == USB_FEATURE_TEST_MODE)&&((req->wIndex & 0xFF) == 0))
					{
						dctl.d32 = USB_OTG_READ_REG32(&pdev->regs.DREGS->DCTL);
						test_mode = req->wIndex >> 8;
						switch (test_mode)
						{
						case 1: // TEST_J
							dctl.b.tstctl = 1;
							break;
						case 2: // TEST_K
							dctl.b.tstctl = 2;
							break;
						case 3: // TEST_SE0_NAK
							dctl.b.tstctl = 3;
							break;
						case 4: // TEST_PACKET
							dctl.b.tstctl = 4;
							break;
						case 5: // TEST_FORCE_ENABLE
							dctl.b.tstctl = 5;
							break;
						}
						SET_TEST_MODE = dctl;
						pdev->dev.test_mode = 1;
						USBD_CtlSendStatus(pdev);
					}
					break;
				case USB_REQ_CLEAR_FEATURE:
					switch (pdev->dev.device_status)
					{
						case USB_OTG_ADDRESSED:
						case USB_OTG_CONFIGURED:
							if (req->wValue == USB_FEATURE_REMOTE_WAKEUP)
							{
								pdev->dev.DevRemoteWakeup = 0;
								USBD_HID_Setup(pdev, req);
								USBD_CtlSendStatus(pdev);
							}
							break;
						default :
							USBD_CtlError(pdev , req);
							break;
					}
					break;
				default:
					USBD_CtlError(pdev , req);
					break;
			}
			break;
		case USB_REQ_RECIPIENT_INTERFACE:
			switch (pdev->dev.device_status)
			{
				case USB_OTG_CONFIGURED:
					if (LOBYTE(req->wIndex) <= USBD_ITF_MAX_NUM)
					{
						USBD_HID_Setup(pdev, req);
						if((req->wLength == 0)&& (ret == USBD_OK)) USBD_CtlSendStatus(pdev);
					}
					else USBD_CtlError(pdev , req);
					break;
				default:
					USBD_CtlError(pdev , req);
					break;
			}
			break;
		case USB_REQ_RECIPIENT_ENDPOINT:
			uint8_t ep_addr;
			ep_addr  = LOBYTE(req->wIndex);
			switch (req->bRequest)
			{
				case USB_REQ_SET_FEATURE :
					switch (pdev->dev.device_status)
					{
						case USB_OTG_ADDRESSED:
							if ((ep_addr != 0x00) && (ep_addr != 0x80)) DCD_EP_Stall(pdev , ep_addr);
							break;
						case USB_OTG_CONFIGURED:
							if (req->wValue == USB_FEATURE_EP_HALT)
							{
								if ((ep_addr != 0x00) && (ep_addr != 0x80)) DCD_EP_Stall(pdev , ep_addr);
							}
							USBD_HID_Setup(pdev, req);
							USBD_CtlSendStatus(pdev);
							break;
						default:
							USBD_CtlError(pdev , req);
							break;
					}
					break;
				case USB_REQ_CLEAR_FEATURE :
					switch (pdev->dev.device_status)
					{
						case USB_OTG_ADDRESSED:
							if ((ep_addr != 0x00) && (ep_addr != 0x80)) DCD_EP_Stall(pdev , ep_addr);
							break;
						case USB_OTG_CONFIGURED:
							if (req->wValue == USB_FEATURE_EP_HALT)
							{
								if ((ep_addr != 0x00) && (ep_addr != 0x80))
								{
									USB_OTG_EP *ep;
									if ((0x80 & ep_addr) == 0x80) ep = &pdev->dev.in_ep[ep_addr & 0x7F];
									else ep = &pdev->dev.out_ep[ep_addr];
									ep->is_stall = 0;
									ep->num   = ep_addr & 0x7F;
									ep->is_in = ((ep_addr & 0x80) == 0x80);
									__IO uint32_t *depctl_addr;
									depctl.d32 = 0;
									if (ep->is_in == 1) depctl_addr = &(pdev->regs.INEP_REGS[ep->num]->DIEPCTL);
									else depctl_addr = &(pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL);
									depctl.d32 = USB_OTG_READ_REG32(depctl_addr);
									depctl.b.stall = 0;
									if (ep->type == EP_TYPE_INTR || ep->type == EP_TYPE_BULK) depctl.b.setd0pid = 1; /* DATA0 */
									USB_OTG_WRITE_REG32(depctl_addr, depctl.d32);
									USBD_HID_Setup(pdev, req);
								}
								USBD_CtlSendStatus(pdev);
							}
							break;
						default:
							USBD_CtlError(pdev , req);
							break;
					}
					break;
				case USB_REQ_GET_STATUS:
					switch (pdev->dev.device_status)
					{
						case USB_OTG_ADDRESSED:
							if ((ep_addr != 0x00) && (ep_addr != 0x80)) DCD_EP_Stall(pdev , ep_addr);
							break;
						case USB_OTG_CONFIGURED:
							if ((ep_addr & 0x80)== 0x80)
							{
								if(pdev->dev.in_ep[ep_addr & 0x7F].is_stall) USBD_ep_status = 0x0001;
								else USBD_ep_status = 0x0000;
							}
							else if ((ep_addr & 0x80)== 0x00)
							{
								if(pdev->dev.out_ep[ep_addr].is_stall) USBD_ep_status = 0x0001;
								else USBD_ep_status = 0x0000;
							}
							USBD_CtlSendData (pdev, uint8_t *)&USBD_ep_status, 2);
							break;
			   			default:
			   				USBD_CtlError(pdev , req);
			   				break;
					}
					break;
				default:
					break;
			}
			break;
		default:
			DCD_EP_Stall(pdev , req.bmRequest & 0x80);
			break;
	}
}
void USBD_DataOutStage(USB_OTG_CORE_HANDLE *pdev , uint8_t epnum)
{
	USB_OTG_EP *ep;
	if(epnum == 0)
	{
		ep = &pdev->dev.out_ep[0];
		if ( pdev->dev.device_state == USB_OTG_EP0_DATA_OUT)
		{
			if(ep->rem_data_len > ep->maxpacket)
			{
				ep->rem_data_len -=  ep->maxpacket;
				ep->xfer_buff += ep->maxpacket;
				DCD_EP_PrepareRx (pdev, 0, ep->xfer_buff, MIN(ep->rem_data_len ,ep->maxpacket));
			}
			else USBD_CtlSendStatus(pdev);
		}
	}
}
void USBD_DataInStage(USB_OTG_CORE_HANDLE *pdev , uint8_t epnum)
{
	USB_OTG_EP *ep;
	if(epnum == 0)
	{
		ep = &pdev->dev.in_ep[0];
		if ( pdev->dev.device_state == USB_OTG_EP0_DATA_IN)
		{
			if(ep->rem_data_len > ep->maxpacket)
			{
				ep->rem_data_len -=  ep->maxpacket;
				ep->xfer_buff += ep->maxpacket;
				DCD_EP_Tx (pdev, 0, ep->xfer_buff, ep->rem_data_len);
			}
			else
			{
				if((ep->total_data_len % ep->maxpacket == 0)&&(ep->total_data_len >= ep->maxpacket)&&(ep->total_data_len < ep->ctl_data_len ))
				{
					DCD_EP_Tx (pdev, 0, 0, 0);
					ep->ctl_data_len = 0;
				}
				else
				{
					pdev->dev.device_state = USB_OTG_EP0_STATUS_OUT;
					DCD_EP_PrepareRx (pdev, 0, NULL, 0);
					USB_OTG_EP0_OutStart(pdev);
				}
			}
		}
		if (pdev->dev.test_mode == 1)
		{
			USB_OTG_WRITE_REG32(&pdev->regs.DREGS->DCTL, SET_TEST_MODE.d32);
			pdev->dev.test_mode = 0;
		}
	}
	else if(pdev->dev.device_status == USB_OTG_CONFIGURED) DCD_EP_Flush(pdev, HID_IN_EP);
}
void USBD_Reset(USB_OTG_CORE_HANDLE  *pdev)
{
	DCD_EP_Open(pdev, 0x00, 64, EP_TYPE_CTRL);
	DCD_EP_Open(pdev, 0x80, 64, EP_TYPE_CTRL);
	pdev->dev.device_status = USB_OTG_DEFAULT;
}
void USBD_Resume(USB_OTG_CORE_HANDLE  *pdev)
{
	pdev->dev.device_status = pdev->dev.device_old_status;
	pdev->dev.device_status = USB_OTG_CONFIGURED;
}
void USBD_Suspend(USB_OTG_CORE_HANDLE  *pdev)
{
	pdev->dev.device_old_status = pdev->dev.device_status;
	pdev->dev.device_status  = USB_OTG_SUSPENDED;
}
void* USB_OTG_ReadPacket(USB_OTG_CORE_HANDLE *pdev, uint8_t *dest, uint16_t len)
{
	uint32_t i=0;
	uint32_t count32b = (len + 3) / 4;
	__IO uint32_t *fifo = pdev->regs.DFIFO[0];
	for ( i = 0; i < count32b; i++, dest += 4 ) *(__attribute__ ((__packed__)) uint32_t *)dest = USB_OTG_READ_REG32(fifo);
	return ((void *)dest);
}
void USB_OTG_EPStartXfer(USB_OTG_CORE_HANDLE *pdev , USB_OTG_EP *ep)
{
	uint32_t fifoemptymsk = 0;
	depctl.d32 = 0;
	deptsiz.d32 = 0;
	if (ep->is_in == 1)
	{
		depctl.d32  = USB_OTG_READ_REG32(&(pdev->regs.INEP_REGS[ep->num]->DIEPCTL));
		deptsiz.d32 = USB_OTG_READ_REG32(&(pdev->regs.INEP_REGS[ep->num]->DIEPTSIZ));
		if (ep->xfer_len == 0)
		{
			deptsiz.b.xfersize = 0;
			deptsiz.b.pktcnt = 1;
		}
		else
		{
			deptsiz.b.xfersize = ep->xfer_len;
			deptsiz.b.pktcnt = (ep->xfer_len - 1 + ep->maxpacket) / ep->maxpacket;
			if (ep->type == EP_TYPE_ISOC) deptsiz.b.mc = 1;
		}
		USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[ep->num]->DIEPTSIZ, deptsiz.d32);
		if (pdev->cfg.dma_enable == 1) USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[ep->num]->DIEPDMA, ep->dma_addr);
		else
		{
			if (ep->type != EP_TYPE_ISOC)
			{
				if (ep->xfer_len > 0)
				{
					fifoemptymsk = 1 << ep->num;
					USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DIEPEMPMSK, 0, fifoemptymsk);
				}
			}
		}
		if (ep->type == EP_TYPE_ISOC)
		{
			dsts.d32 = USB_OTG_READ_REG32(&pdev->regs.DREGS->DSTS);
			if (((dsts.b.soffn)&0x1) == 0) depctl.b.setd1pid = 1;
			else depctl.b.setd0pid = 1;
		}
		depctl.b.cnak = 1;
		depctl.b.epena = 1;
		USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[ep->num]->DIEPCTL, depctl.d32);
	}
	else
	{
		depctl.d32  = USB_OTG_READ_REG32(&(pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL));
		deptsiz.d32 = USB_OTG_READ_REG32(&(pdev->regs.OUTEP_REGS[ep->num]->DOEPTSIZ));
		if (ep->xfer_len == 0)
		{
			deptsiz.b.xfersize = ep->maxpacket;
			deptsiz.b.pktcnt = 1;
		}
		else
		{
			deptsiz.b.pktcnt = (ep->xfer_len + (ep->maxpacket - 1)) / ep->maxpacket;
			deptsiz.b.xfersize = deptsiz.b.pktcnt * ep->maxpacket;
		}
		USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPTSIZ, deptsiz.d32);
		USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPDMA, ep->dma_addr);
		if (ep->type == EP_TYPE_ISOC)
		{
			if (ep->even_odd_frame) depctl.b.setd1pid = 1;
			else depctl.b.setd0pid = 1;
		}
		depctl.b.cnak = 1;
		depctl.b.epena = 1;
		USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL, depctl.d32);
	}
}
void USB_OTG_EP0StartXfer(USB_OTG_CORE_HANDLE *pdev , USB_OTG_EP *ep)
{
	uint32_t fifoemptymsk = 0;
	depctl.d32   = 0;
	deptsiz.d32  = 0;
	if (ep->is_in == 1)
	{
		in_regs = pdev->regs.INEP_REGS[0];
		depctl.d32  = USB_OTG_READ_REG32(&in_regs->DIEPCTL);
		deptsiz.d32 = USB_OTG_READ_REG32(&in_regs->DIEPTSIZ);
		if (ep->xfer_len == 0)
		{
			deptsiz.b.xfersize = 0;
			deptsiz.b.pktcnt = 1;
		}
		else
		{
			if (ep->xfer_len > ep->maxpacket)
			{
				ep->xfer_len = ep->maxpacket;
				deptsiz.b.xfersize = ep->maxpacket;
			}
			else deptsiz.b.xfersize = ep->xfer_len;
			deptsiz.b.pktcnt = 1;
		}
		USB_OTG_WRITE_REG32(&in_regs->DIEPTSIZ, deptsiz.d32);
		USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[ep->num]->DIEPDMA, ep->dma_addr);
		depctl.b.cnak = 1;
		depctl.b.epena = 1;
		USB_OTG_WRITE_REG32(&in_regs->DIEPCTL, depctl.d32);
	}
	else
	{
		depctl.d32  = USB_OTG_READ_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL);
		deptsiz.d32 = USB_OTG_READ_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPTSIZ);
		if (ep->xfer_len == 0)
		{
			deptsiz.b.xfersize = ep->maxpacket;
			deptsiz.b.pktcnt = 1;
		}
		else
		{
			ep->xfer_len = ep->maxpacket;
			deptsiz.b.xfersize = ep->maxpacket;
			deptsiz.b.pktcnt = 1;
		}
		USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPTSIZ, deptsiz.d32);
		USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[ep->num]->DOEPDMA, ep->dma_addr);
		depctl.b.cnak = 1;
		depctl.b.epena = 1;
		USB_OTG_WRITE_REG32 (&(pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL), depctl.d32);
	}
}
void USB_OTG_EP0_OutStart(USB_OTG_CORE_HANDLE *pdev)
{
	USB_OTG_DEP0XFRSIZ_TypeDef  doeptsize0;
	doeptsize0.d32 = 0;
	doeptsize0.b.supcnt = 3;
	doeptsize0.b.pktcnt = 1;
	doeptsize0.b.xfersize = 8 * 3;
	USB_OTG_WRITE_REG32( &pdev->regs.OUTEP_REGS[0]->DOEPTSIZ, doeptsize0.d32 );
    USB_OTG_DEPCTL_TypeDef  doepctl;
    doepctl.d32 = 0;
    USB_OTG_WRITE_REG32( &pdev->regs.OUTEP_REGS[0]->DOEPDMA, (uint32_t)&pdev->dev.setup_packet);
    doepctl.d32 = USB_OTG_READ_REG32(&pdev->regs.OUTEP_REGS[0]->DOEPCTL);
    doepctl.b.epena = 1;
    doepctl.d32 = 0x80008000;
    USB_OTG_WRITE_REG32( &pdev->regs.OUTEP_REGS[0]->DOEPCTL, doepctl.d32);
}
void DCD_EP_Open(USB_OTG_CORE_HANDLE *pdev , uint8_t ep_addr, uint16_t ep_mps, uint8_t ep_type)
{
	USB_OTG_EP *ep;
	if ((ep_addr & 0x80) == 0x80) ep = &pdev->dev.in_ep[ep_addr & 0x7F];
	else ep = &pdev->dev.out_ep[ep_addr & 0x7F];
	ep->num   = ep_addr & 0x7F;
	ep->is_in = (0x80 & ep_addr) != 0;
	ep->maxpacket = ep_mps;
	ep->type = ep_type;
	if (ep->is_in) ep->tx_fifo_num = ep->num;
	if (ep_type == USB_OTG_EP_BULK ) ep->data_pid_start = 0;
	dctl.d32 = 0;
	dsts.d32 = USB_OTG_READ_REG32(&pdev->regs.DREGS->DSTS);
	diepctl.d32 = USB_OTG_READ_REG32(&pdev->regs.INEP_REGS[0]->DIEPCTL);
	switch (dsts.b.enumspd)
	{
	  	  case DSTS_ENUMSPD_HS_PHY_30MHZ_OR_60MHZ:
	  	  case DSTS_ENUMSPD_FS_PHY_30MHZ_OR_60MHZ:
	  	  case DSTS_ENUMSPD_FS_PHY_48MHZ:
	  		  diepctl.b.mps = DEP0CTL_MPS_64;
	  		  break;
	  	  case DSTS_ENUMSPD_LS_PHY_6MHZ:
	  		  diepctl.b.mps = DEP0CTL_MPS_8;
	  		  break;
	}
	USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[0]->DIEPCTL, diepctl.d32);
	dctl.b.cgnpinnak = 1;
	USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DCTL, dctl.d32, dctl.d32);
}
void DCD_EP_PrepareRx( USB_OTG_CORE_HANDLE *pdev, uint8_t   ep_addr, uint8_t *pbuf, uint16_t  buf_len)
{
	USB_OTG_EP *ep;
	ep = &pdev->dev.out_ep[ep_addr & 0x7F];
	ep->xfer_buff = pbuf;
	ep->xfer_len = buf_len;
	ep->xfer_count = 0;
	ep->is_in = 0;
	ep->num = ep_addr & 0x7F;
	ep->dma_addr = (uint32_t)pbuf;    
	if ( ep->num == 0 ) USB_OTG_EP0StartXfer(pdev , ep);
	else USB_OTG_EPStartXfer(pdev, ep );
}
void DCD_EP_Tx ( USB_OTG_CORE_HANDLE *pdev, uint8_t   ep_addr, uint8_t   *pbuf, uint32_t   buf_len)
{
  	SB_OTG_EP *ep;
  	ep = &pdev->dev.in_ep[ep_addr & 0x7F];
  	ep->is_in = 1;
  	ep->num = ep_addr & 0x7F;
  	ep->xfer_buff = pbuf;
  	ep->dma_addr = (uint32_t)pbuf;
  	ep->xfer_count = 0;
  	ep->xfer_len  = buf_len;
  	if ( ep->num == 0 ) USB_OTG_EP0StartXfer(pdev , ep);
  	else USB_OTG_EPStartXfer(pdev, ep );
}
void DCD_EP_Stall (USB_OTG_CORE_HANDLE *pdev, uint8_t   epnum)
{
	USB_OTG_EP *ep;
	if ((0x80 & epnum) == 0x80) ep = &pdev->dev.in_ep[epnum & 0x7F];
	else ep = &pdev->dev.out_ep[epnum];
	ep->is_stall = 1;
	ep->num   = epnum & 0x7F;
	ep->is_in = ((epnum & 0x80) == 0x80);
	__IO uint32_t *depctl_addr;
	depctl.d32 = 0;
	if (ep->is_in == 1)
	{
		depctl_addr = &(pdev->regs.INEP_REGS[ep->num]->DIEPCTL);
		depctl.d32 = USB_OTG_READ_REG32(depctl_addr);
		if (depctl.b.epena) depctl.b.epdis = 1;
		depctl.b.stall = 1;
		USB_OTG_WRITE_REG32(depctl_addr, depctl.d32);
	}
	else
	{
		depctl_addr = &(pdev->regs.OUTEP_REGS[ep->num]->DOEPCTL);
		depctl.d32 = USB_OTG_READ_REG32(depctl_addr);
		depctl.b.stall = 1;
		USB_OTG_WRITE_REG32(depctl_addr, depctl.d32);
	}
}
void USBD_OTG_ISR_Handler (USB_OTG_CORE_HANDLE *pdev)
{
  USB_OTG_GINTSTS_TypeDef  gintr_status;
  uint32_t retval = 0;
    gintr_status.d32 = USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK;
    if (!gintr_status.d32) return 0;
    if (gintr_status.b.outepintr)
    {
    	uint32_t epnum = 0;
    	doepint.d32 = 0;
    	ep_intr = (USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT) >> 16;
    	while ( ep_intr )
    	{
    	    if (ep_intr&0x1)
    	    {
    	    	doepint.d32 = USB_OTG_HS_OUTEP(i)->DOEPINT & USB_OTG_HS_DEVICE->DOEPMSK;
    	    	if ( doepint.b.xfercompl )
    	    	{
    	    		CLEAR_OUT_EP_INTR(epnum, xfercompl);
    	    		deptsiz.d32 = USB_OTG_READ_REG32(&(pdev->regs.OUTEP_REGS[epnum]->DOEPTSIZ));
    	    		pdev->dev.out_ep[epnum].xfer_count = pdev->dev.out_ep[epnum].maxpacket - deptsiz.b.xfersize;
    	    		USBD_DCD_INT_fops->DataOutStage(pdev , epnum);
    	    		if((epnum == 0) && (pdev->dev.device_state == USB_OTG_EP0_STATUS_OUT)) USB_OTG_EP0_OutStart(pdev);
    	    	}
    	    	if ( doepint.b.epdisabled ) CLEAR_OUT_EP_INTR(epnum, epdisabled);
    	    	if ( doepint.b.setup )
    	    	{
    	    		USBD_DCD_INT_fops->SetupStage(pdev);
    	    		CLEAR_OUT_EP_INTR(epnum, setup);
    	    	}
    	    }
    	    epnum++;
    	    ep_intr >>= 1;
    	}
    }
    if (gintr_status.b.inepint)
    {
    	uint32_t epnum = 0;
    	diepint.d32 = 0;
    	ep_intr = USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT;
    	while ( ep_intr )
    	{
    		if (ep_intr&0x1) /* In ITR */
    	    {
    			diepint.d32 = USB_OTG_HS_INEP(i)->DIEPINT & USB_OTG_HS_DEVICE->DIEPMSK; /* Get In ITR status */
    			if ( diepint.b.xfercompl )
    			{
    				fifoemptymsk = 0x1 << epnum;
    				USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DIEPEMPMSK, fifoemptymsk, 0);
    				CLEAR_IN_EP_INTR(epnum, xfercompl);
    				USBD_DCD_INT_fops->DataInStage(pdev , epnum);
    				if((epnum == 0) && (pdev->dev.device_state == USB_OTG_EP0_STATUS_IN)) USB_OTG_EP0_OutStart(pdev);
    			}
    			if ( diepint.b.timeout ) CLEAR_IN_EP_INTR(epnum, timeout);
    			if (diepint.b.intktxfemp) CLEAR_IN_EP_INTR(epnum, intktxfemp);
    			if (diepint.b.inepnakeff) CLEAR_IN_EP_INTR(epnum, inepnakeff);
    			if ( diepint.b.epdisabled ) CLEAR_IN_EP_INTR(epnum, epdisabled);
    			if (diepint.b.emptyintr)
    			{
    				USB_OTG_EP *ep;
    				uint32_t len = 0;
    				txstatus.d32 = 0;
    				ep = &pdev->dev.in_ep[epnum];
    				len = ep->xfer_len - ep->xfer_count;
    				if (len > ep->maxpacket) len = ep->maxpacket;
    				len32b = (len + 3) / 4;
    				txstatus.d32 = USB_OTG_READ_REG32( &pdev->regs.INEP_REGS[epnum]->DTXFSTS);
    				while  (txstatus.b.txfspcavail > len32b && ep->xfer_count < ep->xfer_len && ep->xfer_len != 0)
    				{
    					len = ep->xfer_len - ep->xfer_count;
    				    if (len > ep->maxpacket) len = ep->maxpacket;
    				    len32b = (len + 3) / 4;
    				    ep->xfer_buff  += len;
    				    ep->xfer_count += len;
    				    txstatus.d32 = USB_OTG_READ_REG32(&pdev->regs.INEP_REGS[epnum]->DTXFSTS);
    				}
    				CLEAR_IN_EP_INTR(epnum, emptyintr);
    			}
    	    }
    	    epnum++;
    	    ep_intr >>= 1;
    	}
    }
    if (gintr_status.b.modemismatch)
    {
    	USB_OTG_GINTSTS_TypeDef  gintsts;
    	gintsts.d32 = 0;
    	gintsts.b.modemismatch = 1;
    	USB_OTG_WRITE_REG32(&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    }
    if (gintr_status.b.wkupintr)
    {
    	devctl.d32 = 0;
    	devctl.b.rmtwkupsig = 1;
    	USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DCTL, devctl.d32, 0);
    	USBD_DCD_INT_fops->Resume (pdev);
    	gintsts.d32 = 0;
    	gintsts.b.wkupintr = 1;
    	USB_OTG_WRITE_REG32 (&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    }
    if (gintr_status.b.usbsuspend)
    {
    	__IO uint8_t prev_status = 0;
    	prev_status = pdev->dev.device_status;
    	USBD_DCD_INT_fops->Suspend (pdev);
    	dsts.d32 = USB_OTG_READ_REG32(&pdev->regs.DREGS->DSTS);
    	gintsts.d32 = 0;
    	gintsts.b.usbsuspend = 1;
    	USB_OTG_WRITE_REG32(&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    }
    if (gintr_status.b.sofintr)
    {
    	USBD_DCD_INT_fops->SOF(pdev);
    	GINTSTS.d32 = 0;
    	GINTSTS.b.sofintr = 1;
    	USB_OTG_WRITE_REG32 (&pdev->regs.GREGS->GINTSTS, GINTSTS.d32);
    }
    if (gintr_status.b.rxstsqlvl)
    {
    	USB_OTG_EP *ep;
    	int_mask.d32 = 0;
    	int_mask.b.rxstsqlvl = 1;
    	USB_OTG_MODIFY_REG32( &pdev->regs.GREGS->GINTMSK, int_mask.d32, 0);
    	status.d32 = USB_OTG_READ_REG32( &pdev->regs.GREGS->GRXSTSP );
    	ep = &pdev->dev.out_ep[status.b.epnum];
    	switch (status.b.pktsts)
    	{
    	  	  case STS_GOUT_NAK:
    	  		  break;
    	  	  case STS_DATA_UPDT:
    	  		  if (status.b.bcnt)
    	  		  {
    	  			  USB_OTG_ReadPacket(pdev,ep->xfer_buff, status.b.bcnt);
    	  			  ep->xfer_buff += status.b.bcnt;
    	  			  ep->xfer_count += status.b.bcnt;
    	  		  }
    	  		  break;
    	  	  case STS_XFER_COMP:
    	  		  break;
    	  	  case STS_SETUP_COMP:
    	  		  break;
    	  	  case STS_SETUP_UPDT:
    	  		  USB_OTG_ReadPacket(pdev , pdev->dev.setup_packet, 8);
    	  		  ep->xfer_count += status.b.bcnt;
    	  		  break;
    	  	  default:
    	  		  break;
    	}
    	USB_OTG_MODIFY_REG32( &pdev->regs.GREGS->GINTMSK, 0, int_mask.d32);
    }
    if (gintr_status.b.usbreset)
    {
    	dctl.d32 = 0;
    	daintmsk.d32 = 0;
    	doepmsk.d32 = 0;
    	diepmsk.d32 = 0;
    	dcfg.d32 = 0;
    	gintsts.d32 = 0;
    	dctl.b.rmtwkupsig = 1;
    	USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DCTL, dctl.d32, 0 );
    	USB_OTG_FlushTxFifo(pdev ,  0 );
    	for (i = 0; i < pdev->cfg.dev_endpoints ; i++)
    	{
    	    USB_OTG_WRITE_REG32( &pdev->regs.INEP_REGS[i]->DIEPINT, 0xFF);
    	    USB_OTG_WRITE_REG32( &pdev->regs.OUTEP_REGS[i]->DOEPINT, 0xFF);
    	}
    	USB_OTG_WRITE_REG32( &pdev->regs.DREGS->DAINT, 0xFFFFFFFF );
    	daintmsk.ep.in = 1;
    	daintmsk.ep.out = 1;
    	USB_OTG_WRITE_REG32( &pdev->regs.DREGS->DAINTMSK, daintmsk.d32 );
    	doepmsk.b.setup = 1;
    	doepmsk.b.xfercompl = 1;
    	doepmsk.b.epdisabled = 1;
    	USB_OTG_WRITE_REG32( &pdev->regs.DREGS->DOEPMSK, doepmsk.d32 );
    	diepmsk.b.xfercompl = 1;
    	diepmsk.b.timeout = 1;
    	diepmsk.b.epdisabled = 1;
    	USB_OTG_WRITE_REG32( &pdev->regs.DREGS->DIEPMSK, diepmsk.d32 );
    	dcfg.d32 = USB_OTG_READ_REG32( &pdev->regs.DREGS->DCFG);
    	dcfg.b.devaddr = 0;
    	USB_OTG_WRITE_REG32( &pdev->regs.DREGS->DCFG, dcfg.d32);
    	USB_OTG_EP0_OutStart(pdev);
    	gintsts.d32 = 0;
    	gintsts.b.usbreset = 1;
    	USB_OTG_WRITE_REG32 (&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    	USBD_DCD_INT_fops->Reset(pdev);
    }
    if (gintr_status.b.enumdone)
    {
    	dctl.d32 = 0;
    	dsts.d32 = USB_OTG_READ_REG32(&pdev->regs.DREGS->DSTS);
    	diepctl.d32 = USB_OTG_READ_REG32(&pdev->regs.INEP_REGS[0]->DIEPCTL);
    	switch (dsts.b.enumspd)
    	{
    		case DSTS_ENUMSPD_HS_PHY_30MHZ_OR_60MHZ:
    	  	case DSTS_ENUMSPD_FS_PHY_30MHZ_OR_60MHZ:
    	  	case DSTS_ENUMSPD_FS_PHY_48MHZ:
    	  		diepctl.b.mps = DEP0CTL_MPS_64;
    	  		break;
    	  	case DSTS_ENUMSPD_LS_PHY_6MHZ:
    	  		diepctl.b.mps = DEP0CTL_MPS_8;
    	  		break;
    	}
    	USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[0]->DIEPCTL, diepctl.d32);
    	dctl.b.cgnpinnak = 1;
    	USB_OTG_MODIFY_REG32(&pdev->regs.DREGS->DCTL, dctl.d32, dctl.d32);
    	gusbcfg.d32 = USB_OTG_READ_REG32(&pdev->regs.GREGS->GUSBCFG);
    	pdev->cfg.mps              = 64 ;
    	gusbcfg.b.usbtrdtim = 5;
    	USB_OTG_WRITE_REG32(&pdev->regs.GREGS->GUSBCFG, gusbcfg.d32);
    	gintsts.d32 = 0;
    	gintsts.b.enumdone = 1;
    	USB_OTG_WRITE_REG32( &pdev->regs.GREGS->GINTSTS, gintsts.d32 );
    }
    if (gintr_status.b.incomplisoin)
    {
    	gintsts.d32 = 0;
    	USBD_DCD_INT_fops->IsoINIncomplete (pdev);
    	gintsts.b.incomplisoin = 1;
    	USB_OTG_WRITE_REG32(&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    }
    if (gintr_status.b.incomplisoout)
    {
    	gintsts.d32 = 0;
    	USBD_DCD_INT_fops->IsoOUTIncomplete (pdev);
    	gintsts.b.incomplisoout = 1;
    	USB_OTG_WRITE_REG32(&pdev->regs.GREGS->GINTSTS, gintsts.d32);
    }
}
static uint8_t  USBD_HID_Setup (void  *pdev, USB_SETUP_REQ *req)
{
	uint16_t len = 0;
	uint8_t  *pbuf = NULL;
	switch (req->bmRequest & USB_REQ_TYPE_MASK)
	{
		case USB_REQ_TYPE_CLASS:
			switch (req->bRequest)
			{
				case HID_REQ_SET_PROTOCOL:
					USBD_HID_Protocol = (uint8_t)(req->wValue);
					break;
				case HID_REQ_GET_PROTOCOL:
					USBD_CtlSendData (pdev, (uint8_t *)&USBD_HID_Protocol, 1);
					break;
				case HID_REQ_SET_IDLE:
					USBD_HID_IdleState = (uint8_t)(req->wValue >> 8);
					break;
				case HID_REQ_GET_IDLE:
					USBD_CtlSendData (pdev, (uint8_t *)&USBD_HID_IdleState, 1);
					break;
				default:
					USBD_CtlError (pdev, req);
					return USBD_FAIL;
			}
			break;
		case USB_REQ_TYPE_STANDARD:
			switch (req->bRequest)
			{
				case USB_REQ_GET_DESCRIPTOR:
					if( req->wValue >> 8 == HID_REPORT_DESC)
					{
						len = MIN(74 , req->wLength);
						pbuf = HID_MOUSE_ReportDesc;
					}
					else if( req->wValue >> 8 == HID_DESCRIPTOR_TYPE)
					{
						pbuf = USBD_HID_Desc;
						len = MIN(9 , req->wLength);
					}
					USBD_CtlSendData (pdev, pbuf, len);
					break;
				case USB_REQ_GET_INTERFACE :
					USBD_CtlSendData (pdev, (uint8_t *)&USBD_HID_AltSet, 1);
					break;
				case USB_REQ_SET_INTERFACE :
					USBD_HID_AltSet = (uint8_t)(req->wValue);
					break;
			}
	}
	return USBD_OK;
}
void USBD_HID_SendReport (USB_OTG_CORE_HANDLE  *pdev, uint8_t *report, uint16_t len)
{
	if (pdev->dev.device_status == USB_OTG_CONFIGURED) DCD_EP_Tx (pdev, HID_IN_EP, report, len);
}
static uint8_t  *USBD_HID_GetCfgDesc (uint16_t *length)
{
	*length = sizeof (USBD_HID_CfgDesc);
	return USBD_HID_CfgDesc;
}
void CLEAR_IN_EP_INTR(epnum,intr)
{
	diepint.d32=0;
	diepint.b.intr = 1;
	USB_OTG_WRITE_REG32(&pdev->regs.INEP_REGS[epnum]->DIEPINT,diepint.d32);
}
void CLEAR_OUT_EP_INTR(epnum,intr)
{
	doepint.d32=0;
	doepint.b.intr = 1;
	USB_OTG_WRITE_REG32(&pdev->regs.OUTEP_REGS[epnum]->DOEPINT,doepint.d32);
}
