#include "system.h"
#include "lcd.h"
#include "uart_log.h"
//
#define _SW				1
#define _PWM			0//RGB LED with PWM
//
typedef enum
{
	s_none,
	s_attached,
	s_powered,
	s_default,
	s_address,
	s_configured,
	s_suspended
}
USB_states;
typedef struct
{
	uint8_t bmRequestType;
	uint8_t bRequest;
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
}
std_req;
//
void main(void);
void OTG_HS_IRQHandler(void);
void EXTI0_IRQHandler(void);
void usb_init_dma(void);
void setup_ep0(void);
void data_in(uint8_t size, uint32_t tx_data_address);
void status_out(void);
void inep0_stall(void);
//
const uint8_t DeviceDescriptor[18] __attribute__ ((aligned(4))) =
{
	0x12,             	          	// bLength
	0x01,       					// bDescriptorType
	0x00,                     	  	// bcdUSB low
	0x02,							// bcdUSB high
	0x00,                       	// bDeviceClass
	0x00,                       	// bDeviceSubClass
	0x00,                       	// bDeviceProtocol
	0x40,				           	// bMaxPacketSize0
	0x83,           				// idVendor low
	0x04,           				// 	 high
	0x50,           				// idProduct low
	0x57,          					// idProduct high
	0x00,                       	// bcdDevice low
	0x01,							// bcdDevice high
	0x01,           				// iManufacturer
	0x00,       					// iProduct
	0x00,        					// iSerialNumber
	0x01  							// bNumConfigurations
};
const uint8_t ConfigurationDescriptor[41] __attribute__ ((aligned(4))) =
{
//Configaration descriptor
	0x09, 							//bLength
	0x02, 							//bDescriptorType
	0x29,							//wTotalLength low
	0x00,							//wTotalLength high
	0x01,         					//bNumInterfaces
	0x01,         					//bConfigurationValue
	0x00,         					//iConfiguration
	0xC0,         					//bmAttributes
	0x32,        					//bMaxPower
//Interface descriptor
	0x09,         					//bLength
	0x04,							//bDescriptorType
	0x01,         					//bInterfaceNumber
	0x00,         					//bAlternateSetting
	0x02,         					//bNumEndpoints
	0x03,         					//bInterfaceClass
	0x00,         					//bInterfaceSubClass
	0x00,         					//bInterfaceProtocol
	0x00,            				//iInterface
//HID descriptor
	0x09,         					//bLength
	0x21,							//bDescriptorType
	0x11,         					//bcdHID low
	0x01,							//bcdHID high
	0x00,         					//bCountryCode
	0x01,         					//bNumDescriptors
	0x22,         					//bDescriptorType
	0x26,							//wDescriptorLength
	0x00,
//EP descriptor
	0x07,							//bLength
	0x05,							//bDescriptorType
	0x81,							//bEndpointAddress
	0x03,          					//bmAttributes
	0x04,							//wMaxPacketSize
	0x00,
	0x14,          					//bInterval
//EP descriptor
	0x07,							//bLength
	0x05,							//bDescriptorType
	0x01,							//bEndpointAddress
	0x03,          					//bmAttributes
	0x04,							//wMaxPacketSize
	0x00,
	0x14,          					//bInterval
};
const uint8_t StringDescriptor0[4] __attribute__ ((aligned(4))) =
{
	0x04,							//bLength
	0x03,							//bDescriptorType
	0x09,							//wLANGID low
	0x04							//wLANGID high
};
const uint8_t StringDescriptor1[10] __attribute__ ((aligned(4))) =
{
	0x0a,							//bLength
	0x03,							//bDescriptorType
	0x54,
	0x00,
	0x65,
	0x00,
  	0x53,
	0x00,
	0x74,
	0x00
};
const uint8_t HIDReportDescriptor[38] __attribute__ ((aligned(4))) =
{
	0x06, 							// USAGE_PAGE (Vendor Defined Page 1)
	0x00,
	0xff,
    0x09, 							// USAGE (Vendor Usage 1)
	0x01,
    0xa1, 							// COLLECTION (Application)
	0x01,
    0x09, 							//   USAGE (Undefined)
	0x00,
    0x15, 							//   LOGICAL_MINIMUM (0)
	0x00,
    0x27, 							//   LOGICAL_MAXIMUM (16777215)
	0xff,
	0xff,
	0xff,
	0x00,
    0x95, 							//   REPORT_COUNT (1)
	0x01,
    0x75, 							//   REPORT_SIZE (24)
	0x18,
    0x91, 							//   OUTPUT (Data,Var,Abs,Vol)
	0x82,
    0x09, 							//   USAGE (Vendor Usage 2)
	0x02,
    0x15, 							//   LOGICAL_MINIMUM (0)
	0x00,
    0x27, 							//   LOGICAL_MAXIMUM (2147483647)
	0xff,
	0xff,
	0xff,
	0x7f,
    0x95, 							//   REPORT_COUNT (1)
	0x01,
    0x75, 							//   REPORT_SIZE (31)
	0x1f,
    0x81, 							//   INPUT (Data,Var,Abs,Vol)
	0x82,
    0xc0                           	//END_COLLECTION
};
uint32_t setup[12] = {0};
USB_states dev_state = s_none;
uint8_t pkt_pointer, setup_req, iepxferc, oepxfrc;
uint16_t temp3;
std_req req;
uint32_t rx_buf[6];
uint8_t string[11] = {0};
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x29,
	.month = 0x08,
	.year = 0x18
};
//
void main(void)
{
	sys_init();
	uart1_init();
	uart_log_init(USART1);
	uart_log_add(t_evt, "Start", 0);
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_OTGHSEN|RCC_AHB1ENR_GPIOGEN);
	GPIOB->MODER |= (2 << (14*2))|(2 << (15*2));
	GPIOB->OSPEEDR |= (3 << (14*2))|(3 << (15*2));
	GPIOB->AFR[1] |= (12 << 24)|(12 << 28);
	GPIOG->MODER |= 0x14000000;
	GPIOG->PUPDR |= 0x14000000;
#if _PWM
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIOCEN);
	//B4-CH1-R
	GPIOB->MODER |= (2 << (4*2));
	GPIOB->AFR[0] |= (2 << 16);
	//A7-CH2-G
	GPIOA->MODER |= (2 << (7*2));
	GPIOA->AFR[0] |= (2 << 28);
	//C8-CH3-B
	GPIOC->MODER |= (2 << (8*2));
	GPIOC->AFR[1] |= 2;
	TIM3->ARR = 65026;
	TIM3->PSC = 1;
	TIM3->CCMR1 |= 0x6868;
	TIM3->CCMR2 |= 0x0068;
	TIM3->CCR1 = 0 * 255;
	TIM3->CCR2 = 0 * 255;
	TIM3->CCR3 = 0 * 255;
	TIM3->CCER |= 0x0111;
	TIM3->CR1 |= (TIM_CR1_CEN|TIM_CR1_ARPE);
#endif
#if _SW
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	SYSCFG->EXTICR[0] = 0x00;
	EXTI->IMR |= 1;
	EXTI->FTSR |= 1;
	NVIC_EnableIRQ(EXTI0_IRQn);
#endif
	usb_init_dma();
	board_LED(green, on);
	uart_log_add(t_info, "USB init done", 0);
	while (1)
	{
		if(setup_req)
		{
			board_LED(red, off);
			setup_req = 0;
			switch(req.bmRequestType & 0x60)
			{
				case 0://standart req
					switch (req.bRequest)
					{
						case 0://GET_STATUS
							uart_log_add(t_evt, "Std req", "Get status", 0);
							goto second;
							break;
						case 1://CLEAR_FUTURE
							uart_log_add(t_evt, "Std req", "Clear future", 0);
							data_in(0, 0);//status_in_phase
							while(iepxferc == 0){};
							iepxferc = 0;
							break;
						case 3://SET_FUTURE
							uart_log_add(t_evt, "Std req", "Set future", 0);
							data_in(0, 0);//status_in_phase
							while(iepxferc == 0){};
							iepxferc = 0;
							break;
						case 5://SET_ADDRESS
							uint8_to_hex_string(req.wValue&0xff, string);
							uart_log_add(t_evt, "Std req", "Set address", string, 0);
							USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
							USB_OTG_HS_DEVICE->DCFG |= (req.wValue << 4);
							data_in(0, 0);//status_in_phase
							while(iepxferc == 0){};
							iepxferc = 0;
							dev_state = s_address;
							break;
						case 6://GET_DESCRIPTOR
							switch (req.wValue >> 8)
							{
								case 1://device
									uart_log_add(t_evt, "Std req", "Get descriptor", "Device", 0);
									if(dev_state == s_default) data_in(8, (uint32_t)DeviceDescriptor);
									else data_in(req.wLength, (uint32_t)DeviceDescriptor);
									break;
								case 2://config
									uart_log_add(t_evt, "Std req", "Get descriptor", "Config", 0);
									temp3 = ((req.wLength < 41) ? req.wLength : 41);
									data_in(temp3, (uint32_t)ConfigurationDescriptor);
									break;
								case 3://string
									uint8_to_hex_string(req.wValue&0xff, string);
									uart_log_add(t_evt, "Std req", "Get descriptor", "String", string, 0);
									switch (req.wValue & 0xff)
									{
										case 0: data_in(4, (uint32_t)StringDescriptor0); break;
										case 1: data_in(10, (uint32_t)StringDescriptor1); break;
										default:
											uart_log_add(t_fault, "Fail string", 0);
											goto second;
											break;
									}
									break;
								case 0x22://hid report descriptor
									uart_log_add(t_evt, "Std req", "Get descriptor", "HID report", 0);
									data_in(req.wValue, (uint32_t)HIDReportDescriptor);
									break;
								default:
									uint8_to_hex_string((req.wValue >> 8), string);
									uart_log_add(t_fault, "Unknown req", string, 0);
									goto second;
									break;
							}
							while(iepxferc == 0){};
							iepxferc = 0;
							status_out();
							while(oepxfrc == 0){};
							oepxfrc = 0;
							break;
						case 7://SET_DESCRIPTOR
							uart_log_add(t_evt, "Std req", "Set descriptor", 0);
							goto second;
							break;
						case 8://GET_CONFIGURATION
							uart_log_add(t_evt, "Std req", "Get config", 0);
							goto second;
							break;
						case 9://SET_CONFIGURATION
							uint8_to_hex_string(req.wValue&0xff, string);
							uart_log_add(t_evt, "Std req", "Set config", string, 0);
							data_in(0, 0);//status_in_phase
							while(iepxferc == 0){};
							iepxferc = 0;
							dev_state = s_configured;
							break;
						case 10://GET_INTERFACE
							uart_log_add(t_evt, "Std req", "Get interface", 0);
							goto second;
							break;
						case 11://SET_INTERFACE
							uart_log_add(t_evt, "Std req", "Set interface", 0);
							data_in(0, 0);//status_in_phase
							while(iepxferc == 0){};
							iepxferc = 0;
							break;
						case 12://SYNCH_FRAME
							uart_log_add(t_evt, "Std req", "Synch frame", 0);
							goto second;
							break;
						default:
							uint8_to_hex_string(req.bRequest, string);
							uart_log_add(t_fault, "Unknown req", string, 0);
							goto second;
							break;
					}
					break;
				case 0x20://class req
					uart_log_add(t_evt, "Class req", 0);
					break;
				default:
					uint8_to_hex_string(req.bmRequestType, string);
					uart_log_add(t_fault, "Unknown req", string, 0);
second:
					uart_log_add(t_fault, "IN EP0 STALL", 0);
					inep0_stall();
					break;
			}
			setup_ep0();
		}
		else board_LED(red, on);
	}

}
void OTG_HS_IRQHandler(void)
{
	uint32_t status, status_eps, status_ep_int;
	status = USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK;
	if(status == 0)
	{
		uart_log_add(t_irq, "USB IRQ", "undefine", 0);
		return;
	}
	if((status & USB_OTG_GINTSTS_WKUINT) == USB_OTG_GINTSTS_WKUINT)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_WKUINT;
		uart_log_add(t_irq, "USB IRQ", "wakeup detected", 0);
	}
	if((status & USB_OTG_GINTSTS_USBSUSP) == USB_OTG_GINTSTS_USBSUSP)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_USBSUSP;
		uart_log_add(t_irq, "USB IRQ", "USB suspend", 0);
	}
	if((status & USB_OTG_GINTSTS_SOF) == USB_OTG_GINTSTS_SOF)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_SOF;
		uart_log_add(t_irq, "USB IRQ", "Start of frame", 0);
	}
	if((status & USB_OTG_GINTSTS_ENUMDNE) == USB_OTG_GINTSTS_ENUMDNE)
	{
		USB_OTG_HS_DEVICE->DCTL |= USB_OTG_DCTL_CGINAK;
		USB_OTG_HS_OUTEP(0)->DOEPCTL = USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_ENUMDNE;
		uart_log_add(t_irq, "USB IRQ", "Enumeration done", 0);
	}
	if((status & USB_OTG_GINTSTS_USBRST) == USB_OTG_GINTSTS_USBRST)
	{
		USB_OTG_HS_DEVICE->DCTL &= ~USB_OTG_DCTL_RWUSIG;
		USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4;//All Tx FIFO's
		while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
		USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
		while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
    	for (uint8_t i = 0; i < 6; i++)
    	{
    		USB_OTG_HS_INEP(i)->DIEPINT = 0xFFFFFFFF;
    		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
    	}
    	USB_OTG_HS_DEVICE->DAINTMSK = 0x00010001;
    	USB_OTG_HS_DEVICE->DOEPMSK = USB_OTG_DOEPMSK_STUPM|USB_OTG_DOEPMSK_XFRCM;
    	USB_OTG_HS_DEVICE->DIEPMSK = USB_OTG_DIEPMSK_XFRCM;
    	USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
    	setup_ep0();
    	USB_OTG_HS->GINTSTS = 0xFFFFFFFF;
    	USB_OTG_HS->GINTMSK = USB_OTG_GINTMSK_USBRST|USB_OTG_GINTMSK_ENUMDNEM|USB_OTG_GINTMSK_IEPINT|USB_OTG_GINTMSK_OEPINT;
    	for (uint8_t i = 1; i < 6; i++)
    	{
    		USB_OTG_HS_INEP(i) ->DIEPCTL = 0;
    		USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
    	}
    	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
    	uart_log_add(t_irq, "USB IRQ", "USB reset", 0);
    	dev_state = s_default;
	}
	if((status & USB_OTG_GINTSTS_OEPINT) == USB_OTG_GINTSTS_OEPINT)
	{
		uart_log_add(t_irq, "USB IRQ", "OUT EP IRQ", 0);
		status_eps = (USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT) >> 16;
		for (uint8_t i = 0; i < 6; i++)
		{
			if (status_eps & (1 << i))
			{
				uint8_to_hex_string(i, string);
				status_ep_int = USB_OTG_HS_OUTEP(i)->DOEPINT & USB_OTG_HS_DEVICE->DOEPMSK;
				if ((status_ep_int & USB_OTG_DOEPINT_XFRC) == USB_OTG_DOEPINT_XFRC)
				{
					USB_OTG_HS_OUTEP(i)->DOEPINT = USB_OTG_DOEPINT_XFRC;
					oepxfrc = 1;
					uart_log_add(t_info, "OUT EP", string, "transfer complete", 0);
				}
				if ((status_ep_int & USB_OTG_DOEPINT_STUP) == USB_OTG_DOEPINT_STUP)
				{
					uart_log_add(t_info, "OUT EP", string, "setup", 0);
					if((USB_OTG_HS_OUTEP(0)->DOEPINT & USB_OTG_DOEPINT_B2BSTUP) == USB_OTG_DOEPINT_B2BSTUP)
					{
						USB_OTG_HS_OUTEP(0)->DOEPINT = USB_OTG_DOEPINT_B2BSTUP;
						uart_log_add(t_fault, "Core has received more than three back-to-back SETUP packets", 0);
					}
					else
					{
						pkt_pointer = 4 - 2*(USB_OTG_HS_OUTEP(0)->DOEPTSIZ >> 29);
						req = *((std_req*)(&setup[pkt_pointer]));
						setup_req = 1;
					}
					USB_OTG_HS_OUTEP(0)->DOEPINT = USB_OTG_DOEPINT_STUP;
				}
			}
		}
	}
	if((status & USB_OTG_GINTSTS_IEPINT) == USB_OTG_GINTSTS_IEPINT)
	{
		uart_log_add(t_irq, "USB IRQ", "IN EP IRQ", 0);
		status_eps = USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT;
		for (uint8_t i = 0; i < 6; i++)
		{
			if (status_eps & (1 << i))
			{
				uint8_to_hex_string(i, string);
				status_ep_int = USB_OTG_HS_INEP(i)->DIEPINT & USB_OTG_HS_DEVICE->DIEPMSK;
				if ((status_ep_int & USB_OTG_DIEPINT_XFRC) == USB_OTG_DIEPINT_XFRC)
				{
					USB_OTG_HS_INEP(i)->DIEPINT = USB_OTG_DIEPINT_XFRC;
					iepxferc = 1;
					uart_log_add(t_info, "IN EP", string, "transfer complete", 0);
				}
			}
		}
	}
}
void EXTI0_IRQHandler(void)
{
	EXTI->PR = 1;
	uart_log_add(t_irq, "User button was pressed", 0);
}
void usb_init_dma(void)
{
	USB_OTG_HS->GAHBCFG &= ~USB_OTG_GAHBCFG_GINT;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_AHBIDL) == 0){};
	USB_OTG_HS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_CSRST){};
	delay_us(3);
	USB_OTG_HS->GCCFG = USB_OTG_GCCFG_PWRDWN|USB_OTG_GCCFG_VBUSASEN|USB_OTG_GCCFG_VBUSBSEN;
	delay_ms(20);
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_DMAEN|(5<<1);/* 64 x 32-bits*/
	USB_OTG_HS->GUSBCFG &= ~USB_OTG_GUSBCFG_FHMOD;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_FDMOD;
	delay_ms(50);
	USB_OTG_HS_PCGCCTL = 0;
	USB_OTG_HS_DEVICE->DCFG |= 3;//DCFG_FRAME_INTERVAL_80|USB_OTG_SPEED_PARAM_HIGH_IN_FULL;
	USB_OTG_HS->GRXFSIZ = 512;
	USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = (512 << 16)|512;//Set Tx EP0 FIFO size
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4;
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
	delay_us(3);
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
	delay_us(3);
	USB_OTG_HS_DEVICE->DIEPMSK  = 0;
	USB_OTG_HS_DEVICE->DOEPMSK  = 0;
	USB_OTG_HS_DEVICE->DAINT = 0xFFFFFFFF;
	USB_OTG_HS_DEVICE->DAINTMSK = 0;
	for (uint8_t i = 0; i < 6; i++)
	{
	   	USB_OTG_HS_INEP(i)->DIEPCTL = 0;
	   	USB_OTG_HS_INEP(i)->DIEPTSIZ = 0;
	   	USB_OTG_HS_INEP(i)->DIEPINT = 0xFF;
	   	USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
	   	USB_OTG_HS_OUTEP(i)->DOEPTSIZ = 0;
	   	USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFF;
	}
	USB_OTG_HS_DEVICE->DIEPMSK = USB_OTG_DIEPMSK_TXFURM;
	USB_OTG_HS_DEVICE->DTHRCTL = USB_OTG_DTHRCTL_NONISOTHREN|USB_OTG_DTHRCTL_ISOTHREN|USB_OTG_DTHRCTL_RXTHREN|(64<<17)|(64<<2);
	USB_OTG_HS->GINTMSK = 0;
	USB_OTG_HS->GINTSTS = 0xBFFFFFFF;
	USB_OTG_HS->GINTMSK = USB_OTG_GINTMSK_USBRST|USB_OTG_GINTMSK_ENUMDNEM|USB_OTG_GINTMSK_IEPINT|USB_OTG_GINTMSK_OEPINT|
			USB_OTG_GINTMSK_IISOIXFRM|USB_OTG_GINTMSK_PXFRM_IISOOXFRM;
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
	NVIC_EnableIRQ(OTG_HS_IRQn);
}
void setup_ep0(void)
{
	USB_OTG_HS_OUTEP(0)->DOEPTSIZ = USB_OTG_DOEPTSIZ_STUPCNT;//3
	USB_OTG_HS_OUTEP(0)->DOEPDMA = (uint32_t)&setup[0];
	USB_OTG_HS_OUTEP(0)->DOEPCTL = USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
}
void data_in(uint8_t size, uint32_t tx_data_address)
{
	USB_OTG_HS_INEP(0)->DIEPDMA = tx_data_address;
	USB_OTG_HS_INEP(0)->DIEPTSIZ = (((size/64)+1) << 19)|size;//PKTCNT = 1
	USB_OTG_HS_INEP(0)->DIEPCTL = USB_OTG_DIEPCTL_EPENA|USB_OTG_DIEPCTL_CNAK|64;
}
void status_out(void)
{
	USB_OTG_HS_OUTEP(0)->DOEPTSIZ = (1 << 19);//|64;
	USB_OTG_HS_OUTEP(0)->DOEPDMA = 0;//(uint32_t)&rx_buf[0];
	USB_OTG_HS_OUTEP(0)->DOEPCTL |= USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
}
void inep0_stall(void)
{
	uint32_t temp_l = USB_OTG_DIEPCTL_STALL;
	if(USB_OTG_HS_INEP(0)->DIEPCTL & USB_OTG_DIEPCTL_EPENA) temp_l |= USB_OTG_DIEPCTL_EPDIS;
	USB_OTG_HS_INEP(0)->DIEPCTL = temp_l;
}
