#include "main.h"
//
#define _SW				1
#define _PWM			0//RGB LED with PWM
#define _UART			0//debug
#define _USB_DMA		1//USb with DMA
//
uint32_t setup[12] = {0};
USB_states dev_state = none;
uint8_t irq_text[] = "In taberna quando sumus,\n\rnon curamus, quid sit humus,\n\rsed ad ludum properamus,\n\rcui semper insudamus.\n\rquid agatur in taberna,\n\rubi nummus est pincerna,\n\rhoc est opus, ut queratur,\n\rsed quid loquar, audiatur.\n\r";
uint8_t pkt_pointer;
uint8_t temp2[3] = "0\n\r";
uint16_t temp3;
std_req req;
uint32_t rx_buf[6];
//
void main(void);
void OTG_HS_IRQHandler(void);
void EXTI0_IRQHandler(void);
void usb_print_char(char pc, colors color);
void usb_print_string(uint8_t* string, colors color);
void usb_init(void);
void usb_init_dma(void);
void setup_ep0(void);
void data_in(uint8_t size, uint32_t tx_data_address);
void status_out(void);
//
void main(void)
{
	SysInit();
#if _UART
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->MODER |= (3<<(9*2))|(3<<(10*2));
	GPIOA->PUPDR |= (1<<(9*2))|(1<<(10*2));
	GPIOA->AFR[1] |= (7 << 4)|(7 << 8);
	uart1_init();
#else
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|
			RCC_AHB1ENR_GPIOCEN|RCC_AHB1ENR_GPIODEN|RCC_AHB1ENR_GPIOEEN|
			RCC_AHB1ENR_GPIOFEN|RCC_AHB1ENR_GPIOGEN);
	//A
	GPIOA->AFR[0] |= 0x0E0EE000;
	GPIOA->AFR[1] |= 0x000EE000;
	GPIOA->MODER |= 0x02802280;
	GPIOA->OSPEEDR |= 0x02802280;
	//B
	GPIOB->AFR[0] |= 0x0CC00099;
	GPIOB->AFR[1] |= 0x0000EEEE;
	GPIOB->MODER |= 0x00AA280A;
	GPIOB->OSPEEDR |= 0x00AA280A;
	//C
	GPIOC->AFR[0] |= 0xEE00000C;
	GPIOC->AFR[1] |= 0x00000E00;
	GPIOC->MODER |= 0x0020A012;
	GPIOC->OSPEEDR |= 0x0020A022;
	//D
	GPIOD->AFR[0] |= 0x0E00E0CC;
	GPIOD->AFR[1] |= 0xCC000CCC;
	GPIOD->MODER |= 0xA52A208A;
	GPIOD->OSPEEDR |= 0xAA2A208A;
	//E
	GPIOE->AFR[0] |= 0xC00000CC;
	GPIOE->AFR[1] |= 0xCCCCCCCC;
	GPIOE->MODER |= 0xAAAA800A;
	GPIOE->OSPEEDR |= 0xAAAA800A;
	//F
	GPIOF->AFR[0] |= 0x50CCCCCC;
	GPIOF->AFR[1] |= 0xCCCCCE55;
	GPIOF->MODER |= 0xAAAA8AAA;
	GPIOF->OSPEEDR |= 0xAAA54AAA;
	GPIOF->PUPDR |= 0x000A8000;
	//G
	GPIOG->AFR[0] |= 0xEECC00CC;
	GPIOG->AFR[1] |= 0xC009E90C;
	GPIOG->MODER |= 0x82A2AA0A;
	GPIOG->OSPEEDR |= 0x82A2AA0A;
	LCDInit(C_Black);
	/*for (uint8_t k = 0; k < 20; k++)
	{
		for (uint8_t i = 0x20; i < 0x7F; i++) lcd_print_char(i,C_White);
	}*/
#endif
	usb_print_string("Start\n\r", C_White);
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_OTGHSEN|RCC_AHB1ENR_GPIOGEN);
	GPIOB->MODER |= (2 << (14*2))|(2 << (15*2));
	GPIOB->OSPEEDR |= (3 << (14*2))|(3 << (15*2));
	GPIOB->AFR[1] |= (12 << 24)|(12 << 28);
	GPIOG->MODER |= 0x14000000;
	GPIOG->PUPDR |= 0x14000000;
#if _PWM
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIOCEN);
	//B4-CH1-R
	GPIOB->MODER |= (2 << (4*2));
	GPIOB->AFR[0] |= (2 << 16);
	//A7-CH2-G
	GPIOA->MODER |= (2 << (7*2));
	GPIOA->AFR[0] |= (2 << 28);
	//C8-CH3-B
	GPIOC->MODER |= (2 << (8*2));
	GPIOC->AFR[1] |= 2;
	TIM3->ARR = 65026;
	TIM3->PSC = 1;
	TIM3->CCMR1 |= 0x6868;
	TIM3->CCMR2 |= 0x0068;
	TIM3->CCR1 = 0 * 255;
	TIM3->CCR2 = 0 * 255;
	TIM3->CCR3 = 0 * 255;
	TIM3->CCER |= 0x0111;
	TIM3->CR1 |= (TIM_CR1_CEN|TIM_CR1_ARPE);
#endif
#if _SW
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	SYSCFG->EXTICR[0] = 0x00;
	EXTI->IMR |= 1;
	EXTI->FTSR |= 1;
	NVIC_EnableIRQ(EXTI0_IRQn);
#endif
#if _USB_DMA
	usb_init_dma();
#else
	usb_init();
#endif
	board_LED(green, on);
	usb_print_string("USB init done\n\r", C_White);
	while (1)
	{
		board_LED(red, on);
	}
}
void OTG_HS_IRQHandler(void)
{
	uint32_t status, status_eps, status_ep_int;
	status = USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK;
	if(status == 0)
	{
		usb_print_string("USB IRQ - undefine\n\r", C_Red);
		return;
	}
	board_LED(red, off);
#if _USB_DMA
	if((status & USB_OTG_GINTSTS_WKUINT) == USB_OTG_GINTSTS_WKUINT)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_WKUINT;
		usb_print_string("USB IRQ - wakeup detected\n\r", C_Red);
	}
	if((status & USB_OTG_GINTSTS_USBSUSP) == USB_OTG_GINTSTS_USBSUSP)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_USBSUSP;
		usb_print_string("USB IRQ - USB suspend\n\r", C_Red);
	}
	if((status & USB_OTG_GINTSTS_SOF) == USB_OTG_GINTSTS_SOF)
	{
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_SOF;
		usb_print_string("USB IRQ - Start of frame\n\r", C_Red);
	}
	if((status & USB_OTG_GINTSTS_ENUMDNE) == USB_OTG_GINTSTS_ENUMDNE)
	{
		USB_OTG_HS_DEVICE->DCTL |= USB_OTG_DCTL_CGINAK;
		USB_OTG_HS_OUTEP(0)->DOEPCTL = USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
		USB_OTG_HS->GINTSTS = USB_OTG_GINTSTS_ENUMDNE;
		usb_print_string("Enumeration done\n\r", C_Yellow);
	}
	if((status & USB_OTG_GINTSTS_USBRST) == USB_OTG_GINTSTS_USBRST)
	{
		USB_OTG_HS_DEVICE->DCTL &= ~USB_OTG_DCTL_RWUSIG;
		USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4;//All Tx FIFO's
		while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
		USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
		while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
    	for (uint8_t i = 0; i < 6; i++)
    	{
    		USB_OTG_HS_INEP(i) ->DIEPINT = 0xFFFFFFFF;
    		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
    	}
    	USB_OTG_HS_DEVICE->DAINTMSK = 0x00010001;
    	USB_OTG_HS_DEVICE->DOEPMSK = USB_OTG_DOEPMSK_STUPM|USB_OTG_DOEPMSK_XFRCM;
    	USB_OTG_HS_DEVICE->DIEPMSK = USB_OTG_DIEPMSK_XFRCM;
    	USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
    	setup_ep0();
    	USB_OTG_HS->GINTSTS = 0xFFFFFFFF;
    	USB_OTG_HS->GINTMSK = USB_OTG_GINTMSK_USBRST|USB_OTG_GINTMSK_ENUMDNEM|USB_OTG_GINTMSK_IEPINT|
    			USB_OTG_GINTMSK_OEPINT;
    	for (uint8_t i = 1; i < 6; i++)
    	{
    		USB_OTG_HS_INEP(i) ->DIEPCTL = 0;
    		USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
    	}
    	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
    	usb_print_string("USB reset\n\r", C_Yellow);
	}
	if((status & USB_OTG_GINTSTS_OEPINT) == USB_OTG_GINTSTS_OEPINT)
	{
		status_eps = (USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT) >> 16;
		for (uint8_t i = 0; i < 6; i++)
		{
			if (status_eps & (1 << i))
			{
				status_ep_int = USB_OTG_HS_OUTEP(i)->DOEPINT & USB_OTG_HS_DEVICE->DOEPMSK;
				if ((status_ep_int & USB_OTG_DOEPINT_XFRC) == USB_OTG_DOEPINT_XFRC)
				{
					USB_OTG_HS_OUTEP(i)->DOEPINT = USB_OTG_DOEPINT_XFRC;
					setup_ep0();
					usb_print_string("OUT EP transfer complete\n\r", C_Lime);
				}
				if ((status_ep_int & USB_OTG_DOEPINT_STUP) == USB_OTG_DOEPINT_STUP)
				{
					usb_print_string("OUT EP setup IRQ\n\r", C_Orange);
					if((USB_OTG_HS_OUTEP(0)->DOEPINT & USB_OTG_DOEPINT_B2BSTUP) == USB_OTG_DOEPINT_B2BSTUP)
					{
						USB_OTG_HS_OUTEP(0)->DOEPINT = USB_OTG_DOEPINT_B2BSTUP;
						usb_print_string("Core has received more than three back-to-back SETUP packets\n\r", C_Red);
					}
					else
					{
						pkt_pointer = 4 - 2*(USB_OTG_HS_OUTEP(0)->DOEPTSIZ >> 29);
						req = *((std_req*)(&setup[pkt_pointer]));
						if ((req.bmRequestType & 0x60) == 0)//bmRequestType == std ?
						{
							usb_print_string("Std req - ", C_Pink);
							switch (req.bRequest)
							{
								case 0://GET_STATUS
									usb_print_string("Get status\n\r", C_Pink);
									break;
								case 1://CLEAR_FUTURE
									usb_print_string("Clear future\n\r", C_Pink);
									break;
								case 3://SET_FUTURE
									usb_print_string("Set future\n\r", C_Pink);
									break;
								case 5://SET_ADDRESS
									usb_print_string("Set address: ", C_Pink);
									temp2[0] = req.wValue + 0x30;
									temp3 = req.wValue;
									usb_print_string(temp2, C_Pink);
									USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
									USB_OTG_HS_DEVICE->DCFG |= (temp3 << 4);
									data_in(0, 0);
									dev_state = address;
									break;
								case 6://GET_DESCRIPTOR
									usb_print_string("Get descriptor - ", C_Pink);
									switch (req.wValue >> 8)
									{
										case 1://device
											usb_print_string("Device\n\r", C_Pink);
											status_out();
											data_in(req.wLength, &DeviceDescriptor[0]);
											/*if(dev_state == address) data_in(0x12, &DeviceDescriptor[0]);
											else data_in(0x40, &DeviceDescriptor[0]);
											data_in(0x12, (uint32_t)&DeviceDescriptor[0]);
											*/
											break;
										case 2://config
											usb_print_string("Config\n\r", C_Pink);
											data_in(req.wLength, &ConfigurationDescriptor[0]);
											break;
										case 3://string
											usb_print_string("String\n\r", C_Pink);
											break;
										default:
											usb_print_string("Fail req.\n\r", C_Red);
											break;
									}
									break;
								case 7://SET_DESCRIPTOR
									usb_print_string("Set descriptor\n\r", C_Pink);
									break;
								case 8://GET_CONFIGURATION
									usb_print_string("Get config\n\r", C_Pink);
									break;
								case 9://SET_CONFIGURATION
									usb_print_string("Set config\n\r", C_Pink);
									break;
								case 10://GET_INTERFACE
									usb_print_string("Get interface\n\r", C_Pink);
									break;
								case 11://SET_INTERFACE
									usb_print_string("Set interface\n\r", C_Pink);
									break;
								case 12://SYNCH_FRAME
									usb_print_string("Synch frame\n\r", C_Pink);
									break;
								default:
									usb_print_string("Fail req.\n\r", C_Red);
									break;
							}
						}
						else usb_print_string("Fail req.\n\r", C_Red);
					}
					USB_OTG_HS_OUTEP(0)->DOEPINT = USB_OTG_DOEPINT_STUP;
				}
			}
		}
	}
	if((status & USB_OTG_GINTSTS_IEPINT) == USB_OTG_GINTSTS_IEPINT)
	{
		status_eps = USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT;
		for (uint8_t i = 0; i < 6; i++)
		{
			if (status_eps & (1 << i))
			{
				status_ep_int = USB_OTG_HS_INEP(i)->DIEPINT & USB_OTG_HS_DEVICE->DIEPMSK;
				if ((status_ep_int & USB_OTG_DIEPINT_XFRC) == USB_OTG_DIEPINT_XFRC)
				{
					USB_OTG_HS_INEP(i)->DIEPINT = USB_OTG_DIEPINT_XFRC;
					usb_print_string("IN EP transfer complete\n\r", C_Lime);
				}
			}
		}
	}
#else
    if((USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK & USB_OTG_GINTSTS_OEPINT) == USB_OTG_GINTSTS_OEPINT)
    {
    	temp1 = ((USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT) >> 16);
    	for (uint8_t i = 0; i < 6; i++)
    	{
    		if (((temp1 >> i) & 1) == 1)
    		{
    			temp2 = USB_OTG_HS_OUTEP(i)->DOEPINT & USB_OTG_HS_DEVICE->DOEPMSK;
    		    if ((temp2 & USB_OTG_DOEPINT_STUP) == USB_OTG_DOEPINT_STUP)
    		    {
//setup pucket parser
    		    	bufcount = 0;
    		        if ((setup[0] & 0x00000060) == 0)//bmRequestType == std ?
    		        {
    		        	temp3 = ((setup[0] & 0x0000FF00)>>8);
    		        	usb_print_string("Std req - ", C_Orange);
    		        	switch (temp3)
    		        	{
    		        		case 0://GET_STATUS
    		        			break;
    		        		case 1://CLEAR_FUTURE
    		        		    break;
    		        		case 3://SET_FUTURE
    		        		   	break;
    		        		case 5://SET_ADDRESS
    		        			usb_print_string("Set address: ", C_Orange);
    		        			temp3 = (setup[0] >> 16);
    		        			temp3 = (temp3 >> 4) + 0x30;
    		        			temp3 = (temp3 << 4);
    		        			usb_print_char(temp3, C_Orange);
    		        			usb_print_string("\n\r", C_Orange);
    		        			USB_OTG_HS_INEP(i)->DIEPTSIZ = 0x00080000;
    		        			transmit_data_size = 0;
    		        			USB_OTG_HS_INEP(i)->DIEPCTL |= (USB_OTG_DIEPCTL_CNAK|USB_OTG_DIEPCTL_EPENA);
    		        			USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
    		        			USB_OTG_HS_DEVICE->DCFG |= temp3;
    		        			dev_state = address;
    		        			break;
    		        		case 6://GET_DESCRIPTOR
    		        			usb_print_string("Get descriptor - ", C_Orange);
    		        		  	switch (setup[0]>>24)
    		        		   	{
    		        		   		case 1://device
    		        		   			usb_print_string("Device\n\r", C_Orange);
    		        		   			transmit_data_pointer = &DeviceDescriptor[0];
    		        		   			if(dev_state == address)
    		        		   			{
    		        		   				in_status_stage = 1;
    		        		   				transmit_data_size = 0x12;
    		        		   				USB_OTG_HS_INEP(i)->DIEPTSIZ = 0x00080012;

    		        		   			}
    		        		   			else
    		        		   			{
    		        		   				transmit_data_size = 2;
    		        		   				USB_OTG_HS_INEP(i)->DIEPTSIZ = 0x00080008;
    		        		   			}
    		        					break;
    		        				case 2://config
    		        					usb_print_string("Config\n\r", C_Orange);
    		        					transmit_data_pointer = &ConfigurationDescriptor[0];
    		        					transmit_data_size = 9;
    		        				    break;
    		        				case 3://string
    		        				    break;
    		        			}
    		        		  	USB_OTG_HS_DEVICE->DIEPEMPMSK |= (1 << i);
    		        		  	USB_OTG_HS_INEP(i)->DIEPCTL |= (USB_OTG_DIEPCTL_CNAK|USB_OTG_DIEPCTL_EPENA);
    		        			break;
    		        		case 7://SET_DESCRIPTOR
    		        			usb_print_string("Set descriptor\n\r", C_Orange);
    		        		   	break;
    		        		case 8://GET_CONFIGURATION
    		        			usb_print_string("Get config\n\r", C_Orange);
    		        		   	break;
    		        		case 9://SET_CONFIGURATION
    		        			usb_print_string("Set config\n\r", C_Orange);
    		        		  	break;
    		        		case 10://GET_INTERFACE
    		        			usb_print_string("Get interface\n\r", C_Orange);
    		        		   	break;
    		        		case 11://SET_INTERFACE
    		        			usb_print_string("Set interface\n\r", C_Orange);
    		        		  	break;
    		        		case 12://SYNCH_FRAME
    		        			usb_print_string("Synch frame\n\r", C_Orange);
    		        		   	break;
    		        	}
    		        }
    		        USB_OTG_HS_OUTEP(i)->DOEPTSIZ |= USB_OTG_DOEPTSIZ_STUPCNT;
    		        USB_OTG_HS_OUTEP(i)->DOEPINT |= USB_OTG_DOEPINT_STUP;
    		    }
    		    if ((temp2 & USB_OTG_DOEPINT_XFRC) == USB_OTG_DOEPINT_XFRC)
    		    {
    		    	usb_print_string("XFRC-OEPINT\n\r", C_Yellow);
    		    	USB_OTG_HS_OUTEP(i)->DOEPINT |= USB_OTG_DOEPINT_XFRC;
    		    }
    		}
    	}
    }
    if((USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK & USB_OTG_GINTSTS_IEPINT) == USB_OTG_GINTSTS_IEPINT)
    {
    	temp1 = (USB_OTG_HS_DEVICE->DAINTMSK & USB_OTG_HS_DEVICE->DAINT & 0x0000FFFF);
    	for (uint8_t i = 0; i < 6; i++)
    	{
    		if (((temp1 >> i) & 1) == 1)
    		{
    			temp2 = (USB_OTG_HS_INEP(i)->DIEPINT & (USB_OTG_HS_DEVICE->DIEPMSK|(((USB_OTG_HS_DEVICE->DIEPEMPMSK >> i) & 0x1) << 7)));
    			if ((temp2 & USB_OTG_DIEPINT_XFRC) == USB_OTG_DIEPINT_XFRC)
    			{
    				USB_OTG_HS_DEVICE->DIEPEMPMSK &= ~(uint32_t)(1 << i);
    				usb_print_string("ACK\n\r", C_Yellow);
    				USB_OTG_HS_INEP(i)->DIEPINT |= USB_OTG_DIEPINT_XFRC;
    			}
    			if ((temp2 & USB_OTG_DIEPINT_TXFE) == USB_OTG_DIEPINT_TXFE)
    			{
    				//temp3 = USB_OTG_HS_INEP(i)->DTXFSTS;
    				//(temp3 > transmit_data_size) ? (temp3 = transmit_data_size) : (transmit_data_size = transmit_data_size - temp3);
    				for (uint8_t j=0; j < transmit_data_size; j++)
    				{
    					USB_DFIFO(i) = *((__attribute__((packed)) uint32_t *)transmit_data_pointer);
    					transmit_data_pointer += 4;
    					usb_print_char('T', C_Indigo);
    				}
    				usb_print_string("\n\r", C_Indigo);
    				USB_OTG_HS_DEVICE->DIEPEMPMSK &= ~(uint32_t)(1 << i);
    				transmit_data_size = 0;
    				USB_OTG_HS_INEP(i)->DIEPINT |= USB_OTG_DIEPINT_TXFE;
    			}
    			if ((temp2 & USB_OTG_DIEPINT_ITTXFE) == USB_OTG_DIEPINT_ITTXFE)
    			{
    				USB_OTG_HS_INEP(i)->DIEPINT |= USB_OTG_DIEPINT_ITTXFE;
    			}
    			if ((temp2 & USB_OTG_DIEPINT_TOC) == USB_OTG_DIEPINT_TOC)
    			{
    				usb_print_string("TOC\n\r", C_Indigo);
    				USB_OTG_HS_INEP(i)->DIEPINT |= USB_OTG_DIEPINT_TOC;
    			}

    		}
    	}
    }
    if((USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK & USB_OTG_GINTSTS_USBRST) == USB_OTG_GINTSTS_USBRST)
    {
    	USB_OTG_HS_DEVICE->DCTL &= ~USB_OTG_DCTL_RWUSIG;
    	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH;
    	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH) == USB_OTG_GRSTCTL_TXFFLSH){}
    	for (uint8_t i = 0; i < 6 ; i++)
    	{
    		USB_OTG_HS_INEP(i) ->DIEPINT = 0xFFFFFFFF;
    		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
    	}
    	USB_OTG_HS_DEVICE->DAINT = 0xFFFFFFFF;
    	USB_OTG_HS_DEVICE->DAINTMSK |= 0x00010001;
    	USB_OTG_HS->GINTSTS &= USB_OTG_GINTSTS_USBRST;
    	USB_OTG_HS_DEVICE->DOEPMSK |= (USB_OTG_DOEPMSK_STUPM|USB_OTG_DOEPMSK_XFRCM|USB_OTG_DOEPMSK_EPDM);
    	USB_OTG_HS_DEVICE->DIEPMSK |= (USB_OTG_DIEPMSK_TOM|USB_OTG_DIEPMSK_XFRCM|USB_OTG_DIEPMSK_EPDM);
    	USB_OTG_HS_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
    	USB_OTG_HS_OUTEP(0)->DOEPTSIZ = (USB_OTG_DOEPTSIZ_STUPCNT|(1 << 19)|(3 * 8));
    	USB_OTG_HS->GINTSTS = 0xFFFFFFFF;
    	USB_OTG_HS->GINTMSK |= (USB_OTG_GINTMSK_ENUMDNEM|USB_OTG_GINTMSK_IEPINT|USB_OTG_GINTMSK_OEPINT|USB_OTG_GINTMSK_RXFLVLM);
    	usb_print_string("Reset\n\r", C_Pink);
    }
    if((USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK & USB_OTG_GINTSTS_ENUMDNE) == USB_OTG_GINTSTS_ENUMDNE)
    {
    	USB_OTG_HS_INEP(0)->DIEPCTL &= ~USB_OTG_DIEPCTL_MPSIZ;
    	USB_OTG_HS_DEVICE->DCTL |= USB_OTG_DCTL_CGINAK;
    	USB_OTG_HS->GUSBCFG &= ~USB_OTG_GUSBCFG_TRDT;
    	USB_OTG_HS->GUSBCFG |= (5 << 10);//TRDT value for FS
    	//USB_OTG_HS_DEVICE->DAINTMSK |= 0x00010001;
    	if ((USB_OTG_HS_OUTEP(0)->DOEPCTL & USB_OTG_DOEPCTL_USBAEP) == 0)
    	{
    		USB_OTG_HS_OUTEP(0)->DOEPCTL |= (64|/*USB_OTG_DIEPCTL_SD0PID_SEVNFRM|*/USB_OTG_DOEPCTL_USBAEP);
    	}
    	if ((USB_OTG_HS_INEP(0)->DIEPCTL & USB_OTG_DIEPCTL_USBAEP) == 0)
    	{
    		USB_OTG_HS_INEP(0)->DIEPCTL |= (64|/*USB_OTG_DIEPCTL_SD0PID_SEVNFRM|*/USB_OTG_DIEPCTL_USBAEP);
    	}
    	USB_OTG_HS->GINTSTS &= USB_OTG_GINTSTS_ENUMDNE;
    	dev_state = def;
    	usb_print_string("Enum done\n\r", C_ForestGreen);
    }
    if((USB_OTG_HS->GINTSTS & USB_OTG_HS->GINTMSK & USB_OTG_GINTSTS_RXFLVL) == USB_OTG_GINTSTS_RXFLVL)
    {
    	USB_OTG_HS->GINTMSK &= ~USB_OTG_GINTSTS_RXFLVL;
    	temp1 = USB_OTG_HS->GRXSTSP;
    	usb_print_string("Pkt stat: ", C_White);
    	temp3 = ((temp1 & USB_OTG_GRXSTSP_PKTSTS) >> 17);
    	switch(temp3)
    	{
    		case 1: usb_print_string("Global OUT NAK\r\n", C_White); break;
    		case 2: usb_print_string("OUT data packet received\n\r", C_White); break;
    		case 3: usb_print_string("OUT transfer completed\n\r", C_White); break;
    		case 4: usb_print_string("SETUP transaction completed,\n\r", C_White); break;
    		case 6: usb_print_string("SETUP data packet received\n\r", C_White); break;
    		default: usb_print_string("ERROR !!!\n\r", C_White); break;
    	}
    	if((temp1 & USB_OTG_GRXSTSP_BCNT) != 0)
    	{
    		temp2 = (((temp1 & USB_OTG_GRXSTSP_BCNT) + 0x30) >> 6);
    		while(temp2)
    		{
    			setup[bufcount] = USB_DFIFO(0);
    			usb_print_char('B', C_Lime);
    			bufcount ++;
    			temp2--;
    		}
    		usb_print_string("\n\r", C_Lime);
    	}
    	USB_OTG_HS->GINTMSK |= USB_OTG_GINTSTS_RXFLVL;
    }
#endif
}
void EXTI0_IRQHandler(void)
{
	EXTI->PR = 1;
#if _UART
	uart1_print_string("User button was pressed\n\r");
#else
	new_screen();
	//lcd_print_string("Buka loves Lu!", C_Green);
	lcd_print_string(irq_text, C_Olive);
#endif
}
void usb_print_char(char pc, colors color)
{
#if _UART
	uart1_print_char(pc);
#else
	lcd_print_char(pc, color);
#endif
}
void usb_print_string(uint8_t* string, colors color)
{
#if _UART
	uart1_print_string(string);
#else
	lcd_print_string(string, color);
#endif
}
void usb_init(void)
{
	USB_OTG_HS->GAHBCFG &= ~USB_OTG_GAHBCFG_GINT;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_AHBIDL) == 0){}
	USB_OTG_HS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_CSRST) == USB_OTG_GRSTCTL_CSRST){}
	USB_OTG_HS->GCCFG = (USB_OTG_GCCFG_PWRDWN|USB_OTG_GCCFG_NOVBUSSENS);
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_FDMOD;
	delay(30);
	USB_OTG_HS_PCGCCTL = 0;
	USB_OTG_HS_DEVICE->DCFG |= USB_OTG_DCFG_DSPD;
	//USB_OTG_HS->DCFG |= USB_OTG_DCFG_NZLSOHSK;
	USB_OTG_HS->GRSTCTL = (USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4);
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH) == USB_OTG_GRSTCTL_TXFFLSH){}
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH) == USB_OTG_GRSTCTL_RXFFLSH){}
	USB_OTG_HS->GRXFSIZ = 30;
	USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = 0x0010001e;
	USB_OTG_HS_DEVICE->DIEPMSK  = 0;
	USB_OTG_HS_DEVICE->DOEPMSK  = 0;
	USB_OTG_HS_DEVICE->DAINTMSK = 0;
	USB_OTG_HS_DEVICE->DIEPEMPMSK = 0;
	USB_OTG_HS_DEVICE->DAINT = 0xFFFFFFFF;
	for (uint8_t i = 0; i < 6; i++)
	{
		USB_OTG_HS_INEP(i)->DIEPCTL = 0;
		USB_OTG_HS_INEP(i)->DIEPTSIZ = 0;
		USB_OTG_HS_INEP(i)->DIEPINT = 0xFFFFFFFF;
		USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
		USB_OTG_HS_OUTEP(i)->DOEPTSIZ = 0;
		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
	}
	USB_OTG_HS->GINTSTS = 0xBFFFFFFF;
	USB_OTG_HS->GINTMSK = 0;
	USB_OTG_HS->GINTMSK = (USB_OTG_GINTMSK_USBRST|USB_OTG_GINTMSK_MMISM);
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
	NVIC_EnableIRQ(OTG_HS_IRQn);
}
void usb_init_dma(void)
{
	USB_OTG_HS->GAHBCFG &= ~USB_OTG_GAHBCFG_GINT;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_AHBIDL) == 0){}
	USB_OTG_HS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_CSRST) == USB_OTG_GRSTCTL_CSRST){}
	USB_OTG_HS->GCCFG = USB_OTG_GCCFG_PWRDWN|USB_OTG_GCCFG_NOVBUSSENS;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_FDMOD;
	delay(30);
	USB_OTG_HS_PCGCCTL = 0;
	USB_OTG_HS_DEVICE->DCFG |= USB_OTG_DCFG_DSPD|USB_OTG_DCFG_NZLSOHSK;
	USB_OTG_HS->GAHBCFG = USB_OTG_GAHBCFG_DMAEN|USB_OTG_GAHBCFG_HBSTLEN_0;
	USB_OTG_HS->GRXFSIZ = 0x20;//Set Rx FIFO size
	USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = 0x00100020;//Set Tx EP0 FIFO size
	//USB_OTG_HS->DIEPTXF[i] = some;//Set Tx EP FIFO sizes for all IN ep's
//Flush the FIFO's
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4;//All Tx FIFO's
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
	USB_OTG_HS_DEVICE->DIEPMSK  = 0;
	USB_OTG_HS_DEVICE->DOEPMSK  = 0;
	USB_OTG_HS_DEVICE->DAINTMSK = 0;
	USB_OTG_HS_DEVICE->DIEPEMPMSK = 0;
	for (uint8_t i = 0; i < 6; i++)//Disable all EP's, clear all EP ints.
	{
		USB_OTG_HS_INEP(i)->DIEPCTL = 0;
		USB_OTG_HS_INEP(i)->DIEPTSIZ = 0;
		USB_OTG_HS_INEP(i)->DIEPINT = 0xFFFFFFFF;
		USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
		USB_OTG_HS_OUTEP(i)->DOEPTSIZ = 0;
		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
	}
	USB_OTG_HS->GINTMSK |= USB_OTG_GINTMSK_USBRST;
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
	NVIC_EnableIRQ(OTG_HS_IRQn);
}
void setup_ep0(void)
{
	USB_OTG_HS_OUTEP(0)->DOEPTSIZ = USB_OTG_DOEPTSIZ_STUPCNT;//3
	USB_OTG_HS_OUTEP(0)->DOEPDMA = (uint32_t)&setup[0];
	USB_OTG_HS_OUTEP(0)->DOEPCTL = USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
}
void data_in(uint8_t size, uint32_t tx_data_address)
{
	USB_OTG_HS_INEP(0)->DIEPDMA = tx_data_address;
	USB_OTG_HS_INEP(0)->DIEPTSIZ = (1 << 19)|size;//PKTCNT = 1
	USB_OTG_HS_INEP(0)->DIEPCTL = USB_OTG_DIEPCTL_EPENA|USB_OTG_DIEPCTL_CNAK|64;
}
void status_out(void)
{
	USB_OTG_HS_OUTEP(0)->DOEPTSIZ /*&= USB_OTG_DOEPTSIZ_STUPCNT;
	USB_OTG_HS_OUTEP(0)->DOEPTSIZ |*/= (1 << 19)|64|USB_OTG_DOEPTSIZ_STUPCNT;
	USB_OTG_HS_OUTEP(0)->DOEPDMA = (uint32_t)&rx_buf[0];
	USB_OTG_HS_OUTEP(0)->DOEPCTL |= USB_OTG_DOEPCTL_CNAK|USB_OTG_DOEPCTL_EPENA;
}
void usb_init_dma_old(void)
{
	USB_OTG_HS->GAHBCFG &= ~USB_OTG_GAHBCFG_GINT;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
	while ((USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_AHBIDL) == 0){};
	USB_OTG_HS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_CSRST){};
	USB_OTG_HS->GCCFG = USB_OTG_GCCFG_PWRDWN|USB_OTG_GCCFG_NOVBUSSENS;
	USB_OTG_HS->GUSBCFG |= USB_OTG_GUSBCFG_FDMOD;
	delay(30);
	USB_OTG_HS_PCGCCTL = 0;
	USB_OTG_HS_DEVICE->DCFG |= USB_OTG_DCFG_DSPD|USB_OTG_DCFG_NZLSOHSK;
	USB_OTG_HS->GAHBCFG = USB_OTG_GAHBCFG_DMAEN|USB_OTG_GAHBCFG_HBSTLEN_0;
	USB_OTG_HS->GRXFSIZ = 0x20;//Set Rx FIFO size
	USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = 0x00100020;//Set Tx EP0 FIFO size
	//USB_OTG_HS->DIEPTXF[i] = some;//Set Tx EP FIFO sizes for all IN ep's
//Flush the FIFO's
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_TXFFLSH|USB_OTG_GRSTCTL_TXFNUM_4;//All Tx FIFO's
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_TXFFLSH){};
	USB_OTG_HS->GRSTCTL = USB_OTG_GRSTCTL_RXFFLSH;//The Rx FIFO
	while (USB_OTG_HS->GRSTCTL & USB_OTG_GRSTCTL_RXFFLSH){};
	USB_OTG_HS_DEVICE->DIEPMSK  = 0;
	USB_OTG_HS_DEVICE->DOEPMSK  = 0;
	USB_OTG_HS_DEVICE->DAINTMSK = 0;
	USB_OTG_HS_DEVICE->DIEPEMPMSK = 0;
	for (uint8_t i = 0; i < 6; i++)//Disable all EP's, clear all EP ints.
	{
		USB_OTG_HS_INEP(i)->DIEPCTL = 0;
		USB_OTG_HS_INEP(i)->DIEPTSIZ = 0;
		USB_OTG_HS_INEP(i)->DIEPINT = 0xFFFFFFFF;
		USB_OTG_HS_OUTEP(i)->DOEPCTL = 0;
		USB_OTG_HS_OUTEP(i)->DOEPTSIZ = 0;
		USB_OTG_HS_OUTEP(i)->DOEPINT = 0xFFFFFFFF;
	}
	USB_OTG_HS->GINTMSK |= USB_OTG_GINTMSK_USBRST;
	USB_OTG_HS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
	NVIC_ClearPendingIRQ(OTG_HS_IRQn);
	NVIC_EnableIRQ(OTG_HS_IRQn);
}