#ifndef __LWIPOPTS_H__
#define __LWIPOPTS_H__
//
#define LWIP_NOASSERT
#define NO_SYS							1
#define LWIP_TIMERS						0
#define SYS_LIGHTWEIGHT_PROT			0
#define MEM_LIBC_MALLOC					0
#define MEMP_MEM_MALLOC					0
#define MEMP_MEM_INIT					1
#define MEM_ALIGNMENT					4
#define LWIP_NETCONN 					0
#define LWIP_SOCKET 					0
#define MEM_SIZE 						(32 * 1024)
#define MEMP_OVERFLOW_CHECK				0
#define MEMP_SANITY_CHECK				0
#define MEM_USE_POOLS					0
#define MEM_USE_POOLS_TRY_BIGGER_POOL   0
#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT	0
#define MEMP_NUM_PBUF 					30
#define LWIP_TCP 						1
#define MEMP_NUM_TCP_PCB 				1
#define MEMP_NUM_TCP_PCB_LISTEN 		1
#define TCP_SND_QUEUELEN   				((4 * (TCP_SND_BUF) + (TCP_MSS - 1))/(TCP_MSS))
#define MEMP_NUM_REASSDATA				5
#define MEMP_NUM_FRAG_PBUF   			15
/*the number of IP fragments simultaneously sent (fragments, not whole packets!).
This is only used with LWIP_NETIF_TX_SINGLE_PBUF==0 and only has to be > 1 with DMA-enabled MACs
where the packet is not yet sent when netif->output returns*/
#define MEMP_NUM_SYS_TIMEOUT 			LWIP_NUM_SYS_TIMEOUT_INTERNAL
#define LWIP_NUM_SYS_TIMEOUT_INTERNAL 	(LWIP_TCP + IP_REASSEMBLY + LWIP_ARP + (2*LWIP_DHCP) + LWIP_AUTOIP + LWIP_IGMP + LWIP_DNS + PPP_NUM_TIMEOUTS + (LWIP_IPV6 * (1 + LWIP_IPV6_REASS + LWIP_IPV6_MLD)))
#define PBUF_POOL_SIZE					16
#define TCP_TTL   						IP_DEFAULT_TTL
#define TCP_QUEUE_OOSEQ 				1
#define TCP_MSS 						1460/*(1500 - 40) (Ethernet MTU - IP header size - TCP header size) */
#define MEMP_NUM_TCPIP_MSG_API  		8
#define MEMP_NUM_TCPIP_MSG_INPKT   		8
#define MEMP_NUM_API_MSG   				MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_NETIFAPI_MSG   		MEMP_NUM_TCPIP_MSG_API
#define LWIP_MIB2_CALLBACKS   			0
#define CHECKSUM_GEN_IP					1/*by software*/
#define CHECKSUM_GEN_TCP				1/*by software*/
#define CHECKSUM_CHECK_IP				1/*by software*/
#define CHECKSUM_CHECK_TCP				1/*by software*/
#define LWIP_CHECKSUM_ON_COPY			0
#define TCP_SND_BUF   					(2 * TCP_MSS)
#define MEMP_NUM_TCP_SEG   				16/*TCP_SND_QUEUELEN*/
#define TCP_SNDQUEUELOWAT   			LWIP_MAX(((TCP_SND_QUEUELEN)/2), 5)
#define TCP_WND 				 		(4 * TCP_MSS)
#define LWIP_NETBUF_RECVINFO   			1
#define TCP_MAXRTX   					12
#define TCP_SYNMAXRTX   				6
#define LWIP_TCP_SACK_OUT   			1
#define LWIP_TCP_MAX_SACK_NUM   		4
#define TCP_CALCULATE_EFF_SEND_MSS   	1
#define TCP_LISTEN_BACKLOG				0
#define TCP_SNDLOWAT   					LWIP_MIN(LWIP_MAX(((TCP_SND_BUF)/2), (2 * TCP_MSS) + 1), (TCP_SND_BUF) - 1)
#define TCP_OOSEQ_MAX_BYTES   			0
#define TCP_OOSEQ_MAX_PBUFS				0
#define TCP_DEFAULT_LISTEN_BACKLOG   	0xff
#define TCP_OVERSIZE   					TCP_MSS
#define TCP_WND_UPDATE_THRESHOLD   		LWIP_MIN((TCP_WND / 4), (TCP_MSS * 4))
//
#endif /* __LWIPOPTS_H__ */
