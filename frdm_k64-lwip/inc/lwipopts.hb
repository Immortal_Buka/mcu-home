#ifndef __LWIPOPTS_H__
#define __LWIPOPTS_H__
//
#include <stdio.h>
//
#define NO_SYS						1
#define LWIP_NETCONN 				0
#define LWIP_SOCKET 				0
#ifndef MEM_ALIGNMENT
	#define MEM_ALIGNMENT 			4
#endif
#ifndef MEM_SIZE
	#define MEM_SIZE 				(50 * 1024)
#endif
#ifndef MEMP_NUM_PBUF
	#define MEMP_NUM_PBUF 			30
#endif
#ifndef MEMP_NUM_UDP_PCB
	#define MEMP_NUM_UDP_PCB 		0
#endif
#ifndef MEMP_NUM_TCP_PCB
	#define MEMP_NUM_TCP_PCB 		1
#endif
#ifndef MEMP_NUM_TCP_PCB_LISTEN
	#define MEMP_NUM_TCP_PCB_LISTEN 1
#endif
#ifndef MEMP_NUM_SYS_TIMEOUT
	#define MEMP_NUM_SYS_TIMEOUT 	10
#endif
#ifndef PBUF_POOL_SIZE
	#define PBUF_POOL_SIZE 			9
#endif
#ifndef LWIP_TCP
	#define LWIP_TCP 				1
#endif
#ifndef TCP_TTL
	#define TCP_TTL 				255
#endif
#ifndef TCP_QUEUE_OOSEQ
	#define TCP_QUEUE_OOSEQ 		0
#endif
#ifndef TCP_MSS
	#define TCP_MSS 				1460/*(1500 - 40)*/ /* TCP_MSS = (Ethernet MTU - IP header size - TCP header size) */
#endif
#ifndef TCP_SND_BUF
	#define TCP_SND_BUF 			(10 * TCP_MSS) // 2
#endif
#ifndef TCP_SND_QUEUELEN
	#define TCP_SND_QUEUELEN 		(3 * TCP_SND_BUF) / TCP_MSS // 6
#endif
#ifndef MEMP_NUM_TCP_SEG
	#define MEMP_NUM_TCP_SEG 		TCP_SND_QUEUELEN
#endif
#ifndef TCP_WND
	#define TCP_WND 				(8 * TCP_MSS)
#endif
#ifndef TCP_LISTEN_BACKLOG
	#define TCP_LISTEN_BACKLOG 		0
#endif
#ifndef LWIP_ICMP
	#define LWIP_ICMP 				0
#endif
#ifndef LWIP_DHCP
	#define LWIP_DHCP 				0
#endif
#ifndef LWIP_UDP
	#define LWIP_UDP 				0
#endif
#ifndef UDP_TTL
	#define UDP_TTL 				255
#endif
#ifndef LWIP_STATS
	#define LWIP_STATS 				0
#endif
#ifndef LWIP_PROVIDE_ERRNO
	#define LWIP_PROVIDE_ERRNO 		1
#endif
#define CHECKSUM_BY_HARDWARE
#define CHECKSUM_GEN_IP             0
#define CHECKSUM_GEN_UDP            0
#define CHECKSUM_GEN_TCP            0
#define CHECKSUM_CHECK_IP           0
#define CHECKSUM_CHECK_UDP          0
#define CHECKSUM_CHECK_TCP			0
#ifndef DEFAULT_THREAD_STACKSIZE
	#define DEFAULT_THREAD_STACKSIZE 3000
#endif
#ifndef DEFAULT_THREAD_PRIO
	#define DEFAULT_THREAD_PRIO 	3
#endif
//#define LWIP_DEBUG					1
#ifdef LWIP_DEBUG
	#define U8_F "c"
	#define S8_F "c"
	#define X8_F "02x"
	#define U16_F "u"
	#define S16_F "d"
	#define X16_F "x"
	#define U32_F "u"
	#define S32_F "d"
	#define X32_F "x"
	#define SZT_F "u"
#endif
#define TCPIP_MBOX_SIZE 			32
#define TCPIP_THREAD_STACKSIZE 		1024
#define TCPIP_THREAD_PRIO 			8
#define DEFAULT_RAW_RECVMBOX_SIZE 	12
#define DEFAULT_UDP_RECVMBOX_SIZE 	12
#define DEFAULT_TCP_RECVMBOX_SIZE 	12
#define DEFAULT_ACCEPTMBOX_SIZE 	12
/*#define PRINTF 						printf
#define TCP_DEBUG                   LWIP_DBG_ON
#define ETHARP_DEBUG                LWIP_DBG_ON
#define PBUF_DEBUG                  LWIP_DBG_ON
#define IP_DEBUG                    LWIP_DBG_ON
#define TCPIP_DEBUG                 LWIP_DBG_ON
#define DHCP_DEBUG                  LWIP_DBG_ON
#define UDP_DEBUG                   LWIP_DBG_ON*/
//
#endif /* __LWIPOPTS_H__ */
