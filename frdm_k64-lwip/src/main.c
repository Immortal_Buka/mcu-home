#include "system.h"
#include "print.h"
#include "my_lwip.h"
#include "ksz8081.h"
#include <string.h>
#include "lwip/snmp.h"
//
#define ENET_RXBD_NUM 								(5)
#define ENET_TXBD_NUM 								(3)
#define ENET_FRAME_MAX_FRAMELEN 					1518
#define SDK_SIZEALIGN(var, alignbytes) 				((uint32_t)((var) + ((alignbytes)-1)) & (uint32_t)(~(uint32_t)((alignbytes)-1)))
#define ENET_TIMEOUT        						(0xFFFU)
#define PHY_TIMEOUT_COUNT 							100000
#define kStatus_ENET_TxFrameBusy					4004
#define kStatus_ENET_TxFrameOverLen					4003
#define kStatus_ENET_RxFrameError					4000
#define kStatus_ENET_RxFrameEmpty					4002
#define PKT_SIZE									TCP_MSS
#define ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK			0x8000		/*!< Empty bit mask. */
#define ENET_BUFFDESCRIPTOR_RX_SOFTOWNER1_MASK		0x4000		/*!< Software owner one mask. */
#define ENET_BUFFDESCRIPTOR_RX_WRAP_MASK			0x2000		/*!< Next buffer descriptor is the start address. */
#define ENET_BUFFDESCRIPTOR_RX_SOFTOWNER2_Mask		0x1000		/*!< Software owner two mask. */
#define ENET_BUFFDESCRIPTOR_RX_LAST_MASK			0x0800		/*!< Last BD of the frame mask. */
#define ENET_BUFFDESCRIPTOR_RX_MISS_MASK			0x0100		/*!< Received because of the promiscuous mode. */
#define ENET_BUFFDESCRIPTOR_RX_BROADCAST_MASK		0x0080		/*!< Broadcast packet mask. */
#define ENET_BUFFDESCRIPTOR_RX_MULTICAST_MASK		0x0040		/*!< Multicast packet mask. */
#define ENET_BUFFDESCRIPTOR_RX_LENVLIOLATE_MASK		0x0020		/*!< Length violation mask. */
#define ENET_BUFFDESCRIPTOR_RX_NOOCTET_MASK			0x0010		/*!< Non-octet aligned frame mask. */
#define ENET_BUFFDESCRIPTOR_RX_CRC_MASK				0x0004		/*!< CRC error mask. */
#define ENET_BUFFDESCRIPTOR_RX_OVERRUN_MASK			0x0002		/*!< FIFO overrun mask. */
#define ENET_BUFFDESCRIPTOR_RX_TRUNC_MASK			0x0001		/*!< Frame is truncated mask. */
#define ENET_BUFFDESCRIPTOR_TX_READY_MASK			0x8000		/*!< Ready bit mask. */
#define ENET_BUFFDESCRIPTOR_TX_SOFTOWENER1_MASK		0x4000		/*!< Software owner one mask. */
#define ENET_BUFFDESCRIPTOR_TX_WRAP_MASK			0x2000		/*!< Wrap buffer descriptor mask. */
#define ENET_BUFFDESCRIPTOR_TX_SOFTOWENER2_MASK		0x1000		/*!< Software owner two mask. */
#define ENET_BUFFDESCRIPTOR_TX_LAST_MASK			0x0800		/*!< Last BD of the frame mask. */
#define ENET_BUFFDESCRIPTOR_TX_TRANMITCRC_MASK		0x0400		/*!< Transmit CRC mask. */
#define ENET_BUFFDESCRIPTOR_RX_ERR_MASK				(ENET_BUFFDESCRIPTOR_RX_TRUNC_MASK|ENET_BUFFDESCRIPTOR_RX_OVERRUN_MASK|ENET_BUFFDESCRIPTOR_RX_LENVLIOLATE_MASK|ENET_BUFFDESCRIPTOR_RX_NOOCTET_MASK|ENET_BUFFDESCRIPTOR_RX_CRC_MASK)
//
//
typedef uint8_t rx_buffer_t[SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, 16)];
typedef uint8_t tx_buffer_t[SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, 16)];
typedef struct
{
    uint16_t length;  /*!< Buffer descriptor data length. */
    uint16_t control; /*!< Buffer descriptor control and status. */
    uint8_t *buffer;  /*!< Data buffer pointer. */
} enet_rx_bd_struct_t;
typedef struct
{
    uint16_t length;  /*!< Buffer descriptor data length. */
    uint16_t control; /*!< Buffer descriptor control and status. */
    uint8_t *buffer;  /*!< Data buffer pointer. */
} enet_tx_bd_struct_t;
typedef struct
{
    uint16_t rxBdNumber;      /*!< Receive buffer descriptor number. */
    uint16_t txBdNumber;      /*!< Transmit buffer descriptor number. */
    uint32_t rxBuffSizeAlign; /*!< Aligned receive data buffer size. */
    uint32_t txBuffSizeAlign; /*!< Aligned transmit data buffer size. */
    volatile enet_rx_bd_struct_t *rxBdStartAddrAlign; /*!< Aligned receive buffer descriptor start address: should be non-cacheable. */
    volatile enet_tx_bd_struct_t *txBdStartAddrAlign; /*!< Aligned transmit buffer descriptor start address: should be non-cacheable. */
    uint8_t *rxBufferAlign;  /*!< Receive data buffer start address. */
    uint8_t *txBufferAlign;  /*!< Transmit data buffer start address. */
} enet_buffer_config_t;
typedef enum
{
	speed_unknown,
    speed_10M,
	speed_100M
} enet_speed_t;
typedef enum
{
	duplex_unknown,
	duplex_half,
	duplex_full
} enet_duplex_t;
typedef struct
{
	enet_speed_t speed;
	enet_duplex_t duplex;
	uint8_t* hw_addr;
} conn_prop_t;
typedef struct
{
    enet_rx_bd_struct_t *RxBuffDescrip;
    enet_tx_bd_struct_t *TxBuffDescrip;
    rx_buffer_t *RxDataBuff;
    tx_buffer_t *TxDataBuff;
} ethernetif_t;
//
void main(void);
void SysTick_Handler(void);
static err_t enet_send_frame(unsigned char *data, const uint32_t length);
static uint8_t* enet_get_tx_buffer(void);
uint32_t PHY_Read(uint32_t phyAddr, uint32_t phyReg, uint16_t* dataPtr);
uint32_t PHY_Write(uint32_t phyAddr, uint32_t phyReg, uint16_t data);
uint32_t ENET_SendFrame(const uint8_t *data, uint32_t length);
void low_level_init(struct netif* netif, uint8_t* hw_addr);
void low_level_error(uint8_t* string);
uint32_t PHY_GetLinkStatus(uint32_t* status);
uint32_t PHY_GetLinkSpeedDuplex(conn_prop_t* conn);
void ENET_SetMacController(conn_prop_t* conn);
struct pbuf* low_level_input(void);
uint32_t ENET_GetRxFrameSize(uint32_t *length);
uint32_t ENET_ReadFrame(uint8_t *data, uint32_t length);
void ENET_UpdateReadBuffers(void);
err_t low_level_output(struct netif *netif, struct pbuf *p);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x04,
	.day = 0x25,
	.month = 0x07,
	.year = 0x19,
};
enet_rx_bd_struct_t g_rxBuffDescrip[ENET_RXBD_NUM] __attribute__((aligned(ENET_BUFF_ALIGNMENT))) = {0};
enet_tx_bd_struct_t g_txBuffDescrip[ENET_TXBD_NUM] __attribute__((aligned(ENET_BUFF_ALIGNMENT))) = {0};
uint8_t g_rxDataBuff[ENET_RXBD_NUM][1520] __attribute__((aligned(ENET_BUFF_ALIGNMENT))) = {0};
uint8_t g_txDataBuff[ENET_TXBD_NUM][1520] __attribute__((aligned(ENET_BUFF_ALIGNMENT))) = {0};
enet_rx_bd_struct_t *rxBdCurrent = g_rxBuffDescrip;
enet_tx_bd_struct_t *txBdCurrent = g_txBuffDescrip;
ethernetif_t enetif_inst;
conn_prop_t conn =
{
	.speed = speed_unknown,
	.duplex = duplex_unknown,
};
uint8_t pkt[PKT_SIZE];
uint8_t test_pkt[] = "test";
//
void main(void)
{
	time_ms = 0;
	SysTick_Config(120000);//1ms
	NVIC_EnableIRQ(SysTick_IRQn);
	for(uint16_t i=0; i<PKT_SIZE; i++) pkt[i] = i&0xff;
	init_common_gpio();
	frdm_led_rgb(all, off);
	sda_uart_init();
	uart_print_string(UART0, "\r\nuart init done\r\nbuild date: ");
	uart_print_string(UART0, __DATE__);
	rnga_init();
	mpu_disabled();
	//enet
	SIM->SCGC2 |= SIM_SCGC2_ENET_MASK;
	my_lwip_init();
	frdm_led_rgb(green, on);
	while(1)
	{
		my_lwip_loop();
	}
}
void SysTick_Handler(void)
{
	time_ms++;
	if(tcp_pcb_inst->state == ESTABLISHED)
	{
		tcp_write(tcp_pcb_inst, &pkt, PKT_SIZE, 0);
		tcp_output(tcp_pcb_inst);
	}
}
static err_t enet_send_frame(unsigned char *data, const uint32_t length)
{
	uint32_t counter;
	for (counter = ENET_TIMEOUT; counter != 0U; counter--)
	{
		if (ENET_SendFrame(data, length) != kStatus_ENET_TxFrameBusy) return ERR_OK;
    }
	return ERR_TIMEOUT;
}
err_t low_level_output(struct netif *netif, struct pbuf *p)
{
    err_t result;
    struct pbuf *q;
    unsigned char *pucBuffer;
    unsigned char *pucChar;
    LWIP_ASSERT("\r\nOutput packet buffer empty", p);
    pucBuffer = enet_get_tx_buffer();
    if (pucBuffer == NULL) return ERR_BUF;
#if ETH_PAD_SIZE
    pbuf_header(p, -ETH_PAD_SIZE);
#endif
    if (p->len == p->tot_len) pucBuffer = (unsigned char *)p->payload;
    else
    {
        if (p->tot_len > ENET_FRAME_MAX_FRAMELEN) return ERR_BUF;
        else
        {
            pucChar = pucBuffer;
            for (q = p; q != NULL; q = q->next)
            {
                memcpy(pucChar, q->payload, q->len);
                pucChar += q->len;
            }
        }
    }
    result = enet_send_frame(pucBuffer, p->tot_len);
    MIB2_STATS_NETIF_ADD(netif, ifoutoctets, p->tot_len);
    if (((u8_t *)p->payload)[0] & 1) MIB2_STATS_NETIF_INC(netif, ifoutnucastpkts);
    else MIB2_STATS_NETIF_INC(netif, ifoutucastpkts);
#if ETH_PAD_SIZE
    pbuf_header(p, ETH_PAD_SIZE);
#endif
    LINK_STATS_INC(link.xmit);
    return result;
}
static uint8_t* enet_get_tx_buffer(void)
{
    static unsigned char ucBuffer[ENET_FRAME_MAX_FRAMELEN];
    return ucBuffer;
}
uint32_t PHY_Read(uint32_t phyAddr, uint32_t phyReg, uint16_t* dataPtr)
{
    ENET->EIR = ENET_EIR_MII_MASK;
    ENET->MMFR = ENET_MMFR_ST(1)|ENET_MMFR_OP(2)|ENET_MMFR_PA(phyAddr)|ENET_MMFR_RA(phyReg)|ENET_MMFR_TA(2);
    for(uint32_t counter = PHY_TIMEOUT_COUNT; counter > 0; counter--)
    {
        if(ENET->EIR & ENET_EIR_MII_MASK) break;
        if(counter < 10) return 1;
    }
    *dataPtr = ((ENET->MMFR & ENET_MMFR_DATA_MASK) >> ENET_MMFR_DATA_SHIFT);
    ENET->EIR = ENET_EIR_MII_MASK;
    return 0;
}
uint32_t PHY_Write(uint32_t phyAddr, uint32_t phyReg, uint16_t data)
{
	ENET->EIR = ENET_EIR_MII_MASK;
    ENET->MMFR = ENET_MMFR_ST(1)|ENET_MMFR_OP(1)|ENET_MMFR_PA(phyAddr)|ENET_MMFR_RA(phyReg)|ENET_MMFR_TA(2)|(data & 0xFFFF);
    for(uint32_t counter = PHY_TIMEOUT_COUNT; counter > 0; counter--)
    {
        if(ENET->EIR & ENET_EIR_MII_MASK) break;
        if(counter < 10) return 1;
    }
    ENET->EIR = ENET_EIR_MII_MASK;
    return 0;
}
uint32_t ENET_SendFrame(const uint8_t *data, uint32_t length)
{
    volatile enet_tx_bd_struct_t *curBuffDescrip;
    uint32_t len = 0;
    uint32_t sizeleft = 0;
    uint32_t address;
    if (length > ENET_FRAME_MAX_FRAMELEN) return kStatus_ENET_TxFrameOverLen;
    curBuffDescrip = txBdCurrent;
    if (curBuffDescrip->control & ENET_BUFFDESCRIPTOR_TX_READY_MASK) return kStatus_ENET_TxFrameBusy;
    if (1520 >= length)
    {
        address = (uint32_t)curBuffDescrip->buffer;
        memcpy((void *)address, data, length);
        curBuffDescrip->length = length;
        curBuffDescrip->control |= (ENET_BUFFDESCRIPTOR_TX_READY_MASK | ENET_BUFFDESCRIPTOR_TX_LAST_MASK);
        if (curBuffDescrip->control & ENET_BUFFDESCRIPTOR_TX_WRAP_MASK) txBdCurrent = g_txBuffDescrip;
        else txBdCurrent++;
        ENET->TDAR = ENET_TDAR_TDAR_MASK;
        return 0;
    }
    else
    {
        do
        {
            if (curBuffDescrip->control & ENET_BUFFDESCRIPTOR_TX_WRAP_MASK) txBdCurrent = g_txBuffDescrip;
            else txBdCurrent++;
            sizeleft = length - len;
            address = (uint32_t)curBuffDescrip->buffer;
            if (sizeleft > 1520)
            {
                memcpy((void *)address, data + len, 1520);
                curBuffDescrip->length = 1520;
                len += 1520;
                curBuffDescrip->control &= ~ENET_BUFFDESCRIPTOR_TX_LAST_MASK;
                curBuffDescrip->control |= ENET_BUFFDESCRIPTOR_TX_READY_MASK;
                ENET->TDAR = ENET_TDAR_TDAR_MASK;
            }
            else
            {
                memcpy((void *)address, data + len, sizeleft);
                curBuffDescrip->length = sizeleft;
                curBuffDescrip->control |= ENET_BUFFDESCRIPTOR_TX_READY_MASK | ENET_BUFFDESCRIPTOR_TX_LAST_MASK;
                ENET->TDAR = ENET_TDAR_TDAR_MASK;
                return 0;
            }
            curBuffDescrip = txBdCurrent;

        } while (!(curBuffDescrip->control & ENET_BUFFDESCRIPTOR_TX_READY_MASK));
        return kStatus_ENET_TxFrameBusy;
    }
}
void low_level_init(struct netif* netif, uint8_t* hw_addr)
{
	uint32_t link_status = 0;
	uint16_t temp_loc[2];
	enetif_inst.RxBuffDescrip = g_rxBuffDescrip;
	enetif_inst.TxBuffDescrip = g_txBuffDescrip;
	enetif_inst.RxDataBuff = g_rxDataBuff;
	enetif_inst.TxDataBuff = g_txDataBuff;
    netif->state = &enetif_inst;
	SIM->SCGC2 |= SIM_SCGC2_ENET_MASK;
	ENET->MSCR = ENET_MSCR_MII_SPEED(24)|ENET_MSCR_HOLDTIME(3);
	//phy check
	PHY_Read(0, PHY_ID1_REG, temp_loc);
	if(temp_loc[0] != 0x0022) low_level_error("\r\nKSZ8081 not exist");
	//phy reset
	if(PHY_Write(0, PHY_BASICCONTROL_REG, PHY_BCTL_RESET_MASK)) low_level_error("\r\nKSZ8081 reset write error");
	//Set the negotiation
	if(PHY_Write(0, PHY_AUTONEG_ADVERTISE_REG,
		(PHY_100BASETX_FULLDUPLEX_MASK|PHY_100BASETX_HALFDUPLEX_MASK|PHY_10BASETX_FULLDUPLEX_MASK|PHY_10BASETX_HALFDUPLEX_MASK|1)))
		low_level_error("\r\nKSZ8081 set the negotiation write error");
	if(PHY_Write(0, PHY_BASICCONTROL_REG, (PHY_BCTL_AUTONEG_MASK | PHY_BCTL_RESTART_AUTONEG_MASK)))
		low_level_error("\r\nKSZ8081 set the negotiation write error");
	//Check auto negotiation complete
	while(1)
	{
		PHY_Read(0, PHY_BASICSTATUS_REG, temp_loc);
		PHY_Read(0, PHY_CONTROL1_REG, &temp_loc[1]);
		if(((temp_loc[0] & PHY_BSTATUS_AUTONEGCOMP_MASK) != 0) && (temp_loc[1] & PHY_LINK_READY_MASK)) break;
		delay(1000);
	}
	uart_print_string(UART0, "\r\nauto negotiation completed");
	PHY_GetLinkStatus(&link_status);
	if(link_status)
	{
		PHY_GetLinkSpeedDuplex(&conn);
		uart_print_string(UART0, "\r\nspeed: ");
		switch(conn.speed)
		{
			case speed_10M: uart_print_string(UART0, "10M"); break;
			case speed_100M: uart_print_string(UART0, "100M"); break;
			default: uart_print_string(UART0, "unknown"); break;
		}
		uart_print_string(UART0, "\r\nduplex: ");
		switch(conn.duplex)
		{
			case duplex_full: uart_print_string(UART0, "full"); break;
			case duplex_half: uart_print_string(UART0, "half"); break;
			default: uart_print_string(UART0, "unknown"); break;
		}
	}
	else low_level_error("\r\nlink down");
	//entt reset
	ENET->ECR |= ENET_ECR_RESET_MASK;
	//tx descriptors
	for(uint8_t i = 0; i < ENET_TXBD_NUM; i++)
	{
		g_txBuffDescrip[i].buffer = g_txDataBuff[i];
		g_txBuffDescrip[i].length = 0;
		g_txBuffDescrip[i].control = ENET_BUFFDESCRIPTOR_TX_TRANMITCRC_MASK;
		if(i == (ENET_TXBD_NUM - 1)) g_txBuffDescrip[i].control |= ENET_BUFFDESCRIPTOR_TX_WRAP_MASK;
	}
	//rx descriptors
	for(uint8_t i = 0; i < ENET_RXBD_NUM; i++)
	{
		g_rxBuffDescrip[i].buffer = g_rxDataBuff[i];
		g_rxBuffDescrip[i].length = 0;
		g_rxBuffDescrip[i].control = ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK;
		if(i == (ENET_RXBD_NUM - 1)) g_rxBuffDescrip[i].control |= ENET_BUFFDESCRIPTOR_RX_WRAP_MASK;
	}
	conn.hw_addr = hw_addr;
	ENET_SetMacController(&conn);
	ENET->RDAR = ENET_RDAR_RDAR_MASK;
}
void low_level_error(uint8_t* string)
{
	frdm_led_rgb(blue, on);
	uart_print_string(UART0, string);
	while(1){};
}
uint32_t PHY_GetLinkStatus(uint32_t* status)
{
	uint16_t data;
	if(PHY_Read(0, PHY_BASICSTATUS_REG, &data) == 0)
	{
		if (!(PHY_BSTATUS_LINKSTATUS_MASK & data)) *status = 0;
		else *status = 1;
		return 0;
	}
	else return 1;
}
uint32_t PHY_GetLinkSpeedDuplex(conn_prop_t* conn)
{
    uint16_t temp_loc;
    if(PHY_Read(0, PHY_CONTROL1_REG, &temp_loc)) return 1;
    else
    {
    	temp_loc &= PHY_CTL1_SPEEDUPLX_MASK;
        if((PHY_CTL1_10FULLDUPLEX_MASK == temp_loc)||(PHY_CTL1_100FULLDUPLEX_MASK == temp_loc)) conn->duplex = duplex_full;
        else conn->duplex = duplex_half;
        if((PHY_CTL1_100HALFDUPLEX_MASK == temp_loc)||(PHY_CTL1_100FULLDUPLEX_MASK == temp_loc)) conn->speed = speed_100M;
        else conn->speed = speed_10M;
        return 0;
    }
}
void ENET_SetMacController(conn_prop_t* conn)
{
    uint32_t temp_reg;
    uint32_t ecr = ENET->ECR;
    temp_reg = ENET_RCR_MAX_FL(ENET_FRAME_MAX_FRAMELEN)|ENET_RCR_CRCFWD_MASK|ENET_RCR_MII_MODE_MASK|ENET_RCR_RMII_MODE_MASK;
    if(conn->speed == speed_10M) temp_reg |= ENET_RCR_RMII_10T_MASK;
    if(conn->duplex == duplex_half) temp_reg |= ENET_RCR_DRT_MASK;
    ENET->RCR = temp_reg;
    temp_reg = ENET->TCR & ~(ENET_TCR_FDEN_MASK|ENET_TCR_ADDINS_MASK);
    if(conn->duplex == duplex_full) temp_reg |= ENET_TCR_FDEN_MASK;
    ENET->TCR = temp_reg;
    ENET->TACC = 0;
    ENET->RACC = 0;
    ENET->TFWR = ENET_TFWR_STRFWD_MASK;
    ENET->RSFL = 0;
    ENET->TDSR = (uint32_t)g_txBuffDescrip;
    ENET->RDSR = (uint32_t)g_rxBuffDescrip;
    ENET->MRBR = 1520;
    ENET->PALR = (conn->hw_addr[0] << 24)|(conn->hw_addr[1] << 16)|(conn->hw_addr[2] << 8)|conn->hw_addr[3];
    ENET->PAUR = ((conn->hw_addr[4] << 8)|(conn->hw_addr[5])) << ENET_PAUR_PADDR2_SHIFT;
    ENET->MSCR = ENET_MSCR_MII_SPEED(24)|ENET_MSCR_HOLDTIME(3);
    ecr |= ENET_ECR_ETHEREN_MASK|ENET_ECR_DBSWP_MASK;
    ENET->ECR = ecr;
}
struct pbuf* low_level_input(void)
{
    struct pbuf *p = NULL;
    struct pbuf *q;
    uint32_t len;
    uint32_t status;
    status = ENET_GetRxFrameSize(&len);
    if (kStatus_ENET_RxFrameEmpty != status)
    {
        if (len != 0)
        {
#if ETH_PAD_SIZE
            len += ETH_PAD_SIZE;
#endif
            p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
            if (p != NULL)
            {
#if ETH_PAD_SIZE
                pbuf_header(p, -ETH_PAD_SIZE);
#endif
                if (p->next == 0) ENET_ReadFrame(p->payload, p->len);
                else
                {
                    uint8_t data_tmp[ENET_FRAME_MAX_FRAMELEN];
                    uint32_t data_tmp_len = 0;
					ENET_ReadFrame(data_tmp, p->tot_len);
                    for (q = p; (q != NULL) && ((data_tmp_len + q->len) <= sizeof(data_tmp)); q = q->next)
                    {
                        memcpy(q->payload,  &data_tmp[data_tmp_len], q->len);
                        data_tmp_len += q->len;
                    }
                }
                MIB2_STATS_NETIF_ADD(netif, ifinoctets, p->tot_len);
                if (((u8_t *)p->payload)[0] & 1) MIB2_STATS_NETIF_INC(netif, ifinnucastpkts);
                else MIB2_STATS_NETIF_INC(netif, ifinucastpkts);
#if ETH_PAD_SIZE
                pbuf_header(p, ETH_PAD_SIZE);
#endif
                LINK_STATS_INC(link.recv);
            }
            else
            {
				ENET_ReadFrame(0, 0);
                LWIP_DEBUGF(NETIF_DEBUG, ("\r\nethernetif_linkinput: Fail to allocate new memory space"));
                LINK_STATS_INC(link.memerr);
                LINK_STATS_INC(link.drop);
                MIB2_STATS_NETIF_INC(netif, ifindiscards);
            }
        }
        else
        {
            if (status == kStatus_ENET_RxFrameError)
            {
				ENET_ReadFrame(0, 0);
                LWIP_DEBUGF(NETIF_DEBUG, ("\r\nethernetif_linkinput: RxFrameError"));
                LINK_STATS_INC(link.drop);
                MIB2_STATS_NETIF_INC(netif, ifindiscards);
            }
        }
    }
    return p;
}
uint32_t ENET_GetRxFrameSize(uint32_t *length)
{
    *length = 0;
    volatile enet_rx_bd_struct_t *curBuffDescrip = rxBdCurrent;
    if (curBuffDescrip->control & ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK) return 1;
    do
    {
        if((!(curBuffDescrip->control & ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK)) && (!curBuffDescrip->length)) return 2;
        if((curBuffDescrip->control & (ENET_BUFFDESCRIPTOR_RX_LAST_MASK|ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK)) == ENET_BUFFDESCRIPTOR_RX_LAST_MASK)
        {
            if((curBuffDescrip->control & ENET_BUFFDESCRIPTOR_RX_ERR_MASK)) return 2;
            *length = curBuffDescrip->length;
            return 0;
        }
        if(curBuffDescrip->control & ENET_BUFFDESCRIPTOR_RX_WRAP_MASK) curBuffDescrip = g_rxBuffDescrip;
        else curBuffDescrip++;
    } while(curBuffDescrip != rxBdCurrent);
    return 1;
}
uint32_t ENET_ReadFrame(uint8_t *data, uint32_t length)
{
    uint32_t len = 0;
    uint32_t offset = 0;
    uint16_t control;
    uint32_t isLastBuff = 0;
    volatile enet_rx_bd_struct_t *curBuffDescrip = rxBdCurrent;
    uint32_t address;
    if (!data)
    {
        do
        {
            control = rxBdCurrent->control;
            ENET_UpdateReadBuffers();
            if (control & ENET_BUFFDESCRIPTOR_RX_LAST_MASK) break;
        } while (rxBdCurrent != curBuffDescrip);
        return 0;
    }
    else
    {
        address = (uint32_t)curBuffDescrip->buffer;
        while (!isLastBuff)
        {
            if (curBuffDescrip->control & ENET_BUFFDESCRIPTOR_RX_LAST_MASK)
            {
                isLastBuff = 1;
                if (length == curBuffDescrip->length)
                {
                    len = curBuffDescrip->length - offset;
                    memcpy(data + offset, (void *)address, len);
                    ENET_UpdateReadBuffers();
                    return 0;
                }
                else ENET_UpdateReadBuffers();
            }
            else
            {
                isLastBuff = 0;
                if (offset >= length) break;
                memcpy(data + offset, (void *)address, 1520);
                offset += 1520;
                ENET_UpdateReadBuffers();
            }
            curBuffDescrip = rxBdCurrent;
            address = (uint32_t)curBuffDescrip->buffer;
        }
    }
    return 1;
}
void ENET_UpdateReadBuffers(void)
{
	rxBdCurrent->control &= ENET_BUFFDESCRIPTOR_RX_WRAP_MASK;
	rxBdCurrent->control |= ENET_BUFFDESCRIPTOR_RX_EMPTY_MASK;
    if(rxBdCurrent->control & ENET_BUFFDESCRIPTOR_RX_WRAP_MASK) rxBdCurrent = g_rxBuffDescrip;
    else rxBdCurrent++;
    ENET->RDAR = ENET_RDAR_RDAR_MASK;
}
void low_level_debug_msg(uint8_t* string)
{
}
