#include <project.h>
#include <ble_main.h>
#include <stdio.h>
//
uint8_t devices[256][6] = {0};
uint8_t dev_index = 0;
uint32_t passkey;
CYBLE_CONN_HANDLE_T connectionHandle;
CYBLE_STACK_LIB_VERSION_T ble_ver;
CYBLE_API_RESULT_T result;
//
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8* data, uint16 attrHandle);
CYBLE_API_RESULT_T notify_characteristic_byte(uint8* data, uint16 attrHandle);
void ble_event_handler(uint32 event, void* eventParam);
CY_ISR(halfsec_irq_handler);
CY_ISR(slave_irq_handler);
void board_init(void);
void conn_led_on(void);
void conn_led_off(void);
int main(void);
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer);
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer);
void print_6dec(uint32_t data);
void print_hci_error(CYBLE_HCI_ERROR_T data);
void print_2hex(uint8_t data);
void print_function_state(uint8_t* name, CYBLE_API_RESULT_T* result);
void print_scan_report(CYBLE_GAPC_ADV_REPORT_T* pointer);
//
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8* data, uint16 attrHandle)
{
	CYBLE_GATT_HANDLE_VALUE_PAIR_T handle;
    CYBLE_GATT_ERR_CODE_T result;
	handle.attrHandle = attrHandle;
	handle.value.val = data;
	handle.value.len = 1;
	result = CyBle_GattsWriteAttributeValue(&handle, 0, &connectionHandle, CYBLE_GATT_DB_LOCALLY_INITIATED);
    return result;
}
CYBLE_API_RESULT_T notify_characteristic_byte(uint8* data, uint16 attrHandle)
{
    CYBLE_GATTS_HANDLE_VALUE_NTF_T handle;
    CYBLE_API_RESULT_T result;
    handle.value.val  = data;
    handle.value.len  = 1;
    handle.attrHandle = attrHandle;
    result = CyBle_GattsNotification(connectionHandle, &handle);
    return result;
}
void ble_event_handler(uint32 event, void* eventParam)
{
    uint8_t data;
    uart_debug_UartPutString("\r\n[BLE_EVT] ");
	switch(event)
    {
		case CYBLE_EVT_STACK_ON:
            uart_debug_UartPutString("bluetooth stack ON");
            result = CyBle_GapcStartScan(CYBLE_SCANNING_FAST);
            print_function_state("CyBle_GapcStartScan", &result);
            break;
        case CYBLE_EVT_TIMEOUT:
            uart_debug_UartPutString("timeout: ");
            switch(*(CYBLE_TO_REASON_CODE_T*)eventParam)
            {
                case CYBLE_GAP_SCAN_TO:
                    uart_debug_UartPutString("Scan time set by application has expired");
                    result = CyBle_GapcStartScan(CYBLE_SCANNING_SLOW);
                    print_function_state("CyBle_GapcStartScan", &result);
                    break;
                default: uart_debug_UartPutString("unknown"); break;     
            }
			break;
		case CYBLE_EVT_GAPC_SCAN_PROGRESS_RESULT:
            uart_debug_UartPutString("scan progress result\r\n[INFO] event type: ");
            switch(((CYBLE_GAPC_ADV_REPORT_T *)eventParam)->eventType)
            {
                case CYBLE_GAPC_CONN_UNDIRECTED_ADV: uart_debug_UartPutString("Connectable undirected advertising"); break;
                case CYBLE_GAPC_CONN_DIRECTED_ADV: uart_debug_UartPutString("Connectable directed advertising"); break;
                case CYBLE_GAPC_SCAN_UNDIRECTED_ADV: uart_debug_UartPutString("Scannable undirected advertising"); break;
                case CYBLE_GAPC_NON_CONN_UNDIRECTED_ADV: uart_debug_UartPutString("Non connectable undirected advertising"); break;
                case CYBLE_GAPC_SCAN_RSP: uart_debug_UartPutString("Scan Response"); break;
                default: uart_debug_UartPutString("Unknown"); break;    
            }
            print_scan_report((CYBLE_GAPC_ADV_REPORT_T *)eventParam);
		    break;    
        case CYBLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO:
            uart_debug_UartPutString("SMP has completed pairing properties negotiation");
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break; 
        case CYBLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT:
            uart_debug_UartPutString("SMP keys exchange with peer device is complete\r\n[INFO] long term key: ");
            for(uint8_t i=0; i<CYBLE_GAP_SMP_LTK_SIZE; i++) 
                print_2hex(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->ltkInfo[i]);
            uart_debug_UartPutString("\r\n[INFO] encrypted diversifier and random number: ");
            for(uint8_t i=0; i<CYBLE_GAP_SMP_MID_INFO_SIZE; i++)
                print_2hex(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->midInfo[i]);
            uart_debug_UartPutString("\r\n[INFO] identity resolving key: ");
            for(uint8_t i=0; i<CYBLE_GAP_SMP_IRK_SIZE; i++)
                print_2hex(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->irkInfo[i]);
            uart_debug_UartPutString("\r\n[INFO] address type: ");
            if(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->idAddrInfo[0] == 0) 
                uart_debug_UartPutString("public device");
            else uart_debug_UartPutString("static random");
            uart_debug_UartPutString("\r\n[INFO] address: ");
            for(uint8_t i=1; i<CYBLE_GAP_SMP_IDADDR_DATA_SIZE; i++)
                print_2hex(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->idAddrInfo[i]);
            uart_debug_UartPutString("\r\n[INFO] connection signature resolving key: ");
            for(uint8_t i=0; i<CYBLE_GAP_SMP_CSRK_SIZE; i++)
                print_2hex(((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam)->csrkInfo[i]);
            break;  
        case CYBLE_EVT_PENDING_FLASH_WRITE:
            uart_debug_UartPutString("flash write is pending");
            break;              
        case CYBLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
            uart_debug_UartPutString("gap connection update complete");
            print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*)eventParam);
            break;
        case CYBLE_EVT_GATTS_XCNHG_MTU_REQ:
            uart_debug_UartPutString("gatt mtu exchange request: ");            
            print_6dec(((CYBLE_GATT_XCHG_MTU_PARAM_T *)eventParam)->mtu);
            break;                       
        case CYBLE_EVT_GAP_DEVICE_CONNECTED:
            uart_debug_UartPutString("gap device connected");
            result = CyBle_GapAuthReq(connectionHandle.bdHandle, &cyBle_authInfo);
            print_function_state("CyBle_GapAuthReq", &result);
            print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*)eventParam);
            break;
        case CYBLE_EVT_GAP_AUTH_REQ:
            uart_debug_UartPutString("gap authentication request");
            /*result = CyBle_GappAuthReqReply(connectionHandle.bdHandle,&cyBle_authInfo);
            print_function_state("CyBle_GappAuthReqReply", &result);*/
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break; 
        case CYBLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST:
            uart_debug_UartPutString("gap passkey display request");
            //print_6dec(*((uint32_t*)eventParam));//pass
            break;  
        case CYBLE_EVT_GAP_AUTH_COMPLETE:
            uart_debug_UartPutString("gap authentication OK");
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            conn_led_on();
            break;
        case CYBLE_EVT_GAP_ENCRYPT_CHANGE:
            switch(*(uint8 *)eventParam)
            {
                case 0: uart_debug_UartPutString("encrytpion OFF"); break;
                case 1: uart_debug_UartPutString("encrytpion ON"); break;
                default: uart_debug_UartPutString("encrytpion ERROR"); break;
            }
            break; 
        case CYBLE_EVT_GAP_AUTH_FAILED:
            uart_debug_UartPutString("gap authentication FAIL: ");
            switch(*(CYBLE_GAP_AUTH_FAILED_REASON_T *)eventParam)
            {
                case CYBLE_GAP_AUTH_ERROR_PASSKEY_ENTRY_FAILED:
                    uart_debug_UartPutString("user input of passkey failed");
                    break;
                case CYBLE_GAP_AUTH_ERROR_REPEATED_ATTEMPTS:
                    uart_debug_UartPutString("too little time has elapsed since last request");
                    break;
                case CYBLE_GAP_AUTH_ERROR_CONFIRM_VALUE_NOT_MATCH:
                    uart_debug_UartPutString("confirm value does not match the calculated compare value");
                    break;
                case CYBLE_GAP_AUTH_ERROR_LINK_DISCONNECTED:
                    uart_debug_UartPutString("link disconnected");
                    break;    
                case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_TIMEOUT:
                    uart_debug_UartPutString("authentication process timeout");
                    break;     
                case CYBLE_GAP_AUTH_ERROR_UNSPECIFIED_REASON:
                    uart_debug_UartPutString("pairing failed due to an unspecified reason");
                    break;
                case CYBLE_GAP_AUTH_ERROR_INSUFFICIENT_ENCRYPTION_KEY_SIZE:
                    uart_debug_UartPutString("insufficient key size for the security requirements of this device or LTK is lost");    
                    break;
                case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_REQ_NOT_MET:
                    uart_debug_UartPutString("pairing procedure cannot be performed as authentication requirements cannot be met due to IO capabilities of one or both devices");
                    break; 
                default:
                	print_6dec(*(CYBLE_GAP_AUTH_FAILED_REASON_T *)eventParam);
                    break;
            }
            result = CyBle_GapDisconnect(connectionHandle.bdHandle);
            print_function_state("CyBle_GapDisconnect", &result);
            break;             
        case CYBLE_EVT_GATT_CONNECT_IND:
            uart_debug_UartPutString("gatt connect");
            connectionHandle = *(CYBLE_CONN_HANDLE_T *)eventParam;
            break;
        case CYBLE_EVT_HCI_STATUS:
            uart_debug_UartPutString("hci ERROR: ");
            print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            break;             
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            uart_debug_UartPutString("gap device disconnected: ");
            print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            connectionHandle.bdHandle=0;
            break;    
        case CYBLE_EVT_GATTS_WRITE_REQ:
            uart_debug_UartPutString("gatts write request: ");
            data = *((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val;
            switch(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle)
            {   
                default:
                    uart_debug_UartPutString("unknown: ");
                    break;
            }
            print_6dec(data);
            CyBle_GattsWriteRsp(connectionHandle);
            break;
        case CYBLE_EVT_GATTS_WRITE_CMD_REQ:
            uart_debug_UartPutString("gatts write command: ");
            if(CyBle_GattsWriteAttributeValue(&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair, 0,
            	&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->connHandle, CYBLE_GATT_DB_PEER_INITIATED) ==
            	CYBLE_GATT_ERR_NONE)
            {
                data = *((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.value.val;
                switch(((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle)
                {    
                    default:
                        uart_debug_UartPutString("unknown: ");
                        break;
                }
                print_6dec(data);
                CyBle_GattsWriteRsp(connectionHandle);
            }
            else uart_debug_UartPutString ("GATT ERROR");
            break; 
        case CYBLE_EVT_GATT_DISCONNECT_IND:
            conn_led_off();
            uart_debug_UartPutString("gatt disconnect");
            break;
        case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
            uart_debug_UartPutString("read request: ");
            switch(((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->attrHandle)
            {
                case 1: uart_debug_UartPutString("Handle of the GAP service"); break;
                case 3: uart_debug_UartPutString("Handle of the Device Name characteristic"); break;
                case 5: uart_debug_UartPutString("Handle of the Appearance characteristic"); break;
                default: 
                    uart_debug_UartPutString("unknown: "); 
                    print_6dec((((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->attrHandle));
                    break;
            }
            break;
        case CYBLE_EVT_GAPC_SCAN_START_STOP:
            uart_debug_UartPutString("Central device has started/stopped scanning: ");
            if(*((uint8_t*)eventParam) == 0) uart_debug_UartPutString("success");
            else uart_debug_UartPutString("failure");
            break;
        default:
            uart_debug_UartPutString("unknown: 0x");
            print_2hex(event);
            break;
	}
}
void board_init(void)
{
    led_green_Write(1);
    led_red_Write(1);
    conn_led_off();
}
void conn_led_on(void)
{
    led_blue_Write(0);      
}
void conn_led_off(void)
{
    led_blue_Write(1);     
}
int main(void)
{
    CyGlobalIntEnable;
    rtc_Start();
    uart_debug_Start();
    uart_debug_UartPutString("\r\n[INFO] software building date: ");
    uart_debug_UartPutString(__DATE__);
    result = CyBle_Start(ble_event_handler);
    print_function_state("CyBle_Start", &result);
    uart_debug_UartPutString("\r\n[INFO] BLE stack version: ");
    if(CyBle_GetStackLibraryVersion(&ble_ver) == CYBLE_ERROR_OK)
    {
        print_2hex(ble_ver.majorVersion);
        uart_debug_UartPutChar(0x2e);
        print_2hex(ble_ver.minorVersion);  
    }
    else uart_debug_UartPutString("ERROR");
    board_init();
    while(1)
    {
        CyBle_ProcessEvents();
    }
}
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer)
{
	uart_debug_UartPutString("\r\n[INFO] security: ");
	switch(pointer->security)
	{
		case 0: uart_debug_UartPutString("NO"); break;
	    case 1: uart_debug_UartPutString("unauthenticated pairing with encryption"); break;
	    case 2: uart_debug_UartPutString("authenticated pairing with encryption"); break;
	    case 3: uart_debug_UartPutString("secured connection"); break;
	}
	uart_debug_UartPutString("\r\n[INFO] bonding: ");
	if(pointer->bonding == CYBLE_GAP_BONDING_NONE) uart_debug_UartPutString("NO");
	else uart_debug_UartPutString("YES");
}
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer)
{
	uart_debug_UartPutString("\r\n[INFO] status: ");
	print_hci_error(pointer->status);
	uart_debug_UartPutString("\r\n[INFO] connection interval: ");
	print_6dec(pointer->connIntv);
	uart_debug_UartPutString("\r\n[INFO] slave latency for the connection: ");
	print_6dec(pointer->connLatency);
	uart_debug_UartPutString("\r\n[INFO] supervision timeout for the LE link: ");
	print_6dec(pointer->supervisionTO);
}
void print_6dec(uint32_t data)
{
	char chars[7];
    chars[0] = data/100000 + 0x30;
    chars[1] = ((data/10000)%10) + 0x30;
    chars[2] = ((data/1000)%10) + 0x30;
    chars[3] = ((data/100)%10) + 0x30;
    chars[4] = ((data/10)%10) + 0x30;
    chars[5] = (data%10) + 0x30;
    chars[6] = 0;
    uart_debug_UartPutString(chars);
}
void print_hci_error(CYBLE_HCI_ERROR_T data)
{
    switch(data)
    {
        case CYBLE_HCI_NO_CONNECTION_ERROR:
            uart_debug_UartPutString("unknown connection identifier");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_USER_ERROR:
            uart_debug_UartPutString("remote user terminated connection");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_LOCAL_HOST_ERROR:
            uart_debug_UartPutString("connection terminated by local host");
            break;
        case CYBLE_HCI_COMMAND_SUCCEEDED:
            uart_debug_UartPutString("command success");
            break;
        case CYBLE_HCI_CONNECTION_FAILED_TO_BE_ESTABLISHED:
            uart_debug_UartPutString("connection failed to be established");
            break;
        case CYBLE_HCI_CONNECTION_TIMEOUT_ERROR:
            uart_debug_UartPutString("connection timeout");
            break;              
        default:
        	print_6dec(data);
            break;
    }
}
void print_2hex(uint8_t data)
{
    uint8_t temp[2];
	temp[0] = (data&0xf0)>>4;
	temp[1] = data&0x0f;
    for(uint8_t i=0; i<2; i++)
    {
        if(temp[i] < 10) uart_debug_UartPutChar(temp[i] + 0x30);
        else uart_debug_UartPutChar(temp[i] + 0x57);
    }
}
void print_function_state(uint8_t* name, CYBLE_API_RESULT_T* result)
{
    if(*result == CYBLE_ERROR_OK) 
    {
        uart_debug_UartPutString("\r\n[OK] ");
        uart_debug_UartPutString(name);
    }
    else
    {
        uart_debug_UartPutString("\r\n[FAIL] ");
        uart_debug_UartPutString(name);
        uart_debug_UartPutString(": 0x");
        print_2hex(*result);
    }
}
void print_scan_report(CYBLE_GAPC_ADV_REPORT_T* pointer)
{
    uint8_t temp;
    for(uint8_t i=0; i<dev_index; i++)
    {
        if((devices[i][0] == pointer->peerBdAddr[0])&&(devices[i][1] == pointer->peerBdAddr[1])&&(devices[i][2] == pointer->peerBdAddr[2])&&
            (devices[i][3] == pointer->peerBdAddr[3])&&(devices[i][4] == pointer->peerBdAddr[4])&&(devices[i][5] == pointer->peerBdAddr[5])) 
        {
            uart_debug_UartPutString("\r\n[INFO] device in base: ");
            uart_debug_UartPutChar((i/10) + 0x30);
            uart_debug_UartPutChar((i%10) + 0x30);
            return;
        }
    }
    uart_debug_UartPutString("\r\n[INFO] peer address type: ");
    switch(pointer->peerAddrType)
    {
        case CYBLE_GAP_ADDR_TYPE_PUBLIC: uart_debug_UartPutString("public"); break;
        case CYBLE_GAP_ADDR_TYPE_RANDOM: uart_debug_UartPutString("rundom"); break;
		case CYBLE_GAP_ADDR_TYPE_PUBLIC_RPA: uart_debug_UartPutString("public with RPA"); break;
        case CYBLE_GAP_ADDR_TYPE_RANDOM_RPA: uart_debug_UartPutString("rundom with RPA"); break;
        default: uart_debug_UartPutString("unknown"); break;
    }
    uart_debug_UartPutString("\r\n[INFO] peer address: ");
    for(int8_t i=5; i>-1; i--) print_2hex(pointer->peerBdAddr[i]);
    uart_debug_UartPutString("\r\n[INFO] RSSI: -");
    temp = pointer->rssi * (-1);
    uart_debug_UartPutChar((temp/10) + 0x30);
    uart_debug_UartPutChar((temp%10) + 0x30);
    uart_debug_UartPutString("dBm\r\n[INFO] length of the data: ");
    uart_debug_UartPutChar((pointer->dataLen/10) + 0x30);
    uart_debug_UartPutChar((pointer->dataLen%10) + 0x30);
    for(uint8_t i=0; i<6; i++) devices[dev_index][i] = pointer->peerBdAddr[i];
    uart_debug_UartPutString("\r\n[INFO] device saved with index: ");
    uart_debug_UartPutChar((dev_index/10) + 0x30);
    uart_debug_UartPutChar((dev_index%10) + 0x30);
    dev_index++;
}