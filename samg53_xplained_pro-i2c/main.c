#include "system.h"
#include "print.h"
//
#define I2C_DELAY		1000000
//
void main (void);
void uart_print_char(void* uart, uint8_t data);
void UART0_Handler(void);
uint8_t twihs_rd(uint8_t addr, uint8_t reg, uint8_t* data);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x21,
	.month = 0x03,
	.year = 0x19
};
//
void main(void)
{
	PMC->PMC_PCER0 |= PMC_PCER0_PID8|//uart0
			PMC_PCER0_PID11|//pioa
			PMC_PCER0_PID21|//spi
			PMC_PCER0_PID19;//twi0
	//a3-sda, a4-sck, a9-rx, a10-tx, a16-led
	PIOA->PIO_PDR = (1<<3)|(1<<4)|(1<<9)|(1<<10);
	PIOA->PIO_PER = (1<<16);
	PIOA->PIO_OER = (1<<16);
	PIOA->PIO_SODR = (1<<16);
	//twi0
	TWI0->TWIHS_CWGR = 23|(23<<8)|(2<<16);//Thigh = Tlow = 2us
	TWI0->TWIHS_CR = TWIHS_CR_MSEN|TWIHS_CR_SVDIS;
	//uart0
	UART0->UART_CR = UART_CR_RSTRX|UART_CR_RSTTX|UART_CR_RXDIS|UART_CR_TXDIS;
	UART0->UART_BRGR = 26;
	UART0->UART_MR = (4<<9);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);
	UART0->UART_IER = UART_IER_RXRDY;
	UART0->UART_CR = UART_CR_RXEN|UART_CR_TXEN;
	//
	PIOA->PIO_CODR = 1<<16;
	uart_print_string(UART0, "\r\nstart. wait command\r\n");
	while(1){}
}
void uart_print_char(void* uart, uint8_t data)
{
	Uart* periph = (Uart*)uart;
	while((periph->UART_SR & UART_SR_TXRDY) == 0){};
	periph->UART_THR = data;
}
void UART0_Handler(void)
{
	uint8_t loc_temp = UART0->UART_RHR;
	uint8_t loc_temp_2;
	uint8_t string[] = "\r\n0x00";
	uart_print_char(UART0, loc_temp);
	switch(loc_temp)
	{
		case 's':
			uart_print_string(UART0, "\r\ni2c scan");
			for(uint8_t i=0; i<0x80; i++)
			{
				if(twihs_rd(i, 0, &loc_temp_2) == 0)
				{
					uint8_to_hex_string(i, &string[2]);
					uart_print_string(UART0, string);
					uart_print_string(UART0, " exist\r\n");
				}
				else uart_print_char(UART0, '.');
			}
			uart_print_string(UART0, "\r\ni2c scan end");
		break;
		case 'j':
			uart_print_string(UART0, "\r\nread id\r\n0x38: ");
			if(twihs_rd(0x38, 0x92, &loc_temp_2) == 0)
			{
				if(loc_temp_2 == 0xe0) uart_print_string(UART0, "OK");
				else uart_print_string(UART0, "WRONG");
			}
			else uart_print_string(UART0, "ERROR");
			uart_print_string(UART0, "\r\n0x39: ");
			if(twihs_rd(0x39, 0x92, &loc_temp_2) == 0)
			{
				if(loc_temp_2 == 0xe0) uart_print_string(UART0, "OK");
				else uart_print_string(UART0, "WRONG");
			}
			else uart_print_string(UART0, "ERROR");
			uart_print_string(UART0, "\r\n0x29: ");
			if(twihs_rd(0x29, 0x12, &loc_temp_2) == 0)
			{
				if(loc_temp_2 == 0x70) uart_print_string(UART0, "OK");
				else uart_print_string(UART0, "WRONG");
			}
			else uart_print_string(UART0, "ERROR");
		break;
		default: break;
	}
}
uint8_t twihs_rd(uint8_t addr, uint8_t reg, uint8_t* data)
{
	//p597
	uint32_t temp_loc = 0;
	TWI0->TWIHS_MMR = (addr & 0x7f)<<16|TWIHS_MMR_MREAD|TWIHS_MMR_IADRSZ_1_BYTE;
	TWI0->TWIHS_IADR = reg;
	TWI0->TWIHS_CR = TWIHS_CR_START|TWIHS_CR_STOP;
	while((TWI0->TWIHS_SR & TWIHS_SR_RXRDY) == 0)
	{
		if(temp_loc < I2C_DELAY) temp_loc++;
		else return 1;
	}
	*data = TWI0->TWIHS_RHR;
	temp_loc = 0;
	while((TWI0->TWIHS_SR & TWIHS_SR_TXCOMP) == 0)
	{
		if(temp_loc < I2C_DELAY) temp_loc++;
		else return 2;
	}
	return 0;
}
