#include <project.h>
#include <stdio.h>
//
rtc_DATE_TIME times;
uint32_t temp_t;
//
CY_ISR_PROTO(SysTickIsrHandler);
int main(void);
//
int main(void)
{
    uart_debug_Start();
    CySysTickStart();
    CySysTickSetReload(CYDEV_BCLK__SYSCLK__HZ/10);
    for (uint32 i = 0u; i < CY_SYS_SYST_NUM_OF_CALLBACKS; ++i)
    {
        if (CySysTickGetCallback(i) == NULL)
        {
            CySysTickSetCallback(i, SysTickIsrHandler);
            break;
        }
    }
    rtc_Start();
    rtc_SetPeriod(1, 10);
    CyGlobalIntEnable;
    uart_debug_UartPutString("\r\nRTC enabled: ");
    rtc_GetDateAndTime(&times);
    temp_t = rtc_GetDay(times.date);
    uart_debug_UartPutChar((temp_t/10) + 0x30);
    uart_debug_UartPutChar((temp_t%10) + 0x30);
    switch(rtc_GetMonth(times.date))
    {
        case 1: uart_debug_UartPutString(" Jan "); break;
        case 2: uart_debug_UartPutString(" Feb "); break;
        case 3: uart_debug_UartPutString(" Mar "); break;
        case 4: uart_debug_UartPutString(" Apr "); break;
        case 5: uart_debug_UartPutString(" May "); break;
        case 6: uart_debug_UartPutString(" Jun "); break;
        case 7: uart_debug_UartPutString(" Jul "); break;
        case 8: uart_debug_UartPutString(" Aug "); break;
        case 9: uart_debug_UartPutString(" Sep "); break;
        case 10: uart_debug_UartPutString(" Oct "); break;
        case 11: uart_debug_UartPutString(" Nov "); break;
        case 12: uart_debug_UartPutString(" Dec "); break;
    }
    temp_t = rtc_GetYear(times.date);
    uart_debug_UartPutChar((temp_t/1000) + 0x30);
    uart_debug_UartPutChar((temp_t/100)%10 + 0x30);
    uart_debug_UartPutChar((temp_t/10)%10 + 0x30);
    uart_debug_UartPutChar((temp_t%10) + 0x30);
    uart_debug_UartPutChar(0x20);
    temp_t = rtc_GetHours(times.time);
    uart_debug_UartPutChar((temp_t/10) + 0x30);
    uart_debug_UartPutChar((temp_t%10) + 0x30);
    uart_debug_UartPutChar(0x3a);
    temp_t = rtc_GetMinutes(times.time);
    uart_debug_UartPutChar((temp_t/10) + 0x30);
    uart_debug_UartPutChar((temp_t%10) + 0x30);
    uart_debug_UartPutChar(0x3a);
    temp_t = rtc_GetSecond(times.time);
    uart_debug_UartPutChar((temp_t/10) + 0x30);
    uart_debug_UartPutChar((temp_t%10) + 0x30);
    while(1)
    {
        uint32_t time = rtc_GetTime();
        uint32_t date = rtc_GetDate();
        char timeBuffer[16u];
        char dateBuffer[16u];
        sprintf(timeBuffer, "%02lu:%02lu:%02lu", rtc_GetHours(time), rtc_GetMinutes(time), rtc_GetSecond(time));
        sprintf(dateBuffer, "%02lu/%02lu/%02lu", rtc_GetMonth(date), rtc_GetDay(date), rtc_GetYear(date));
        uart_debug_UartPutString(timeBuffer);
        uart_debug_UartPutString(" | ");
        uart_debug_UartPutString(dateBuffer);
        uart_debug_UartPutString("\r");
        CyDelay(200u);
    }
}
void SysTickIsrHandler(void)
{
    rtc_Update();
}