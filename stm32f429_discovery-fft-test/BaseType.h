#ifndef _BT_H_
#define _BT_H_
//
#include "system.h"
#include "arm_math.h"
//
#define RAM_ALLOC			__attribute__ ((section (".data")))
//
void ConvertInit(void);
void Convert(float32_t* fltG, float32_t* fltA, float32_t* fltTermoG, float32_t* fltTermoA) RAM_ALLOC;
float32_t LineInterp(
		float32_t fltParam,
		const float32_t* pfltFrom,
		const float32_t* pfltTo,
		int32_t iLastIndex,
		int32_t* piCurrIndex) RAM_ALLOC;
float32_t TwinLineInterp(
		float32_t fltValue,
		float32_t fltTermo,
		const float32_t* pfltTMKAX,
		int32_t* piCurrIndexT,
		int32_t*piCurrIndexFirstV,
		int32_t* piCurrIndexLastV,
		int32_t Dir) RAM_ALLOC;
int32_t CountBaseDIndex(float32_t fltTermo, const float32_t* pfltData, int32_t* piCurrIndex) RAM_ALLOC;
//
#endif//_BT_H_
