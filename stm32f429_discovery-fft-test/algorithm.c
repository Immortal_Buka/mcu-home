#include "BaseType.h"
#include "coeff.h"
//
#define RAD_TO_DEG                 (180.0f / PI)
#define DEG_TO_RAD                 (PI / 180.0f)
#define SENSORS_GRAVITY_EARTH      (9.818654f)
/*
acceleration
nonorthogonality
angular velocity
calculation
*/
typedef struct
{
	float32_t x;
	float32_t y;
	float32_t z;
} axes_data_t;
typedef struct
{
	float32_t xy;
	float32_t xz;
	float32_t yx;
	float32_t yz;
	float32_t zx;
	float32_t zy;
} nonorthogonality_angles_t;
typedef struct
{
	float32_t kxx;
	float32_t kxy;
	float32_t kxz;
	float32_t kyx;
	float32_t kyy;
	float32_t kyz;
	float32_t kzx;
	float32_t kzy;
	float32_t kzz;
} nonorthogonality_angles_coefficients_t;
//
void MatInit(const UNORT* unort, float32_t A[3][3], float32_t Ainv[3][3], float32_t K[3]);
//
nonorthogonality_angles_coefficients_t acceleration_nac, angular_velocity_nac;
float32_t fltFirstT,fltLastT,fltFirstV,fltLastV, fltRez[3];
int32_t indexAcclBiasT[3] = {0}, indexGyroBiasT[3] = {0}, indexAcclScaleT[3] = {0}, indexGyroScaleT[3] = {0};
int32_t indexAcclScaleFirstV[3] = {0}, indexAcclScaleLastV[3] = {0}, indexGyroScaleFirstV[3] = {0};
int32_t indexGyroScaleLastV[3] = {0}, indexGyroDebalFirstV[3] = {0};
#if defined (GYRO_SCALE_CORRECTION_ENABLED)
int32_t indexGyroSfT[3] = {0}, indexGyroSfFirstV[3] = {0}, indexGyroSfLastV[3] = {0};
#endif
float32_t A_gyro[3][3],A_accl[3][3],Ainv_accl[3][3], K_gyro[3],K_accl[3];
//
void nonorthogonality_angles_compensation(nonorthogonality_angles_coefficients_t* coeffs, axes_data_t* data_in, axes_data_t* data_out)
{
	data_out->x = data_in->x * coeffs->kxx + data_in->y * coeffs->kxy + data_in->z * coeffs->kxz;
	data_out->y = data_in->x * coeffs->kyx + data_in->y * coeffs->kyy + data_in->z * coeffs->kyz;
	data_out->z = data_in->x * coeffs->kzx + data_in->y * coeffs->kzy + data_in->z * coeffs->kzz;
}
void nonorthogonality_angles_calculation(nonorthogonality_angles_t* angles, nonorthogonality_angles_coefficients_t* coeffs)
{
	float32_t sin, cos;
	arm_sin_cos_f32(angles->xy, &sin, &cos);
	coeffs->kxx = cos;
	coeffs->kyz = sin;
	coeffs->kzy = cos;
	arm_sin_cos_f32(angles->xz, &sin, &cos);
	coeffs->kxx *= cos;
	coeffs->kyz *= cos;
	coeffs->kzy *= sin;
	arm_sin_cos_f32(angles->yx, &sin, &cos);
	coeffs->kxy = sin;
	coeffs->kyx = cos;
	coeffs->kzz = cos;
	arm_sin_cos_f32(angles->yz, &sin, &cos);
	coeffs->kxy *= cos;
	coeffs->kyx *= cos;
	coeffs->kzz *= sin;
	arm_sin_cos_f32(angles->zx, &sin, &cos);
	coeffs->kxz = sin;
	coeffs->kyy = cos;
	coeffs->kzx = cos;
	arm_sin_cos_f32(angles->zy, &sin, &cos);
	coeffs->kxz *= cos;
	coeffs->kyy *= sin;
	coeffs->kzx *= cos;
}
float32_t LineInterp(float32_t fltParam, const float32_t* pfltFrom, const float32_t* pfltTo, int32_t iLastIndex, int32_t* piCurrIndex)
{
	int32_t bGrow;
	if(pfltFrom[0]<pfltFrom[iLastIndex]) bGrow = 1;
	else bGrow = -1;
	while (((fltParam*bGrow)<=(pfltFrom[(*piCurrIndex)]*bGrow)) && ((*piCurrIndex) > 0)) (*piCurrIndex)--;
	while (((fltParam*bGrow)>(pfltFrom[(*piCurrIndex)+1]*bGrow)) && ((*piCurrIndex) < (iLastIndex-1))) (*piCurrIndex)++;
	return pfltTo[(*piCurrIndex)]+((fltParam-pfltFrom[(*piCurrIndex)])/(pfltFrom[(*piCurrIndex)+1]-pfltFrom[(*piCurrIndex)]))*(pfltTo[(*piCurrIndex)+1]-pfltTo[(*piCurrIndex)]);
}
float32_t TwinLineInterp(float32_t fltValue, float32_t fltTermo, const float32_t* pfltTMKAX, int32_t* piCurrIndexT, int32_t*piCurrIndexFirstV, int32_t* piCurrIndexLastV, int32_t Dir)
{
	int32_t iIndex = (*piCurrIndexT)+1;
	int32_t iSizeString = pfltTMKAX[0]+2;
	int32_t iCountString = pfltTMKAX[1];
	int32_t bGrow;
	if(pfltTMKAX[iSizeString+1]<pfltTMKAX[iSizeString*iCountString+1]) bGrow = 1;
	else bGrow = -1;
	while (((fltTermo*bGrow)<=(pfltTMKAX[iSizeString*iIndex+1]*bGrow)) && (iIndex > 1)) iIndex--;
	while (((fltTermo*bGrow)>((pfltTMKAX[iSizeString*(iIndex+1)+1]*bGrow))) && (iIndex < (iCountString-1))) iIndex++;
	fltFirstT=pfltTMKAX[iIndex*iSizeString+1];
	fltLastT=pfltTMKAX[(iIndex+1)*iSizeString+1];
	if (!Dir)
	{
		fltFirstV=LineInterp(fltValue,&pfltTMKAX[iIndex*iSizeString+2],&pfltTMKAX[2],pfltTMKAX[0]-1,piCurrIndexFirstV);
		fltLastV=LineInterp(fltValue,&pfltTMKAX[(iIndex+1)*iSizeString+2],&pfltTMKAX[2],pfltTMKAX[0]-1,piCurrIndexLastV);
	}
	else
	{
		fltFirstV=LineInterp(fltValue,&pfltTMKAX[2],&pfltTMKAX[iIndex*iSizeString+2],pfltTMKAX[0]-1,piCurrIndexFirstV);
		fltLastV=LineInterp(fltValue,&pfltTMKAX[2],&pfltTMKAX[(iIndex+1)*iSizeString+2],pfltTMKAX[0]-1,piCurrIndexLastV);
	}
	(*piCurrIndexT) = iIndex - 1;
	return fltFirstV+((fltTermo-fltFirstT)/(fltLastT-fltFirstT))*(fltLastV-fltFirstV);
}
int32_t CountBaseDIndex(float32_t fltTermo, const float32_t* pfltData, int32_t* piCurrIndex)
{
	int32_t iCountString=pfltData[0];
	int32_t bGrow;
	if(pfltData[1]<pfltData[5*(iCountString-1)+1]) bGrow = 1;
	else bGrow = -1;
	while (((fltTermo*bGrow)<=(pfltData[5*(*piCurrIndex)+1]*bGrow)) && ((*piCurrIndex) > 0)) (*piCurrIndex)--;
	while (((fltTermo*bGrow)>((pfltData[5*((*piCurrIndex)+1)+1]*bGrow))) && ((*piCurrIndex) < (iCountString-2))) (*piCurrIndex)++;
	return (*piCurrIndex);
}
void Convert(float32_t* fltG, float32_t* fltA, float32_t* fltTermoG, float32_t* fltTermoA)
{
	int32_t iIndex;
	float32_t fltG1[3], fltA1[3];
	/* Apply accelerometer bias */
	fltA[0] -= LineInterp(fltTermoA[0], &m_vTOffsetAX[0][1], &m_vTOffsetAX[1][1], m_vTOffsetAX[1][0]-1, &indexAcclBiasT[0]);
	fltA[1] -= LineInterp(fltTermoA[1], &m_vTOffsetAY[0][1], &m_vTOffsetAY[1][1], m_vTOffsetAY[1][0]-1, &indexAcclBiasT[1]);
	fltA[2] -= LineInterp(fltTermoA[2], &m_vTOffsetAZ[0][1], &m_vTOffsetAZ[1][1], m_vTOffsetAZ[1][0]-1, &indexAcclBiasT[2]);
	/* Apply accelerometer scale factor */
	fltA[0] = K_accl[0]*TwinLineInterp(fltA[0],fltTermoA[0],&m_vTMKAX[0][0],&indexAcclScaleT[0],&indexAcclScaleFirstV[0],&indexAcclScaleLastV[0],0);
	fltA[1] = K_accl[1]*TwinLineInterp(fltA[1],fltTermoA[1],&m_vTMKAY[0][0],&indexAcclScaleT[1],&indexAcclScaleFirstV[1],&indexAcclScaleLastV[1],0);
	fltA[2] = K_accl[2]*TwinLineInterp(fltA[2],fltTermoA[2],&m_vTMKAZ[0][0],&indexAcclScaleT[2],&indexAcclScaleFirstV[2],&indexAcclScaleLastV[2],0);
	/* Apply gyroscope bias */
	fltG[0]-=LineInterp(fltTermoG[0],&m_vTOffsetGX[0][1],&m_vTOffsetGX[1][1],m_vTOffsetGX[1][0]-1,&indexGyroBiasT[0]);
	fltG[1]-=LineInterp(fltTermoG[1],&m_vTOffsetGY[0][1],&m_vTOffsetGY[1][1],m_vTOffsetGY[1][0]-1,&indexGyroBiasT[1]);
	fltG[2]-=LineInterp(fltTermoG[2],&m_vTOffsetGZ[0][1],&m_vTOffsetGZ[1][1],m_vTOffsetGZ[1][0]-1,&indexGyroBiasT[2]);
	/* Apply gyroscope scale factor */
	fltG[0]=K_gyro[0]*TwinLineInterp(fltG[0],fltTermoG[0],&m_vTMKGX[0][0],&indexGyroScaleT[0],&indexGyroScaleFirstV[0],&indexGyroScaleLastV[0],0)*DEG_TO_RAD;
	fltG[1]=K_gyro[1]*TwinLineInterp(fltG[1],fltTermoG[1],&m_vTMKGY[0][0],&indexGyroScaleT[1],&indexGyroScaleFirstV[1],&indexGyroScaleLastV[1],0)*DEG_TO_RAD;
	fltG[2]=K_gyro[2]*TwinLineInterp(fltG[2],fltTermoG[2],&m_vTMKGZ[0][0],&indexGyroScaleT[2],&indexGyroScaleFirstV[2],&indexGyroScaleLastV[2],0)*DEG_TO_RAD;
	/* Apply gyroscope bias g-sensitivity */
	iIndex=CountBaseDIndex(fltTermoG[0],&debalanceToX[0][0],&indexGyroDebalFirstV[0]);
	fltRez[0]=debalanceToX[iIndex][2]+((fltTermoG[0]-debalanceToX[iIndex][1])/(debalanceToX[iIndex+1][1]-debalanceToX[iIndex][1]))*(debalanceToX[iIndex+1][2]-debalanceToX[iIndex][2]);
	fltRez[1]=debalanceToX[iIndex][3]+((fltTermoG[0]-debalanceToX[iIndex][1])/(debalanceToX[iIndex+1][1]-debalanceToX[iIndex][1]))*(debalanceToX[iIndex+1][3]-debalanceToX[iIndex][3]);
	fltRez[2]=debalanceToX[iIndex][4]+((fltTermoG[0]-debalanceToX[iIndex][1])/(debalanceToX[iIndex+1][1]-debalanceToX[iIndex][1]))*(debalanceToX[iIndex+1][4]-debalanceToX[iIndex][4]);
	fltG[0]-=(fltRez[0]*fltA[0]+fltRez[1]*fltA[1]+fltRez[2]*fltA[2]);
	iIndex=CountBaseDIndex(fltTermoG[1],&debalanceToY[0][0],&indexGyroDebalFirstV[1]);
	fltRez[0]=debalanceToY[iIndex][2]+((fltTermoG[1]-debalanceToY[iIndex][1])/(debalanceToY[iIndex+1][1]-debalanceToY[iIndex][1]))*(debalanceToY[iIndex+1][2]-debalanceToY[iIndex][2]);
	fltRez[1]=debalanceToY[iIndex][3]+((fltTermoG[1]-debalanceToY[iIndex][1])/(debalanceToY[iIndex+1][1]-debalanceToY[iIndex][1]))*(debalanceToY[iIndex+1][3]-debalanceToY[iIndex][3]);
	fltRez[2]=debalanceToY[iIndex][4]+((fltTermoG[1]-debalanceToY[iIndex][1])/(debalanceToY[iIndex+1][1]-debalanceToY[iIndex][1]))*(debalanceToY[iIndex+1][4]-debalanceToY[iIndex][4]);
	fltG[1]-=(fltRez[0]*fltA[0]+fltRez[1]*fltA[1]+fltRez[2]*fltA[2]);
	iIndex=CountBaseDIndex(fltTermoG[2],&debalanceToZ[0][0],&indexGyroDebalFirstV[2]);
	fltRez[0]=debalanceToZ[iIndex][2]+((fltTermoG[2]-debalanceToZ[iIndex][1])/(debalanceToZ[iIndex+1][1]-debalanceToZ[iIndex][1]))*(debalanceToZ[iIndex+1][2]-debalanceToZ[iIndex][2]);
	fltRez[1]=debalanceToZ[iIndex][3]+((fltTermoG[2]-debalanceToZ[iIndex][1])/(debalanceToZ[iIndex+1][1]-debalanceToZ[iIndex][1]))*(debalanceToZ[iIndex+1][3]-debalanceToZ[iIndex][3]);
	fltRez[2]=debalanceToZ[iIndex][4]+((fltTermoG[2]-debalanceToZ[iIndex][1])/(debalanceToZ[iIndex+1][1]-debalanceToZ[iIndex][1]))*(debalanceToZ[iIndex+1][4]-debalanceToZ[iIndex][4]);
	fltG[2]-=(fltRez[0]*fltA[0]+fltRez[1]*fltA[1]+fltRez[2]*fltA[2]);
	/* ----------FIRST ITERATION---------- */
	/* Apply gyroscope scale factor g-sensitivity */
#if defined (GYRO_SCALE_CORRECTION_ENABLED)
	fltRez[0]=TwinLineInterp(fltA[GYRO_SENS_AXIS_X],fltTermoG[0],&m_vTSFGX[0][0],&indexGyroSfT[0],&indexGyroSfFirstV[0],&indexGyroSfLastV[0],1);
	fltRez[1]=TwinLineInterp(fltA[GYRO_SENS_AXIS_Y],fltTermoG[1],&m_vTSFGY[0][0],&indexGyroSfT[1],&indexGyroSfFirstV[1],&indexGyroSfLastV[1],1);
	fltRez[2]=TwinLineInterp(fltA[GYRO_SENS_AXIS_Z],fltTermoG[2],&m_vTSFGZ[0][0],&indexGyroSfT[2],&indexGyroSfFirstV[2],&indexGyroSfLastV[2],1);

	fltG1[0]=fltG[0]/(1.0f-fltRez[0]);
	fltG1[1]=fltG[1]/(1.0f-fltRez[1]);
	fltG1[2]=fltG[2]/(1.0f-fltRez[2]);
#else
	fltG1[0]=fltG[0];
	fltG1[1]=fltG[1];
	fltG1[2]=fltG[2];
#endif
	/* Apply gyroscope misalignment */
	fltRez[0]=fltG1[0]; fltRez[1]=fltG1[1]; fltRez[2]=fltG1[2];
	fltG1[0]=A_gyro[0][0]*fltRez[0]+A_gyro[0][1]*fltRez[1]+A_gyro[0][2]*fltRez[2];
	fltG1[1]=A_gyro[1][0]*fltRez[0]+A_gyro[1][1]*fltRez[1]+A_gyro[1][2]*fltRez[2];
	fltG1[2]=A_gyro[2][0]*fltRez[0]+A_gyro[2][1]*fltRez[1]+A_gyro[2][2]*fltRez[2];
	/* Apply accelerometer size-effect compensation */
#if defined (SIZE_EFFECT_COMPENSATION_ENABLED)
	fltRez[0]=-fltG1[1]*(fltG1[1]*radiusAcc[0].R_X-fltG1[0]*radiusAcc[0].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[0].R_X-fltG1[0]*radiusAcc[0].R_Z);
	fltRez[1]=+fltG1[0]*(fltG1[1]*radiusAcc[0].R_X-fltG1[0]*radiusAcc[0].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[0].R_Y-fltG1[1]*radiusAcc[0].R_Z);
	fltRez[2]=+fltG1[0]*(fltG1[2]*radiusAcc[0].R_X-fltG1[0]*radiusAcc[0].R_Z)+fltG1[1]*(fltG1[2]*radiusAcc[0].R_Y-fltG1[1]*radiusAcc[0].R_Z);
	fltA1[0]=fltA[0]-(Ainv_accl[0][0]*fltRez[0]+Ainv_accl[0][1]*fltRez[1]+Ainv_accl[0][2]*fltRez[2]);
	fltRez[0]=-fltG1[1]*(fltG1[1]*radiusAcc[1].R_X-fltG1[0]*radiusAcc[1].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[1].R_X-fltG1[0]*radiusAcc[1].R_Z);
	fltRez[1]=+fltG1[0]*(fltG1[1]*radiusAcc[1].R_X-fltG1[0]*radiusAcc[1].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[1].R_Y-fltG1[1]*radiusAcc[1].R_Z);
	fltRez[2]=+fltG1[0]*(fltG1[2]*radiusAcc[1].R_X-fltG1[0]*radiusAcc[1].R_Z)+fltG1[1]*(fltG1[2]*radiusAcc[1].R_Y-fltG1[1]*radiusAcc[1].R_Z);
	fltA1[1]=fltA[1]-(Ainv_accl[1][0]*fltRez[0]+Ainv_accl[1][1]*fltRez[1]+Ainv_accl[1][2]*fltRez[2]);
	fltRez[0]=-fltG1[1]*(fltG1[1]*radiusAcc[2].R_X-fltG1[0]*radiusAcc[2].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[2].R_X-fltG1[0]*radiusAcc[2].R_Z);
	fltRez[1]=+fltG1[0]*(fltG1[1]*radiusAcc[2].R_X-fltG1[0]*radiusAcc[2].R_Y)-fltG1[2]*(fltG1[2]*radiusAcc[2].R_Y-fltG1[1]*radiusAcc[2].R_Z);
	fltRez[2]=+fltG1[0]*(fltG1[2]*radiusAcc[2].R_X-fltG1[0]*radiusAcc[2].R_Z)+fltG1[1]*(fltG1[2]*radiusAcc[2].R_Y-fltG1[1]*radiusAcc[2].R_Z);
	fltA1[2]=fltA[2]-(Ainv_accl[2][0]*fltRez[0]+Ainv_accl[2][1]*fltRez[1]+Ainv_accl[2][2]*fltRez[2]);
#else
	fltA1[0]=fltA[0];
	fltA1[1]=fltA[1];
	fltA1[2]=fltA[2];
#endif
	/* Apply accelerometer misalignment */
	fltRez[0]=fltA1[0]; fltRez[1]=fltA1[1]; fltRez[2]=fltA1[2];
	fltA1[0]=A_accl[0][0]*fltRez[0]+A_accl[0][1]*fltRez[1]+A_accl[0][2]*fltRez[2];
	fltA1[1]=A_accl[1][0]*fltRez[0]+A_accl[1][1]*fltRez[1]+A_accl[1][2]*fltRez[2];
	fltA1[2]=A_accl[2][0]*fltRez[0]+A_accl[2][1]*fltRez[1]+A_accl[2][2]*fltRez[2];
	/* ----------SECOND ITERATION---------- */
	/* Apply gyroscope scale factor g-sensitivity */
#if defined (GYRO_SCALE_CORRECTION_ENABLED)
	fltRez[0]=TwinLineInterp(fltA1[GYRO_SENS_AXIS_X],fltTermoG[0],&m_vTSFGX[0][0],&indexGyroSfT[0],&indexGyroSfFirstV[0],&indexGyroSfLastV[0],1);
	fltRez[1]=TwinLineInterp(fltA1[GYRO_SENS_AXIS_Y],fltTermoG[1],&m_vTSFGY[0][0],&indexGyroSfT[1],&indexGyroSfFirstV[1],&indexGyroSfLastV[1],1);
	fltRez[2]=TwinLineInterp(fltA1[GYRO_SENS_AXIS_Z],fltTermoG[2],&m_vTSFGZ[0][0],&indexGyroSfT[2],&indexGyroSfFirstV[2],&indexGyroSfLastV[2],1);
	fltG[0]=fltG[0]/(1.0f-fltRez[0]);
	fltG[1]=fltG[1]/(1.0f-fltRez[1]);
	fltG[2]=fltG[2]/(1.0f-fltRez[2]);
#endif
	/* Apply gyroscope misalignment */
	fltRez[0]=fltG[0]; fltRez[1]=fltG[1]; fltRez[2]=fltG[2];
	fltG[0]=A_gyro[0][0]*fltRez[0]+A_gyro[0][1]*fltRez[1]+A_gyro[0][2]*fltRez[2];
	fltG[1]=A_gyro[1][0]*fltRez[0]+A_gyro[1][1]*fltRez[1]+A_gyro[1][2]*fltRez[2];
	fltG[2]=A_gyro[2][0]*fltRez[0]+A_gyro[2][1]*fltRez[1]+A_gyro[2][2]*fltRez[2];
	/* Apply accelerometer size-effect compensation */
#if defined (SIZE_EFFECT_COMPENSATION_ENABLED)
	fltRez[0]=-fltG[1]*(fltG[1]*radiusAcc[0].R_X-fltG[0]*radiusAcc[0].R_Y)-fltG[2]*(fltG[2]*radiusAcc[0].R_X-fltG[0]*radiusAcc[0].R_Z);
	fltRez[1]=+fltG[0]*(fltG[1]*radiusAcc[0].R_X-fltG[0]*radiusAcc[0].R_Y)-fltG[2]*(fltG[2]*radiusAcc[0].R_Y-fltG[1]*radiusAcc[0].R_Z);
	fltRez[2]=+fltG[0]*(fltG[2]*radiusAcc[0].R_X-fltG[0]*radiusAcc[0].R_Z)+fltG[1]*(fltG[2]*radiusAcc[0].R_Y-fltG[1]*radiusAcc[0].R_Z);
	fltA[0]=fltA[0]-(Ainv_accl[0][0]*fltRez[0]+Ainv_accl[0][1]*fltRez[1]+Ainv_accl[0][2]*fltRez[2]);
	fltRez[0]=-fltG[1]*(fltG[1]*radiusAcc[1].R_X-fltG[0]*radiusAcc[1].R_Y)-fltG[2]*(fltG[2]*radiusAcc[1].R_X-fltG[0]*radiusAcc[1].R_Z);
	fltRez[1]=+fltG[0]*(fltG[1]*radiusAcc[1].R_X-fltG[0]*radiusAcc[1].R_Y)-fltG[2]*(fltG[2]*radiusAcc[1].R_Y-fltG[1]*radiusAcc[1].R_Z);
	fltRez[2]=+fltG[0]*(fltG[2]*radiusAcc[1].R_X-fltG[0]*radiusAcc[1].R_Z)+fltG[1]*(fltG[2]*radiusAcc[1].R_Y-fltG[1]*radiusAcc[1].R_Z);
	fltA[1]=fltA[1]-(Ainv_accl[1][0]*fltRez[0]+Ainv_accl[1][1]*fltRez[1]+Ainv_accl[1][2]*fltRez[2]);
	fltRez[0]=-fltG[1]*(fltG[1]*radiusAcc[2].R_X-fltG[0]*radiusAcc[2].R_Y)-fltG[2]*(fltG[2]*radiusAcc[2].R_X-fltG[0]*radiusAcc[2].R_Z);
	fltRez[1]=+fltG[0]*(fltG[1]*radiusAcc[2].R_X-fltG[0]*radiusAcc[2].R_Y)-fltG[2]*(fltG[2]*radiusAcc[2].R_Y-fltG[1]*radiusAcc[2].R_Z);
	fltRez[2]=+fltG[0]*(fltG[2]*radiusAcc[2].R_X-fltG[0]*radiusAcc[2].R_Z)+fltG[1]*(fltG[2]*radiusAcc[2].R_Y-fltG[1]*radiusAcc[2].R_Z);
	fltA[2]=fltA[2]-(Ainv_accl[2][0]*fltRez[0]+Ainv_accl[2][1]*fltRez[1]+Ainv_accl[2][2]*fltRez[2]);
#endif
	/* Apply accelerometer misalignment */
	fltRez[0]=fltA[0]; fltRez[1]=fltA[1]; fltRez[2]=fltA[2];
	fltA[0]=A_accl[0][0]*fltRez[0]+A_accl[0][1]*fltRez[1]+A_accl[0][2]*fltRez[2];
	fltA[1]=A_accl[1][0]*fltRez[0]+A_accl[1][1]*fltRez[1]+A_accl[1][2]*fltRez[2];
	fltA[2]=A_accl[2][0]*fltRez[0]+A_accl[2][1]*fltRez[1]+A_accl[2][2]*fltRez[2];
}
void MatInit(const UNORT* unort, float32_t A[3][3], float32_t Ainv[3][3], float32_t K[3])
{
	float32_t cos_dxy, cos_dxz, cos_dyx, cos_dyz, cos_dzx, cos_dzy;
	float32_t sin_dxy, sin_dxz, sin_dyx, sin_dyz, sin_dzx, sin_dzy;
	arm_sin_cos_f32(unort->K_XY, &sin_dxy, &cos_dxy);
	arm_sin_cos_f32(unort->K_XZ, &sin_dxz, &cos_dxz);
	arm_sin_cos_f32(unort->K_YX, &sin_dyx, &cos_dyx);
	arm_sin_cos_f32(unort->K_YZ, &sin_dyz, &cos_dyz);
	arm_sin_cos_f32(unort->K_ZX, &sin_dzx, &cos_dzx);
	arm_sin_cos_f32(unort->K_ZY, &sin_dzy, &cos_dzy);
	float32_t den=cos_dxy*cos_dxz*cos_dyx*cos_dyz*cos_dzx*cos_dzy -
		cos_dxy*cos_dxz*cos_dyx*cos_dzx*sin_dyz*sin_dzy -
		cos_dxy*cos_dyx*cos_dyz*cos_dzy*sin_dxz*sin_dzx -
		cos_dxz*cos_dyz*cos_dzx*cos_dzy*sin_dxy*sin_dyx +
		cos_dxy*cos_dyz*cos_dzx*sin_dxz*sin_dyx*sin_dzy +
		cos_dxz*cos_dyx*cos_dzy*sin_dxy*sin_dyz*sin_dzx;
	A[0][0]=+(cos_dyx*cos_dyz*cos_dzx*cos_dzy-cos_dyx*cos_dzx*sin_dyz*sin_dzy)/den;
	A[0][1]=-(cos_dxz*cos_dzx*cos_dzy*sin_dxy-cos_dxy*cos_dzx*sin_dxz*sin_dzy)/den;
	A[0][2]=-(cos_dxy*cos_dyx*cos_dyz*sin_dxz-cos_dxz*cos_dyx*sin_dxy*sin_dyz)/den;
	A[1][0]=-(cos_dyz*cos_dzx*cos_dzy*sin_dyx-cos_dyx*cos_dzy*sin_dyz*sin_dzx)/den;
	A[1][1]=+(cos_dxy*cos_dxz*cos_dzx*cos_dzy-cos_dxy*cos_dzy*sin_dxz*sin_dzx)/den;
	A[1][2]=-(cos_dxy*cos_dxz*cos_dyx*sin_dyz-cos_dxy*cos_dyz*sin_dxz*sin_dyx)/den;
	A[2][0]=-(cos_dyx*cos_dyz*cos_dzy*sin_dzx-cos_dyz*cos_dzx*sin_dyx*sin_dzy)/den;
	A[2][1]=-(cos_dxy*cos_dxz*cos_dzx*sin_dzy-cos_dxz*cos_dzy*sin_dxy*sin_dzx)/den;
	A[2][2]=+(cos_dxy*cos_dxz*cos_dyx*cos_dyz-cos_dxz*cos_dyz*sin_dxy*sin_dyx)/den;
	K[0]=cos_dxy*cos_dxz;
	K[1]=cos_dyx*cos_dyz;
	K[2]=cos_dzx*cos_dzy;
	if(Ainv == 0) return;
	Ainv[0][0]=cos_dxy*cos_dxz;
	Ainv[0][1]=cos_dxz*sin_dxy;
	Ainv[0][2]=cos_dxy*sin_dxz;
	Ainv[1][0]=cos_dyz*sin_dyx;
	Ainv[1][1]=cos_dyx*cos_dyz;
	Ainv[1][2]=cos_dyx*sin_dyz;
	Ainv[2][0]=cos_dzy*sin_dzx;
	Ainv[2][1]=cos_dzx*sin_dzy;
	Ainv[2][2]=cos_dzx*cos_dzy;
}
void ConvertInit(void)
{
	MatInit(&unortGyr, A_gyro, 0, K_gyro);
	MatInit(&unortAcc, A_accl, Ainv_accl, K_accl);
}
