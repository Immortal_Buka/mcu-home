#include "system.h"
#include "lcd.h"
#include "print.h"
#include "arm_const_structs.h"
#include "BaseType.h"
//
#define FFT_SIZE 			8192
//
void main(void);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x26,
	.month = 0x10,
	.year = 0x18
};
//
volatile float32_t input_float[FFT_SIZE], output_float[FFT_SIZE];
volatile float32_t gyro[3];
volatile float32_t tg[3];
volatile float32_t ta[3];
volatile float32_t accel[3];
volatile uint32_t time[2];
float32_t glob_temp_1;
uint32_t glob_temp_2;
//
void main(void)
{
	init_gpio();
	lcd_init(C_White);
	rng_init();
	ConvertInit();
	for(uint16_t i=0; i<3; i++)
	{
		glob_temp_2 = rng_random_data();
		glob_temp_1 = (float32_t)(glob_temp_2 & 0xff);
		tg[i] = glob_temp_1;
		ta[i] = glob_temp_1;
		glob_temp_2 = rng_random_data();
		glob_temp_1 = (float32_t)(glob_temp_2 & 0xffff);
		gyro[i] = glob_temp_1;
		glob_temp_1 = (float32_t)(glob_temp_2 >> 16);
		accel[i] = glob_temp_1;
	}
	for(uint16_t i=0; i<FFT_SIZE; i++)
	{
		glob_temp_2 = rng_random_data();
		glob_temp_1 = (float32_t)(glob_temp_2 >> 16);
		input_float[i] = glob_temp_1;
	}
 	arm_rfft_fast_instance_f32 inst32;
 	arm_rfft_fast_init_f32(&inst32, FFT_SIZE);
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	TIM2->PSC = 83;
	TIM2->CR1 = TIM_CR1_CEN;
	//for(uint16_t i=0; i<FFT_SIZE; i++) Convert(gyro, accel, tg, ta);
	time[0] = TIM2->CNT;
	arm_rfft_fast_f32(&inst32, input_float, output_float, 0);
	arm_cmplx_mag_squared_f32(output_float, input_float, FFT_SIZE>>1);
	time[1] = TIM2->CNT;
	while(1)
	{
		__WFI();
	}
}
