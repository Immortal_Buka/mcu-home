#include "system.h"
#include "ff.h"
#include "arm_math.h"
#include "print.h"
#include "time.h"
#include "arm_const_structs.h"
//
#define FFT_SIZE 			4096
#define SIN_TABLE 			400
//
void main (void);
void DMA0_IRQHandler(void);
uint8_t ADC_Calibration(void);
//
q15_t input[2][FFT_SIZE], temp_buf[FFT_SIZE], output[FFT_SIZE];
float32_t input_float[FFT_SIZE], output_float[FFT_SIZE];
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x02,
	.day = 0x14,
	.month = 0x11,
	.year = 0x18
};
volatile uint8_t mutex = 0;
volatile uint8_t pointer = 1;
volatile uint16_t sinetable[SIN_TABLE] = {0};
//
void main (void)
{
	fpu_on();
	for(uint16_t i=0; i<SIN_TABLE; i++)
	{
		float32_t temp_in_place = (float32_t)i;
		temp_in_place = 2048.0 + 2047.0*arm_sin_f32(temp_in_place*PI*2.0/((float32_t)SIN_TABLE));
		sinetable[i] = temp_in_place;
	}
	init_common_gpio();
	frdm_led_rgb(all, off);
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK|SIM_SCGC6_DMAMUX_MASK|SIM_SCGC6_ADC0_MASK;
	SIM->SCGC7 |= SIM_SCGC7_DMA_MASK;
	DMAMUX->CHCFG[1] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_TRIG_MASK|DMAMUX_CHCFG_SOURCE(63);
	DMA0->TCD[1].SADDR = (uint32_t)sinetable;
	DMA0->TCD[1].DADDR = (uint32_t)&(DAC0->DAT[0].DATL);
	DMA0->TCD[1].DLAST_SGA = 0;
	DMA0->TCD[1].SOFF = 2;
	DMA0->TCD[1].DOFF = 0;
	DMA0->TCD[1].SLAST = -(SIN_TABLE*2);
	DMA0->TCD[1].NBYTES_MLNO = 2;
	DMA0->TCD[1].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[1].CITER_ELINKNO = SIN_TABLE;
	DMA0->TCD[1].BITER_ELINKNO = SIN_TABLE;
	DMAMUX->CHCFG[0] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_SOURCE(40);
	DMA0->ERQ = DMA_ERQ_ERQ0_MASK|DMA_ERQ_ERQ1_MASK;
	DMA0->TCD[0].SADDR = (uint32_t)&ADC0->R[0];
	DMA0->TCD[0].DADDR = (uint32_t)input;
	DMA0->TCD[0].DOFF = 2;
	DMA0->TCD[0].DLAST_SGA = -(FFT_SIZE*4);
	DMA0->TCD[0].NBYTES_MLNO = 2;
	DMA0->TCD[0].SOFF = 0;
	DMA0->TCD[0].SLAST = 0;
	DMA0->TCD[0].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[0].CITER_ELINKNO = FFT_SIZE*2;
	DMA0->TCD[0].BITER_ELINKNO = FFT_SIZE*2;
	DMA0->TCD[0].CSR = DMA_CSR_INTMAJOR_MASK|DMA_CSR_INTHALF_MASK;
	NVIC_EnableIRQ(DMA0_IRQn);
	ADC0->CFG1 |= ADC_CFG1_MODE(3)|ADC_CFG1_ADIV(3)|ADC_CFG1_ADLSMP_MASK;
	ADC0->SC1[0] = ADC_SC1_DIFF_MASK;//DAD0
	ADC0->SC3 = ADC_SC3_AVGE_MASK|ADC_SC3_AVGS(3);
	if(ADC_Calibration() == 1) error_signal();
	ADC0->SC2 = ADC_SC2_ADTRG_MASK|ADC_SC2_DMAEN_MASK;
	SIM->SCGC2 |= SIM_SCGC2_DAC0_MASK;
	DAC0->C0 = DAC_C0_DACEN_MASK|DAC_C0_DACRFS_MASK;
	SIM->SOPT7 = SIM_SOPT7_ADC0TRGSEL(4)|SIM_SOPT7_ADC0ALTTRGEN_MASK;
	PIT->MCR = PIT_MCR_FRZ_MASK;
	PIT->CHANNEL[1].LDVAL = 1806;//dac n+1 = 60M/(SIN_TABLE*Freq)
	PIT->CHANNEL[1].TCTRL = PIT_TCTRL_TEN_MASK;
	PIT->CHANNEL[0].LDVAL = 59999;//adc n+1 = 60M/Freq
	PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TEN_MASK;
	arm_rfft_instance_q15 inst15;
 	arm_rfft_init_q15(&inst15, FFT_SIZE, 0, 1);
 	arm_rfft_fast_instance_f32 inst32;
 	arm_rfft_fast_init_f32(&inst32, FFT_SIZE);
	while(1)
	{
		if(mutex)
		{
			arm_q15_to_float(input[pointer], input_float, FFT_SIZE);
			arm_rfft_fast_f32(&inst32, input_float, output_float, 0);
			arm_cmplx_mag_squared_f32(output_float, input_float, FFT_SIZE>>1);
			//arm_rfft_q15(&inst15, input[pointer], temp_buf);
			//arm_cmplx_mag_squared_q15(temp_buf, output, FFT_SIZE>>1);
			mutex = 0;
		}
	}
}
void DMA0_IRQHandler(void)
{
	mutex = 1;
	pointer ^= 1;
	DMA0->INT = DMA_INT_INT0_MASK;
}
uint8_t ADC_Calibration(void)
{
    ADC0->SC3 |= ADC_SC3_CAL_MASK;
    while ((ADC0->SC1[0] & ADC_SC1_COCO_MASK) != ADC_SC1_COCO_MASK)
    {
        if (ADC0->SC3 & ADC_SC3_CALF_MASK) return 1;
    }
    if (ADC0->SC3 & ADC_SC3_CALF_MASK) return 1;
    ADC0->PG = (0x8000|((ADC0->CLP0 + ADC0->CLP1 + ADC0->CLP2 + ADC0->CLP3 + ADC0->CLP4 + ADC0->CLPS) >> 1));
    ADC0->MG = (0x8000|((ADC0->CLM0 + ADC0->CLM1 + ADC0->CLM2 + ADC0->CLM3 + ADC0->CLM4 + ADC0->CLMS) >> 1));
    return 0;
}
