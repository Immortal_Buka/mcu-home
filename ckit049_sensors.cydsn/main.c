#include <project.h>
//
#define BH1745_ADDR_1	0x38
#define BH1745_ADDR_2	0x39
#define BH1745_ADDR(x)	BH1745_ADDR_##x
#define BH1730_ADDR	    0x29
//
void main(void);
void print_2hex(uint8_t data);
void print_stat_error(uint32_t status);
void i2c_scan(void);
//
uint8_t led_state = 1, temp = 0, operate = 0, rx_data, temp2;
uint32_t status;
uint16_t timeout = 10;
//
CY_ISR(sw1_irq_handler)
{
    sw1_in_ClearInterrupt();
    UART_1_UartPutString("\r\nSW pushed");
}
void main(void)
{
    CyGlobalIntEnable;
    SW1_IRQ_StartEx(sw1_irq_handler);
    led_Write(led_state);
    UART_1_Start();
    UART_1_UartPutString("\r\nUART init done");
    I2C_1_Start();
    UART_1_UartPutString("\r\nI2C init done");
    while(1)
    {
    	rx_data = UART_1_UartGetChar();
    	if(rx_data != 0)
    	{
    		UART_1_UartPutString("\r\nUART TX - ");
    		UART_1_UartPutChar(rx_data);
    		UART_1_UartPutString("\r\n");
    		switch(rx_data)
    		{
    			case 's':
    				UART_1_UartPutString("I2C scan");
    	            i2c_scan();
    	        break;
    	        case '0':
    	            UART_1_UartPutString("LED off");
    	            led_state = 0;
    	            led_Write(led_state);
    	        break;
    	        case '1':
    	            UART_1_UartPutString("LED on");
    	            led_state = 1;
    	            led_Write(led_state);
    	        break;
    	        case '2':
    	            UART_1_UartPutString("LED sw");
    	            if(led_state) led_state = 0;
    	            else led_state = 1;
    	            led_Write(led_state);
    	        break;
    	        case 'a':
    	            UART_1_UartPutString("BH1745 0x38");
    	            operate = 1;
    	        break;
    	        case 'b':
    	            UART_1_UartPutString("BH1745 0x39");
    	            operate = 2;
    	        break;
    	        case 'c':
    	            UART_1_UartPutString("BH1730 ID");
    	            operate = 3;
    	        break;
    	        case 'd':
    	            UART_1_UartPutString("BH1730 read");
    	            operate = 4;
    	        break;
    	        case 'p':
    	        	UART_1_UartPutString("timeout inc");
    	        	timeout += 10;
    	        break;
    	        case 'm':
    	        	UART_1_UartPutString("timeout dec");
    	        	if(timeout > 10) timeout -= 10;
    	        break;
    	        case 'r':
    	        	UART_1_UartPutString("I2C restart");
    	        	I2C_1_Stop();
    	        	I2C_1_Start();
    	        break;
    	        default: break;
    		}
    	}
        if(operate != 0)
        {
            switch(operate)
            {
                case 1:
                case 2:
                    status = I2C_1_I2CMasterSendStart(0x37+operate, I2C_1_I2C_WRITE_XFER_MODE, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterWriteByte(0x92,timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterSendRestart(0x37+operate, I2C_1_I2C_READ_XFER_MODE, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterReadByte(0, &temp, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterSendStop(timeout);
                    print_stat_error(status);
                    if(temp == 0xe0)
                    {
                    	UART_1_UartPutString("\r\nID good\r\nred - ");
                    	I2C_1_I2CMasterSendStart(0x37+operate, I2C_1_I2C_WRITE_XFER_MODE, timeout);
                    	I2C_1_I2CMasterWriteByte(0x42,timeout);
                    	I2C_1_I2CMasterWriteByte(0x10,timeout);
                    	I2C_1_I2CMasterWriteByte(0x02,timeout);
                    	I2C_1_I2CMasterSendRestart(0x37+operate, I2C_1_I2C_WRITE_XFER_MODE, timeout);
                    	I2C_1_I2CMasterWriteByte(0x50,timeout);
                    	I2C_1_I2CMasterSendRestart(0x37+operate, I2C_1_I2C_READ_XFER_MODE, timeout);
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	UART_1_UartPutString("\r\ngreen - ");
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	UART_1_UartPutString("\r\nblue - ");
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	UART_1_UartPutString("\r\nclear - ");
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                    	print_2hex(temp);
                    	I2C_1_I2CMasterSendStop(timeout);
                    }
    	            else UART_1_UartPutString("\r\nID bad");
                break;
                case 3:
                    status = I2C_1_I2CMasterSendStart(BH1730_ADDR, I2C_1_I2C_WRITE_XFER_MODE, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterWriteByte(0x92,timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterSendRestart(BH1730_ADDR, I2C_1_I2C_READ_XFER_MODE, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterReadByte(0, &temp, timeout);
                    print_stat_error(status);
                    status = I2C_1_I2CMasterSendStop(timeout);
                    print_stat_error(status);
                    if((temp & 0xf0) == 0x70) UART_1_UartPutString("\r\nID good");
    	            else
    	            {
    		            UART_1_UartPutString("\r\nID bad - ");
    		            print_2hex(temp);
    	            }
                break;
                case 4:
                    status = I2C_1_I2CMasterSendStart(BH1730_ADDR, I2C_1_I2C_READ_XFER_MODE, timeout);
                    print_stat_error(status);
                    for(uint8_t i=0; i<14; i++)
                    {
    	                temp = 0;
    	                UART_1_UartPutString("\r\nread ");
    	                print_2hex(i+1);
    	                UART_1_UartPutString(" - ");
    	                status = I2C_1_I2CMasterReadByte(I2C_1_I2C_ACK_DATA, &temp, timeout);
                        if(status != I2C_1_I2C_MSTR_NO_ERROR) UART_1_UartPutString("read error");
                        else print_2hex(temp);
                    }
                    status = I2C_1_I2CMasterSendStop(timeout);
                    print_stat_error(status);
                break;
                default: break;    
            }
            operate = 0;
            temp = 0;
        }
    }
}
void print_2hex(uint8_t data)
{
    uint8_t temp[2];
	temp[0] = (data&0xf0)>>4;
	temp[1] = data&0x0f;
    for(uint8_t i=0; i<2; i++)
    {
        if(temp[i] < 10) UART_1_UartPutChar(temp[i] + 0x30);
        else UART_1_UartPutChar(temp[i] + 0x57);
    }
}
void print_stat_error(uint32_t status)
{
    UART_1_UartPutString("\r\n");
    if(status != I2C_1_I2C_MSTR_NO_ERROR)
    {
    	switch(status)
    	{
    		case I2C_1_I2C_MSTR_ERR_ARB_LOST: UART_1_UartPutString("Master lost arbitration"); break;
    		case I2C_1_I2C_MSTR_ERR_LB_NAK: UART_1_UartPutString("Last Byte Naked"); break;
    		case I2C_1_I2C_MSTR_NOT_READY: UART_1_UartPutString("Master on the bus or Slave operation is in progress"); break;
    		case I2C_1_I2C_MSTR_BUS_BUSY: UART_1_UartPutString("Bus is busy, process not started"); break;
    		case I2C_1_I2C_MSTR_ERR_ABORT_START: UART_1_UartPutString("Slave was addressed before master begin Start gen"); break;
    		case I2C_1_I2C_MSTR_ERR_BUS_ERR: UART_1_UartPutString("Bus error has"); break;
    		case I2C_1_I2C_MSTR_ERR_TIMEOUT: UART_1_UartPutString("Operation timeout"); break;
    		default: UART_1_UartPutString("unknown"); break;
    	}
    }
    else UART_1_UartPutString("OK");
}
void i2c_scan(void)
{
    for(uint8_t i=0; i<128; i++)
    {
    	UART_1_UartPutString("\r\n0x");
    	print_2hex(i);
    	status = I2C_1_I2CMasterSendStart(i, I2C_1_I2C_READ_XFER_MODE, timeout);
    	print_stat_error(status);
    	status = I2C_1_I2CMasterReadByte(I2C_1_I2C_NAK_DATA, &temp, timeout);
    	print_stat_error(status);
    	status = I2C_1_I2CMasterSendStop(timeout);
        print_stat_error(status);
    }
}
