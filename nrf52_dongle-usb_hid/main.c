#include "system.h"
#include "print.h"
#include "uart_log.h"
#include "busby.h"
#include "SEGGER_RTT.h"
//
#define LED_DELAY		3000000
//#define RTT_EN
#define CNT				600
//
void main(void);
void USBD_IRQHandler(void);
void POWER_CLOCK_IRQHandler(void);
void segger_rtt_add_log(uint8_t* string);
void epin0_tx(void);
void ep0_tx(uint32_t data, uint16_t size);
//
typedef struct
{
	usb_device_descriptor_t dev_descriptor;
	usb_configuration_descriptor_t conf_descriptor;
	usb_interface_descriptor_t int_descriptor;
	usb_hid_descriptor_t hid_descriptor;
	uint8_t report_descriptor[38];
	usb_ep_descriptor_t ep_descriptor[2];
} descriptors_t;
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x00,
	.day = 0x17,
	.month = 0x04,
	.year = 0x19,
};
descriptors_t descriptors =
{
	.dev_descriptor.bLength = USB_DESC_SIZE_DEV,
	.dev_descriptor.bDescriptorType = USB_DESC_TYPE_DEVICE,
	.dev_descriptor.bcdUSB = 0x0200,
	.dev_descriptor.bDeviceClass = 0,
	.dev_descriptor.bDeviceSubClass = 0,
	.dev_descriptor.bDeviceProtocol = 0,
	.dev_descriptor.bMaxPacketSize = NRF_DRV_USBD_EPSIZE,
	.dev_descriptor.idVendor = 0xb01a,
	.dev_descriptor.idProduct = 0x0001,
	.dev_descriptor.bcdDevice = 0x0001,
	.dev_descriptor.iManufacturer = 1,
	.dev_descriptor.iProduct = 2,
	.dev_descriptor.iSerialNumber = 3,
	.dev_descriptor.bNumConfigurations = 1,
	.conf_descriptor.bLength = USB_DESC_SIZE_CONF,
	.conf_descriptor.bDescriptorType = USB_DESC_TYPE_CONFIGURATION,
	.conf_descriptor.wTotalLength = sizeof(descriptors) - USB_DESC_SIZE_DEV,
	.conf_descriptor.bNumInterfaces = 1,
	.conf_descriptor.bConfigurationValue = 1,
	.conf_descriptor.iConfiguration = 4,
	.conf_descriptor.bmAttributes.reserved0 = 0,
	.conf_descriptor.bmAttributes.remote_wakeup = 0,
	.conf_descriptor.bmAttributes.self_powered = 0,
	.conf_descriptor.bmAttributes.reserved1 = 1,
	.conf_descriptor.bMaxPower = 250,
	.int_descriptor.bLength = USB_DESC_SIZE_IFACE,
	.int_descriptor.bDescriptorType = USB_DESC_TYPE_INTERFACE,
	.int_descriptor.bInterfaceNumber = 0,
	.int_descriptor.bAlternateSetting = 0,
	.int_descriptor.bNumEndpoints = 2,
	.int_descriptor.bInterfaceClass = 3,//HID
	.int_descriptor.bInterfaceSubClass = 0,//no
	.int_descriptor.bInterfaceProtocol = 0,//no
	.int_descriptor.iInterface = 5,
	.hid_descriptor.bLength = USB_DESC_SIZE_HID_DEV + sizeof(descriptors.report_descriptor),
	.hid_descriptor.bDescriptorType = USB_DESC_TYPE_HID_DEV,
	.hid_descriptor.bcdHID = 0x1101,
	.hid_descriptor.bCountryCode = 0,
	.hid_descriptor.bNumDescriptors = 1,
	.hid_descriptor.bDescriptorType2 = USB_DESC_TYPE_HID_REPORT,
	.hid_descriptor.wItemLength = sizeof(descriptors.report_descriptor),
	.report_descriptor =
	{
		0x06, 0x00, 0xff,
		0x09, 0x01,
		0xa1, 0x01,
		0x19, 0x01,
		0x29, 0x08,
		0x15, 0x00,
		0x26, 0xff, 0x00,
		0x75, 0x08,
		0x95, 0x08,
		0x91, 0x02,
		0x19, 0x01,
		0x29, 0x08,
		0x15, 0x00,
		0x26, 0xff, 0x00,
		0x75, 0x08,
		0x95, 0x08,
		0x81, 0x02,
		0xc0
	},
	.ep_descriptor[0].bLength = USB_DESC_SIZE_EP,
	.ep_descriptor[0].bDescriptorType = USB_DESC_TYPE_EP,
	.ep_descriptor[0].bEndpointAddress.ep_number = 1,
	.ep_descriptor[0].bEndpointAddress.reserved = 0,
	.ep_descriptor[0].bEndpointAddress.direction = 1,
	.ep_descriptor[0].bmAttributes.transfer_type = USB_EP_TYPE_INT,
	.ep_descriptor[0].bmAttributes.only_isoc = 0,
	.ep_descriptor[0].wMaxPacketSize = 0x0008,
	.ep_descriptor[0].bInterval = 10,
	.ep_descriptor[1].bLength = USB_DESC_SIZE_EP,
	.ep_descriptor[1].bDescriptorType = USB_DESC_TYPE_EP,
	.ep_descriptor[1].bEndpointAddress.ep_number = 2,
	.ep_descriptor[1].bEndpointAddress.reserved = 0,
	.ep_descriptor[1].bEndpointAddress.direction = 0,
	.ep_descriptor[1].bmAttributes.transfer_type = USB_EP_TYPE_INT,
	.ep_descriptor[1].bmAttributes.only_isoc = 0,
	.ep_descriptor[1].wMaxPacketSize = 0x0008,
	.ep_descriptor[1].bInterval = 10,
};
volatile uint_fast8_t usb_flags = 0;
extern SEGGER_RTT_CB _SEGGER_RTT;
volatile uint16_t counter = 0;
usb_setup_req_t req[CNT] = {0};
uint8_t usb_tx_data[8];
volatile usb_device_t usb_dev =
{
	.state = dev_unconnected,
	.ep0_tx_size = 0,
	.active_conf = 0,
	.active_future = 0,
	.descr_ptr = &descriptors,
};
//
void main(void)
{
	//High voltage mode
	//both LDO
	if(NRF_POWER->MAINREGSTATUS & POWER_MAINREGSTATUS_MAINREGSTATUS_High)
	{
	    //Configure UICR_REGOUT0 register only if it is set to default value.
	    if ((NRF_UICR->REGOUT0 & UICR_REGOUT0_VOUT_Msk) == (UICR_REGOUT0_VOUT_DEFAULT << UICR_REGOUT0_VOUT_Pos))
	    {
	        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
	        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	        NRF_UICR->REGOUT0 = (NRF_UICR->REGOUT0 & ~((uint32_t)UICR_REGOUT0_VOUT_Msk))|(UICR_REGOUT0_VOUT_3V0 << UICR_REGOUT0_VOUT_Pos);
	        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
	        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){};
	        //System reset is needed to update UICR registers.
	        NVIC_SystemReset();
	    }
	}
	//start HFXO
	if((NRF_CLOCK->HFCLKSTAT & CLOCK_HFCLKSTAT_SRC_Xtal) == 0)
	{
		NRF_CLOCK->TASKS_HFCLKSTART = 1;
		while((NRF_CLOCK->HFCLKSTAT & CLOCK_HFCLKSTAT_SRC_Xtal) == 0){};
	}
#ifdef RTT_EN
	SEGGER_RTT_Init();
#endif
	segger_rtt_add_log("\r\nRTT log start");
	//led1
	NRF_P0->PIN_CNF[6] = (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos)|(GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)|(GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)|
		(GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
	//led2_r
	NRF_P0->PIN_CNF[8] = (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos)|(GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)|(GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)|
		(GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
	//led2_b
	NRF_P0->PIN_CNF[12] = (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos)|(GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)|(GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)|
		(GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
	//led2_g
	NRF_P1->PIN_CNF[9] = (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos)|(GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)|
		(GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)|(GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)|
		(GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
	//start USB
	NRF_POWER->INTENSET = (POWER_INTENSET_USBDETECTED_Msk|POWER_INTENSET_USBPWRRDY_Msk);
	NVIC_EnableIRQ(POWER_CLOCK_IRQn);
	NRF_USBD->INTENSET = (USBD_INTENSET_EP0SETUP_Msk|USBD_INTENSET_EP0DATADONE_Msk);
	NVIC_EnableIRQ(USBD_IRQn);
	while((usb_flags & 1) == 0){};
	usb_flags = 0;
	NRF_USBD->EVENTCAUSE &= ~USBD_EVENTCAUSE_READY_Msk;
	//errata 187
    if (*((volatile uint32_t *)(0x4006EC00)) == 0x00000000)
    {
    	*((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
        *((volatile uint32_t *)(0x4006ED14)) = 0x00000003;
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
    }
    else *((volatile uint32_t *)(0x4006ED14)) = 0x00000003;
    //errata 171
    if (*((volatile uint32_t *)(0x4006EC00)) == 0x00000000)
    {
    	*((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
        *((volatile uint32_t *)(0x4006EC14)) = 0x000000C0;
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
    }
    else *((volatile uint32_t *)(0x4006EC14)) = 0x000000C0;
	NRF_USBD->ENABLE = 1;
	while((NRF_USBD->EVENTCAUSE & USBD_EVENTCAUSE_READY_Msk) == 0){};
	NRF_USBD->EVENTCAUSE = USBD_EVENTCAUSE_READY_Msk;
	//errata 171
    if (*((volatile uint32_t *)(0x4006EC00)) == 0x00000000)
    {
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
        *((volatile uint32_t *)(0x4006EC14)) = 0x00000000;
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
    }
    else *((volatile uint32_t*)(0x4006EC14)) = 0x00000000;
    //errata 187
    if (*((volatile uint32_t *)(0x4006EC00)) == 0x00000000)
    {
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
        *((volatile uint32_t *)(0x4006ED14)) = 0x00000000;
        *((volatile uint32_t *)(0x4006EC00)) = 0x00009375;
    }
    else *((volatile uint32_t *)(0x4006ED14)) = 0x00000000;
	while((usb_flags & 2) == 0){};
	usb_flags = 0;
	NRF_USBD->USBPULLUP = 1;
	NRF_USBD->SIZE.EPOUT[0] = descriptors.dev_descriptor.bMaxPacketSize;
	NRF_P0->OUTSET = (1<<8)|(1<<12);
	NRF_P1->OUTSET = (1<<9);
	NRF_P0->OUTCLR = (1<<6);
	//enumeration...
	while(1)
	{
		/*NRF_P1->OUTCLR = (1<<9);
		delay(LED_DELAY);
		NRF_P0->OUTSET = (1<<12);
		delay(LED_DELAY);
		NRF_P0->OUTCLR = (1<<8);
		delay(LED_DELAY);
		NRF_P1->OUTSET = (1<<9);
		delay(LED_DELAY);
		NRF_P0->OUTCLR = (1<<12);
		delay(LED_DELAY);
		NRF_P0->OUTSET = (1<<8);
		delay(LED_DELAY);*/
	}
}
void USBD_IRQHandler(void)
{
	uint8_t string[11];
	uint16_t temp_loc;
	usb_setup_req_t std_req;
	if(NRF_USBD->EVENTS_EP0SETUP)
	{
		NRF_USBD->EVENTS_EP0SETUP = 0;
		std_req.bmRequestType.raw = NRF_USBD->BMREQUESTTYPE;
		std_req.bmRequest = NRF_USBD->BREQUEST;
		std_req.wIndex = (NRF_USBD->WINDEXH << 8)|NRF_USBD->WINDEXL;
		std_req.wLength = (NRF_USBD->WLENGTHH <<8)|NRF_USBD->WLENGTHL;
		std_req.wValue = (NRF_USBD->WVALUEH <<8)|NRF_USBD->WVALUEL;
		if(counter < CNT)
		{
			req[counter].bmRequestType.raw = std_req.bmRequestType.raw;
			req[counter].bmRequest = std_req.bmRequest;
			req[counter].wIndex = std_req.wIndex;
			req[counter].wLength = std_req.wLength;
			req[counter++].wValue = std_req.wValue;
		}
		else NRF_P0->OUTCLR = (1<<12);
		segger_rtt_add_log("\r\nbmRequestType: ");
		switch(std_req.bmRequestType.bits.destination)
		{
			case DEST_DEVICE:
				segger_rtt_add_log("device");
				segger_rtt_add_log("\r\nbRequest: ");
				switch (std_req.bmRequest)
				{
					case SET_DESCRIPTOR:
						__attribute__ ((fallthrough));
					case GET_DESCRIPTOR:
						segger_rtt_add_log("set/get descriptor: ");
						switch(std_req.wValue >> 8)
						{
							case USB_DESC_TYPE_DEVICE:
								temp_loc = std_req.wLength;
								if(descriptors.dev_descriptor.bMaxPacketSize < temp_loc) temp_loc = descriptors.dev_descriptor.bMaxPacketSize;
								NRF_USBD->EPIN[0].MAXCNT = temp_loc;
								ep0_tx((uint32_t)&descriptors.dev_descriptor, temp_loc);
								segger_rtt_add_log("device");
							break;
							case USB_DESC_TYPE_CONFIGURATION:
								if(descriptors.conf_descriptor.wTotalLength < std_req.wLength) temp_loc = descriptors.conf_descriptor.wTotalLength;
								else temp_loc = std_req.wLength;
								ep0_tx((uint32_t)&descriptors.conf_descriptor, temp_loc);
								segger_rtt_add_log("configuration");
							break;
							default:
								segger_rtt_add_log("unknown");
							break;
						}
					break;
					case GET_STATUS:
						usb_tx_data[0] = (descriptors.conf_descriptor.bmAttributes.remote_wakeup << 1)|
							descriptors.conf_descriptor.bmAttributes.self_powered;
						usb_tx_data[1] = 0;
						ep0_tx((uint32_t)usb_tx_data, 2);
					break;
					case SET_ADDRESS:
						usb_dev.state = dev_addressed;
						segger_rtt_add_log("set address");
					break;
					case CLEAR_FEATURE:
						switch(std_req.wValue)
						{
							case 1://DEVICE_REMOTE_WAKEUP
								usb_dev.active_future &= 0xfd;
							break;
							case 2://TEST_MODE
								usb_dev.active_future &= 0xfb;
							break;
						}
						NRF_USBD->TASKS_EP0STATUS = 1;
					break;
					case SET_FEATURE:
						switch(std_req.wValue)
						{
							case 1://DEVICE_REMOTE_WAKEUP
								usb_dev.active_future |= 2;
							break;
							case 2://TEST_MODE
								usb_dev.active_future |= 4;
							break;
						}
						NRF_USBD->TASKS_EP0STATUS = 1;
					break;
					case GET_CONFIGURATION:
						ep0_tx((uint32_t)&usb_dev.active_conf, 1);
					break;
					case SET_CONFIGURATION:
						usb_dev.active_conf = std_req.wValue & 0xff;
						NRF_USBD->TASKS_EP0STATUS = 1;
					break;
					default:
						segger_rtt_add_log("unknown");
					break;
				}
				break;
			default:
				segger_rtt_add_log("unknown");
			break;
		}
	}
	if(NRF_USBD->EVENTS_EP0DATADONE)
	{
		NRF_USBD->EVENTS_EP0DATADONE = 0;
		if(usb_dev.ep0_tx_size != 0) ep0_tx(usb_dev.ep0_tx_ptr, usb_dev.ep0_tx_size);
		else NRF_USBD->TASKS_EP0STATUS = 1;
	}
}
void POWER_CLOCK_IRQHandler(void)
{
	if(NRF_POWER->EVENTS_USBDETECTED)
	{
		NRF_POWER->EVENTS_USBDETECTED = 0;
		usb_flags = 1;
	}
	if(NRF_POWER->EVENTS_USBPWRRDY)
	{
		NRF_POWER->EVENTS_USBPWRRDY = 0;
		usb_flags |= 2;
	}
}
void segger_rtt_add_log(uint8_t* string)
{
#ifdef RTT_EN
	SEGGER_RTT_WriteString(0, string);
#endif
}
void ep0_tx(uint32_t data, uint16_t size)
{
	usb_dev.ep0_tx_size = size;
	NRF_USBD->EPIN[0].PTR = data;
	if(usb_dev.ep0_tx_size > descriptors.dev_descriptor.bMaxPacketSize)
	{
		usb_dev.ep0_tx_size = usb_dev.ep0_tx_size - descriptors.dev_descriptor.bMaxPacketSize;
		NRF_USBD->EPIN[0].MAXCNT = descriptors.dev_descriptor.bMaxPacketSize;
		usb_dev.ep0_tx_ptr = data + descriptors.dev_descriptor.bMaxPacketSize;
	}
	else
	{
		NRF_USBD->EPIN[0].MAXCNT = usb_dev.ep0_tx_size;
		usb_dev.ep0_tx_size = 0;
	}
	//epin0_tx
	*(volatile uint32_t *)0x40027C1C = 0x00000082;
	NRF_USBD->TASKS_STARTEPIN[0] = 1;
	while(NRF_USBD->EVENTS_ENDEPIN[0] = 0){};
	NRF_USBD->EVENTS_ENDEPIN[0] = 0;
	*(volatile uint32_t *)0x40027C1C = 0x00000000;
}
