#include "system.h"
#include "lcd.h"
#include "gfx.h"
//
void main(void);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x27,
	.month = 0x09,
	.year = 0x18
};
uint16_t data;
uint32_t ms;
//
uint8_t gui_memory[1000];
//
systemticks_t gfxSystemTicks(void)
{
	return SysTick->VAL;
}
systemticks_t gfxMillisecondsToTicks(delaytime_t ms)//1 tick = 1 ms
{
	return ms;
}
void main(void)
{
	sys_init();
	fpu_on();
	init_gpio();
	lcd_init(C_DarkGoldenRod);
	SysTick_Config(168000);//uGFX
	SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
	gfxInit();
	gdispDrawBox(10, 10, 20, 20, 0);
	while(1)
	{}
	//GDISPVMT_OnlyOne
}
