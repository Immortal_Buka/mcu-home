#include "system.h"
#include "lcd.h"
#include "color.h"
//
#define _RNG				0
#define _ADC_DMA_UART		0
#define _PWM_RGB_LED		0
//
void main(void);
void DMA2_Stream0_IRQHandler(void);
void draw_rng_rect(void);
void TIM4_IRQHandler(void);
void USART1_IRQHandler(void);
//
uint8_t mutex = 0, adc_result;
uint16_t adc_mean;
uint16_t* adc_ptr;
uint16_t adc_buf[512];
uint8_t string[11] = {0};
hsv_t led_hsv =
{
	.color.h = 0,
	.color.s = 0,
	.color.v = 0,
};
rgb_888_t led_rgb;
rgb_565_t lcd_color;
//
void main(void)
{
	fpu_on();
	init_gpio();
	lcd_init(C_Black);
	//PA9-TX, PA10-RX
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->OSPEEDR |= (3<<(9*2))|(3<<(10*2));
	GPIOA->AFR[1] |= (7<<4)|(7<<8);
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->BRR = (45<<4)|9;//115200
	USART1->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE|USART_CR1_RXNEIE;
	NVIC_EnableIRQ(USART1_IRQn);
#if _PWM_RGB_LED
	//gpio
	//B4-CH1-R, A7-CH2-G, C8-CH3-B
	GPIOB->MODER |= (2 << (4*2));
	GPIOB->AFR[0] |= (2 << 16);
	GPIOA->MODER |= (2 << (7*2));
	GPIOA->AFR[0] |= (2 << 28);
	GPIOC->MODER |= (2 << (8*2));
	GPIOC->AFR[1] |= 2;
	//tim3
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->ARR = 65026;
	TIM3->PSC = 1;
	TIM3->CCMR1 |= 0x6868;
	TIM3->CCMR2 |= 0x0068;
	TIM3->CCR1 = 0 * 255;
	TIM3->CCR2 = 0 * 255;
	TIM3->CCR3 = 0 * 255;
	TIM3->CCER |= 0x0111;
	TIM3->CR1 |= (TIM_CR1_CEN|TIM_CR1_ARPE);
#endif
#if _RNG
	rng_init();
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	TIM4->ARR = 1999;
	TIM4->PSC = 4199;//9;
	TIM4->DIER = 1;
	TIM4->CR1 = (TIM_CR1_CEN|TIM_CR1_ARPE);
	NVIC_EnableIRQ(TIM4_IRQn);
#endif
#if _ADC_DMA_UART
	//uart
	uart1_init();
	uart_print_string(USART1, "\n\rStart!");
	//dma
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	DMA2_Stream0->PAR = (uint32_t)&ADC1->DR;
	DMA2_Stream0->M0AR = (uint32_t)&adc_buf[0];
	DMA2_Stream0->NDTR = 512;
	DMA2_Stream0->CR = DMA_SxCR_MSIZE_0|DMA_SxCR_MINC|DMA_SxCR_PSIZE_0|DMA_SxCR_CIRC|DMA_SxCR_TCIE|DMA_SxCR_EN|DMA_SxCR_HTIE;
	NVIC_EnableIRQ(DMA2_Stream0_IRQn);
	//adc
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	ADC->CCR = ADC_CCR_ADCPRE_0|ADC_CCR_TSVREFE;
	ADC1->CR1 = ADC_CR1_SCAN;
	ADC1->SQR3 = 18;
	ADC1->CR2 = ADC_CR2_EXTSEL_0|ADC_CR2_EXTSEL_1|ADC_CR2_EXTEN_0|ADC_CR2_ADON|ADC_CR2_DMA|ADC_CR2_DDS|ADC_CR2_EOCS;
	tim_init();
	mutex = 0;
	//tim2
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	TIM2->PSC = 255;//84mHz/(255+1) = 328125;
	TIM2->ARR = 328124;
	TIM2->CCR2 = 164062;
	TIM2->CCMR1 |= 0x6800;
	TIM2->CCER |= 0x10;
	TIM2->CR1 |= (TIM_CR1_CEN|TIM_CR1_ARPE);
#endif
	while(1)
	{
#if _RNG
		while((GPIOA->IDR & 1) != 0){};
		while((GPIOA->IDR & 1) == 0){};
		draw_rng_rect();
#endif
#if _ADC_DMA_UART
		if(mutex == 1)
		{
			arm_mean_q15(adc_ptr, 256, &adc_mean);
			adc_result = (uint8_t)(((3000.0f * (float32_t)adc_mean)/4095.0f - 760.0f)/2.5f + 25.0f);
			uint8_to_string(adc_result, string);
			uart_print_string(USART1, string);
			mutex = 0;
		}
#endif
	}
}
void DMA2_Stream0_IRQHandler(void)
{
	if(DMA2->LISR & DMA_LISR_HTIF0)
	{
		adc_ptr = adc_buf;
		mutex = 1;
		DMA2->LIFCR = DMA_LIFCR_CHTIF0;
	}
	if(DMA2->LISR & DMA_LISR_TCIF0)
	{
		adc_ptr = &adc_buf[256];
		mutex = 1;
		DMA2->LIFCR = DMA_LIFCR_CTCIF0;
	}
}
void TIM4_IRQHandler(void)
{
	TIM4->SR = 0;
	//draw_rng_rect();
	HSV_to_RGB(&led_hsv, &led_rgb);
	/*TIM3->CCR1 = led_rgb.r * 255;
	TIM3->CCR2 = led_rgb.g * 255;
	TIM3->CCR3 = led_rgb.b * 255;*/
	RGB_888_to_565(&led_rgb, &lcd_color);
	screen_color(lcd_color.raw);
	led_hsv.color.h++;
}
void draw_rng_rect(void)
{
	my_point p1,p2;
	uint32_t temp1 = rng_random_data();
	p1.x = temp1 & 0x7f;
	p2.x = p1.x + ((temp1>>7) & 0x3f);
	p1.y = ((temp1>>13)&0xff);
	p2.y = p1.y + ((temp1>>21) & 0x3f);
	temp1 = rng_random_data();
	lcd_draw_rectangle(&p1, &p2, (temp1&0xffff));
}
void USART1_IRQHandler(void)
{
	if(USART1->SR & USART_SR_RXNE)
	{
		lcd_print_char(USART1->DR, C_White);
	}
}
