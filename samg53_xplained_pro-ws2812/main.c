#include "system.h"
#include "print.h"
#include "color.h"
//
#define WS_Q		32
//
void main (void);
void uart_print_char(void* uart, uint8_t data);
void UART0_Handler(void);
void TC3_Handler(void);
void rgb_to_ws(rgb_888_t* rgb, uint8_t* ws);
void hsv_to_ws(hsv_t* hsv, uint8_t* ws);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x12,
	.month = 0x03,
	.year = 0x19
};
hsv_t led_hsv[2][WS_Q] = {0};
uint8_t active = 0, target = 0;
//
void main(void)
{
	fpu_on();
	for(uint8_t i=0; i<WS_Q; i++)
	{
		led_hsv[0][i].color.s = 255;
		led_hsv[0][i].color.v = 5;
		led_hsv[0][i].color.h = 10*i;
		led_hsv[1][i].color.v = 0;
		led_hsv[1][i].color.h = 290;
		led_hsv[0][i].color.s = 255;
	}
	led_hsv[1][active].color.v = 20;
	PMC->PMC_PCER0 |= PMC_PCER0_PID8|//uart0
			PMC_PCER0_PID11|//pioa
			PMC_PCER0_PID21|//spi
			PMC_PCER0_PID26;//tc3
	//a9-rx, a10-tx, a13-mosi, a16-led
	PIOA->PIO_PDR = (1<<9)|(1<<10)|(1<<13);
	PIOA->PIO_PER = (1<<16)|(1<<11);
	PIOA->PIO_OER = (1<<16)|(1<<11);
	PIOA->PIO_SODR = 1<<16;
	//spi + dma + tc3
	SPI->SPI_MR = SPI_MR_MSTR|SPI_MR_MODFDIS;
	SPI->SPI_CSR[0] = (19<<8);//48M*400n
	SPI->SPI_CR = SPI_CR_SPIEN;
	PDC_SPI->PERIPH_PTCR = PERIPH_PTCR_TXTEN;
	TC1->TC_CHANNEL[0].TC_CMR = 4|TC_CMR_WAVE|(2<<13);
	TC1->TC_CHANNEL[0].TC_RC = 326;
	TC1->TC_CHANNEL[0].TC_IER = TC_IER_CPCS;
	NVIC_ClearPendingIRQ(TC3_IRQn);
	NVIC_EnableIRQ(TC3_IRQn);
	TC1->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN|TC_CCR_SWTRG;
	//uart0
	UART0->UART_CR = UART_CR_RSTRX|UART_CR_RSTTX|UART_CR_RXDIS|UART_CR_TXDIS;
	UART0->UART_BRGR = 26;
	UART0->UART_MR = (4<<9);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);
	UART0->UART_IER = UART_IER_RXRDY;
	UART0->UART_CR = UART_CR_RXEN|UART_CR_TXEN;
	//
	PIOA->PIO_CODR = 1<<16;
	while(1)
	{
		delay(20000);
		for(uint8_t i=0; i<WS_Q; i++) led_hsv[0][i].color.h++;
		if(active < target)
		{
			if(led_hsv[1][active].color.v != 0)
			{
				led_hsv[1][active].color.v--;
				if(active < 15)led_hsv[1][active+1].color.v++;
			}
			else active++;
		}
		if(active > target)
		{
			if(led_hsv[1][active].color.v != 0)
			{
				led_hsv[1][active].color.v--;
				if(active > 0) led_hsv[1][active-1].color.v++;
			}
			else active--;
		}
	}
}
void uart_print_char(void* uart, uint8_t data)
{
	Uart* periph = (Uart*)uart;
	while((periph->UART_SR & UART_SR_TXRDY) == 0){};
	periph->UART_THR = data;
}
void UART0_Handler(void)
{
	uint8_t loc_temp = UART0->UART_RHR;
	uart_print_char(UART0, loc_temp);
	//if((loc_temp > 0x60)&&(loc_temp < 'q')) target = loc_temp - 0x61;
	switch(loc_temp)
	{
		case '1': if(led_hsv[1][active].color.v < 246) led_hsv[1][active].color.v += 10; break;
		case '2': if(led_hsv[1][active].color.v > 9) led_hsv[1][active].color.v -= 10; break;
		case '3': if(led_hsv[1][active].color.s < 246) led_hsv[1][active].color.s += 10; break;
		case '4': if(led_hsv[1][active].color.s > 9) led_hsv[1][active].color.s -= 10; break;
		case '5': for(uint8_t i=0; i<WS_Q; i++) led_hsv[1][i].color.h += 10; break;
		case '6':
			for(uint8_t i=0; i<WS_Q; i++)
			{
				if(led_hsv[1][i].color.h > 9) led_hsv[1][i].color.h -= 10;
				else led_hsv[1][i].color.h = 350;
			}
		break;
		case '7': for(uint8_t i=0; i<WS_Q; i++) if(led_hsv[0][i].color.v < 251) led_hsv[0][i].color.v += 5; break;
		case '8': for(uint8_t i=0; i<WS_Q; i++) if(led_hsv[0][i].color.v > 4) led_hsv[0][i].color.v -= 5;
		case '9': for(uint8_t i=0; i<WS_Q; i++) if(led_hsv[0][i].color.s < 251) led_hsv[0][i].color.s += 5; break;
		case '0': for(uint8_t i=0; i<WS_Q; i++) if(led_hsv[0][i].color.s > 4) led_hsv[0][i].color.s -= 5;
		case 'a': target = 0; break;
		case 'b': target = 1; break;
		case 'c': target = 2; break;
		case 'd': target = 3; break;
		case 'e': target = 4; break;
		case 'f': target = 5; break;
		case 'g': target = 6; break;
		case 'h': target = 7; break;
		case 'i': target = 8; break;
		case 'j': target = 9; break;
		case 'k': target = 10; break;
		case 'l': target = 11; break;
		case 'm': target = 12; break;
		case 'n': target = 13; break;
		case 'o': target = 14; break;
		case 'p': target = 15; break;
     }
}
void TC3_Handler(void)
{
	static uint8_t to_ws[WS_Q][9] = {0};
	if(TC1->TC_CHANNEL[0].TC_SR & TC_IER_CPCS)
	{
		//PIOA->PIO_CODR = 1<<11;
		for(uint8_t i=0; i<WS_Q; i++) hsv_to_ws(&led_hsv[0][i], to_ws[i]);
		PDC_SPI->PERIPH_TPR = (uint32_t)to_ws;
		PDC_SPI->PERIPH_TCR = sizeof(to_ws);
		/*while(PDC_SPI->PERIPH_TCR > 0){};
		delay(100);
		PIOA->PIO_SODR = 1<<11;
		for(uint8_t i=0; i<WS_Q; i++) hsv_to_ws(&led_hsv[1][i], to_ws[i]);
		PDC_SPI->PERIPH_TPR = (uint32_t)to_ws;
		PDC_SPI->PERIPH_TCR = sizeof(to_ws);*/
	}
}
void rgb_to_ws(rgb_888_t* rgb, uint8_t* ws)
{
	for(uint8_t i=0; i<3; i++)
	{
		ws[0+3*i] = 0x24;
		if((rgb->raw[i] & 0x80) == 0) ws[0+3*i] |= 0x40;
		if((rgb->raw[i] & 0x40) == 0) ws[0+3*i] |= 8;
		if((rgb->raw[i] & 0x20) == 0) ws[0+3*i] |= 1;
		ws[1+3*i] = 0x92;
		if((rgb->raw[i] & 0x10) == 0) ws[1+3*i] |= 0x20;
		if((rgb->raw[i] & 8) == 0) ws[1+3*i] |= 4;
		ws[2+3*i] = 0x49;
		if((rgb->raw[i] & 4) == 0) ws[2+3*i] |= 0x80;
		if((rgb->raw[i] & 2) == 0) ws[2+3*i] |= 0x10;
		if((rgb->raw[i] & 1) == 0) ws[2+3*i] |= 2;
	}
}
void hsv_to_ws(hsv_t* hsv, uint8_t* ws)
{
	rgb_888_t temp_loc;
	HSV_to_RGB_int(hsv, &temp_loc);
	rgb_to_ws(&temp_loc, ws);
}
