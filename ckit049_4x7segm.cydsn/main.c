#include "project.h"
#include <stdio.h>   
//
const uint8_t DIG[18] = 
{
    0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 
    0x82, 0xf8, 0x80, 0x90, 0x88, 0x83,
    0xc6, 0xa1, 0x86, 0x8e, 0x7f, 0xff//dp, off
};
/*
01234567
segments
0 - off dp
1 - off g
2 - off f
3 - off e
4 - off d
5 - off c
6 - off b
7 - off a
*/ 
uint8_t digit[8] = {0};
/*
0 - UP_3 L
1 - UP_2
2 - UP_1
3 - UP_0 R
4 - DOWN_3 L
5 - DOWN_2
6 - DOWN_1
7 - DOWN_0 R
*/
//
CY_ISR(tim_irq_handler)
{
    static uint8_t dig_counter = 0;
    uint16_t l_temp = 0;
    TIM_1ms_ClearInterrupt(TIM_1ms_INTR_MASK_TC);
    l_temp = (digit[dig_counter]<<8)|(1<<dig_counter);
    SPI_SpiUartWriteTxData(l_temp);
    if(dig_counter > 6) dig_counter = 0;
    else dig_counter++;
}
CY_ISR(sw1_irq_handler)
{
    static uint8_t number = 0;
    sw1_in_ClearInterrupt();
    for(uint8_t i=0; i<7; i++) digit[i] = DIG[number+1];
    if(number < 16) number++;
    else number = 0;
}
int main(void)
{
    CyGlobalIntEnable;
    SPI_Start();
    for(uint8_t i=0; i<7; i++) digit[i] = DIG[17];
    TIM_IRQ_StartEx(tim_irq_handler);
    SW1_IRQ_StartEx(sw1_irq_handler);
    TIM_1ms_Start();
    led_Write(1);
    while(1)
    {}
}
