#include <project.h>
#define MUSIC_COUNTER   8
typedef struct 
{
    uint8_t repeat;
    uint16_t lenght;
    uint32_t pointer;
}music_descriptor;
//(clk/prescaler)/note_freq => 12M/16=750k	
const uint16_t music_1[4] = {5734,2700,0,0};//включение прибора
const uint16_t music_2[5] = {1434,0,0,0,0};//прибор не подключён (кажд 4 сек)
const uint16_t music_3[3] = {4054,0,0};//прибор присоединён
const uint16_t music_4[3] = {852,0,0};//разрыв соединения
const uint16_t music_5[3] = {3826,0,0};//тест громкости
const uint16_t music_6[3] = {4054,0,0};//светодиоды включеы (кажд 2 сек)
const uint16_t music_7[3] = {11466,0,0};//получены настройки
const uint16_t music_8[3] = {2700,0,0};//ошибка слэйва
music_descriptor descriptors[MUSIC_COUNTER] = 
{
    {0,4,(uint32_t)&music_1},
    {1,5,(uint32_t)&music_2},
    {0,3,(uint32_t)&music_3},
    {0,3,(uint32_t)&music_4},
    {0,3,(uint32_t)&music_5},
    {1,3,(uint32_t)&music_6},
    {0,3,(uint32_t)&music_7},
    {0,3,(uint32_t)&music_8}    
};
uint8_t music_number, music_work, rx_char, temp1;
uint16_t music_counter;
uint8_t chars[3] = {0,'\r','\n'};
CYBLE_CONN_HANDLE_T  connectionHandle;
//
CY_ISR(halfsec_irq_handler);
int main(void);
void ble_event_handler(uint32 event, void * eventParam);
//
CY_ISR(halfsec_irq_handler)
{
    timer_halfsec_ClearInterrupt(timer_halfsec_INTR_MASK_TC);
    if(music_number > 0)
	{
		music_work = music_number;
		music_number = 0;
		music_counter = 0;
	}
    if(music_work > 0)
    {
        uint16_t temp = *(uint16_t *)(descriptors[music_work-1].pointer + music_counter*2);
        pwm_sound_WritePeriod(temp);
        pwm_sound_WriteCompare(temp>>1);
        music_counter++;
        if(music_counter == descriptors[music_work-1].lenght)
        {
            if(descriptors[music_work-1].repeat) music_number = music_work;
            music_work = 0;
            pwm_sound_WriteCompare(0);
        }
    }
}
int main(void)
{
    CyGlobalIntEnable;
    uart_debug_Start();
    idac_vol_Start();
    idac_vol_SetValue(200);
    pwm_sound_Start();
    pwm_sound_WriteCompare(0);
    irq_0_StartEx(halfsec_irq_handler);
    timer_halfsec_Start();
    led_green_Write(0);
    led_control_Write(1);
    led_red_Write(1);
    music_number = 0;
    music_counter = 0;
    music_work = 0;
    CyBle_Start(ble_event_handler);
    while(1)
    {
        CyBle_ProcessEvents();
        /*rx_char = 0;
        uart_debug_UartPutString("Sound number(1-...): ");
        while(rx_char == 0) rx_char = uart_debug_UartGetChar();
        chars[0] = rx_char;
        uart_debug_UartPutString(chars);
        temp1 = rx_char - 0x30;
        if(temp1 < (MUSIC_COUNTER+1)) music_number = temp1;*/
    }
}
void ble_event_handler(uint32 event, void * eventParam)
{
    CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;
    uint8_t data;
	switch(event)
    {
		case CYBLE_EVT_STACK_ON:
            uart_debug_UartPutString("bluetooth stack ON\r\n");
            if(CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST) == CYBLE_ERROR_OK) 
                uart_debug_UartPutString("start advertisement OK\r\n");
            else uart_debug_UartPutString("start advertisement FAIL\r\n");
            break;          
        case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
            if(CyBle_GetState() == CYBLE_STATE_ADVERTISING) uart_debug_UartPutString("advertising ON\r\n");                    
            else uart_debug_UartPutString("advertising OFF\r\n");
            break;                     
        case CYBLE_EVT_GATT_CONNECT_IND:
            uart_debug_UartPutString("gatt connect\r\n");
            connectionHandle = *(CYBLE_CONN_HANDLE_T *)eventParam;
            break;            
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            uart_debug_UartPutString("gap device disconnected\r\n");
            connectionHandle.bdHandle=0;
            if(CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST) == CYBLE_ERROR_OK) 
                uart_debug_UartPutString("start advertisement OK\r\n");
            else uart_debug_UartPutString("start advertisement FAIL\r\n");
            break;    
        case CYBLE_EVT_GATTS_WRITE_REQ:
            wrReqParam =(CYBLE_GATTS_WRITE_REQ_PARAM_T*) eventParam;
            uart_debug_UartPutString("gatts write request: ");
            data = *wrReqParam->handleValPair.value.val;
            if(wrReqParam->handleValPair.attrHandle == CYBLE_TEST_SERVICE_VOLUME_CHAR_HANDLE)
            {
                uart_debug_UartPutString("set volume - ");
                if(data < (MUSIC_COUNTER + 1)) music_number = data;
            }
            else uart_debug_UartPutString("unknown - ");
            uart_debug_UartPutString(chars);
            CyBle_GattsWriteRsp(connectionHandle);
            break;
        case CYBLE_EVT_GATTS_WRITE_CMD_REQ:
            wrReqParam =(CYBLE_GATTS_WRITE_REQ_PARAM_T*) eventParam;
            uart_debug_UartPutString("gatts write command: ");
            if(CyBle_GattsWriteAttributeValue(&wrReqParam->handleValPair, 0u, &wrReqParam->connHandle, 
                CYBLE_GATT_DB_PEER_INITIATED) == CYBLE_GATT_ERR_NONE)
            {
                data = *wrReqParam->handleValPair.value.val;
                if(wrReqParam->handleValPair.attrHandle == CYBLE_TEST_SERVICE_VOLUME_CHAR_HANDLE)
                {
                    uart_debug_UartPutString("set volume - ");
                    if(data < (MUSIC_COUNTER + 1)) music_number = data;
                }
                else uart_debug_UartPutString("unknown - ");
                uart_debug_UartPutString(chars);
                CyBle_GattsWriteRsp(connectionHandle);
            }
            else uart_debug_UartPutString ("GATT ERROR\r\n");
            break;
        default:
            break;
	}
}
