#include "system.h"
#include "lcd.h"
#include "print.h"
//
#define TEXT_COLOR		C_YellowGreen
//
void main(void);
//
uint8_t temp_1, string[11];
float64_t a;
//
void main(void)
{
	init_gpio();
	lcd_init(C_Black);
	i2c3_init();
	lcd_print_string("\r\ni2c scanning have been started", TEXT_COLOR);
	for(uint16_t i=0; i<256; i++)
	{
		if(i2c3_rd(i, 0, 1, &temp_1) == 0)
		{
			lcd_print_string("\r\nnew device have been found. address: ", TEXT_COLOR);
			uint8_to_hex_string(i, string);
			lcd_print_string(string, TEXT_COLOR);
		}
	}
	lcd_print_string("\r\ni2c scanning have been finished", TEXT_COLOR);
	while(1)
	{}
}
