#include "std_system.hpp"
#include "sysclk.hpp"
#include "udc.h"
//
#define C42412A_ICON_USB            1, 1
#define C42412A_ICON_COLON          3, 1
#define C42412A_ICON_BAT            0, 0
#define C42412A_ICON_ATMEL          0, 1
/* Icon without selected segments blinking feature */
#define C42412A_ICON_MINUS          0, 17
#define C42412A_ICON_MINUS_SEG1     0, 13
#define C42412A_ICON_MINUS_SEG2     0, 9
#define C42412A_ICON_DOT_1          0, 5
#define C42412A_ICON_DOT_2          3, 6
#define C42412A_ICON_DOT_3          3, 10
#define C42412A_ICON_DOT_4          3, 14
#define C42412A_ICON_DOT_5          3, 18
#define C42412A_ICON_BAT_LEVEL_1    2, 0
#define C42412A_ICON_BAT_LEVEL_2    3, 0
#define C42412A_ICON_BAT_LEVEL_3    1, 0
#define C42412A_ICON_WLESS_LEVEL_0  3, 3
#define C42412A_ICON_WLESS_LEVEL_1  3, 2
#define C42412A_ICON_WLESS_LEVEL_2  2, 3
#define C42412A_ICON_WLESS_LEVEL_3  2, 2
#define C42412A_ICON_AUDIO_PLAY     2, 1
#define C42412A_ICON_AM             0, 2
#define C42412A_ICON_PM             0, 3
#define C42412A_ICON_DEGREE_C       3, 22
#define C42412A_ICON_DEGREE_F       0, 21
#define C42412A_ICON_VOLT           1, 2
#define C42412A_ICON_MILLI_VOLT     1, 3
//
void main(void);
void sys_init(void);
void project_def_handler(void);
void disable_watchdog(void);
void c42412a_init(void);
void lcdca_set_contrast(int8_t contrast);
void pdca_channel_disable(uint8_t pdca_ch_number);
void c42412a_show_icon(uint8_t icon_com, uint8_t icon_seg);
void lcdca_set_pixel(uint8_t pix_com, uint8_t pix_seg);
uint64_t lcdca_get_pixel_register(uint8_t pix_com);
void project_systick_handler(void);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x22,
	.month = 0x10,
	.year = 0x20,
};
//
void main(void)
{
	disable_watchdog();
	*((volatile uint32_t*)0xE0042000) = 7;//Peripheral Debug
	//c5-lcd_backlight, 7-led
	sysclk_enable_pbc_module(SYSCLK_GPIO);
	GPIO->GPIO_PORT[2].GPIO_ODERS = (1<<7)|(1<<5);
	GPIO->GPIO_PORT[2].GPIO_STERC = (1<<7)|(1<<5);
	GPIO->GPIO_PORT[2].GPIO_OVRS = 1<<5;
	//
	c42412a_init();
	//all on
	LCDCA->LCDCA_CR  = LCDCA_CR_BSTOP;
	LCDCA->LCDCA_CR  = LCDCA_CR_CSTOP;
	pdca_channel_disable(3);
	/*LCDCA->LCDCA_ACMCFG &= ~LCDCA_ACMCFG_EN;
	LCDCA->LCDCA_DRL0 = 0xFFFFFFFF;
	LCDCA->LCDCA_DRH0 = 0xFF;
	LCDCA->LCDCA_DRL1 = 0xFFFFFFFF;
	LCDCA->LCDCA_DRH1 = 0xFF;
	LCDCA->LCDCA_DRL2 = 0xFFFFFFFF;
	LCDCA->LCDCA_DRH2 = 0xFF;
	LCDCA->LCDCA_DRL3 = 0xFFFFFFFF;
	LCDCA->LCDCA_DRH3 = 0xFF;*/
	c42412a_show_icon(C42412A_ICON_ATMEL);
	//udc_start();
	while(1)
	{
	}
}
void sys_init(void)
{
	uint32_t temp_loc_1;
	//
	sysclk_priv_enable_module(PM_CLK_GRP_HSB, SYSCLK_HRAMC1_DATA);
	sysclk_enable_pbb_module(SYSCLK_HRAMC1_REGS);
	HCACHE->HCACHE_CTRL = HCACHE_CTRL_CEN_YES;
	while ((HCACHE->HCACHE_SR & HCACHE_SR_CSTS_EN) == 0){};
	if((SCIF->SCIF_PCLKSR & SCIF_PCLKSR_PLL0LOCK) == 0)
	{
		if((SCIF->SCIF_PCLKSR & SCIF_PCLKSR_OSC0RDY) == 0)
		{
			SCIF->SCIF_UNLOCK = SCIF_UNLOCK_KEY(0xAAu)|
				SCIF_UNLOCK_ADDR((uint32_t)&SCIF->SCIF_OSCCTRL0 - (uint32_t)SCIF);
			SCIF->SCIF_OSCCTRL0 = SCIF_OSCCTRL0_STARTUP(3)|SCIF_OSCCTRL0_GAIN(3)|SCIF_OSCCTRL0_MODE|SCIF_OSCCTRL0_OSCEN;
			while((SCIF->SCIF_PCLKSR & SCIF_PCLKSR_OSC0RDY) == 0){};
		}
		SCIF->SCIF_UNLOCK = SCIF_UNLOCK_KEY(0xAAu)|
			SCIF_UNLOCK_ADDR((uint32_t)&SCIF->SCIF_PLL[0].SCIF_PLL - (uint32_t)SCIF);
		SCIF->SCIF_PLL[0].SCIF_PLL  = ((8 - 1) << SCIF_PLL_PLLMUL_Pos)|(1 << SCIF_PLL_PLLDIV_Pos)|
			((SCIF_PLL_PLLCOUNT_Msk >> SCIF_PLL_PLLCOUNT_Pos) << SCIF_PLL_PLLCOUNT_Pos)|
			(1 << (SCIF_PLL_PLLOPT_Pos + 1))|SCIF_PLL_PLLEN;
		while((SCIF->SCIF_PCLKSR & SCIF_PCLKSR_PLL0LOCK) == 0){};
	}
	HFLASHC->FLASHCALW_FCR = (HFLASHC->FLASHCALW_FCR & ~FLASHCALW_FCR_FWS)|FLASHCALW_FCR_FWS_1;
	flashcalw_issue_command(FLASHCALW_FCMD_CMD_HSEN, -1);
	flashcalw_issue_command(FLASHCALW_FCMD_CMD_HSDIS, -1);
	PM->PM_UNLOCK = PM_UNLOCK_KEY(0xAAu)|PM_UNLOCK_ADDR((uint32_t)&PM->PM_MCCTRL - (uint32_t)PM);
	PM->PM_MCCTRL = 2;
	temp_loc_1 = BPM->BPM_PMCON & ~BPM_PMCON_PS_Msk;
	temp_loc_1 |= BPM_PMCON_PS(0)|BPM_PMCON_PSCM|BPM_PMCON_PSCREQ;
	bpm_ps_no_halt_exec(temp_loc_1);
}
void project_def_handler(void)
{
}
void project_systick_handler(void)
{
}
void disable_watchdog(void)
{
	uint32_t temp_loc = WDT->WDT_CTRL & ~WDT_CTRL_EN;
	WDT->WDT_CTRL = temp_loc| WDT_CTRL_KEY(0x55ul);
	WDT->WDT_CTRL = temp_loc| WDT_CTRL_KEY(0xAAul);
	while (WDT->WDT_CTRL & WDT_CTRL_EN){};
	temp_loc = PM->PM_PBDMASK & ~(1 << 3);
	PM->PM_UNLOCK = PM_UNLOCK_KEY(0xAAu)|BPM_UNLOCK_ADDR(((uint32_t)&PM->PM_PBDMASK - (uint32_t)PM));
	PM->PM_PBDMASK = temp_loc;
}
void c42412a_init(void)
{
	sysclk_enable_pba_module(SYSCLK_LCDCA);
	if((BSCIF->BSCIF_PCLKSR & BSCIF_PCLKSR_OSC32RDY) == 0)
	{
		osc_priv_enable_osc32();
		while((BSCIF->BSCIF_PCLKSR & BSCIF_PCLKSR_OSC32RDY) == 0){};
	}
	BPM->BPM_UNLOCK = BPM_UNLOCK_KEY(0xAAu)|BPM_UNLOCK_ADDR((uint32_t)&BPM->BPM_PMCON - (uint32_t)BPM);
	BPM->BPM_PMCON = BPM->BPM_PMCON & (~BPM_PMCON_CK32S);
	LCDCA->LCDCA_CR = LCDCA_CR_DIS|LCDCA_CR_FC0DIS|LCDCA_CR_FC1DIS|LCDCA_CR_FC2DIS;
	LCDCA->LCDCA_TIM = LCDCA_TIM_FC2(1)|LCDCA_TIM_FC1(2)|LCDCA_TIM_FC0(2)|LCDCA_TIM_CLKDIV(7);
	LCDCA->LCDCA_CFG = LCDCA_CFG_NSU(24)|LCDCA_CFG_DUTY(0);
	lcdca_set_contrast(30);
	LCDCA->LCDCA_CFG &= ~LCDCA_CFG_BLANK;
	LCDCA->LCDCA_CR = LCDCA_CR_EN;
	LCDCA->LCDCA_CR = LCDCA_CR_CDM;
	while((LCDCA->LCDCA_SR & LCDCA_SR_EN) == 0){};
	LCDCA->LCDCA_CR = LCDCA_CR_FC0EN;
	while((LCDCA->LCDCA_SR & LCDCA_SR_FC0S) == 0){};
	LCDCA->LCDCA_CR = LCDCA_CR_FC1EN;
	while((LCDCA->LCDCA_SR & LCDCA_SR_FC1S) == 0){};
	LCDCA->LCDCA_CR = LCDCA_CR_FC2EN;
	while((LCDCA->LCDCA_SR & LCDCA_SR_FC2S) == 0){};
}
void lcdca_set_contrast(int8_t contrast)
{
	uint32_t cfg = LCDCA->LCDCA_CFG;
	uint32_t fcst_filed;
	int8_t fcst_val_msk = (LCDCA_CFG_FCST_Msk >> (LCDCA_CFG_FCST_Pos + 1));
	int8_t fcst_sign_msk = ((LCDCA_CFG_FCST_Msk >> LCDCA_CFG_FCST_Pos) + 1) >> 1;
	cfg &= ~LCDCA_CFG_FCST_Msk;
	fcst_filed = (contrast & fcst_val_msk);
	if (contrast < 0) fcst_filed |= fcst_sign_msk;
	LCDCA->LCDCA_CFG = cfg | LCDCA_CFG_FCST(fcst_filed);
}
void pdca_channel_disable(uint8_t pdca_ch_number)
{
	volatile PdcaChannel *pdca_channel = &(PDCA->PDCA_CHANNEL[pdca_ch_number]);
	pdca_channel->PDCA_CR = PDCA_CR_TDIS;
}
void c42412a_show_icon(uint8_t icon_com, uint8_t icon_seg)
{
	//if(icon_com < 2) lcdca_clear_blink_pixel(icon_com, icon_seg);
	lcdca_set_pixel(icon_com, icon_seg);
}
void lcdca_set_pixel(uint8_t pix_com, uint8_t pix_seg)
{
	uint64_t register_value = lcdca_get_pixel_register(pix_com);
	register_value |= ((uint64_t)1 << pix_seg);
	switch (pix_com)
	{
		case 0:
			LCDCA->LCDCA_DRL0 = register_value;
			LCDCA->LCDCA_DRH0 = (register_value >> 32);
		break;
		case 1:
			LCDCA->LCDCA_DRL1 = register_value;
			LCDCA->LCDCA_DRH1 = (register_value >> 32);
		break;
		case 2:
			LCDCA->LCDCA_DRL2 = register_value;
			LCDCA->LCDCA_DRH2 = (register_value >> 32);
		break;
		case 3:
			LCDCA->LCDCA_DRL3 = register_value;
			LCDCA->LCDCA_DRH3 = (register_value >> 32);
		break;
		default: break;
	}
}
uint64_t lcdca_get_pixel_register(uint8_t pix_com)
{
	uint64_t register_value = 0;
	switch (pix_com)
	{
		case 0:
			register_value = (uint64_t)LCDCA->LCDCA_DRL0;
			register_value |= ((uint64_t)LCDCA->LCDCA_DRH0 << 32);
		break;
		case 1:
			register_value = (uint64_t)LCDCA->LCDCA_DRL1;
			register_value |= ((uint64_t)LCDCA->LCDCA_DRH1 << 32);
		break;
		case 2:
			register_value = (uint64_t)LCDCA->LCDCA_DRL2;
			register_value |= ((uint64_t)LCDCA->LCDCA_DRH2 << 32);
		break;
		case 3:
			register_value = (uint64_t)LCDCA->LCDCA_DRL3;
			register_value |= ((uint64_t)LCDCA->LCDCA_DRH3 << 32);
		break;
		default: break;
	}
	return register_value;
}

