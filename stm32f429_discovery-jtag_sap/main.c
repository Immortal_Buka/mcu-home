#include "system.h"
#include "lcd.h"
#include "print.h"
#include "crc.h"
#include "jtag.h"
//
typedef union
{
	uint64_t l;
	uint32_t s[2];
} data_t;
//
void main(void);
void TIM4_IRQHandler(void);
void jtag_print_state(void);
void USART1_IRQHandler(void);
void print_data(uint32_t i, uint8_t a);
//
volatile uint8_t buffer[SDRAM_SIZE] __attribute__ ((section (".sdram")));
volatile uint32_t counter = 1;
volatile uint8_t parse_req = 0;
jtag_states_t state;
data_t tdi, tdo;
//
void main(void)
{
	init_gpio();
	sdram_init();
	if(parse_req)
	{
		for(uint32_t i=0; i< SDRAM_SIZE; i++) buffer[i] = 0;
		parse_req = 0;
	}
	//a9-tx, a10-rx
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->OSPEEDR |= (3<<(9*2))|(3<<(10*2));
	GPIOA->AFR[1] |= (7<<4)|(7<<8);
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->BRR = (22<<4)|13;//230400
	USART1->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE|USART_CR1_RXNEIE;
	NVIC_EnableIRQ(USART1_IRQn);
	//c11-tck
	//c12-tms
	//c13-tdi
	//c14-tdo
	//c15-tmode
	GPIOC->PUPDR |= 0xaa800000;
	//tim4 - 1M
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	TIM4->ARR = 3;
	TIM4->PSC = 20;
	TIM4->DIER = 1;
	TIM4->CR1 = (TIM_CR1_CEN|TIM_CR1_ARPE);
	NVIC_EnableIRQ(TIM4_IRQn);
	board_LED(green, on);
	while(1)
	{
		if(counter >= SDRAM_SIZE)
		{
			NVIC_DisableIRQ(TIM4_IRQn);
			board_LED(red, on);
		}
		if(parse_req)
		{
			state = test_logic_reset;
			for(uint32_t i=1; i< SDRAM_SIZE; i++) switch(state)
			{
				case test_logic_reset:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = run_test_idle;
				break;
				case run_test_idle:
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = select_dr_scan;
				break;
				case select_dr_scan:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = capture_dr;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = select_ir_scan;
				break;
				case capture_dr:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1))
					{
						tdi.l = 0;
						tdo.l = 0;
						state = shift_dr;
					}
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = exit1_dr;
				break;
				case shift_dr:
					if(((buffer[i-1] & 1) == 0) && ((buffer[i] & 1) == 1))
					{
						tdi.l |= (buffer[i] & 4) >> 2;
						tdi.l <<= 1;
					}
					if(((buffer[i-1] & 1) == 1) && ((buffer[i] & 1) == 0))
					{
						tdo.l |= (buffer[i] & 8) >> 3;
						tdo.l <<= 1;
					}
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3))
					{
						state = exit1_dr;
						print_data(i, 0);
					}
				break;
				case exit1_dr:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = pause_dr;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = update_dr;
				break;
				case pause_dr:
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = exit2_dr;
				break;
				case exit2_dr:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = shift_dr;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = update_dr;
				break;
				case update_dr:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = run_test_idle;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = select_dr_scan;
				break;
				case select_ir_scan:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = capture_ir;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = test_logic_reset;
				break;
				case capture_ir:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1))
					{
						tdi.l = 0;
						tdo.l = 0;
						state = shift_ir;
					}
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = exit1_ir;
				break;
				case shift_ir:
					if(((buffer[i-1] & 1) == 0) && ((buffer[i] & 1) == 1))
					{
						tdi.l |= (buffer[i] & 4) >> 2;
						tdi.l <<= 1;
					}
					if(((buffer[i-1] & 1) == 1) && ((buffer[i] & 1) == 0))
					{
						tdo.l |= (buffer[i] & 8) >> 3;
						tdo.l <<= 1;
					}
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3))
					{
						state = exit1_ir;
						print_data(i, 1);
					}
				break;
				case exit1_ir:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = pause_ir;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = update_ir;
				break;
				case pause_ir:
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = exit2_ir;
				break;
				case exit2_ir:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = shift_ir;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = update_ir;
				break;
				case update_ir:
					if(((buffer[i-1] & 3) == 0) && ((buffer[i] & 3) == 1)) state = run_test_idle;
					if(((buffer[i-1] & 3) == 2) && ((buffer[i] & 3) == 3)) state = select_dr_scan;
				break;
				default: break;
			}
			parse_req = 0;
		}
	}
}
void TIM4_IRQHandler(void)
{
	TIM4->SR = 0;
	uint8_t temp_loc = (GPIOC->IDR)>>11;
	if(temp_loc != buffer[counter-1]) buffer[counter++] = temp_loc;
}
void jtag_print_state(void)
{
	switch(state)
	{
		case test_logic_reset: uart_print_string(USART1, "\r\ntest_logic_reset"); break;
		case run_test_idle: uart_print_string(USART1, "\r\nrun_test_idle"); break;
		case select_dr_scan: uart_print_string(USART1, "\r\nselect_dr_scan"); break;
		case capture_dr: uart_print_string(USART1, "\r\ncapture_dr"); break;
		case shift_dr: uart_print_string(USART1, "\r\nshift_dr"); break;
		case exit1_dr: uart_print_string(USART1, "\r\nexit1_dr"); break;
		case pause_dr: uart_print_string(USART1, "\r\npause_dr"); break;
		case exit2_dr: uart_print_string(USART1, "\r\nexit2_dr"); break;
		case update_dr: uart_print_string(USART1, "\r\nupdate_dr"); break;
		case select_ir_scan: uart_print_string(USART1, "\r\nselect_ir_scan"); break;
		case capture_ir: uart_print_string(USART1, "\r\ncapture_irt"); break;
		case shift_ir: uart_print_string(USART1, "\r\nshift_ir"); break;
		case exit1_ir: uart_print_string(USART1, "\r\nexit1_ir"); break;
		case pause_ir: uart_print_string(USART1, "\r\npause_ir"); break;
		case exit2_ir: uart_print_string(USART1, "\r\nexit2_ir"); break;
		case update_ir: uart_print_string(USART1, "\r\nupdate_ir"); break;
		default: break;
	}
}
void USART1_IRQHandler(void)
{
	uint8_t loc_temp;
	USART1->SR = 0;
	loc_temp = USART1->DR;
	if(loc_temp == 'p') parse_req = 1;
}
void print_data(uint32_t i, uint8_t a)
{
	uint8_t string[19] = {0};
	if((tdi.l != 0) && (tdo.l != 0))
	{
		tdi.l >>= 1;
		tdo.l >>= 1;
		if(a)
		{
			uart_print_string(USART1, "\r\n\e[33mIR");
			tdi.s[0] = __RBIT(tdi.s[0]);
			tdi.s[1] = __RBIT(tdi.s[1]);
			tdo.s[0] = __RBIT(tdo.s[0]);
			tdo.s[1] = __RBIT(tdo.s[1]);
		}
		else uart_print_string(USART1, "\r\n\e[32mDR");
		uart_print_string(USART1, "  TDI: ");
		uint64_to_hex_string(tdi.l, string);
		uart_print_string(USART1, string);
		uart_print_string(USART1, "  TDO: ");
		uint64_to_hex_string(tdo.l, string);
		uart_print_string(USART1, string);
		uart_print_string(USART1, "  I: ");
		uint32_to_hex_string(i, string);
		uart_print_string(USART1, string);
	}
}
