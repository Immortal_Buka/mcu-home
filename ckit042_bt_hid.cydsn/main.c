#include <project.h>
#include <ble_main.h>
#include <stdio.h>
#include "cy_ble.h"
//
int main(void);
void uart_print_char(void* uart, uint8_t data);
CY_ISR(HFI_Handler);
//
int main(void)
{
    CyGlobalIntEnable;
    CyIntSetSysVector(3,HFI_Handler);
    uart_debug_Start();
    uart_log_init(1);
    uart_log_add(t_info, "software building date", __DATE__, 0);
    ble_init();
    led_green_Write(1);
    led_red_Write(1);
    led_blue_Write(0);
    while(1)
    {
        CyBle_ProcessEvents();     
    }
}
void uart_print_char(void* uart, uint8_t data)
{
    uart_debug_UartPutChar(data);
}
CY_ISR(HFI_Handler)
{
	led_red_Write(0);
	uart_log(0, t_irq, "HardFault", 0);
	while(1);
}
