#include "main.h"
//
void main(void)
{
	fpu_on();
	init_gpio();
	lcd_init(C_Black);
	//a9 - uart1_tx, a10 - uart1_rx, c12 - uart5_tx, d2 - uart5_rx
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->OSPEEDR |= (3<<(9*2))|(3<<(10*2));
	GPIOA->AFR[1] |= (7<<4)|(7<<8);
	GPIOC->MODER |= (2<<(12*2));
	GPIOC->OSPEEDR |= (3<<(12*2));
	GPIOC->AFR[1] |= (8<<((12-8)*4));
	GPIOD->MODER |= (2<<(2*2));
	GPIOD->OSPEEDR |= (3<<(2*2));
	GPIOD->AFR[0] |= (8<<(2*4));
	//
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	RCC->APB1ENR |= RCC_APB1ENR_UART5EN;
	USART1->BRR = (45<<4)|9;//115200
	USART1->CR3 = USART_CR3_DMAT;
	USART1->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE|USART_CR1_RXNEIE;
	UART5->BRR = (22<<4)|13;//115200
	UART5->CR3 = USART_CR3_DMAT;
	UART5->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE|USART_CR1_RXNEIE;
	NVIC_SetPriority(USART1_IRQn, 2);
	NVIC_EnableIRQ(USART1_IRQn);
	NVIC_SetPriority(UART5_IRQn, 2);
	NVIC_EnableIRQ(UART5_IRQn);
	//
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN|RCC_AHB1ENR_DMA2EN;
	DMA1_Stream7->PAR = (uint32_t)&UART5->DR;
	DMA1_Stream7->M0AR = (uint32_t)CDC_Data[1];
	DMA1_Stream7->NDTR = 0;
	DMA1_Stream7->CR = (4<<25)|DMA_SxCR_MINC|(1<<6)|DMA_SxCR_TCIE;
	NVIC_SetPriority(DMA1_Stream7_IRQn, 2);
	NVIC_EnableIRQ(DMA1_Stream7_IRQn);
	DMA2_Stream7->PAR = (uint32_t)&USART1->DR;
	DMA2_Stream7->M0AR = (uint32_t)CDC_Data[0];
	DMA2_Stream7->NDTR = 0;
	DMA2_Stream7->CR = (4<<25)|DMA_SxCR_MINC|(1<<6)|DMA_SxCR_TCIE;
	NVIC_SetPriority(DMA2_Stream7_IRQn, 2);
	NVIC_EnableIRQ(DMA2_Stream7_IRQn);
	//
	RCC->AHB1ENR |= RCC_AHB1ENR_OTGHSEN;
	USBD_Init(&usb_dev, &usbd_desc, 0);
	USBD_RegisterClass(&usb_dev, &DCDC_cbs);
	USBD_Start(&usb_dev);
	while(1)
	{
	    if(CDC_DataLen[0])
	    {
	    	for(uint16_t i=0; i<CDC_DataLen[0];i++) lcd_print_char(CDC_Data[0][i], C_Magenta);
	    	/*DMA2_Stream7->NDTR = CDC_DataLen[0];
	    	DMA2_Stream7->CR |= 1;*/
	        DCDC_TransmitData(0, (uint8_t*)CDC_Data[0], CDC_DataLen[0]);
	        CDC_DataLen[0] = 0;
	    }
	    if(CDC_DataLen[1])
	    {
	    	for(uint16_t i=0; i<CDC_DataLen[1];i++) lcd_print_char(CDC_Data[1][i], C_LightGreen);
	    	/*DMA1_Stream7->NDTR = CDC_DataLen[1];
	    	DMA1_Stream7->CR |= 1;
	        //DCDC_TransmitData(1, (uint8_t*)CDC_Data[1], CDC_DataLen[1]);*/
	        CDC_DataLen[1] = 0;
	    }
#ifdef CDC3
	    if(CDC_DataLen[2])
	    {
	        //DCDC_TransmitData(2, (uint8_t*)CDC_Data[2], CDC_DataLen[2]);
	        CDC_DataLen[2] = 0;
	    }
#endif
	}
}
uint8_t* descriptor_device(uint8_t speed, uint16_t *length)
{
	*length = 0x12;
	return dev_desc;
}
uint8_t* descriptor_lang_id_str(uint8_t speed, uint16_t *length)
{
	*length = 4;
	return lang_id_desc;
}
uint8_t* descriptor_manufacturer_str(uint8_t speed , uint16_t *length)
{
	USBD_GetString((uint8_t *) USBD_MANUFACTURER_STRING, str_desc, length);
	return str_desc;
}
uint8_t* descriptor_product_str(uint8_t speed , uint16_t *length)
{
	if (speed == USBD_SPEED_HIGH) USBD_GetString((uint8_t *) USBD_PRODUCT_HS_STRING, str_desc, length);
	else USBD_GetString((uint8_t *) USBD_PRODUCT_FS_STRING, str_desc, length);
	return str_desc;
}
uint8_t* descriptor_serial_str(uint8_t speed , uint16_t *length)
{
	if (speed == USBD_SPEED_HIGH) USBD_GetString((uint8_t *) USBD_SERIAL_HS_STRING, str_desc, length);
	else USBD_GetString((uint8_t *) USBD_SERIAL_FS_STRING, str_desc, length);
	return str_desc;
}
uint8_t* descriptor_configuration_str(uint8_t speed , uint16_t *length)
{
	if (speed == USBD_SPEED_HIGH) USBD_GetString((uint8_t *) USBD_CONFIGURATION_HS_STRING, str_desc, length);
	else USBD_GetString((uint8_t *) USBD_CONFIGURATION_FS_STRING, str_desc, length);
	return str_desc;
}
uint8_t* descriptor_interface_str(uint8_t speed , uint16_t *length)
{
	if (speed == USBD_SPEED_HIGH) USBD_GetString((uint8_t *) USBD_INTERFACE_HS_STRING, str_desc, length);
	else USBD_GetString((uint8_t *) USBD_INTERFACE_FS_STRING, str_desc, length);
	return str_desc;
}
static uint8_t DCDC_Init(USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
    pdev->pClassData = &dev_dcdc;
	if(pdev->dev_speed == USBD_SPEED_HIGH)
    {
        USBD_LL_OpenEP(pdev, DCDC_P1_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P1_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_OUT_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P2_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P2_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_OUT_PACKET_SIZE);
#ifdef CDC3
        USBD_LL_OpenEP(pdev, DCDC_P3_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P3_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_HS_OUT_PACKET_SIZE);
#endif
    }
    else
    {
        USBD_LL_OpenEP(pdev, DCDC_P1_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P1_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_OUT_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P2_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P2_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_OUT_PACKET_SIZE);
#ifdef CDC3
        USBD_LL_OpenEP(pdev, DCDC_P3_BULKIN_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_IN_PACKET_SIZE);
        USBD_LL_OpenEP(pdev, DCDC_P3_BULKOUT_EP, USBD_EP_TYPE_BULK, DCDC_DATA_FS_OUT_PACKET_SIZE);
#endif
    }
    USBD_LL_OpenEP(pdev, DCDC_P1_INTRIN_EP, USBD_EP_TYPE_INTR, DCDC_CMD_PACKET_SIZE);
    USBD_LL_OpenEP(pdev, DCDC_P2_INTRIN_EP, USBD_EP_TYPE_INTR, DCDC_CMD_PACKET_SIZE);
#ifdef CDC3
    USBD_LL_OpenEP(pdev, DCDC_P3_INTRIN_EP, USBD_EP_TYPE_INTR, DCDC_CMD_PACKET_SIZE);
#endif
    dev_dcdc.hcdc1.TxState = 0;
    dev_dcdc.hcdc1.RxState = 0;
    dev_dcdc.hcdc2.TxState = 0;
    dev_dcdc.hcdc2.RxState = 0;
#ifdef CDC3
    dev_dcdc.hcdc3.TxState = 0;
    dev_dcdc.hcdc3.RxState = 0;
#endif
    DCDC_SetRxBuffer(pdev, DCDC_P1_BULKOUT_EP, DCDC_RxBuf_P1);
    DCDC_SetTxBuffer(pdev, DCDC_P1_BULKIN_EP, DCDC_TxBuf_P1, 0);
    DCDC_SetRxBuffer(pdev, DCDC_P2_BULKOUT_EP, DCDC_RxBuf_P2);
    DCDC_SetTxBuffer(pdev, DCDC_P2_BULKIN_EP, DCDC_TxBuf_P2, 0);
#ifdef CDC3
    DCDC_SetRxBuffer(pdev, DCDC_P3_BULKOUT_EP, DCDC_RxBuf_P3);
    DCDC_SetTxBuffer(pdev, DCDC_P3_BULKIN_EP, DCDC_TxBuf_P3, 0);
#endif
    if(pdev->dev_speed == USBD_SPEED_HIGH)
    {
        USBD_LL_PrepareReceive(pdev, DCDC_P1_BULKOUT_EP, dev_dcdc.hcdc1.RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
        USBD_LL_PrepareReceive(pdev, DCDC_P2_BULKOUT_EP, dev_dcdc.hcdc2.RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
#ifdef CDC3
        USBD_LL_PrepareReceive(pdev, DCDC_P3_BULKOUT_EP, dev_dcdc.hcdc3.RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
#endif
    }
    else
    {
        USBD_LL_PrepareReceive(pdev, DCDC_P1_BULKOUT_EP, dev_dcdc.hcdc1.RxBuffer, DCDC_DATA_FS_OUT_PACKET_SIZE);
        USBD_LL_PrepareReceive(pdev, DCDC_P2_BULKOUT_EP, dev_dcdc.hcdc2.RxBuffer, DCDC_DATA_FS_OUT_PACKET_SIZE);
#ifdef CDC3
        USBD_LL_PrepareReceive(pdev, DCDC_P3_BULKOUT_EP, dev_dcdc.hcdc3.RxBuffer, DCDC_DATA_FS_OUT_PACKET_SIZE);
#endif
    }
    return 0;
}
static uint8_t DCDC_DeInit(USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
    USBD_LL_FlushEP(pdev, DCDC_P1_BULKIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P1_BULKIN_EP);
    USBD_LL_FlushEP(pdev, DCDC_P1_BULKOUT_EP);
    USBD_LL_CloseEP(pdev, DCDC_P1_BULKOUT_EP);
    USBD_LL_FlushEP(pdev, DCDC_P1_INTRIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P1_INTRIN_EP);
    USBD_LL_FlushEP(pdev, DCDC_P2_BULKIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P2_BULKIN_EP);
    USBD_LL_FlushEP(pdev, DCDC_P2_BULKOUT_EP);
    USBD_LL_CloseEP(pdev, DCDC_P2_BULKOUT_EP);
    USBD_LL_FlushEP(pdev, DCDC_P2_INTRIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P2_INTRIN_EP);
#ifdef CDC3
    USBD_LL_FlushEP(pdev, DCDC_P3_BULKIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P3_BULKIN_EP);
    USBD_LL_FlushEP(pdev, DCDC_P3_BULKOUT_EP);
    USBD_LL_CloseEP(pdev, DCDC_P3_BULKOUT_EP);
    USBD_LL_FlushEP(pdev, DCDC_P3_INTRIN_EP);
    USBD_LL_CloseEP(pdev, DCDC_P3_INTRIN_EP);
#endif
    if(pdev->pClassData != NULL) pdev->pClassData = NULL;
    return 0;
}
static uint8_t DCDC_Setup(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    switch (req->bmRequest & USB_REQ_TYPE_MASK)
    {
        case USB_REQ_TYPE_CLASS :
        if (req->wLength)
        {
            if (req->bmRequest & 0x80)
            {
                switch(req->wIndex)
                {
#ifdef CDC3
					case 4:
						CDC_Control(2, req->bRequest, (uint8_t*)(dev_dcdc.hcdc3.data), req->wLength);
						USBD_CtlSendData(pdev, (uint8_t*)(dev_dcdc.hcdc3.data), req->wLength);
					break;
#endif
                    case 2:
                    	CDC_Control(1, req->bRequest, (uint8_t*)(dev_dcdc.hcdc2.data), req->wLength);
                        USBD_CtlSendData(pdev, (uint8_t*)(dev_dcdc.hcdc2.data), req->wLength);
                    break;
                    case 0:
                    	CDC_Control(0, req->bRequest, (uint8_t*)(dev_dcdc.hcdc1.data), req->wLength);
                        USBD_CtlSendData(pdev, (uint8_t*)(dev_dcdc.hcdc1.data), req->wLength);
                    break;
					default:
                    break;
                }
            }
            else
            {
                switch(req->wIndex)
                {
#ifdef CDC3
					case 4:
						dev_dcdc.hcdc3.CmdOpCode = req->bRequest;
						dev_dcdc.hcdc3.CmdLength = req->wLength;
						USBD_CtlPrepareRx(pdev, (uint8_t *)(dev_dcdc.hcdc3.data), req->wLength);
					break;
#endif
                    case 2:
                    	dev_dcdc.hcdc2.CmdOpCode = req->bRequest;
                    	dev_dcdc.hcdc2.CmdLength = req->wLength;
                        USBD_CtlPrepareRx(pdev, (uint8_t *)(dev_dcdc.hcdc2.data), req->wLength);
                    break;
					case 0:
						dev_dcdc.hcdc1.CmdOpCode = req->bRequest;
						dev_dcdc.hcdc1.CmdLength = req->wLength;
                        USBD_CtlPrepareRx(pdev, (uint8_t *)(dev_dcdc.hcdc1.data), req->wLength);
                    break;
					default:
                    break;
                }
            }
        }
        else
        {
            switch(req->wIndex)
            {
            	case 4: CDC_Control(2, req->bRequest, NULL, 0); break;
                case 2: CDC_Control(1, req->bRequest, NULL, 0); break;
                case 0: CDC_Control(0, req->bRequest, NULL, 0); break;
				default: break;
            }
        }
        break;
        default:
        break;
    }
    return 0;
}
static uint8_t DCDC_EP0_TxSent(USBD_HandleTypeDef *pdev)
{
    return 0;
}
static uint8_t DCDC_EP0_RxReady(USBD_HandleTypeDef *pdev)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
	if(dev_dcdc.hcdc1.CmdOpCode != 0xFF)
    {
		CDC_Control(0, dev_dcdc.hcdc1.CmdOpCode, (uint8_t *)(dev_dcdc.hcdc1.data), dev_dcdc.hcdc1.CmdLength);
        dev_dcdc.hcdc1.CmdOpCode = 0xFF;
    }
    return 0;
}
static uint8_t DCDC_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    uint8_t ep_addr = epnum | 0x80;
    switch(ep_addr)
    {
    	case DCDC_P1_BULKIN_EP: dev_dcdc.hcdc1.TxState = 0; break;
    	case DCDC_P2_BULKIN_EP: dev_dcdc.hcdc2.TxState = 0; break;
#ifdef CDC3
    	case DCDC_P3_BULKIN_EP: dev_dcdc.hcdc3.TxState = 0; break;
#endif
    	default: return USBD_FAIL;
    }
    return USBD_OK;
}
static uint8_t DCDC_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    USBD_CDC_HandleTypeDef *hcdc;
    switch(epnum)
    {
    	case DCDC_P1_BULKOUT_EP:
    		hcdc = &dev_dcdc.hcdc1;
    		hcdc->RxLength = USBD_LL_GetRxDataSize(pdev, epnum);
    		CDC_Receive(0, hcdc->RxBuffer, &hcdc->RxLength);
    		USBD_LL_PrepareReceive(pdev, DCDC_P1_BULKOUT_EP, hcdc->RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
    	break;
    	case DCDC_P2_BULKOUT_EP:
            hcdc = &dev_dcdc.hcdc2;
            hcdc->RxLength = USBD_LL_GetRxDataSize (pdev, epnum);
            CDC_Receive(1, hcdc->RxBuffer, &hcdc->RxLength);
            USBD_LL_PrepareReceive(pdev, DCDC_P2_BULKOUT_EP, hcdc->RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
        break;
#ifdef CDC3
    	case DCDC_P3_BULKOUT_EP:
            hcdc = &dev_dcdc.hcdc3;
            hcdc->RxLength = USBD_LL_GetRxDataSize (pdev, epnum);
            CDC_Receive(2, hcdc->RxBuffer, &hcdc->RxLength);
            USBD_LL_PrepareReceive(pdev, DCDC_P3_BULKOUT_EP, hcdc->RxBuffer, DCDC_DATA_HS_OUT_PACKET_SIZE);
        break;
#endif
    	default: return USBD_FAIL;
    }
    return USBD_OK;
}
static uint8_t DCDC_SOF(USBD_HandleTypeDef *pdev)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    return USBD_OK;
}
static uint8_t *DCDC_GetFSCfgDesc(uint16_t *length)
{
	*length = DCDC_CONFIG_DESC_SIZE;
	return config_desc;
}
static uint8_t *DCDC_GetHSCfgDesc(uint16_t *length)
{
	*length = DCDC_CONFIG_DESC_SIZE;
	return config_desc;
}
static uint8_t *DCDC_GetOtherSpeedCfgDesc(uint16_t *length)
{
	*length = DCDC_CONFIG_DESC_SIZE;
	return config_desc;
}
static uint8_t *DCDC_GetDeviceQualifierDescriptor(uint16_t *length)
{
	*length = 0x0a;
	return device_qualifier_desc;
}
void OTG_HS_IRQHandler(void)
{
	USB_OTG_GlobalTypeDef *USBx = USB_OTG_HS;
	uint32_t i = 0U, ep_intr = 0U, epint = 0U, epnum = 0U, fifoemptymsk = 0U, temp = 0U;
	USB_OTG_EPTypeDef* ep;
	if (USB_GetMode(USB_OTG_HS) == USB_OTG_MODE_DEVICE)
	{
		if(USB_ReadInterrupts(USB_OTG_HS) == 0U) return;
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_MMIS)) PCD_CLEAR_FLAG(USB_OTG_GINTSTS_MMIS);
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_OEPINT))
	    {
	    	epnum = 0U;
	    	ep_intr = USB_ReadDevAllOutEpInterrupt(USB_OTG_HS);
	    	while ( ep_intr )
	    	{
	    		if (ep_intr & 0x1U)
	    		{
	    			epint = USB_ReadDevOutEPInterrupt(USB_OTG_HS, epnum);
	    			if(( epint & USB_OTG_DOEPINT_XFRC) == USB_OTG_DOEPINT_XFRC)
	    			{
	    				CLEAR_OUT_EP_INTR(epnum, USB_OTG_DOEPINT_XFRC);
	    				if(init_cfg.dma_enable == 1U)
	    				{
	    					dev_hpcd.OUT_ep[epnum].xfer_count = dev_hpcd.OUT_ep[epnum].maxpacket - (USBx_OUTEP(epnum)->DOEPTSIZ & USB_OTG_DOEPTSIZ_XFRSIZ);
	    					dev_hpcd.OUT_ep[epnum].xfer_buff += dev_hpcd.OUT_ep[epnum].maxpacket;
	    				}
	    				USBD_LL_DataOutStage(&usb_dev, epnum, dev_hpcd.OUT_ep[epnum].xfer_buff);
	    				if(init_cfg.dma_enable == 1U)
	    				{
	    					if((epnum == 0U) && (dev_hpcd.OUT_ep[epnum].xfer_len == 0U)) USB_EP0_OutStart(USB_OTG_HS, 1U, (uint8_t *)dev_hpcd.Setup);
	    				}
	    			}
	    			if(( epint & USB_OTG_DOEPINT_STUP) == USB_OTG_DOEPINT_STUP)
	    			{
	    				USBD_LL_SetupStage(&usb_dev, (uint8_t *)dev_hpcd.Setup);
	    				CLEAR_OUT_EP_INTR(epnum, USB_OTG_DOEPINT_STUP);
	    			}
	    			if(( epint & USB_OTG_DOEPINT_OTEPDIS) == USB_OTG_DOEPINT_OTEPDIS) CLEAR_OUT_EP_INTR(epnum, USB_OTG_DOEPINT_OTEPDIS);
	    		}
	    		epnum++;
	    		ep_intr >>= 1U;
	    	}
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_IEPINT))
	    {
	    	ep_intr = USB_ReadDevAllInEpInterrupt(USB_OTG_HS);
	    	epnum = 0U;
	    	while ( ep_intr )
	    	{
	    		if (ep_intr & 0x1U)
	    		{
	    			epint = USB_ReadDevInEPInterrupt(USB_OTG_HS, epnum);
	    			if(( epint & USB_OTG_DIEPINT_XFRC) == USB_OTG_DIEPINT_XFRC)
	    			{
	    				fifoemptymsk = 0x1U << epnum;
	    				USBx_DEVICE->DIEPEMPMSK &= ~fifoemptymsk;
	    				CLEAR_IN_EP_INTR(epnum, USB_OTG_DIEPINT_XFRC);
	    				if (init_cfg.dma_enable == 1U) dev_hpcd.IN_ep[epnum].xfer_buff += dev_hpcd.IN_ep[epnum].maxpacket;
	    				USBD_LL_DataInStage(&usb_dev, epnum, dev_hpcd.IN_ep[epnum].xfer_buff);
	    				if (init_cfg.dma_enable == 1U)
	    				{
	    					if((epnum == 0U) && (dev_hpcd.IN_ep[epnum].xfer_len == 0U)) USB_EP0_OutStart(USB_OTG_HS, 1U, (uint8_t *)dev_hpcd.Setup);
	    				}
	    			}
	    			if(( epint & USB_OTG_DIEPINT_TOC) == USB_OTG_DIEPINT_TOC) CLEAR_IN_EP_INTR(epnum, USB_OTG_DIEPINT_TOC);
	    			if(( epint & USB_OTG_DIEPINT_ITTXFE) == USB_OTG_DIEPINT_ITTXFE) CLEAR_IN_EP_INTR(epnum, USB_OTG_DIEPINT_ITTXFE);
	    			if(( epint & USB_OTG_DIEPINT_INEPNE) == USB_OTG_DIEPINT_INEPNE) CLEAR_IN_EP_INTR(epnum, USB_OTG_DIEPINT_INEPNE);
	    			if(( epint & USB_OTG_DIEPINT_EPDISD) == USB_OTG_DIEPINT_EPDISD) CLEAR_IN_EP_INTR(epnum, USB_OTG_DIEPINT_EPDISD);
	    			if(( epint & USB_OTG_DIEPINT_TXFE) == USB_OTG_DIEPINT_TXFE)
	    			{
	    				USB_OTG_EPTypeDef* ept;
	    				int32_t len = 0U;
	    				uint32_t len32b, fifoemptymsk = 0U;
	    				ept = &dev_hpcd.IN_ep[epnum];
	    				len = ept->xfer_len - ept->xfer_count;
	    				if (len > ept->maxpacket) len = ept->maxpacket;
	    				len32b = (len + 3U) / 4U;
	    				while(((USBx_INEP(epnum)->DTXFSTS & USB_OTG_DTXFSTS_INEPTFSAV) > len32b) && (ept->xfer_count < ept->xfer_len) && (ept->xfer_len != 0U))
	    				{
	    					len = ept->xfer_len - ept->xfer_count;
	    				    if (len > ept->maxpacket) len = ept->maxpacket;
	    				    len32b = (len + 3U) / 4U;
	    				    USB_WritePacket(USBx, ept->xfer_buff, epnum, len, init_cfg.dma_enable);
	    				    ept->xfer_buff  += len;
	    				    ept->xfer_count += len;
	    				}
	    				if(len <= 0U)
	    				{
	    				    fifoemptymsk = 0x1U << epnum;
	    				    USBx_DEVICE->DIEPEMPMSK &= ~fifoemptymsk;
	    				}
	    			}
	    		}
	    		epnum++;
	    		ep_intr >>= 1U;
	    	}
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_WKUINT))
	    {
	    	USBx_DEVICE->DCTL &= ~USB_OTG_DCTL_RWUSIG;
	    	USBD_LL_Resume(&usb_dev);
	    	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_WKUINT);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_USBSUSP))
	    {
	    	if((USBx_DEVICE->DSTS & USB_OTG_DSTS_SUSPSTS) == USB_OTG_DSTS_SUSPSTS) USBD_LL_Suspend(&usb_dev);
	    	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_USBSUSP);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_USBRST))
	    {
	    	USBx_DEVICE->DCTL &= ~USB_OTG_DCTL_RWUSIG;
	    	USB_FlushTxFifo(USB_OTG_HS, 0x10U);
	    	for (i = 0U; i < init_cfg.dev_endpoints; i++)
	    	{
	    		USBx_INEP(i)->DIEPINT = 0xFFU;
	    		USBx_OUTEP(i)->DOEPINT = 0xFFU;
	    	}
	    	USBx_DEVICE->DAINT = 0xFFFFFFFFU;
	    	USBx_DEVICE->DAINTMSK |= 0x10001U;
	    	if(init_cfg.use_dedicated_ep1)
	    	{
	    		USBx_DEVICE->DOUTEP1MSK |= (USB_OTG_DOEPMSK_STUPM | USB_OTG_DOEPMSK_XFRCM | USB_OTG_DOEPMSK_EPDM);
	    		USBx_DEVICE->DINEP1MSK |= (USB_OTG_DIEPMSK_TOM | USB_OTG_DIEPMSK_XFRCM | USB_OTG_DIEPMSK_EPDM);
	    	}
	    	else
	    	{
	    		USBx_DEVICE->DOEPMSK |= (USB_OTG_DOEPMSK_STUPM | USB_OTG_DOEPMSK_XFRCM | USB_OTG_DOEPMSK_EPDM);
	    		USBx_DEVICE->DIEPMSK |= (USB_OTG_DIEPMSK_TOM | USB_OTG_DIEPMSK_XFRCM | USB_OTG_DIEPMSK_EPDM);
	    	}
	    	USBx_DEVICE->DCFG &= ~USB_OTG_DCFG_DAD;
	    	USB_EP0_OutStart(USB_OTG_HS, init_cfg.dma_enable, (uint8_t *)dev_hpcd.Setup);
	    	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_USBRST);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_ENUMDNE))
	    {
	    	USB_ActivateSetup(USB_OTG_HS);
	    	USB_OTG_HS->GUSBCFG &= ~USB_OTG_GUSBCFG_TRDT;
	    	if ( USB_GetDevSpeed(USB_OTG_HS) == USB_OTG_SPEED_HIGH)
	    	{
	    		init_cfg.speed = USB_OTG_SPEED_HIGH;
	    		init_cfg.ep0_mps = USB_OTG_HS_MAX_PACKET_SIZE ;
	    		USB_OTG_HS->GUSBCFG |= (uint32_t)((9 << 10) & USB_OTG_GUSBCFG_TRDT);
	    	}
	    	else
	    	{
	    		init_cfg.speed = USB_OTG_SPEED_FULL;
	    		init_cfg.ep0_mps = USB_OTG_FS_MAX_PACKET_SIZE ;
	    		USB_OTG_HS->GUSBCFG |= (uint32_t)((6 << 10) & USB_OTG_GUSBCFG_TRDT);
	        }
	        USBD_SpeedTypeDef speed = USBD_SPEED_FULL;
	        switch (init_cfg.speed)
	        {
	        	case 0: speed = USBD_SPEED_HIGH; break;
	        	case 2: speed = USBD_SPEED_FULL; break;
	        	default: speed = USBD_SPEED_FULL; break;
	        }
	        USBD_LL_SetSpeed(&usb_dev, speed);
	        USBD_LL_Reset(&usb_dev);
	      	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_ENUMDNE);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_RXFLVL))
	    {
	    	USB_MASK_INTERRUPT(USB_OTG_HS, USB_OTG_GINTSTS_RXFLVL);
	    	temp = USBx->GRXSTSP;
	    	ep = &dev_hpcd.OUT_ep[temp & USB_OTG_GRXSTSP_EPNUM];
	    	if(((temp & USB_OTG_GRXSTSP_PKTSTS) >> 17U) ==  STS_DATA_UPDT)
	    	{
	    		if((temp & USB_OTG_GRXSTSP_BCNT) != 0U)
	    		{
	    			USB_ReadPacket(USBx, ep->xfer_buff, (temp & USB_OTG_GRXSTSP_BCNT) >> 4U);
	    			ep->xfer_buff += (temp & USB_OTG_GRXSTSP_BCNT) >> 4U;
	    			ep->xfer_count += (temp & USB_OTG_GRXSTSP_BCNT) >> 4U;
	    		}
	    	}
	    	else
	    	{
	    		if (((temp & USB_OTG_GRXSTSP_PKTSTS) >> 17U) ==  STS_SETUP_UPDT)
	    		{
	    			USB_ReadPacket(USBx, (uint8_t *)dev_hpcd.Setup, 8U);
	    			ep->xfer_count += (temp & USB_OTG_GRXSTSP_BCNT) >> 4U;
	    		}
	    	}
	    	USB_UNMASK_INTERRUPT(USB_OTG_HS, USB_OTG_GINTSTS_RXFLVL);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_SOF))
	    {
	    	USBD_LL_SOF(&usb_dev);
	    	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_SOF);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_IISOIXFR)) PCD_CLEAR_FLAG(USB_OTG_GINTSTS_IISOIXFR);
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_PXFR_INCOMPISOOUT)) PCD_CLEAR_FLAG(USB_OTG_GINTSTS_PXFR_INCOMPISOOUT);
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_SRQINT))
	    {
	    	USBD_LL_DevConnected(&usb_dev);
	    	PCD_CLEAR_FLAG(USB_OTG_GINTSTS_SRQINT);
	    }
	    if(PCD_GET_FLAG(USB_OTG_GINTSTS_OTGINT))
	    {
	    	temp = USB_OTG_HS->GOTGINT;
	    	if((temp & USB_OTG_GOTGINT_SEDET) == USB_OTG_GOTGINT_SEDET) USBD_LL_DevDisconnected(&usb_dev);
	    	USB_OTG_HS->GOTGINT |= temp;
	    }
	}
}
static int8_t CDC_Receive(uint8_t i, uint8_t* Buf, uint32_t *Len)
{
    CDC_DataLen[i] = *Len;
    for(uint32_t j=0; j<CDC_DataLen[i] ; j++) CDC_Data[i][j] = Buf[j];
    return USBD_OK;
}
USBD_StatusTypeDef USBD_LL_Init(USBD_HandleTypeDef *pdev)
{
	init_cfg.dev_endpoints = 6;
	init_cfg.use_dedicated_ep1 = 0;
	init_cfg.ep0_mps = 0x40;
	init_cfg.dma_enable = 0;
	init_cfg.low_power_enable = 0;
	init_cfg.phy_itface = 2;
	init_cfg.speed = 1;
	init_cfg.Sof_enable = 0;
	init_cfg.vbus_sensing_enable = 0;
	NVIC_EnableIRQ(OTG_HS_IRQn);
	USB_DisableGlobalInt(USB_OTG_HS);
	USB_CoreInit(USB_OTG_HS, init_cfg);
	USB_SetCurrentMode(USB_OTG_HS, USB_OTG_DEVICE_MODE);
 	for (uint8_t i = 0; i < 15; i++)
 	{
 		dev_hpcd.IN_ep[i].is_in = 1U;
 		dev_hpcd.IN_ep[i].num = i;
 		dev_hpcd.IN_ep[i].tx_fifo_num = i;
 		dev_hpcd.IN_ep[i].type = EP_TYPE_CTRL;
 		dev_hpcd.IN_ep[i].maxpacket = 0U;
 		dev_hpcd.IN_ep[i].xfer_buff = 0U;
 		dev_hpcd.IN_ep[i].xfer_len = 0U;
 		dev_hpcd.OUT_ep[i].is_in = 0U;
 		dev_hpcd.OUT_ep[i].num = i;
 		dev_hpcd.IN_ep[i].tx_fifo_num = i;
 		dev_hpcd.OUT_ep[i].type = EP_TYPE_CTRL;
 		dev_hpcd.OUT_ep[i].maxpacket = 0U;
 		dev_hpcd.OUT_ep[i].xfer_buff = 0U;
 		dev_hpcd.OUT_ep[i].xfer_len = 0U;
 		USB_OTG_HS->DIEPTXF[i] = 0U;
 	}
 	USB_DevInit(USB_OTG_HS, init_cfg);
 	USB_DevDisconnect(USB_OTG_HS);
 	NVIC_SetPriority(OTG_HS_IRQn, 0);
 	NVIC_EnableIRQ(OTG_HS_IRQn);
  	PCDEx_SetRxFiFo(0x200);
  	PCDEx_SetTxFiFo(0, 0x100);
  	PCDEx_SetTxFiFo(1, 0x100);
  	PCDEx_SetTxFiFo(2, 0x100);
  	PCDEx_SetTxFiFo(3, 0x100);
  	PCDEx_SetTxFiFo(4, 0x100);
#ifdef CDC3
  	PCDEx_SetTxFiFo(5, 0x100);
  	PCDEx_SetTxFiFo(6, 0x100);
#endif
  	return USBD_OK;
}
USBD_StatusTypeDef USBD_LL_Start(USBD_HandleTypeDef * pdev)
{
	USB_DevConnect(USB_OTG_HS);
	USB_EnableGlobalInt(USB_OTG_HS);
	return USBD_OK;
}
USBD_StatusTypeDef USBD_LL_OpenEP(USBD_HandleTypeDef *pdev, uint8_t ep_addr, uint8_t ep_type, uint16_t ep_mps)
{
	USB_OTG_EPTypeDef *ep;
	if ((ep_addr & 0x80) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr & 0x7F];
	ep->num   = ep_addr & 0x7F;
	ep->is_in = (0x80 & ep_addr) != 0;
	ep->maxpacket = ep_mps;
	ep->type = ep_type;
	if (ep->is_in) ep->tx_fifo_num = ep->num;
	if (ep_type == EP_TYPE_BULK) ep->data_pid_start = 0U;
	USB_ActivateEndpoint(USB_OTG_HS, ep);
	return USBD_OK;
}
USBD_StatusTypeDef USBD_LL_PrepareReceive(USBD_HandleTypeDef * pdev, uint8_t ep_addr, uint8_t * pbuf, uint16_t size)
{
	USB_OTG_EPTypeDef *ep;
	ep = &dev_hpcd.OUT_ep[ep_addr & 0x7F];
	ep->xfer_buff = pbuf;
	ep->xfer_len = size;
	ep->xfer_count = 0U;
	ep->is_in = 0U;
	ep->num = ep_addr & 0x7F;
	if (init_cfg.dma_enable == 1U) ep->dma_addr = (uint32_t)pbuf;
	if ((ep_addr & 0x7F) == 0) USB_EP0StartXfer(USB_OTG_HS, ep, init_cfg.dma_enable);
	else USB_EPStartXfer(USB_OTG_HS, ep, init_cfg.dma_enable);
	return HAL_OK;
}
USBD_StatusTypeDef USBD_LL_CloseEP(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	USB_OTG_EPTypeDef* ep;
	if ((ep_addr & 0x80) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr & 0x7F];
	ep->num   = ep_addr & 0x7F;
	ep->is_in = (0x80 & ep_addr) != 0;
	USB_DeactivateEndpoint(USB_OTG_HS, ep);
	return HAL_OK;
}
USBD_StatusTypeDef USBD_LL_Transmit(USBD_HandleTypeDef * pdev, uint8_t ep_addr, uint8_t * pbuf, uint16_t size)
{
	USB_OTG_EPTypeDef *ep;
	ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	ep->xfer_buff = pbuf;
	ep->xfer_len = size;
	ep->xfer_count = 0U;
	ep->is_in = 1U;
	ep->num = ep_addr & 0x7F;
	if (init_cfg.dma_enable == 1U) ep->dma_addr = (uint32_t)pbuf;
	if ((ep_addr & 0x7F) == 0) USB_EP0StartXfer(USB_OTG_HS , ep, init_cfg.dma_enable);
	else USB_EPStartXfer(USB_OTG_HS , ep, init_cfg.dma_enable);
	return HAL_OK;
}
uint32_t USBD_LL_GetRxDataSize(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	return dev_hpcd.OUT_ep[ep_addr & 0xF].xfer_count;
}
USBD_StatusTypeDef USBD_LL_StallEP(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	USB_OTG_EPTypeDef *ep;
	if ((0x80 & ep_addr) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr];
	ep->is_stall = 1U;
	ep->num   = ep_addr & 0x7F;
	ep->is_in = ((ep_addr & 0x80) == 0x80);
	USB_EPSetStall(USB_OTG_HS, ep);
	if((ep_addr & 0x7F) == 0) USB_EP0_OutStart(USB_OTG_HS, init_cfg.dma_enable, (uint8_t *)dev_hpcd.Setup);
	return 0;
}
USBD_StatusTypeDef USBD_LL_ClearStallEP(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	USB_OTG_EPTypeDef *ep;
	if ((0x80 & ep_addr) == 0x80) ep = &dev_hpcd.IN_ep[ep_addr & 0x7F];
	else ep = &dev_hpcd.OUT_ep[ep_addr];
	ep->is_stall = 0U;
	ep->num   = ep_addr & 0x7F;
	ep->is_in = ((ep_addr & 0x80) == 0x80);
	USB_EPClearStall(USB_OTG_HS, ep);
	return 0;
}
uint8_t USBD_LL_IsStallEP(USBD_HandleTypeDef * pdev, uint8_t ep_addr)
{
	if ((ep_addr & 0x80) == 0x80) return dev_hpcd.IN_ep[ep_addr & 0xF].is_stall;
	else return dev_hpcd.OUT_ep[ep_addr & 0xF].is_stall;
}
USBD_StatusTypeDef USBD_LL_SetUSBAddress(USBD_HandleTypeDef * pdev, uint8_t dev_addr)
{
	USB_SetDevAddress(USB_OTG_HS, dev_addr);
	return 0;
}
static uint8_t DCDC_SetTxBuffer(USBD_HandleTypeDef   *pdev, uint8_t epnum, uint8_t  *pbuff, uint16_t length)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    USBD_CDC_HandleTypeDef *hcdc;
    switch(epnum)
    {
    	case DCDC_P1_BULKIN_EP: hcdc = &dev_dcdc.hcdc1; break;
    	case DCDC_P2_BULKIN_EP: hcdc = &dev_dcdc.hcdc2; break;
#ifdef CDC3
    	case DCDC_P3_BULKIN_EP: hcdc = &dev_dcdc.hcdc3; break;
#endif
    	default: return USBD_FAIL;
    }
    hcdc->TxBuffer = pbuff;
    hcdc->TxLength = length;
    return USBD_OK;
}
static uint8_t DCDC_SetRxBuffer(USBD_HandleTypeDef* pdev, uint8_t epnum, uint8_t* pbuff)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    switch(epnum)
    {
    	case DCDC_P1_BULKOUT_EP: dev_dcdc.hcdc1.RxBuffer = pbuff; break;
    	case DCDC_P2_BULKOUT_EP: dev_dcdc.hcdc2.RxBuffer = pbuff; break;
#ifdef CDC3
    	case DCDC_P3_BULKOUT_EP: dev_dcdc.hcdc3.RxBuffer = pbuff; break;
#endif
    	default: return USBD_FAIL;
    }
    return USBD_OK;
}
static uint8_t DCDC_TransmitPacket(USBD_HandleTypeDef *pdev, uint8_t ep_addr)
{
    if(pdev->pClassData == NULL) return USBD_FAIL;
    USBD_CDC_HandleTypeDef *hcdc;
    switch(ep_addr)
	{
    	case DCDC_P1_BULKIN_EP: hcdc = &dev_dcdc.hcdc1; break;
    	case DCDC_P2_BULKIN_EP: hcdc = &dev_dcdc.hcdc2; break;
#ifdef CDC3
    	case DCDC_P3_BULKIN_EP: hcdc = &dev_dcdc.hcdc3; break;
#endif
    	default: return USBD_FAIL;
	}
    if(hcdc->TxState == 0)
    {
        hcdc->TxState = 1;
        USBD_LL_Transmit(pdev, ep_addr, hcdc->TxBuffer, hcdc->TxLength);
        return USBD_OK;
    }
    else return USBD_BUSY;
}
uint8_t DCDC_TransmitData(uint8_t com_port, uint8_t *tx_buf, uint16_t tx_len)
{
    uint8_t epnum = 0;
    uint8_t* buf = 0;
    if(tx_buf == NULL) return USBD_FAIL;
    switch(com_port)
    {
    	case 0:
			epnum = DCDC_P1_BULKIN_EP;
			buf = DCDC_TxBuf_P1;
			if(tx_len > sizeof(DCDC_TxBuf_P1)) return USBD_FAIL;
		break;
    	case 1:
			epnum = DCDC_P2_BULKIN_EP;
			buf = DCDC_TxBuf_P2;
			if(tx_len > sizeof(DCDC_TxBuf_P2)) return USBD_FAIL;
		break;
#ifdef CDC3
    	case 2:
			epnum = DCDC_P3_BULKIN_EP;
			buf = DCDC_TxBuf_P3;
			if(tx_len > sizeof(DCDC_TxBuf_P3)) return USBD_FAIL;
		break;
#endif
    	default: return USBD_FAIL;
    }
    for(uint32_t i=0; i<tx_len; i++) buf[i] = tx_buf[i];
    uint8_t ret = DCDC_SetTxBuffer(&usb_dev, epnum, buf, tx_len);
    if(ret == USBD_OK) ret = DCDC_TransmitPacket(&usb_dev, epnum);
    return ret;
}
HAL_StatusTypeDef PCDEx_SetRxFiFo(uint16_t size)
{
	USB_OTG_HS->GRXFSIZ = size;
	return HAL_OK;
}
HAL_StatusTypeDef PCDEx_SetTxFiFo(uint8_t fifo, uint16_t size)
{
	uint32_t Tx_Offset = USB_OTG_HS->GRXFSIZ;
	if(fifo == 0) USB_OTG_HS->DIEPTXF0_HNPTXFSIZ = (uint32_t)(((uint32_t)size << 16U) | Tx_Offset);
	else
	{
		Tx_Offset += (USB_OTG_HS->DIEPTXF0_HNPTXFSIZ) >> 16U;
		for (uint8_t i = 0; i < (fifo - 1); i++) Tx_Offset += (USB_OTG_HS->DIEPTXF[i] >> 16U);
		USB_OTG_HS->DIEPTXF[fifo - 1] = (uint32_t)(((uint32_t)size << 16U) | Tx_Offset);
	}
	return HAL_OK;
}
USBD_StatusTypeDef USBD_LL_FlushEP(USBD_HandleTypeDef *pdev, uint8_t ep_addr)
{
    if ((ep_addr & 0x80) == 0x80) USB_FlushTxFifo(USB_OTG_HS, ep_addr & 0x7F);
    else USB_FlushRxFifo(USB_OTG_HS);
    return USBD_OK;
}
static int8_t CDC_Control(uint8_t i, uint8_t cmd, uint8_t* pbuf, uint16_t length)
{
    switch (cmd)
    {
        case CDC_SEND_ENCAPSULATED_COMMAND:
        break;
        case CDC_GET_ENCAPSULATED_RESPONSE:
        break;
        case CDC_SET_COMM_FEATURE:
        break;
        case CDC_GET_COMM_FEATURE:
        break;
        case CDC_CLEAR_COMM_FEATURE:
        break;
        case CDC_SET_LINE_CODING:
            LineCoding[i].bitrate = (uint32_t)(pbuf[0] | (pbuf[1] << 8)|(pbuf[2] << 16) | (pbuf[3] << 24));
            LineCoding[i].format     = pbuf[4];
            LineCoding[i].paritytype = pbuf[5];
            LineCoding[i].datatype   = pbuf[6];
            switch(i)
            {
            	case 0: USART1->BRR = 84000000/LineCoding[0].bitrate; break;
            	case 1: UART5->BRR = 42000000/LineCoding[1].bitrate; break;
            	case 2: break;//third uart
            	default: break;
            }
        break;
        case CDC_GET_LINE_CODING:
            pbuf[0] = (uint8_t)(LineCoding[i].bitrate);
            pbuf[1] = (uint8_t)(LineCoding[i].bitrate >> 8);
            pbuf[2] = (uint8_t)(LineCoding[i].bitrate >> 16);
            pbuf[3] = (uint8_t)(LineCoding[i].bitrate >> 24);
            pbuf[4] = LineCoding[i].format;
            pbuf[5] = LineCoding[i].paritytype;
            pbuf[6] = LineCoding[i].datatype;
        break;
        case CDC_SET_CONTROL_LINE_STATE:
        break;
        case CDC_SEND_BREAK:
        break;
        default:
        break;
    }
    return USBD_OK;
}
void USART1_IRQHandler(void)
{
	uint8_t loc_temp;
	USART1->SR = 0;
	loc_temp = USART1->DR;
	DCDC_TransmitData(0, &loc_temp, 1);
}
void UART5_IRQHandler(void)
{
	uint8_t loc_temp;
	UART5->SR = 0;
	loc_temp = UART5->DR;
	DCDC_TransmitData(1, &loc_temp, 1);
}
void DMA1_Stream7_IRQHandler(void)
{
	DMA1->HIFCR = DMA1->HISR;
	DMA1_Stream7->CR &= ~1;
}
void DMA2_Stream7_IRQHandler(void)
{
	DMA2->HIFCR = DMA2->HISR;
	DMA2_Stream7->CR &= ~1;
}
