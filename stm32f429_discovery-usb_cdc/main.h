#include "system.h"
#include "usbd_ioreq.h"
#include "usbd_ctlreq.h"
#include "usbd_cdc.h"
#include "usbd_core.h"
#include "stm32f4xx_ll_usb.h"
#include "lcd.h"
//
#ifdef CDC3
#endif
//
#define USBD_MAX_NUM_INTERFACES         6
#define USBD_MAX_NUM_CONFIGURATION      1
#define USBD_MAX_STR_DESC_SIZ           0x100
#define USBD_SUPPORT_USER_STRING        0
#define USBD_SELF_POWERED               1
#define USBD_DEBUG_LEVEL                0
#define DCDC_RXBUF_SIZE  				(512)
#define DCDC_TXBUF_SIZE  				(512)
#define DCDC_P1_INTRIN_EP  				(0x81)  	// Port 1 EP for CDC commands
#define DCDC_P1_BULKIN_EP  				(0x82)  	// Port 1 EP for data IN
#define DCDC_P1_BULKOUT_EP 				(0x01)  	// Port 1 EP for data OUT
#define DCDC_P2_INTRIN_EP  				(0x83)  	// Port 2 EP for CDC commands
#define DCDC_P2_BULKIN_EP  				(0x84)  	// Port 2 EP for data IN
#define DCDC_P2_BULKOUT_EP 				(0x03)  	// Port 2 EP for data OUT
#ifdef CDC3
	#define DCDC_P3_INTRIN_EP  			(0x85)  	// Port 3 EP for CDC commands
	#define DCDC_P3_BULKIN_EP  			(0x86)  	// Port 3 EP for data IN
	#define DCDC_P3_BULKOUT_EP 			(0x05)  	// Port 3 EP for data OUT
#endif
#define DCDC_DATA_PACKET_SIZE   		(0x0200)	// High Speed data packet size
#define DCDC_CMD_PACKET_SIZE    		(0x40)  	// High speed command packet size
#define DCDC_DATA_HS_IN_PACKET_SIZE  	(DCDC_DATA_PACKET_SIZE)
#define DCDC_DATA_HS_OUT_PACKET_SIZE 	(DCDC_DATA_PACKET_SIZE)
#define DCDC_DATA_FS_IN_PACKET_SIZE  	(DCDC_DATA_PACKET_SIZE)
#define DCDC_DATA_FS_OUT_PACKET_SIZE 	(DCDC_DATA_PACKET_SIZE)
#ifdef CDC3
	#define DCDC_CONFIG_DESC_SIZE 		(0xcf)
#else
	#define DCDC_CONFIG_DESC_SIZE 		(0x8D)
#endif
#define USBD_MANUFACTURER_STRING        "STMicroelectronics"
#define USBD_PRODUCT_HS_STRING          "STM32 Virtual ComPort in HS mode"
#define USBD_PRODUCT_FS_STRING          "STM32 Virtual ComPort in FS Mode"
#define USBD_CONFIGURATION_HS_STRING    "VCP HS Config"
#define USBD_INTERFACE_HS_STRING        "VCP HS Interface"
#define USBD_CONFIGURATION_FS_STRING    "VCP FS Config"
#define USBD_INTERFACE_FS_STRING       	"VCP FS Interface"
#define USBD_SERIAL_FS_STRING       	"FS-001"
#define USBD_SERIAL_HS_STRING       	"HS-001"
#define PCD_GET_FLAG(__INTERRUPT__)     ((USB_ReadInterrupts(USB_OTG_HS) & (__INTERRUPT__)) == (__INTERRUPT__))
#define PCD_CLEAR_FLAG(__INTERRUPT__)   ((USB_OTG_HS->GINTSTS) &= (__INTERRUPT__))
#define USB_DESC_TYPE_DEV				0x01
#define USB_DESC_TYPE_CONF
#define USB_DESC_TYPE_INT				0x04
#define USB_DESC_TYPE_EP				0x05
#define USB_DESC_SIZE_DEV				0x12
#define USB_DESC_SIZE_INT				0x09
#define USB_DESC_SIZE_EP				0x07
//
typedef struct
{
	USB_OTG_EPTypeDef IN_ep[16U];   /*!< IN endpoint parameters             */
	USB_OTG_EPTypeDef OUT_ep[16U];  /*!< OUT endpoint parameters            */
	uint32_t Setup[12U];   /*!< Setup packet buffer                */
	void* pData;       /*!< Pointer to upper stack Handler */
} PCD_HandleTypeDef;
typedef struct
{
    USBD_CDC_HandleTypeDef hcdc1;
    USBD_CDC_HandleTypeDef hcdc2;
#ifdef CDC3
    USBD_CDC_HandleTypeDef hcdc3;
#endif
} DCDC_HandleTypeDef;
//
void main(void);
uint8_t* descriptor_device(uint8_t speed, uint16_t *length);
uint8_t* descriptor_lang_id_str(uint8_t speed, uint16_t *length);
uint8_t* descriptor_manufacturer_str(uint8_t speed , uint16_t *length);
uint8_t* descriptor_product_str(uint8_t speed , uint16_t *length);
uint8_t* descriptor_serial_str(uint8_t speed , uint16_t *length);
uint8_t* descriptor_configuration_str(uint8_t speed , uint16_t *length);
uint8_t* descriptor_interface_str(uint8_t speed , uint16_t *length);
static uint8_t DCDC_Init(USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t DCDC_DeInit(USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t DCDC_Setup(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req);
static uint8_t DCDC_EP0_TxSent(USBD_HandleTypeDef *pdev);
static uint8_t DCDC_EP0_RxReady(USBD_HandleTypeDef *pdev);
static uint8_t DCDC_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t DCDC_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t DCDC_SOF(USBD_HandleTypeDef *pdev);
static uint8_t *DCDC_GetFSCfgDesc(uint16_t *length);
static uint8_t *DCDC_GetHSCfgDesc(uint16_t *length);
static uint8_t *DCDC_GetOtherSpeedCfgDesc(uint16_t *length);
static uint8_t *DCDC_GetDeviceQualifierDescriptor(uint16_t *length);
void OTG_HS_IRQHandler(void);
static int8_t CDC_Receive(uint8_t i, uint8_t* Buf, uint32_t *Len);
static uint8_t DCDC_SetTxBuffer(USBD_HandleTypeDef   *pdev, uint8_t epnum, uint8_t  *pbuff, uint16_t length);
static uint8_t DCDC_SetRxBuffer(USBD_HandleTypeDef* pdev, uint8_t epnum, uint8_t* pbuff);
static uint8_t DCDC_TransmitPacket(USBD_HandleTypeDef *pdev, uint8_t ep_addr);
uint8_t DCDC_TransmitData(uint8_t com_port, uint8_t *tx_buf, uint16_t tx_len);
HAL_StatusTypeDef PCDEx_SetRxFiFo(uint16_t size);
HAL_StatusTypeDef PCDEx_SetTxFiFo(uint8_t fifo, uint16_t size);
static int8_t CDC_Control(uint8_t i, uint8_t cmd, uint8_t* pbuf, uint16_t length);
void USART1_IRQHandler(void);
void UART5_IRQHandler(void);
void DMA1_Stream7_IRQHandler(void);
void DMA2_Stream7_IRQHandler(void);
//
USBD_DescriptorsTypeDef usbd_desc =
{
	descriptor_device,
	descriptor_lang_id_str,
	descriptor_manufacturer_str,
	descriptor_product_str,
	descriptor_serial_str,
	descriptor_configuration_str,
	descriptor_interface_str,
};
__ALIGN_BEGIN uint8_t str_desc[128] __ALIGN_END ;
USBD_HandleTypeDef usb_dev;
uint8_t DCDC_RxBuf_P1[DCDC_RXBUF_SIZE]; // VCP1 RX Buffer
uint8_t DCDC_TxBuf_P1[DCDC_TXBUF_SIZE]; // VCP1 TX Buffer
uint8_t DCDC_RxBuf_P2[DCDC_RXBUF_SIZE]; // VCP2 RX Buffer
uint8_t DCDC_TxBuf_P2[DCDC_TXBUF_SIZE]; // VCP2 TX Buffer
#ifdef CDC3
uint8_t DCDC_RxBuf_P3[DCDC_RXBUF_SIZE]; // VCP3 RX Buffer
uint8_t DCDC_TxBuf_P3[DCDC_TXBUF_SIZE]; // VCP3 TX Buffer
#endif
USBD_ClassTypeDef  DCDC_cbs =
{
    DCDC_Init,
    DCDC_DeInit,
    DCDC_Setup,
    DCDC_EP0_TxSent,
    DCDC_EP0_RxReady,
    DCDC_DataIn,
    DCDC_DataOut,
    DCDC_SOF,
    NULL,
    NULL,
    DCDC_GetHSCfgDesc,
    DCDC_GetFSCfgDesc,
    DCDC_GetOtherSpeedCfgDesc,
    DCDC_GetDeviceQualifierDescriptor,
};
__ALIGN_BEGIN uint8_t dev_desc[USB_DESC_SIZE_DEV] __ALIGN_END =
{
	USB_DESC_SIZE_DEV,
	USB_DESC_TYPE_DEV,
	0x00,
	0x02,
	0xef,
	0x02,
	0x01,
	0x40,
	0x83,
	0x04,
	0x41,
	0x57,
	0x00,
	0x02,
	0x01,
	0x02,
	0x03,
	0x01
};
__ALIGN_BEGIN uint8_t device_qualifier_desc[0x0a] __ALIGN_END =
{
	0x0a,
	0x06,
	0x00,
	0x02,
	0xef,
	0x02,
	0x01,
	0x40,
	0x01,
	0x00,
};
__ALIGN_BEGIN uint8_t config_desc[DCDC_CONFIG_DESC_SIZE] __ALIGN_END =
{
	0x09,
	0x02,
	DCDC_CONFIG_DESC_SIZE,
	0x00,
	0x04,
	0x01,
	0x04,
	0xc0,
	0x32,
	0x08,
	0x0b,
	0x00,
	0x02,
	0x02,
	0x02,
	0x01,
	0x00,
	0x09,
	0x04,
	0x00,
	0x00,
	0x01,
	0x02,
	0x02,
	0x01,
	0x05,
	0x05,
	0x24,
	0x00,
	0x10,
	0x01,
	0x05,
	0x24,
	0x01,
	0x00,
	0x01,
	0x04,
	0x24,
	0x02,
	0x02,
	0x05,
	0x24,
	0x06,
	0x00,
	0x01,
	USB_DESC_SIZE_EP,
	USB_DESC_TYPE_EP,
	DCDC_P1_INTRIN_EP,
	0x03,
	0x08,
	0x00,
	0xFF,
	0x09,
	0x04,
	0x01,
	0x00,
	0x02,
	0x0A,
	0x00,
	0x00,
	0x00,
	0x07,
	0x05,
	0x01,
	0x02,
	0x40,
	0x00,
	0x00,
	USB_DESC_SIZE_EP,
	USB_DESC_TYPE_EP,
	DCDC_P1_BULKIN_EP,
	0x02,
	0x40,
	0x00,
	0x00,
	0x08,
	0x0b,
	0x02,
	0x02,
	0x02,
	0x02,
	0x01,
	0x00,
	0x09,
	0x04,
	0x02,
	0x00,
	0x01,
	0x02,
	0x02,
	0x01,
	0x05,
	0x05,
	0x24,
	0x00,
	0x10,
	0x01,
	0x05,
	0x24,
	0x01,
	0x00,
	0x03,
	0x04,
	0x24,
	0x02,
	0x02,
	0x05,
	0x24,
	0x06,
	0x02,
	0x03,
	USB_DESC_SIZE_EP,
	USB_DESC_TYPE_EP,
	DCDC_P2_INTRIN_EP,
	0x03,
	0x08,
	0x00,
	0xFF,
	USB_DESC_SIZE_INT,
	USB_DESC_TYPE_INT,
	0x03,
	0x00,
	0x02,
	0x0A,
	0x00,
	0x00,
	0x00,
	USB_DESC_SIZE_EP,
	USB_DESC_TYPE_EP,
	DCDC_P2_BULKOUT_EP,
	0x02,
	0x40,
	0x00,
	0x00,
	USB_DESC_SIZE_EP,
	USB_DESC_TYPE_EP,
	DCDC_P2_BULKIN_EP,
	0x02,
	0x40,
	0x00,
	0x00,
#ifdef CDC3
	0x08,
	0x0b,
	0x04,
	0x02,
	0x02,
	0x02,
	0x01,
	0x00,
	0x09,
	0x04,
	0x04,
	0x00,
	0x01,
	0x02,
	0x02,
	0x01,
	0x05,
	0x05,
	0x24,
	0x00,
	0x10,
	0x01,
	0x05,
	0x24,
	0x01,
	0x00,
	0x05,
	0x04,
	0x24,
	0x02,
	0x02,
	0x05,
	0x24,
	0x06,
	0x04,
	0x05,
	0x07,
	0x05,
	0x85,
	0x03,
	0x08,
	0x00,
	0xFF,
	0x09,
	0x04,
	0x05,
	0x00,
	0x02,
	0x0A,
	0x00,
	0x00,
	0x00,
	0x07,
	0x05,
	0x05,
	0x02,
	0x40,
	0x00,
	0x00,
	0x07,
	0x05,
	0x86,
	0x02,
	0x40,
	0x00,
	0x00,
#endif
};
USBD_CDC_LineCodingTypeDef LineCoding[3] =
{
	{
		115200,   /* baud rate*/
		0x00,   /* stop bits-1*/
		0x00,   /* parity - none*/
		0x08    /* nb. of bits 8*/
	},
	{
		115200,   /* baud rate*/
		0x00,   /* stop bits-1*/
		0x00,   /* parity - none*/
    	0x08    /* nb. of bits 8*/
	},
	{
		115200,   /* baud rate*/
		0x00,   /* stop bits-1*/
		0x00,   /* parity - none*/
    	0x08    /* nb. of bits 8*/
	}
};
uint8_t CDC_Data[3][512];
uint32_t CDC_DataLen[3] = {0};
__ALIGN_BEGIN uint8_t lang_id_desc[4] __ALIGN_END =
{
	0x04,
	0x03,
	0x09,
	0x04,
};
USB_OTG_CfgTypeDef init_cfg;
PCD_HandleTypeDef dev_hpcd;
DCDC_HandleTypeDef dev_dcdc;
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x22,
	.month = 0x10,
	.year = 0x18
};
