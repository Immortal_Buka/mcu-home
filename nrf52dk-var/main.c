#include "system.h"
#include "ble.h"
#include "nrf_sdm.h"
#include "nrf_nvic.h"
#include "ble_hci.h"
#include "print.h"
#include "time.h"
#include "uart_log.h"
//
#define ADV_SIZE		21
#define SR_SIZE			31
#define UUID128_QUANT	2
//#define TIMESTAMP
//
void main(void);
void softdevice_fault_handler(uint32_t id, uint32_t pc, uint32_t info);
void GPIOTE_IRQHandler(void);
void SWI2_EGU2_IRQHandler(void);
void print_addr(ble_gap_addr_t* addr);
void print_gap_conn_params(ble_gap_conn_params_t* params);
void print_sd_version(uint16_t id);
void print_uuid(ble_uuid_t* uuid);
void print_sec_kdist(ble_gap_sec_kdist_t* data);
void log_sd_functions(uint8_t* function_name_string, uint32_t err_code);
void print_ic_info(void);
void print_mem_size(uint32_t size);
void print_char_handlers(ble_gatts_char_handles_t* handlers);
//
extern uint32_t __data_start__;
uint32_t err_code, temp;
ble_enable_params_t ble_enable_params;
ble_gap_addr_t addr;
uint8_t event_buf[sizeof(ble_evt_t)] __attribute__((aligned(4)));
uint16_t m_conn_handle, service_handler;
ble_gap_conn_sec_mode_t sec_mode;
ble_gap_conn_params_t gap_conn_params;
ble_gap_adv_params_t p_adv_params;
ble_version_t sd_ver;
ble_gatts_char_handles_t p_handles;
ble_gatts_attr_md_t p_user_desc_md_4;
uint8_t string[11] = {0};
uint8_t hid_report_descriptor[] =
{
    0x06, 0x00, 0xFF,  // Usage Page (Vendor Defined 0xFF00)
    0x09, 0x01,        // Usage (0x01)
    0xA1, 0x01,        // Collection (Application)
    0x15, 0x00,        //   Logical Minimum (0)
    0x26, 0xFF, 0x00,  //   Logical Maximum (255)
    0x75, 0x08,        //   Report Size (8)
    0x95, 0x40,        //   Report Count (64)
    0x09, 0x01,        //   Usage (0x01)
    0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    0x95, 0x40,        //   Report Count (64)
    0x09, 0x01,        //   Usage (0x01)
    0x91, 0x02,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    0x95, 0x01,        //   Report Count (1)
    0x09, 0x01,        //   Usage (0x01)
    0xB1, 0x02,        //   Feature (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    0xC0,              // End Collection
};
uint8_t hid_info[] =
{
	0x01,0x00,	//bcdHID
	0,			//bCountryCode
	0x02		//Flags
};
uint8_t hid_cp_command;
uint8_t pnp_id[] =
{
	0x02,		//Vendor ID Source
	0x09,		//VID LSB
	0x12,		//VID MSB
	0x37,		//PID LSB
	0x29,		//PID MSB
	0x01,		//Product Version LSB
	0x00		//Product Version MSB
};
uint8_t serial_number[] = "0F001SDEA415";
uint8_t manufacturer[] = "buka";
uint8_t hid_report[128] = {0};
nrf_nvic_state_t nrf_nvic_state = {0};
volatile uint8_t gpiote_flag = 0;
uint8_t dev_state = 0;
//
const version_t version __attribute__ ((section (".version"))) = {0};
const nrf_clock_lf_cfg_t clock_lf_cfg =
{
	.source = NRF_CLOCK_LF_SRC_XTAL,	//LFCLK crystal oscillator
	.rc_ctiv = 0,						//only for NRF_CLOCK_LF_SRC_RC
	.rc_temp_ctiv = 0,					//Must be 0 if source is not NRF_CLOCK_LF_SRC_RC
	.xtal_accuracy = 7					//External crystal clock accuracy used in the LL to compute timing windows
};
const ble_uuid128_t uuid128[2] =
{
	{.uuid128 = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x00,0x00,0x0d,0x0e}},
	{.uuid128 = {0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x00,0x00,0x1d,0x1e}}
};
const ble_uuid_t uuid16_HID[5] =
{
	{.uuid = 0x1812,//BLE_UUID_HUMAN_INTERFACE_DEVICE_SERVICE
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2A4B,//BLE_UUID_REPORT_MAP_CHAR
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2A4A,//BLE_UUID_HID_INFORMATION_CHAR
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2A4C,//BLE_UUID_HID_CONTROL_POINT_CHAR
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2A4D,//characteristic - Report
	.type = BLE_UUID_TYPE_BLE},
};
const ble_uuid_t uuid16_DevInfo[4] =
{
	{.uuid = 0x180A,//Service - Device Information
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2a50,//characteristic - PnP ID
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2a25,//characteristic - Serial Number String
	.type = BLE_UUID_TYPE_BLE},
	{.uuid = 0x2a29,//characteristic - Manufacturer Name String
	.type = BLE_UUID_TYPE_BLE},
};
const ble_gap_sec_params_t p_sec_params =
{
	.bond = 0,
	.mitm = 0,
	.lesc = 0,
	.keypress = 0,
	.io_caps = BLE_GAP_IO_CAPS_NONE,
	.oob = 0,
	.min_key_size = 7,
	.max_key_size = 16,
	.kdist_own.enc = 0,
	.kdist_own.id = 0,
	.kdist_own.sign = 0,
	.kdist_own.link = 0,
	.kdist_peer.enc = 0,
	.kdist_peer.id = 0,
	.kdist_peer.sign = 0,
	.kdist_peer.link = 0
};
const ble_gap_sec_keyset_t p_sec_keyset =
{
	.keys_own.p_enc_key = 0,
	.keys_own.p_id_key = 0,
	.keys_own.p_sign_key = 0,
	.keys_own.p_sign_key = 0,
	.keys_peer.p_enc_key = 0,
	.keys_peer.p_id_key = 0,
	.keys_peer.p_sign_key = 0,
	.keys_peer.p_sign_key = 0
};
const ble_gatts_char_md_t p_char_1 =
{
//Standard properties
	.char_props.broadcast = 0,
	.char_props.read = 1,
	.char_props.write_wo_resp = 0,
	.char_props.write = 0,
	.char_props.notify = 0,
	.char_props.indicate = 0,
	.char_props.auth_signed_wr = 0,
	.char_ext_props.reliable_wr = 0,
	.char_ext_props.wr_aux = 0,
	.p_char_user_desc = 0,
	.char_user_desc_max_size = 0,
	.char_user_desc_size = 0,
	.p_char_pf = 0,
	.p_user_desc_md = 0,
	.p_cccd_md = 0,
	.p_sccd_md = 0
};
const ble_gatts_char_md_t p_char_2 =
{
//Standard properties
	.char_props.broadcast = 0,
	.char_props.read = 0,
	.char_props.write_wo_resp = 1,
	.char_props.write = 0,
	.char_props.notify = 0,
	.char_props.indicate = 0,
	.char_props.auth_signed_wr = 0,
	.char_ext_props.reliable_wr = 0,
	.char_ext_props.wr_aux = 0,
	.p_char_user_desc = 0,
	.char_user_desc_max_size = 0,
	.char_user_desc_size = 0,
	.p_char_pf = 0,
	.p_user_desc_md = 0,
	.p_cccd_md = 0,
	.p_sccd_md = 0
};
const ble_gatts_char_md_t p_char_3 =
{
//Standard properties
	.char_props.broadcast = 0,
	.char_props.read = 1,
	.char_props.write_wo_resp = 0,
	.char_props.write = 1,
	.char_props.notify = 1,
	.char_props.indicate = 0,
	.char_props.auth_signed_wr = 0,
	.char_ext_props.reliable_wr = 0,
	.char_ext_props.wr_aux = 0,
	.p_char_user_desc = 0,
	.char_user_desc_max_size = 0,
	.char_user_desc_size = 0,
	.p_char_pf = 0,
	.p_user_desc_md = 0,
	.p_cccd_md = 0,
	.p_sccd_md = 0
};
const ble_gatts_attr_t p_attr_char_1 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_HID[1],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(hid_report_descriptor),
	.init_offs = 0,
	.max_len = sizeof(hid_report_descriptor),
	.p_value = hid_report_descriptor,
};
const ble_gatts_attr_t p_attr_char_2 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_HID[2],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(hid_info),
	.init_offs = 0,
	.max_len = sizeof(hid_info),
	.p_value = hid_info,
};
const ble_gatts_attr_t p_attr_char_3 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_HID[3],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = 1,
	.init_offs = 0,
	.max_len = 1,
	.p_value = &hid_cp_command,
};
const ble_gatts_attr_t p_attr_char_4 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_DevInfo[1],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(pnp_id),
	.init_offs = 0,
	.max_len = sizeof(pnp_id),
	.p_value = pnp_id,
};
const ble_gatts_attr_t p_attr_char_5 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_DevInfo[2],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(serial_number),
	.init_offs = 0,
	.max_len = sizeof(serial_number),
	.p_value = serial_number,
};
const ble_gatts_attr_t p_attr_char_6 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_DevInfo[3],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(manufacturer),
	.init_offs = 0,
	.max_len = sizeof(manufacturer),
	.p_value = manufacturer,
};
const ble_gatts_attr_t p_attr_char_7 =
{
	.p_uuid = (ble_uuid_t*)&uuid16_HID[4],
	.p_attr_md = &p_user_desc_md_4,
	.init_len = sizeof(hid_report),
	.init_offs = 0,
	.max_len = sizeof(hid_report),
	.p_value = hid_report,
};
const uint8_t adv_packet[ADV_SIZE] =
{
/*1*/
	2,											/*length*/
	BLE_GAP_AD_TYPE_FLAGS,							/*type - flags*/
	BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE,
/*2*/
	17,											/*length*/
	BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME,			/*type - Complete Local Name*/
	'n', 'r', 'f', '5', '2', '_', 'b', 'l', 'e', '_', 'd', 'e', 'v', 'i', 'c', 'e'
};
//
void main(void)
{
#ifdef TIMESTAMP
	init_time();
#endif
//pins
	NRF_P0->PIN_CNF[6] = (1|(1 << 1));//uart0 tx
	NRF_P0->PIN_CNF[8] = 0;//uart0 rx
	NRF_P0->PIN_CNF[17] = (1|(1 << 1));//led1
	NRF_P0->PIN_CNF[18] = (1|(1 << 1));//led2
	NRF_P0->PIN_CNF[19] = (1|(1 << 1));//led3
	NRF_P0->PIN_CNF[20] = (1|(1 << 1));//led4
	NRF_P0->OUTSET = (1<<6)|(1<<18)|(1<<19)|(1<<20);
	NRF_P0->OUTCLR = (1<<17);
	NRF_P0->PIN_CNF[13] = (3<<2);//button1
	NRF_GPIOTE->CONFIG[0] = (1|(13<<8)|(2<<16));
	NRF_GPIOTE->INTENSET = 1;
//uart
	NRF_UART0->BAUDRATE = 0x01D7E000;//115200
	NRF_UART0->PSELTXD = 6;
	NRF_UART0->PSELRXD = 8;
	NRF_UART0->ENABLE = 4;
	NRF_UART0->EVENTS_ERROR = 0;
	NRF_UART0->EVENTS_RXDRDY = 0;
	NRF_UART0->TASKS_STARTTX = 1;
	uart_log_init(NRF_UART0);
	uart_print_string(NRF_UART0, "\r\n\e[94mBLUE LINK\e[97m");
	print_ic_info();
//ble
	//vars
    ble_enable_params.common_enable_params.vs_uuid_count = UUID128_QUANT;
    ble_enable_params.common_enable_params.p_conn_bw_counts = 0;
    ble_enable_params.gatts_enable_params.attr_tab_size = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
    ble_enable_params.gatts_enable_params.service_changed = 0;
    ble_enable_params.gap_enable_params.periph_conn_count = 1;
    ble_enable_params.gap_enable_params.central_conn_count = 0;
    ble_enable_params.gap_enable_params.central_sec_count = 0;
    gap_conn_params.conn_sup_timeout = 100;//100*10 = 1000ms
    gap_conn_params.max_conn_interval = 40,//40*1.25 = 50ms
    gap_conn_params.min_conn_interval = 6;//6*1.25 = 7,5ms
	gap_conn_params.slave_latency = 0;
	p_adv_params.channel_mask.ch_37_off = 0;
	p_adv_params.channel_mask.ch_38_off = 0;
	p_adv_params.channel_mask.ch_39_off = 0;
	p_adv_params.fp = BLE_GAP_ADV_FP_ANY;
	p_adv_params.interval = 64;//64*0.625 = 40ms
	p_adv_params.p_whitelist = 0;
	p_adv_params.timeout = BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED;
	p_adv_params.type = BLE_GAP_ADV_TYPE_ADV_IND;
	p_adv_params.p_peer_addr = &addr;
	//Security Mode 1 Level 1: No security is needed (aka open link)
	sec_mode.lv = 0;
	sec_mode.sm = 0;
	p_user_desc_md_4.read_perm.lv = 1;
	p_user_desc_md_4.read_perm.sm = 1;
	p_user_desc_md_4.write_perm.lv = 1;
	p_user_desc_md_4.write_perm.sm = 1;
	p_user_desc_md_4.vlen = 0;
	p_user_desc_md_4.vloc = BLE_GATTS_VLOC_STACK;
	p_user_desc_md_4.rd_auth = 0;
	p_user_desc_md_4.wr_auth = 0;
	//code
	err_code = sd_softdevice_enable(&clock_lf_cfg, softdevice_fault_handler);
	log_sd_functions("sd_softdevice_enable", err_code);
	temp = (uint32_t)&__data_start__;
    err_code = sd_ble_enable(&ble_enable_params, &temp);
    log_sd_functions("sd_ble_enable", err_code);
	err_code = sd_ble_version_get(&sd_ver);
	log_sd_functions("sd_ble_version_get", err_code);
	if(err_code == 0) print_sd_version(sd_ver.subversion_number);
    err_code = sd_ble_gap_address_get(&addr);
    log_sd_functions("sd_ble_gap_address_get", err_code);
    print_addr(&addr);
    /*
    //set address
    addr.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE;
    addr.addr[0] = 0x11;
    addr.addr[1] = 0x22;
    addr.addr[2] = 0x33;
    addr.addr[3] = 0x44;
    addr.addr[4] = 0x55;
    addr.addr[5] = 0x66;
    err_code = sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_NONE, &addr);
    log_sd_functions("sd_ble_gap_address_set", err_code);*/
    err_code = sd_ble_gap_adv_data_set(adv_packet, ADV_SIZE, 0, 0);
    log_sd_functions("sd_ble_gap_adv_data_set", err_code);
    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    log_sd_functions("sd_ble_gap_ppcp_set", err_code);
    err_code = sd_ble_gap_device_name_set(&sec_mode, "nrf52_ble_device", 17);
    log_sd_functions("sd_ble_gap_device_name_set", err_code);
    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_HID);
    log_sd_functions("sd_ble_gap_appearance_set", err_code);
    temp = BLE_UUID_TYPE_VENDOR_BEGIN;
    for(uint8_t i=0; i<UUID128_QUANT; i++)
    {
    	err_code = sd_ble_uuid_vs_add(&uuid128[i], (uint8_t*)&temp);
    	log_sd_functions("sd_ble_uuid_vs_add", err_code);
    }
//HID service
	uart_log_add(t_info, "HID service", 0);
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &uuid16_HID[0], &service_handler);
    log_sd_functions("sd_ble_gatts_service_add", err_code);
    uint16_to_hex_string(service_handler, string);
    uart_log_add(t_info, "Service handler", string, 0);
    //Report Map characteristic
    uart_log_add(t_info, "Report Map characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_1, &p_attr_char_1, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	//HID Information Characteristic
    uart_log_add(t_info, "HID Information Characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_1, &p_attr_char_2, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	//HID Control Point characteristic
    uart_log_add(t_info, "HID Control Point characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_2, &p_attr_char_3, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	//Report characteristic
    uart_log_add(t_info, "Report characteristic characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_3, &p_attr_char_7, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
//DevInfo service
	uart_log_add(t_info, "DevInfo service", 0);
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &uuid16_DevInfo[0], &service_handler);
    log_sd_functions("sd_ble_gatts_service_add", err_code);
    uint16_to_hex_string(service_handler, string);
    uart_log_add(t_info, "Service handler", string, 0);
	//PnP_ID characteristic
    uart_log_add(t_info, "PnP_ID characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_1, &p_attr_char_4, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	//Manufacturer Name String characteristic
    uart_log_add(t_info, "Manufacturer Name String characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_1, &p_attr_char_6, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	//Serial Number String characteristic
    uart_log_add(t_info, "Serial Number String characteristic", 0);
    err_code = sd_ble_gatts_characteristic_add(service_handler, &p_char_1, &p_attr_char_5, &p_handles);
    log_sd_functions("sd_ble_gatts_characteristic_add", err_code);
    print_char_handlers(&p_handles);
	err_code = sd_nvic_EnableIRQ(SWI2_EGU2_IRQn);
	log_sd_functions("sd_softdevice_enable - SWI2_EGU2_IRQn", err_code);
	err_code = sd_nvic_SetPriority(GPIOTE_IRQn, 3);
	log_sd_functions("sd_nvic_SetPriority - GPIOTE_IRQn", err_code);
	err_code = sd_nvic_EnableIRQ(GPIOTE_IRQn);
	log_sd_functions("sd_softdevice_enable - GPIOTE_IRQn", err_code);
	while(1)
	{
		delay(7000000);
		NRF_P0->OUTCLR = (1<<18);
		delay(3000000);
		NRF_P0->OUTSET = (1<<18);
		if(gpiote_flag)
		{
			switch(dev_state)
			{
				case 0://Start advertising
					err_code = sd_ble_gap_adv_start(&p_adv_params);
					log_sd_functions("sd_ble_gap_adv_start", err_code);
					dev_state = 1;
					break;
				case 1:
					err_code = sd_ble_gap_adv_stop();
					log_sd_functions("sd_ble_gap_adv_stop", err_code);
					dev_state = 0;
					break;
				case 2:
					err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
					log_sd_functions("sd_ble_gap_disconnect", err_code);
					dev_state = 0;
					break;
				default: break;
			}
			gpiote_flag = 0;
			NRF_GPIOTE->INTENSET = 1;
		}
	}
}
void softdevice_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
	NRF_P0->OUTCLR = (1<<19);//on led3
	switch(id)
	{
		case NRF_FAULT_ID_SD_ASSERT:
			uart_log_add(t_fault, "SoftDevice assertion", 0);
			break;
		case NRF_FAULT_ID_APP_MEMACC:
			uint32_to_hex_string(info, string);
			uart_log_add(t_fault, "Application invalid memory access", string, 0);
			break;
		default:
			uint32_to_hex_string(id, string);
			uart_log_add(t_fault, "Unknown", string, 0);
			break;
	}
    uint32_to_hex_string(pc, string);
    uart_log_add(t_data, "PC", string, 0);
    while(1){};
}
void GPIOTE_IRQHandler(void)
{
	uart_log_add(t_irq, "GPIOTE_IRQ", 0);
	NRF_GPIOTE->EVENTS_IN[0] = 0;
	NRF_GPIOTE->INTENCLR = 1;
	gpiote_flag = 1;
}
void SWI2_EGU2_IRQHandler(void)
{
	uint16_t event_len;
	ble_evt_t* evt = (ble_evt_t*)event_buf;
	const uint8_t string_1[] = "GAP event";
	const uint8_t string_2[] = "GATTS event";
	const uint8_t string_3[] = "GATTC event";
	uart_log_add(t_irq, "SWI2_EGU2_IRQ", 0);
	while(1)
	{
		event_len = 0xffff;
		err_code = sd_ble_evt_get(event_buf, &event_len);
		log_sd_functions("sd_ble_evt_get", err_code);
		if(err_code != NRF_SUCCESS) return;
		else
		{
			switch(evt->header.evt_id)
			{
//common events 0x01 - 0x0F
//GAP events 0x10 - 0x2F
				case BLE_GAP_EVT_CONNECTED:
					uart_log_add(t_info, string_1, "Connection established", 0);
					uart_log_add(t_info, "local device address", 0);
					print_addr(&(evt->evt.gap_evt.params.connected.own_addr));
					uart_log_add(t_info, "peer device address", 0);
					print_addr(&(evt->evt.gap_evt.params.connected.peer_addr));
					uart_log_add(t_info, "BLE role for this connection: ", 0);
					switch(evt->evt.gap_evt.params.connected.role)
					{
						case BLE_GAP_ROLE_INVALID: uart_print_string(NRF_UART0, "Invalid"); break;
						case BLE_GAP_ROLE_PERIPH: uart_print_string(NRF_UART0, "Peripheral"); break;
						case BLE_GAP_ROLE_CENTRAL: uart_print_string(NRF_UART0, "Central"); break;
					}
					print_gap_conn_params(&(evt->evt.gap_evt.params.connected.conn_params));
					m_conn_handle = evt->evt.gap_evt.conn_handle;
					dev_state = 2;
					break;
				case BLE_GAP_EVT_DISCONNECTED:
					uart_log_add(t_info, string_1, "Disconnected from peer - ", 0);
					switch(evt->evt.gap_evt.params.disconnected.reason)
					{
						case BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION:
							uart_print_string(NRF_UART0, "Remote User Terminated Connection");
							break;
						case BLE_HCI_CONNECTION_TIMEOUT:
							uart_print_string(NRF_UART0, "Connection Timeout");
							break;
						case BLE_HCI_LOCAL_HOST_TERMINATED_CONNECTION:
							uart_print_string(NRF_UART0, "Local Host Terminated Connection");
							break;
						case BLE_HCI_STATUS_CODE_LMP_RESPONSE_TIMEOUT:
							uart_print_string(NRF_UART0, "LMP Response Timeout");
							break;
						default:
							uint8_to_hex_string(evt->evt.gap_evt.params.disconnected.reason, string);
							uart_print_string(NRF_UART0, string);
							break;
					}
					m_conn_handle = BLE_CONN_HANDLE_INVALID;
					/*err_code = sd_ble_gap_adv_start(&p_adv_params);
					log_sd_functions("sd_ble_gap_adv_start", err_code);*/
					dev_state = 0;
					break;
				case BLE_GAP_EVT_CONN_PARAM_UPDATE:
					uart_log_add(t_info, string_1, "Connection Parameters updated", 0);
					print_gap_conn_params(&(evt->evt.gap_evt.params.conn_param_update.conn_params));
					break;
				case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
					uart_log_add(t_info, string_1, "Request to provide security parameters", 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.bond, string);
					uart_log_add(t_info, "Perform bonding", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.mitm, string);
					uart_log_add(t_info, "Enable Man In The Middle protection", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.lesc, string);
					uart_log_add(t_info, "Enable LE Secure Connection pairing", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.keypress, string);
					uart_log_add(t_info, "Enable generation of keypress notifications", string, 0);
					uart_log_add(t_info, "IO capabilities", 0);
					switch(evt->evt.gap_evt.params.sec_params_request.peer_params.io_caps)
					{
						case BLE_GAP_IO_CAPS_DISPLAY_ONLY: uart_print_string(NRF_UART0, "Display Only"); break;
						case BLE_GAP_IO_CAPS_DISPLAY_YESNO: uart_print_string(NRF_UART0, "Display and Yes/No entry"); break;
						case BLE_GAP_IO_CAPS_KEYBOARD_ONLY: uart_print_string(NRF_UART0, "Keyboard Only"); break;
						case BLE_GAP_IO_CAPS_NONE: uart_print_string(NRF_UART0, "No I/O capabilities"); break;
						case BLE_GAP_IO_CAPS_KEYBOARD_DISPLAY: uart_print_string(NRF_UART0, "Keyboard and Display"); break;
					}
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.oob, string);
					uart_log_add(t_info, "Out Of Band data available", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.min_key_size, string);
					uart_log_add(t_info, "Minimum encryption key size in octets", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.sec_params_request.peer_params.max_key_size, string);
					uart_log_add(t_info, "Maximum encryption key size in octets", string, 0);
					uart_log_add(t_info, "Key distribution bitmap: keys that the local device will distribute", 0);
					print_sec_kdist(&(evt->evt.gap_evt.params.sec_params_request.peer_params.kdist_own));
					uart_log_add(t_info, "Key distribution bitmap: keys that the remote device will distribute", 0);
					print_sec_kdist(&(evt->evt.gap_evt.params.sec_params_request.peer_params.kdist_peer));
					err_code = sd_ble_gap_sec_params_reply(evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_SUCCESS, &p_sec_params, &p_sec_keyset);
					log_sd_functions("sd_ble_gap_sec_params_reply", err_code);
					break;
				case BLE_GAP_EVT_AUTH_STATUS:
					uart_log_add(t_info, string_1, "Authentication procedure completed with status", 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.auth_status, string);
					uart_log_add(t_info, "Authentication status", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.error_src, string);
					uart_log_add(t_info, "Source that caused the failure", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.bonded, string);
					uart_log_add(t_info, "Procedure resulted in a bond", string, 0);
					uart_log_add(t_info, "Levels supported in Security Mode 1", 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm1_levels.lv1, string);
					uart_log_add(t_info, "Level 1 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm1_levels.lv2, string);
					uart_log_add(t_info, "Level 2 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm1_levels.lv3, string);
					uart_log_add(t_info, "Level 3 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm1_levels.lv4, string);
					uart_log_add(t_info, "Level 4 is supported", string, 0);
					uart_log_add(t_info, "Levels supported in Security Mode 2", 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm2_levels.lv1, string);
					uart_log_add(t_info, "Level 1 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm2_levels.lv2, string);
					uart_log_add(t_info, "Level 2 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm2_levels.lv3, string);
					uart_log_add(t_info, "Level 3 is supported", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.auth_status.sm2_levels.lv4, string);
					uart_log_add(t_info, "Level 4 is supported", string, 0);
					uart_log_add(t_info, "Bitmap stating which keys were exchanged (distributed) by the local device", 0);
					print_sec_kdist(&(evt->evt.gap_evt.params.auth_status.kdist_own));
					uart_log_add(t_info, "Bitmap stating which keys were exchanged (distributed) by the remote device", 0);
					print_sec_kdist(&(evt->evt.gap_evt.params.auth_status.kdist_peer));
					break;
				case BLE_GAP_EVT_CONN_SEC_UPDATE:
					uart_log_add(t_info, string_1, "Connection security updated", 0);
					uint8_to_string(evt->evt.gap_evt.params.conn_sec_update.conn_sec.encr_key_size, string);
					uart_log_add(t_info, "Length of currently active encryption key", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.conn_sec_update.conn_sec.sec_mode.lv, string);
					uart_log_add(t_info, "Level", string, 0);
					uint8_to_string(evt->evt.gap_evt.params.conn_sec_update.conn_sec.sec_mode.sm, string);
					uart_log_add(t_info, "Security Mode", string, 0);
					break;
//GATTC events 0x30 - 0x4F
				case BLE_GATTC_EVT_HVX:
					uart_log_add(t_info, string_3, "Handle Value Notification or Indication event", 0);
					uint16_to_string(evt->evt.gattc_evt.params.hvx.handle, string);
					uart_log_add(t_info, "Handle to which the HVx operation applies", string, 0);
					uart_log_add(t_info, "Indication or Notification: ", 0);
					switch(evt->evt.gattc_evt.params.hvx.type)
					{
						case BLE_GATT_HVX_INVALID: uart_print_string(NRF_UART0, "Invalid Operation"); break;
						case BLE_GATT_HVX_NOTIFICATION: uart_print_string(NRF_UART0, "Handle Value Notification"); break;
						case BLE_GATT_HVX_INDICATION: uart_print_string(NRF_UART0, "Handle Value Indication"); break;
						default: break;
					}
					uint16_to_string(evt->evt.gattc_evt.params.hvx.len, string);
					uart_log_add(t_info, "Attribute data length", string, 0);
					print_ble_data("", evt->evt.gattc_evt.params.hvx.len, evt->evt.gattc_evt.params.hvx.data);
					break;
//GATTS events 0x50 - 0x6F
				case BLE_GATTS_EVT_WRITE:
					uart_log_add(t_info, string_2, "Write operation performed", 0);
					uint16_to_hex_string(evt->evt.gatts_evt.params.write.handle, string);
					uart_log_add(t_info, "Attribute Handle", string, 0);
					print_uuid(&(evt->evt.gatts_evt.params.write.uuid));
					uart_log_add(t_info, "Type of write operation: ", 0);
					switch(evt->evt.gatts_evt.params.write.op)
					{
						case BLE_GATTS_OP_INVALID: uart_print_string(NRF_UART0, "Invalid Operation"); break;
						case BLE_GATTS_OP_WRITE_REQ: uart_print_string(NRF_UART0, "Write Request"); break;
						case BLE_GATTS_OP_WRITE_CMD: uart_print_string(NRF_UART0, "Write Command"); break;
						case BLE_GATTS_OP_SIGN_WRITE_CMD: uart_print_string(NRF_UART0, "Signed Write Command"); break;
						case BLE_GATTS_OP_PREP_WRITE_REQ: uart_print_string(NRF_UART0, "Prepare Write Request"); break;
						case BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL: uart_print_string(NRF_UART0, "Execute Write Request: Cancel all prepared writes"); break;
						case BLE_GATTS_OP_EXEC_WRITE_REQ_NOW: uart_print_string(NRF_UART0, "Execute Write Request: Immediately execute all prepared writes"); break;
						default:
							uint8_to_hex_string(evt->evt.gatts_evt.params.write.op, string);
							uart_print_string(NRF_UART0, string);
							break;
					}
					uint8_to_string(evt->evt.gatts_evt.params.write.auth_required, string);
					uart_log_add(t_info, "Writing operation deferred due to authorization requirement", string, 0);
					uint16_to_string(evt->evt.gatts_evt.params.write.offset, string);
					uart_log_add(t_info, "Offset for the write operation", string, 0);
					uint16_to_string(evt->evt.gatts_evt.params.write.len, string);
					uart_log_add(t_info, "Length of the received data", string, 0);
					print_ble_data("", evt->evt.gattc_evt.params.hvx.len, evt->evt.gattc_evt.params.hvx.data);
					break;
				case BLE_GATTS_EVT_SYS_ATTR_MISSING:
					uart_log_add(t_info, string_2, "A persistent system attribute access is pending", 0);
					break;
//L2CAP events 0x70 - 0x8F
//unknown
				default:
					uint8_to_hex_string((evt->header.evt_id) & 0xff, string);
					uart_log_add(t_info, "Unknown event", string, 0);
			}
		}
	}
}
void print_addr(ble_gap_addr_t* addr)
{
	uint8_t string[3] = {0};
	uart_log_add(t_info, "address: ", 0);
	print_address(addr->addr);
	uart_log_add(t_info, "address type: ", 0);
	switch(addr->addr_type)
	{
		case BLE_GAP_ADDR_TYPE_PUBLIC: uart_print_string(NRF_UART0, "Public address"); break;
		case BLE_GAP_ADDR_TYPE_RANDOM_STATIC: uart_print_string(NRF_UART0, "Random Static address"); break;
		case BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE: uart_print_string(NRF_UART0, "Private Resolvable address"); break;
		case BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_NON_RESOLVABLE: uart_print_string(NRF_UART0, "Private Non-Resolvable address"); break;
	}
}
void print_gap_conn_params(ble_gap_conn_params_t* params)
{
	uint8_t string[6] = {0};
	uint16_to_string(params->min_conn_interval, string);
	uart_log_add(t_info, "Minimum Connection Interval in 1.25 ms units", string, 0);
	uint16_to_string(params->max_conn_interval, string);
	uart_log_add(t_info, "Maximum Connection Interval in 1.25 ms units", string, 0);
	uint16_to_string(params->slave_latency, string);
	uart_log_add(t_info, "Slave Latency in number of connection events", string, 0);
	uint16_to_string(params->conn_sup_timeout, string);
	uart_log_add(t_info, "Connection Supervision Timeout in 10 ms units", string, 0);
}
void print_sd_version(uint16_t id)
{
	uart_log_add(t_info, "SD version: ", 0);
	switch(id)
	{
//nrf51 SD110
		/*case 0x002c: uart_print_string(NRF_UART0, "S110 v5.0.0"); break;
		case 0x0043: uart_print_string(NRF_UART0, "S110 v5.2.1"); break;
		case 0x0049: uart_print_string(NRF_UART0, "S110 v6.0.0"); break;
		case 0x0035: uart_print_string(NRF_UART0, "S110 v6.2.1"); break;
		case 0x004F: uart_print_string(NRF_UART0, "S110 v7.0.0"); break;
		case 0x005A: uart_print_string(NRF_UART0, "S110 v7.1.0"); break;
		case 0x0063: uart_print_string(NRF_UART0, "S110 v7.3.0"); break;
		case 0x0064: uart_print_string(NRF_UART0, "S110 v8.0.0"); break;*/
//nrf51 SD120
		/*case 0x0055: uart_print_string(NRF_UART0, "S120 v1.0.0"); break;
		case 0x0058: uart_print_string(NRF_UART0, "S120 v1.0.1"); break;
		case 0x005B: uart_print_string(NRF_UART0, "S120 v2.0.0-1.alpha"); break;
		case 0x0060: uart_print_string(NRF_UART0, "S120 v2.0.0"); break;
		case 0x006B: uart_print_string(NRF_UART0, "S120 v2.1.0"); break;*/
//nrf51 SD130
		/*case 0x005E: uart_print_string(NRF_UART0, "S130 v0.9.0-1.alpha"); break;
		case 0x0066: uart_print_string(NRF_UART0, "S130 v1.0.0-3.alpha"); break;
		case 0x0067: uart_print_string(NRF_UART0, "S130 v1.0.0"); break;
		case 0x0078: uart_print_string(NRF_UART0, "S130 v2.0.0-7.alpha"); break;
		case 0x0080: uart_print_string(NRF_UART0, "S130 v2.0.0"); break;
		case 0x0087: uart_print_string(NRF_UART0, "S130 v2.0.1"); break;*/
//nrf51 SD210
		/*case 0x004B: uart_print_string(NRF_UART0, "S210 v3.0.0"); break;
		case 0x0057: uart_print_string(NRF_UART0, "S210 v4.0.0 & S210 v4.0.1"); break;
		case 0x0069: uart_print_string(NRF_UART0, "S210 v5.0.0"); break;
		*/
//nrf51 SD310
		/*case 0x004D: uart_print_string(NRF_UART0, "S310 v1.0.0"); break;
		case 0x005D: uart_print_string(NRF_UART0, "S310 v2.0.0 & v2.0.1"); break;
		case 0x0065: uart_print_string(NRF_UART0, "S310 v3.0.0"); break;*/
//nrf52 SD112
		case 0x00A6: uart_print_string(NRF_UART0, "S112 v5.1.0"); break;
		case 0x00A7: uart_print_string(NRF_UART0, "S112 v6.0.0"); break;
		case 0x00b0: uart_print_string(NRF_UART0, "S112 v6.1.0"); break;
//nrf52 SD132
		case 0x006D: uart_print_string(NRF_UART0, "S132 v1.0.0-3.alpha"); break;
		case 0x0074: uart_print_string(NRF_UART0, "S132 v2.0.0-4.alpha"); break;
		case 0x0079: uart_print_string(NRF_UART0, "S132 v2.0.0-7.alpha"); break;
		case 0x0081: uart_print_string(NRF_UART0, "S132 v2.0.0"); break;
		case 0x0088: uart_print_string(NRF_UART0, "S132 v2.0.1"); break;
		case 0x008C: uart_print_string(NRF_UART0, "S132 v3.0.0"); break;
		case 0x0091: uart_print_string(NRF_UART0, "S132 v3.1.0"); break;
		case 0x0092: uart_print_string(NRF_UART0, "S132 v4.0.0-1.alpha"); break;
		case 0x0095: uart_print_string(NRF_UART0, "S132 v4.0.0"); break;
		case 0x0098: uart_print_string(NRF_UART0, "S132 v4.0.2"); break;
		case 0x0099: uart_print_string(NRF_UART0, "S132 v4.0.3"); break;
		case 0x009e: uart_print_string(NRF_UART0, "S132 v4.0.4"); break;
		case 0x009f: uart_print_string(NRF_UART0, "S132 v4.0.5"); break;
		case 0x0090: uart_print_string(NRF_UART0, "S132 v5.0.0-1.alpha"); break;
		case 0x009D: uart_print_string(NRF_UART0, "S132 v5.0.0"); break;
		case 0x00A0: uart_print_string(NRF_UART0, "S132 v5.0.1"); break;
		case 0x00A5: uart_print_string(NRF_UART0, "S132 v5.1.0"); break;
		case 0x00A8: uart_print_string(NRF_UART0, "S132 v6.0.0"); break;
		case 0x00Af: uart_print_string(NRF_UART0, "S132 v6.1.0"); break;
//nrf52 SD140
		case 0x008f: uart_print_string(NRF_UART0, "S140 v5.0.0-1.alpha"); break;
		case 0x0096: uart_print_string(NRF_UART0, "S140 v5.0.0-2.alpha"); break;
		case 0x00A9: uart_print_string(NRF_UART0, "S140 v6.0.0"); break;
		case 0x00Ae: uart_print_string(NRF_UART0, "S140 v6.1.0"); break;
//nrf52 SD212
		case 0x007F: uart_print_string(NRF_UART0, "S212 v0.6.0.alpha"); break;
		case 0x0083: uart_print_string(NRF_UART0, "S212 v0.9.1.alpha"); break;
		case 0x008D: uart_print_string(NRF_UART0, "S212 v2.0.1"); break;
		case 0x0093: uart_print_string(NRF_UART0, "S212 v4.0.5"); break;
		case 0x009C: uart_print_string(NRF_UART0, "S212 v5.0.0"); break;
//nrf52 SD332
		case 0x007E: uart_print_string(NRF_UART0, "S332 v0.6.0.alpha"); break;
		case 0x0082: uart_print_string(NRF_UART0, "S332 v0.9.1.alpha"); break;
		case 0x008E: uart_print_string(NRF_UART0, "S332 v2.0.1"); break;
		case 0x0094: uart_print_string(NRF_UART0, "S332 v4.0.5"); break;
		case 0x009B: uart_print_string(NRF_UART0, "S332 v5.0.0"); break;
//other
		case 0xFFFE: uart_print_string(NRF_UART0, "Development"); break;
		default:
			uart_print_string(NRF_UART0, "Unknown: ");
			uint8_t string[6] = {0};
			uint16_to_string(id, string);
			uart_print_string(NRF_UART0, string);
			break;
	}
}
void print_uuid(ble_uuid_t* uuid)
{
	uint8_t string[7] = {0};
	uart_log_add(t_info, "UUID type", string, 0);
	switch(uuid->type)
	{
		case BLE_UUID_TYPE_UNKNOWN: uart_print_string(NRF_UART0, "Invalid UUID type"); break;
		case BLE_UUID_TYPE_BLE: uart_print_string(NRF_UART0, "Bluetooth SIG UUID (16-bit)"); break;
		case BLE_UUID_TYPE_VENDOR_BEGIN: uart_print_string(NRF_UART0, "Vendor UUID types start at this index (128-bit)"); break;
	}
	uint16_to_hex_string(uuid->uuid, string);
	uart_log_add(t_info, "UUID", string, 0);
}
void print_sec_kdist(ble_gap_sec_kdist_t* data)
{
	uint8_to_string(data->enc, string);
	uart_log_add(t_info, "Long Term Key and Master Identification", string, 0);
	uint8_to_string(data->id, string);
	uart_log_add(t_info, "Identity Resolving Key and Identity Address Information", string, 0);
	uint8_to_string(data->sign, string);
	uart_log_add(t_info, "Connection Signature Resolving Key", string, 0);
	uint8_to_string(data->link, string);
	uart_log_add(t_info, "Derive the Link Key from the LTK", string, 0);
}
void log_sd_functions(uint8_t* function_name_string, uint32_t err_code)
{
	if(err_code == NRF_SUCCESS) uart_log_add(t_function, function_name_string, 0);
	else
	{
		uart_log_add(t_function, function_name_string, "", 0);
		switch(err_code)
		{
			case NRF_ERROR_SVC_HANDLER_MISSING: uart_print_string(NRF_UART0, "SVC handler is missing"); break;
			case NRF_ERROR_SOFTDEVICE_NOT_ENABLED: uart_print_string(NRF_UART0, "SoftDevice has not been enabled"); break;
			case NRF_ERROR_INTERNAL: uart_print_string(NRF_UART0, "Internal Error"); break;
			case NRF_ERROR_NO_MEM: uart_print_string(NRF_UART0, "No Memory for operation"); break;
			case NRF_ERROR_NOT_FOUND: uart_print_string(NRF_UART0, "Not found"); break;
			case NRF_ERROR_NOT_SUPPORTED: uart_print_string(NRF_UART0, "Not supported"); break;
			case NRF_ERROR_INVALID_PARAM: uart_print_string(NRF_UART0, "Invalid Parameter"); break;
			case NRF_ERROR_INVALID_STATE: uart_print_string(NRF_UART0, "Invalid state, operation disallowed in this state"); break;
			case NRF_ERROR_INVALID_LENGTH: uart_print_string(NRF_UART0, "Invalid Length"); break;
			case NRF_ERROR_INVALID_FLAGS: uart_print_string(NRF_UART0, "Invalid Flags"); break;
			case NRF_ERROR_INVALID_DATA: uart_print_string(NRF_UART0, "Invalid Data"); break;
			case NRF_ERROR_DATA_SIZE: uart_print_string(NRF_UART0, "Invalid Data size"); break;
			case NRF_ERROR_TIMEOUT: uart_print_string(NRF_UART0, "Operation timed out"); break;
			case NRF_ERROR_NULL: uart_print_string(NRF_UART0, "Null Pointer"); break;
			case NRF_ERROR_FORBIDDEN: uart_print_string(NRF_UART0, "Forbidden Operation"); break;
			case NRF_ERROR_INVALID_ADDR: uart_print_string(NRF_UART0, "Bad Memory Address"); break;
			case NRF_ERROR_BUSY: uart_print_string(NRF_UART0, "Busy"); break;
			case NRF_ERROR_CONN_COUNT: uart_print_string(NRF_UART0, "Maximum connection count exceeded"); break;
			case NRF_ERROR_RESOURCES: uart_print_string(NRF_UART0, "Not enough resources for operation"); break;
			default:
				uint32_to_hex_string(err_code, string);
				uart_print_string(NRF_UART0, string);
				break;
		}
	}
}
void print_ic_info(void)
{
	uint32_t temp = NRF_FICR->INFO.PART;
	uart_log_add(t_info, "Part code", "", 0);
	switch(temp)
	{
		case 0x52000: uart_print_string(NRF_UART0, "nRF52"); break;
		case 0x52832: uart_print_string(NRF_UART0, "nRF52832"); break;
		case 0x52840: uart_print_string(NRF_UART0, "nRF52840"); break;
		default: uart_print_string(NRF_UART0, "Unspecified"); break;
	}
	/*temp = NRF_FICR->INFO.VARIANT;
	uart_log_add(t_info, "Part Variant", "", 0);
	if(temp != 0xffffffff)
	{
		for(uint8_t i=4; i>0; i--) uart_print_char(NRF_UART0, (temp >> (4*(i-1)))&0xff);
	}
	else uart_print_string(NRF_UART0, "Unspecified");
	temp = NRF_FICR->INFO.PACKAGE;
	uart_log_add(t_info, "Package option", "", 0);
	switch(temp)
	{
		case 0x2000: uart_print_string(NRF_UART0, "48-pin QFN"); break;
		case 0x2001: uart_print_string(NRF_UART0, "7x8 WLCSP 56 balls"); break;
		case 0x2002: uart_print_string(NRF_UART0, "7x8 WLCSP 56 balls"); break;
		case 0x2004: uart_print_string(NRF_UART0, "73-pin aQFN"); break;
		case 0x2005: uart_print_string(NRF_UART0, "7x8 WLCSP 56 balls with backside coating for light protection"); break;
		default: uart_print_string(NRF_UART0, "Unspecified"); break;
	}*/
	temp = NRF_FICR->INFO.RAM;
	uart_log_add(t_info, "RAM size", "", 0);
	print_mem_size(temp);
	temp = NRF_FICR->INFO.FLASH;
	uart_log_add(t_info, "Flash size", "", 0);
	print_mem_size(temp);
	uart_log_add(t_info, "Build date", __DATE__, 0);
	uart_log_add(t_info, "Build time", __TIME__, 0);
}
void print_mem_size(uint32_t size)
{
	switch(size)
	{
		case 0x10: uart_print_string(NRF_UART0, "16 kByte"); break;
		case 0x20: uart_print_string(NRF_UART0, "32 kByte"); break;
		case 0x40: uart_print_string(NRF_UART0, "64 kByte"); break;
		case 0x80: uart_print_string(NRF_UART0, "128 kByte"); break;
		case 0x100: uart_print_string(NRF_UART0, "256 kByte"); break;
		case 0x200: uart_print_string(NRF_UART0, "512 kByte"); break;
		case 0x400: uart_print_string(NRF_UART0, "1 MByte"); break;
		case 0x800: uart_print_string(NRF_UART0, "2 MByte"); break;
		default: uart_print_string(NRF_UART0, "Unspecified"); break;
	}
}
void print_char_handlers(ble_gatts_char_handles_t* handlers)
{
    uint16_to_hex_string(handlers->value_handle, string);
    uart_log_add(t_info, "Handle to the characteristic value", string, 0);
	uint16_to_hex_string(handlers->user_desc_handle, string);
	uart_log_add(t_info, "Handle to the User Description descriptor", string, 0);
	uint16_to_hex_string(handlers->cccd_handle, string);
	uart_log_add(t_info, "Handle to the Client Characteristic Configuration Descriptor", string, 0);
	uint16_to_hex_string(handlers->sccd_handle, string);
	uart_log_add(t_info, "Handle to the Server Characteristic Configuration Descriptor", string, 0);
}
