#include "system.h"
//
void main (void);
//
const fw_header_t header __attribute__ ((section (".fw_header"))) =
{
	.crc = 0xffffffff,
	.size = 0xffffffff,
	.version.number = 0,
	.version.day = 0x02,
	.version.month = 0x01,
	.version.year = 0x18
};
volatile uint16_t ms_cnt = 0;
//
void main (void)
{
	init_gpio();
	board_LED(red, off);
	SysTick_Config(168000);
	NVIC_EnableIRQ(SysTick_IRQn);
	while(1)
	{
		/*delay(30000000);
		board_LED(green, on);
		delay(20000000);
		board_LED(green, off);*/
		if(ms_cnt == 500) board_LED(green, on);
		else
		{
			if(ms_cnt == 1000)
			{
				ms_cnt = 0;
				board_LED(green, off);
			}
		}
	}
}
void SysTick_Handler(void)
{
	ms_cnt++;
}
