#include "system.h"
//
void main (void);
//
uint16_t sensor_data[6] = {0, 0x1000, 0x2000, 0x3000, 0x4000, 0x5000};
//
void main (void)
{
	init_common_gpio();
	frdm_led_rgb(all, off);
	sda_uart_init();
	//spi0
	PORTD->PCR[0] = 2<<8;//spi0_cs0
	PORTD->PCR[1] = 2<<8;//spi0_clk
	PORTD->PCR[2] = 2<<8;//spi0_out
	PORTD->PCR[3] = 2<<8;//spi0_in
	SIM->SCGC6 |= SIM_SCGC6_SPI0_MASK;
	SPI0->CTAR[0] = SPI_CTAR_CPHA_MASK|SPI_CTAR_CPOL_MASK|(3<<16)|7|(0xf<<27)|(6<<12);
	SPI0->MCR = SPI_MCR_MSTR_MASK|(1<<16);
	//i2c0
	/*
	PORTE->PCR[24] = 0x523;
	PORTE->PCR[25] = 0x523;
	SIM->SCGC4 |= SIM_SCGC4_I2C0_MASK;
    I2C0->C1 = 0;
    I2C0->FLT = 0x50;
    I2C0->S = 0x12;
    I2C0->F = 0x21;
    I2C0->C2 = 0;
    */
	while(1)
	{
		delay(2000000);
		for(uint8_t i=0; i<6;i++)
		{
			while((SPI0->SR & SPI_SR_TFFF_MASK) == 0){};
			SPI0->PUSHR = sensor_data[i]++|(1<<16);
		}
	}
}
