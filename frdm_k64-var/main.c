#include "system.h"
#include "print.h"
#include "time.h"
#include "color.h"
#include "arm_math.h"
//
#define _DAC_DMA		0
#define _RNGA			0
#define _RTC			0
#define _FXOS			0
#define _ADC_DMA_TEMP	0
#define _DS18B20		0
#define _MMCAU			0
#define _DS18B20		0
#define _PWM_RGB_LED	0
//
#if _FXOS
	#define FXOS_ADDR		0x1D
	#define STATUS_00_REG 	0x00
	#define ZYXOW_BIT 		Bit._7
	#define ZOW_BIT 		Bit._6
	#define YOW_BIT 		Bit._5
	#define XOW_BIT 		Bit._4
	#define ZYXDR_BIT 		Bit._3
	#define ZDR_BIT 		Bit._2
	#define YDR_BIT 		Bit._1
	#define XDR_BIT 		Bit._0
	#define ZYXOW_MASK 		0x80
	#define ZOW_MASK 		0x40
	#define YOW_MASK 		0x20
	#define XOW_MASK 		0x10
	#define ZYXDR_MASK 		0x08
	#define ZDR_MASK 		0x04
	#define YDR_MASK 		0x02
	#define XDR_MASK 		0x01
	#define F_STATUS_REG 	0x00
	#define F_OVF_BIT 		Bit._7
	#define F_WMRK_FLAG_BIT Bit._6
	#define F_CNT5_BIT 		Bit._5
	#define F_CNT4_BIT 		Bit._4
	#define F_CNT3_BIT 		Bit._3
	#define F_CNT2_BIT 		Bit._2
	#define F_CNT1_BIT 		Bit._1
	#define F_CNT0_BIT 		Bit._0
	#define F_OVF_MASK 		0x80
	#define F_WMRK_FLAG_MASK 0x40
	#define F_CNT5_MASK 0x20
	#define F_CNT4_MASK 0x10
	#define F_CNT3_MASK 0x08
	#define F_CNT2_MASK 0x04
	#define F_CNT1_MASK 0x02
	#define F_CNT0_MASK 0x01
	#define F_CNT_MASK 0x3F
	#define OUT_X_MSB_REG 0x01
	#define OUT_X_LSB_REG 0x02
	#define OUT_Y_MSB_REG 0x03
	#define OUT_Y_LSB_REG 0x04
	#define OUT_Z_MSB_REG 0x05
	#define OUT_Z_LSB_REG 0x06
	#define F_SETUP_REG 0x09
	#define F_MODE1_BIT Bit._7
	#define F_MODE0_BIT Bit._6
	#define F_WMRK5_BIT Bit._5
	#define F_WMRK4_BIT Bit._4
	#define F_WMRK3_BIT Bit._3
	#define F_WMRK2_BIT Bit._2
	#define F_WMRK1_BIT Bit._1
	#define F_WMRK0_BIT Bit._0
	#define F_MODE1_MASK 0x80
	#define F_MODE0_MASK 0x40
	#define F_WMRK5_MASK 0x20
	#define F_WMRK4_MASK 0x10
	#define F_WMRK3_MASK 0x08
	#define F_WMRK2_MASK 0x04
	#define F_WMRK1_MASK 0x02
	#define F_WMRK0_MASK 0x01
	#define F_MODE_MASK 0xC0
	#define F_WMRK_MASK 0x3F
	#define F_MODE_DISABLED 0x00
	#define F_MODE_CIRCULAR (F_MODE0_MASK)
	#define F_MODE_FILL (F_MODE1_MASK)
	#define F_MODE_TRIGGER (F_MODE1_MASK + F_MODE0_MASK)
	#define TRIG_CFG_REG 0x0A
	#define TRIG_TRANS_BIT Bit._5
	#define TRIG_LNDPRT_BIT Bit._4
	#define TRIG_PULSE_BIT Bit._3
	#define TRIG_FF_MT_BIT Bit._2
	#define TRIG_TRANS_MASK 0x20
	#define TRIG_LNDPRT_MASK 0x10
	#define TRIG_PULSE_MASK 0x08
	#define TRIG_FF_MT_MASK 0x04
	#define SYSMOD_REG 0x0B
	#define FGERR_BIT Bit._7 /* MMA8451 only */
	#define FGT_4_BIT Bit._6 /* MMA8451 only */
	#define FGT_3_BIT Bit._5 /* MMA8451 only */
	#define FGT_2_BIT Bit._4 /* MMA8451 only */
	#define FGT_1_BIT Bit._3 /* MMA8451 only */
	#define FGT_0_BIT Bit._2 /* MMA8451 only */
	#define SYSMOD1_BIT Bit._1
	#define SYSMOD0_BIT Bit._0
	#define FGERR_MASK 0x80 /* MMA8451 only */
	#define FGT_4_MASK 0x40 /* MMA8451 only */
	#define FGT_3_MASK 0x20 /* MMA8451 only */
	#define FGT_2_MASK 0x10 /* MMA8451 only */
	#define FGT_1_MASK 0x08 /* MMA8451 only */
	#define FGT_0_MASK 0x04 /* MMA8451 only */
	#define FGT_MASK 0x7C   /* MMA8451 only */
	#define SYSMOD1_MASK 0x02
	#define SYSMOD0_MASK 0x01
	#define SYSMOD_MASK 0x03
	#define SYSMOD_STANDBY 0x00
	#define SYSMOD_WAKE (SYSMOD0_MASK)
	#define SYSMOD_SLEEP (SYSMOD1_MASK)
	#define INT_SOURCE_REG 0x0C
	#define SRC_ASLP_BIT Bit._7
	#define SRC_FIFO_BIT Bit._6 /* MMA8451 only */
	#define SRC_TRANS_BIT Bit._5
	#define SRC_LNDPRT_BIT Bit._4
	#define SRC_PULSE_BIT Bit._3
	#define SRC_FF_MT_BIT Bit._2
	#define SRC_DRDY_BIT Bit._0
	#define SRC_ASLP_MASK 0x80
	#define SRC_FIFO_MASK 0x40 /* MMA8451 only */
	#define SRC_TRANS_MASK 0x20
	#define SRC_LNDPRT_MASK 0x10
	#define SRC_PULSE_MASK 0x08
	#define SRC_FF_MT_MASK 0x04
	#define SRC_DRDY_MASK 0x01
	#define WHO_AM_I_REG 0x0D
	#define kFXOS_WHO_AM_I_Device_ID 0xC7
	#define XYZ_DATA_CFG_REG 0x0E
	#define HPF_OUT_BIT Bit._4 /* MMA8451 and MMA8452 only */
	#define FS1_BIT Bit._1
	#define FS0_BIT Bit._0
	#define HPF_OUT_MASK 0x10 /* MMA8451 and MMA8452 only */
	#define FS1_MASK 0x02
	#define FS0_MASK 0x01
	#define FS_MASK 0x03
	#define FULL_SCALE_2G 0x00
	#define FULL_SCALE_4G (FS0_MASK)
	#define FULL_SCALE_8G (FS1_MASK)
	#define HP_FILTER_CUTOFF_REG 0x0F
	#define PULSE_HPF_BYP_BIT Bit._5
	#define PULSE_LPF_EN_BIT Bit._4
	#define SEL1_BIT Bit._1
	#define SEL0_BIT Bit._0
	#define PULSE_HPF_BYP_MASK 0x20
	#define PULSE_LPF_EN_MASK 0x10
	#define SEL1_MASK 0x02
	#define SEL0_MASK 0x01
	#define SEL_MASK 0x03
	#define PL_STATUS_REG 0x10
	#define NEWLP_BIT Bit._7
	#define LO_BIT Bit._6
	#define LAPO1_BIT Bit._2
	#define LAPO0_BIT Bit._1
	#define BAFRO_BIT Bit._0
	#define NEWLP_MASK 0x80
	#define LO_MASK 0x40
	#define LAPO1_MASK 0x04
	#define LAPO0_MASK 0x02
	#define BAFRO_MASK 0x01
	#define LAPO_MASK 0x06
	#define PL_CFG_REG 0x11
	#define DBCNTM_BIT Bit._7
	#define PL_EN_BIT Bit._6
	#define DBCNTM_MASK 0x80
	#define PL_EN_MASK 0x40
	#define PL_COUNT_REG 0x12
	#define PL_BF_ZCOMP_REG 0x13
	#define BKFR1_BIT Bit._7
	#define BKFR0_BIT Bit._6
	#define ZLOCK2_BIT Bit._2
	#define ZLOCK1_BIT Bit._1
	#define ZLOCK0_BIT Bit._0
	#define BKFR1_MASK 0x80
	#define BKFR0_MASK 0x40
	#define ZLOCK2_MASK 0x04
	#define ZLOCK1_MASK 0x02
	#define ZLOCK0_MASK 0x01
	#define BKFR_MASK 0xC0
	#define ZLOCK_MASK 0x07
	#define PL_P_L_THS_REG 0x14
	#define P_L_THS4_BIT Bit._7
	#define P_L_THS3_BIT Bit._6
	#define P_L_THS2_BIT Bit._5
	#define P_L_THS1_BIT Bit._4
	#define P_L_THS0_BIT Bit._3
	#define HYS2_BIT Bit._2
	#define HYS1_BIT Bit._1
	#define HYS0_BIT Bit._0
	#define P_L_THS4_MASK 0x80
	#define P_L_THS3_MASK 0x40
	#define P_L_THS2_MASK 0x20
	#define P_L_THS1_MASK 0x10
	#define P_L_THS0_MASK 0x08
	#define HYS2_MASK 0x04
	#define HYS1_MASK 0x02
	#define HYS0_MASK 0x01
	#define P_L_THS_MASK 0xF8
	#define HYS_MASK 0x07
	#define FF_MT_CFG_REG 0x15
	#define ELE_BIT Bit._7
	#define OAE_BIT Bit._6
	#define ZEFE_BIT Bit._5
	#define YEFE_BIT Bit._4
	#define XEFE_BIT Bit._3
	#define ELE_MASK 0x80
	#define OAE_MASK 0x40
	#define ZEFE_MASK 0x20
	#define YEFE_MASK 0x10
	#define XEFE_MASK 0x08
	#define FF_MT_SRC_REG 0x16
	#define EA_BIT Bit._7
	#define ZHE_BIT Bit._5
	#define ZHP_BIT Bit._4
	#define YHE_BIT Bit._3
	#define YHP_BIT Bit._2
	#define XHE_BIT Bit._1
	#define XHP_BIT Bit._0
	#define EA_MASK 0x80
	#define ZHE_MASK 0x20
	#define ZHP_MASK 0x10
	#define YHE_MASK 0x08
	#define YHP_MASK 0x04
	#define XHE_MASK 0x02
	#define XHP_MASK 0x01
	#define FT_MT_THS_REG 0x17
	#define TRANSIENT_THS_REG 0x1F
	#define DBCNTM_BIT Bit._7
	#define THS6_BIT Bit._6
	#define THS5_BIT Bit._5
	#define THS4_BIT Bit._4
	#define THS3_BIT Bit._3
	#define THS2_BIT Bit._2
	#define THS1_BIT Bit._1
	#define THS0_BIT Bit._0
	#define DBCNTM_MASK 0x80
	#define THS6_MASK 0x40
	#define THS5_MASK 0x20
	#define THS4_MASK 0x10
	#define THS3_MASK 0x08
	#define THS2_MASK 0x04
	#define TXS1_MASK 0x02
	#define THS0_MASK 0x01
	#define THS_MASK 0x7F
	#define FF_MT_COUNT_REG 0x18
	#define TRANSIENT_CFG_REG 0x1D
	#define TELE_BIT Bit._4
	#define ZTEFE_BIT Bit._3
	#define YTEFE_BIT Bit._2
	#define XTEFE_BIT Bit._1
	#define HPF_BYP_BIT Bit._0
	#define TELE_MASK 0x10
	#define ZTEFE_MASK 0x08
	#define YTEFE_MASK 0x04
	#define XTEFE_MASK 0x02
	#define HPF_BYP_MASK 0x01
	#define TRANSIENT_SRC_REG 0x1E
	#define TEA_BIT Bit._6
	#define ZTRANSE_BIT Bit._5
	#define Z_TRANS_POL_BIT Bit._4
	#define YTRANSE_BIT Bit._3
	#define Y_TRANS_POL_BIT Bit._2
	#define XTRANSE_BIT Bit._1
	#define X_TRANS_POL_BIT Bit._0
	#define TEA_MASK 0x40
	#define ZTRANSE_MASK 0x20
	#define Z_TRANS_POL_MASK 0x10
	#define YTRANSE_MASK 0x08
	#define Y_TRANS_POL_MASK 0x04
	#define XTRANSE_MASK 0x02
	#define X_TRANS_POL_MASK 0x01
	#define TRANSIENT_COUNT_REG 0x20
	#define PULSE_CFG_REG 0x21
	#define DPA_BIT Bit._7
	#define PELE_BIT Bit._6
	#define ZDPEFE_BIT Bit._5
	#define ZSPEFE_BIT Bit._4
	#define YDPEFE_BIT Bit._3
	#define YSPEFE_BIT Bit._2
	#define XDPEFE_BIT Bit._1
	#define XSPEFE_BIT Bit._0
	#define DPA_MASK 0x80
	#define PELE_MASK 0x40
	#define ZDPEFE_MASK 0x20
	#define ZSPEFE_MASK 0x10
	#define YDPEFE_MASK 0x08
	#define YSPEFE_MASK 0x04
	#define XDPEFE_MASK 0x02
	#define XSPEFE_MASK 0x01
	#define PULSE_SRC_REG 0x22
	#define PEA_BIT Bit._7
	#define AXZ_BIT Bit._6
	#define AXY_BIT Bit._5
	#define AXX_BIT Bit._4
	#define DPE_BIT Bit._3
	#define POLZ_BIT Bit._2
	#define POLY_BIT Bit._1
	#define POLX_BIT Bit._0
	#define PEA_MASK 0x80
	#define AXZ_MASK 0x40
	#define AXY_MASK 0x20
	#define AXX_MASK 0x10
	#define DPE_MASK 0x08
	#define POLZ_MASK 0x04
	#define POLY_MASK 0x02
	#define POLX_MASK 0x01
	#define PULSE_THSX_REG 0x23
	#define PULSE_THSY_REG 0x24
	#define PULSE_THSZ_REG 0x25
	#define PTHS_MASK 0x7F
	#define PULSE_TMLT_REG 0x26
	#define PULSE_LTCY_REG 0x27
	#define PULSE_WIND_REG 0x28
	#define ASLP_COUNT_REG 0x29
	#define CTRL_REG1 0x2A
	#define ASLP_RATE1_BIT Bit._7
	#define ASLP_RATE0_BIT Bit._6
	#define DR2_BIT Bit._5
	#define DR1_BIT Bit._4
	#define DR0_BIT Bit._3
	#define LNOISE_BIT Bit._1
	#define FREAD_BIT Bit._1
	#define ACTIVE_BIT Bit._0
	#define ASLP_RATE1_MASK 0x80
	#define ASLP_RATE0_MASK 0x40
	#define DR2_MASK 0x20
	#define DR1_MASK 0x10
	#define DR0_MASK 0x08
	#define LNOISE_MASK 0x04
	#define FREAD_MASK 0x02
	#define ACTIVE_MASK 0x01
	#define ASLP_RATE_MASK 0xC0
	#define DR_MASK 0x38
	#define ASLP_RATE_20MS 0x00
	#define ASLP_RATE_80MS (ASLP_RATE0_MASK)
	#define ASLP_RATE_160MS (ASLP_RATE1_MASK)
	#define ASLP_RATE_640MS (ASLP_RATE1_MASK + ASLP_RATE0_MASK)
	#define ASLP_RATE_50HZ (ASLP_RATE_20MS)
	#define ASLP_RATE_12_5HZ (ASLP_RATE_80MS)
	#define ASLP_RATE_6_25HZ (ASLP_RATE_160MS)
	#define ASLP_RATE_1_56HZ (ASLP_RATE_640MS)
	#define HYB_ASLP_RATE_25HZ (ASLP_RATE_20MS)
	#define HYB_ASLP_RATE_6_25HZ (ASLP_RATE_80MS)
	#define HYB_ASLP_RATE_1_56HZ (ASLP_RATE_160MS)
	#define HYB_ASLP_RATE_0_8HZ (ASLP_RATE_640MS)
	#define DATA_RATE_1250US 0x00
	#define DATA_RATE_2500US (DR0_MASK)
	#define DATA_RATE_5MS (DR1_MASK)
	#define DATA_RATE_10MS (DR1_MASK + DR0_MASK)
	#define DATA_RATE_20MS (DR2_MASK)
	#define DATA_RATE_80MS (DR2_MASK + DR0_MASK)
	#define DATA_RATE_160MS (DR2_MASK + DR1_MASK)
	#define DATA_RATE_640MS (DR2_MASK + DR1_MASK + DR0_MASK)
	#define DATA_RATE_800HZ (DATA_RATE_1250US)
	#define DATA_RATE_400HZ (DATA_RATE_2500US)
	#define DATA_RATE_200HZ (DATA_RATE_5MS)
	#define DATA_RATE_100HZ (DATA_RATE_10MS)
	#define DATA_RATE_50HZ (DATA_RATE_20MS)
	#define DATA_RATE_12_5HZ (DATA_RATE_80MS)
	#define DATA_RATE_6_25HZ (DATA_RATE_160MS)
	#define DATA_RATE_1_56HZ (DATA_RATE_640MS)
	#define HYB_DATA_RATE_400HZ (DATA_RATE_1250US)
	#define HYB_DATA_RATE_200HZ (DATA_RATE_2500US)
	#define HYB_DATA_RATE_100HZ (DATA_RATE_5MS)
	#define HYB_DATA_RATE_50HZ (DATA_RATE_10MS)
	#define HYB_DATA_RATE_25HZ (DATA_RATE_20MS)
	#define HYB_DATA_RATE_6_25HZ (DATA_RATE_80MS)
	#define HYB_DATA_RATE_3_15HZ (DATA_RATE_160MS)
	#define HYB_DATA_RATE_0_8HZ (DATA_RATE_640MS)
	#define ACTIVE (ACTIVE_MASK)
	#define STANDBY 0x00
	#define CTRL_REG2 0x2B
	#define ST_BIT Bit._7
	#define RST_BIT Bit._6
	#define SMODS1_BIT Bit._4
	#define SMODS0_BIT Bit._3
	#define SLPE_BIT Bit._2
	#define MODS1_BIT Bit._1
	#define MODS0_BIT Bit._0
	#define ST_MASK 0x80
	#define RST_MASK 0x40
	#define SMODS1_MASK 0x10
	#define SMODS0_MASK 0x08
	#define SLPE_MASK 0x04
	#define MODS1_MASK 0x02
	#define MODS0_MASK 0x01
	#define SMODS_MASK 0x18
	#define MODS_MASK 0x03
	#define SMOD_NORMAL 0x00
	#define SMOD_LOW_NOISE (SMODS0_MASK)
	#define SMOD_HIGH_RES (SMODS1_MASK)
	#define SMOD_LOW_POWER (SMODS1_MASK + SMODS0_MASK)
	#define MOD_NORMAL 0x00
	#define MOD_LOW_NOISE (MODS0_MASK)
	#define MOD_HIGH_RES (MODS1_MASK)
	#define MOD_LOW_POWER (MODS1_MASK + MODS0_MASK)
	#define CTRL_REG3 0x2C
	#define FIFO_GATE_BIT Bit._7 /* MMA8451 only */
	#define WAKE_TRANS_BIT Bit._6
	#define WAKE_LNDPRT_BIT Bit._5
	#define WAKE_PULSE_BIT Bit._4
	#define WAKE_FF_MT_BIT Bit._3
	#define IPOL_BIT Bit._1
	#define PP_OD_BIT Bit._0
	#define FIFO_GATE_MASK 0x80 /* MMA8451 only */
	#define WAKE_TRANS_MASK 0x40
	#define WAKE_LNDPRT_MASK 0x20
	#define WAKE_PULSE_MASK 0x10
	#define WAKE_FF_MT_MASK 0x08
	#define IPOL_MASK 0x02
	#define PP_OD_MASK 0x01
	#define CTRL_REG4 0x2D
	#define INT_EN_ASLP_BIT Bit._7
	#define INT_EN_FIFO_BIT Bit._6 /* MMA8451 only */
	#define INT_EN_TRANS_BIT Bit._5
	#define INT_EN_LNDPRT_BIT Bit._4
	#define INT_EN_PULSE_BIT Bit._3
	#define INT_EN_FF_MT_BIT Bit._2
	#define INT_EN_DRDY_BIT Bit._0
	#define INT_EN_ASLP_MASK 0x80
	#define INT_EN_FIFO_MASK 0x40 /* MMA8451 only */
	#define INT_EN_TRANS_MASK 0x20
	#define INT_EN_LNDPRT_MASK 0x10
	#define INT_EN_PULSE_MASK 0x08
	#define INT_EN_FF_MT_MASK 0x04
	#define INT_EN_DRDY_MASK 0x01
	#define CTRL_REG5 0x2E
	#define INT_CFG_ASLP_BIT Bit._7
	#define INT_CFG_FIFO_BIT Bit._6
	#define INT_CFG_TRANS_BIT Bit._5
	#define INT_CFG_LNDPRT_BIT Bit._4
	#define INT_CFG_PULSE_BIT Bit._3
	#define INT_CFG_FF_MT_BIT Bit._2
	#define INT_CFG_DRDY_BIT Bit._0
	#define INT_CFG_ASLP_MASK 0x80
	#define INT_CFG_FIFO_MASK 0x40
	#define INT_CFG_TRANS_MASK 0x20
	#define INT_CFG_LNDPRT_MASK 0x10
	#define INT_CFG_PULSE_MASK 0x08
	#define INT_CFG_FF_MT_MASK 0x04
	#define INT_CFG_DRDY_MASK 0x01
	#define OFF_X_REG 0x2F
	#define OFF_Y_REG 0x30
	#define OFF_Z_REG 0x31
	#define M_DR_STATUS_REG 0x32
	#define ZYXOW_BIT Bit._7
	#define ZOW_BIT Bit._6
	#define YOW_BIT Bit._5
	#define XOW_BIT Bit._4
	#define ZYXDR_BIT Bit._3
	#define ZDR_BIT Bit._2
	#define YDR_BIT Bit._1
	#define XDR_BIT Bit._0
	#define ZYXOW_MASK 0x80
	#define ZOW_MASK 0x40
	#define YOW_MASK 0x20
	#define XOW_MASK 0x10
	#define ZYXDR_MASK 0x08
	#define ZDR_MASK 0x04
	#define YDR_MASK 0x02
	#define XDR_MASK 0x01
	#define M_OUT_X_MSB_REG 0x33
	#define M_OUT_X_LSB_REG 0x34
	#define M_OUT_Y_MSB_REG 0x35
	#define M_OUT_Y_LSB_REG 0x36
	#define M_OUT_Z_MSB_REG 0x37
	#define M_OUT_Z_LSB_REG 0x38
	#define CMP_X_MSB_REG 0x39
	#define CMP_X_LSB_REG 0x3A
	#define CMP_Y_MSB_REG 0x3B
	#define CMP_Y_LSB_REG 0x3C
	#define CMP_Z_MSB_REG 0x3D
	#define CMP_Z_LSB_REG 0x3E
	#define M_OFF_X_MSB_REG 0x3F
	#define M_OFF_X_LSB_REG 0x40
	#define M_OFF_Y_MSB_REG 0x41
	#define M_OFF_Y_LSB_REG 0x42
	#define M_OFF_Z_MSB_REG 0x43
	#define M_OFF_Z_LSB_REG 0x44
	#define MAX_X_MSB_REG 0x45
	#define MAX_X_LSB_REG 0x46
	#define MAX_Y_MSB_REG 0x47
	#define MAX_Y_LSB_REG 0x48
	#define MAX_Z_MSB_REG 0x49
	#define MAX_Z_LSB_REG 0x4A
	#define MIN_X_MSB_REG 0x4B
	#define MIN_X_LSB_REG 0x4C
	#define MIN_Y_MSB_REG 0x4D
	#define MIN_Y_LSB_REG 0x4E
	#define MIN_Z_MSB_REG 0x4F
	#define MIN_Z_LSB_REG 0x50
	#define TEMP_REG 0x51
	#define M_THS_CFG_REG 0x52
	#define M_THS_SRC_REG 0x53
	#define M_THS_X_MSB_REG 0x54
	#define M_THS_X_LSB_REG 0x55
	#define M_THS_Y_MSB_REG 0x56
	#define M_THS_Y_LSB_REG 0x57
	#define M_THS_Z_MSB_REG 0x58
	#define M_THS_Z_LSB_REG 0x59
	#define M_THS_COUNT 0x5A
	#define M_CTRL_REG1 0x5B
	#define M_HMS_0_BIT Bit._7
	#define M_HMS_1_BIT Bit._6
	#define M_OS_2_BIT Bit._5
	#define M_OS_1_BIT Bit._4
	#define M_OS_0_BIT Bit._3
	#define M_OST_BIT Bit._2
	#define M_RST_BIT Bit._1
	#define M_ACAL_BIT Bit._0
	#define M_ACAL_MASK 0x80
	#define M_RST_MASK 0x40
	#define M_OST_MASK 0x20
	#define M_OSR2_MASK 0x10
	#define M_OSR1_MASK 0x08
	#define M_OSR0_MASK 0x04
	#define M_HMS1_MASK 0x02
	#define M_HMS0_MASK 0x01
	#define M_OSR_MASK 0x1C
	#define M_HMS_MASK 0x03
	#define M_OSR_1_56_HZ 0x00
	#define M_OSR_6_25_HZ M_OSR0_MASK
	#define M_OSR_12_5_HZ M_OSR1_MASK
	#define M_OSR_50_HZ M_OSR1_MASK + M_OSR0_MASK
	#define M_OSR_100_HZ M_OSR2_MASK
	#define M_OSR_200_HZ M_OSR2_MASK + M_OSR0_MASK
	#define M_OSR_400_HZ M_OSR2_MASK + M_OSR1_MASK
	#define M_OSR_800_HZ M_OSR2_MASK + M_OSR1_MASK + M_OSR0_MASK
	#define ACCEL_ACTIVE 0x00
	#define MAG_ACTIVE M_HMS0_MASK
	#define HYBRID_ACTIVE (M_HMS1_MASK | M_HMS0_MASK)
	#define M_CTRL_REG2 0x5C
	#define M_HYB_AUTOINC_BIT Bit._5
	#define M_MAXMIN_DIS_BIT Bit._4
	#define M_MAXMIN_DIS_THS_BIT Bit._3
	#define M_MAXMIN_RST_BIT Bit._2
	#define M_RST_CNT1_BIT Bit._1
	#define M_RST_CNT0_BIT Bit._0
	#define M_HYB_AUTOINC_MASK 0x20
	#define M_MAXMIN_DIS_MASK 0x10
	#define M_MAXMIN_DIS_THS_MASK 0x08
	#define M_MAXMIN_RST_MASK 0x04
	#define M_RST_CNT1_MASK 0x02
	#define M_RST_CNT0_MASK 0x01
	#define RST_ODR_CYCLE 0x00
	#define RST_16_ODR_CYCLE M_RST_CNT0_MASK
	#define RST_512_ODR_CYCLE M_RST_CNT1_MASK
	#define RST_DISABLED M_RST_CNT1_MASK + M_RST_CNT0_MASK
	#define M_CTRL_REG3 0x5D
	#define M_RAW_BIT Bit._7
	#define M_ASLP_OS_2_BIT Bit._6
	#define M_ASLP_OS_1_BIT Bit._5
	#define M_ASLP_OS_0_BIT Bit._4
	#define M_THS_XYZ_BIT Bit._3
	#define M_ST_Z_BIT Bit._2
	#define M_ST_XY1_BIT Bit._1
	#define M_ST_XY0_BIT Bit._0
	#define M_RAW_MASK 0x80
	#define M_ASLP_OS_2_MASK 0x40
	#define M_ASLP_OS_1_MASK 0x20
	#define M_ASLP_OS_0_MASK 0x10
	#define M_THS_XYZ_MASK 0x08
	#define M_ST_Z_MASK 0x04
	#define M_ST_XY1_MASK 0x02
	#define M_ST_XY0_MASK 0x01
	#define M_ASLP_OSR_MASK 0x70
	#define M_ST_XY_MASK 0x03
	#define M_ASLP_OSR_1_56_HZ 0x00
	#define M_ASLP_OSR_6_25_HZ M_ASLP_OS_0_MASK
	#define M_ASLP_OSR_12_5_HZ M_ASLP_OS_1_MASK
	#define M_ASLP_OSR_50_HZ M_ASLP_OS_1_MASK + M_ASLP_OS_0_MASK
	#define M_ASLP_OSR_100_HZ M_ASLP_OS_2_MASK
	#define M_ASLP_OSR_200_HZ M_ASLP_OS_2_MASK + M_ASLP_OS_0_MASK
	#define M_ASLP_OSR_400_HZ M_ASLP_OS_2_MASK + M_ASLP_OS_1_MASK
	#define M_ASLP_OSR_800_HZ M_ASLP_OS_2_MASK + M_ASLP_OS_1_MASK + M_ASLP_OS_0_MASK
	#define M_INT_SOURCE 0x5E
	#define SRC_M_DRDY_BIT Bit._2
	#define SRC_M_VECM_BIT Bit._1
	#define SRC_M_THS_BIT Bit._0
	#define SRC_M_DRDY_MASK 0x04
	#define SRC_M_VECM_MASK 0x02
	#define SRC_M_THS_MASK 0x01
	#define A_VECM_CFG 0x5F
	#define A_VECM_INIT_CFG_BIT Bit._6
	#define A_VECM_INIT_EN_BIT Bit._5
	#define A_VECM_WAKE_EN_BIT Bit._4
	#define A_VECM_EN_BIT Bit._3
	#define A_VECM_UPDM_BIT Bit._2
	#define A_VECM_INITM_BIT Bit._1
	#define A_VECM_ELE_BIT Bit._0
	#define A_VECM_INIT_CFG_MASK 0x40
	#define A_VECM_INIT_EN_MASK 0x20
	#define A_VECM_WAKE_EN_MASK 0x10
	#define A_VECM_EN_MASK 0x08
	#define A_VECM_UPDM_MASK 0x04
	#define A_VECM_INITM_MASK 0x02
	#define A_VECM_ELE_MASK 0x01
	#define A_VECM_THS_MSB 0x60
	#define A_VECM_DBCNTM_BIT Bit._7
	#define A_VECM_DBCNTM_MASK 0x80
	#define A_VECM_THS_LSB 0x61
	#define A_VECM_CNT 0x62
	#define A_VECM_INITX_MSB 0x63
	#define A_VECM_INITX_LSB 0x64
	#define A_VECM_INITY_MSB 0x65
	#define A_VECM_INITY_LSB 0x66
	#define A_VECM_INITZ_MSB 0x67
	#define A_VECM_INITZ_LSB 0x68
	#define M_VECM_CFG 0x69
	#define M_VECM_INIT_CFG_BIT Bit._6
	#define M_VECM_INIT_EN_BIT Bit._5
	#define M_VECM_WAKE_EN_BIT Bit._4
	#define M_VECM_EN_BIT Bit._3
	#define M_VECM_UPDM_BIT Bit._2
	#define M_VECM_INITM_BIT Bit._1
	#define M_VECM_ELE_BIT Bit._0
	#define M_VECM_INIT_CFG_MASK 0x40
	#define M_VECM_INIT_EN_MASK 0x20
	#define M_VECM_WAKE_EN_MASK 0x10
	#define M_VECM_EN_MASK 0x08
	#define M_VECM_UPDM_MASK 0x04
	#define M_VECM_INITM_MASK 0x02
	#define M_VECM_ELE_MASK 0x01
	#define M_VECM_THS_MSB 0x6A
	#define M_VECM_DBCNTM_BIT Bit._7
	#define M_VECM_DBCNTM_MASK 0x80
	#define M_VECM_THS_LSB 0x6B
	#define M_VECM_CNT 0x6C
	#define M_VECM_INITX_MSB 0x6D
	#define M_VECM_INITX_LSB 0x6E
	#define M_VECM_INITY_MSB 0x6F
	#define M_VECM_INITY_LSB 0x70
	#define M_VECM_INITZ_MSB 0x71
	#define M_VECM_INITZ_LSB 0x72
	#define A_FFMT_THS_X_MSB 0x73
	#define A_FFMT_THS_X_EN_BIT Bit._7
	#define A_FFMT_THS_XYZ_EN_MASK 0x80
	#define A_FFMT_THS_X_LSB 0x74
	#define A_FFMT_THS_X_LSB_MASK 0xFC
	#define A_FFMT_THS_Y_MSB 0x75
	#define A_FFMT_THS_Y_EN_BIT Bit._7
	#define A_FFMT_THS_Y_EN_MASK 0x80
	#define A_FFMT_THS_Y_LSB 0x76
	#define A_FFMT_THS_Y_LSB_MASK 0xFC
	#define A_FFMT_THS_Z_MSB 0x77
	#define A_FFMT_THS_Z_EN_BIT Bit._7
	#define A_FFMT_THS_Z_EN_MASK 0x80
	#define A_FFMT_THS_Z_LSB 0x78
	#define A_FFMT_THS_Z_LSB_MASK 0xFC
#endif
#define SKIP_ROM_COMMAND		0xCC
#define WRITE_SCRATCHPAD		0x4E
#define COPY_SCRATCHPAD			0x48
#define RESOLUTION_10_BIT		0x3F
#define CONVERT_TEMPERATURE		0x44
#define READ_SCRATCHPAD			0xBE
//
void main (void);
uint8_t get_data(void);
void print_time(void);
uint8_t i2c0_rd(uint8_t address, uint8_t reg);
void i2c0_wr(uint8_t address, uint8_t reg, uint8_t data);
void DMA0_IRQHandler(void);
void DMA1_IRQHandler(void);
void pwm_rgb_led_set_color(rgb_888_t* color);
void ow_send_presence(void);
void ow_wr_bit(uint8_t bit);
void ow_wr_byte(uint8_t data);
uint8_t ow_rd_bit(void);
void ow_start(void);
void print_temp(void);
//
uint8_t state = 0, adc_ch_string[12] = "\n\rChannel 2\0", switcher1 = 1, switcher2 = 0, recieve_char, mutex = 0, pointer = 1;
uint16_t adc_buf[32], sinetable[128] = {0};
rgb_888_t color = {0};
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x24,
	.month = 0x12,
	.year = 0x18
};
//
void main (void)
{
	init_common_gpio();
	frdm_led_rgb(all, off);
	sda_uart_init();
#if _PWM_RGB_LED
	SIM->SCGC6 |= SIM_SCGC6_FTM0_MASK;
	PORTC->PCR[1] = 0x400;
	PORTC->PCR[2] = 0x400;
	PORTC->PCR[3] = 0x400;
	FTM0->SC |= 0xF;
	FTM0->MOD = 99;
	FTM0->CONTROLS[0].CnSC = 0x28;
	FTM0->CONTROLS[1].CnSC = 0x28;
	FTM0->CONTROLS[2].CnSC = 0x28;
	pwm_rgb_led_set_color(&color);
#endif
#if _DAC_DMA
	//dma
	SIM->SCGC7 |= SIM_SCGC7_DMA_MASK;
	SIM->SCGC6 |= SIM_SCGC6_DMAMUX_MASK;
	DMAMUX->CHCFG[1] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_TRIG_MASK|DMAMUX_CHCFG_SOURCE(63);
	DMA0->ERQ = DMA_ERQ_ERQ1_MASK;
	DMA0->TCD[1].SADDR = (uint32_t)sinetable;
	DMA0->TCD[1].DADDR = (uint32_t)&(DAC0->DAT[0].DATL);
	DMA0->TCD[1].DLAST_SGA = 0;
	DMA0->TCD[1].SOFF = 2;
	DMA0->TCD[1].DOFF = 0;
	DMA0->TCD[1].SLAST = (uint32_t)-256;
	DMA0->TCD[1].NBYTES_MLNO = 2;
	DMA0->TCD[1].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[1].CITER_ELINKNO = 128;
	DMA0->TCD[1].BITER_ELINKNO = 128;
	//dac
	SIM->SCGC2 |= SIM_SCGC2_DAC0_MASK;
	DAC0->C0 = DAC_C0_DACEN_MASK|DAC_C0_DACRFS_MASK;
	//pit
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
	PIT->MCR = 0;
	PIT->CHANNEL[1].LDVAL = 0x039386FF;
	PIT->CHANNEL[1].TCTRL = PIT_TCTRL_TEN_MASK;
#endif
#if _ADC_DMA_TEMP
	fpu_on();
	//dma
	SIM->SCGC6 |= SIM_SCGC6_DMAMUX_MASK;
	SIM->SCGC7 |= SIM_SCGC7_DMA_MASK;
	DMAMUX->CHCFG[0] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_SOURCE(40);
	//DMAMUX->CHCFG[1] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_SOURCE(40);
	//DMA0->CR |= DMA_CR_CLM_MASK;
	DMA0->ERQ |= DMA_ERQ_ERQ0_MASK;//|DMA_ERQ_ERQ1_MASK;
	//tcd0
	DMA0->TCD[0].SADDR = (uint32_t)&ADC0->R[0];
	DMA0->TCD[0].DADDR = (uint32_t)adc_buf;
	DMA0->TCD[0].DOFF = 2;
	DMA0->TCD[0].DLAST_SGA = -64;
	DMA0->TCD[0].NBYTES_MLNO = 2;
	DMA0->TCD[0].SOFF = 0;
	DMA0->TCD[0].SLAST = 0;
	DMA0->TCD[0].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[0].CITER_ELINKNO = 32;
	DMA0->TCD[0].BITER_ELINKNO = 32;
	DMA0->TCD[0].CSR = DMA_CSR_INTMAJOR_MASK|DMA_CSR_INTHALF_MASK;//|DMA_CSR_MAJORLINKCH(1)|DMA_CSR_MAJORELINK_MASK;
	//tcd1
	/*DMA0->TCD[1].SADDR = (uint32_t)&ADC0->R[0];
	DMA0->TCD[1].DADDR = (uint32_t)&adc_buf[16];
	DMA0->TCD[1].DOFF = 2;
	DMA0->TCD[1].DLAST_SGA = -32;
	DMA0->TCD[1].NBYTES_MLNO = 2;
	DMA0->TCD[1].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[1].CITER_ELINKNO = 16;
	DMA0->TCD[1].BITER_ELINKNO = 16;
	DMA0->TCD[1].CSR = DMA_CSR_INTMAJOR_MASK|DMA_CSR_MAJORELINK_MASK;*/
	NVIC_EnableIRQ(DMA0_IRQn);
	//NVIC_EnableIRQ(DMA1_IRQn);
	//adc
	SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK;
	ADC0->CFG1 |= ADC_CFG1_MODE(3);
	ADC0->SC1[0] = ADC_SC1_ADCH(26);
	ADC0->SC2 = ADC_SC2_ADTRG_MASK|ADC_SC2_DMAEN_MASK;
	if (ADC16_Calibration() != 0) error_signal();
	//pit
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
	SIM->SOPT7 = SIM_SOPT7_ADC0TRGSEL(5)|SIM_SOPT7_ADC0ALTTRGEN_MASK;
	PIT->MCR = 0;
	PIT->CHANNEL[1].LDVAL = 0x039386FF;
	PIT->CHANNEL[1].TCTRL = PIT_TCTRL_TEN_MASK;
#endif
#if _FXOS
	//i2c
    //BOARD_I2C_ReleaseBus();
    PORTE->PCR[24] = 0x523;
    PORTE->PCR[25] = 0x523;
    SIM->SCGC4 |= SIM_SCGC4_I2C0_MASK;
    I2C0->C1 = 0;
    I2C0->FLT = 0x50;
    I2C0->S = 0x12;
    I2C0->F = 0x21;
    I2C0->C2 = 0;
    I2C0->C1 = I2C_C1_IICEN_MASK;//|I2C_C1_IICIE_MASK;
    //NVIC_EnableIRQ(I2C0_IRQn);
	if(i2c0_rd(FXOS_ADDR, WHO_AM_I_REG) == kFXOS_WHO_AM_I_Device_ID)
	{
		uart_print_string(UART0, "\r\nFXOS exist on I2C bus");
		i2c0_wr(FXOS_ADDR, CTRL_REG1, i2c0_rd(FXOS_ADDR, CTRL_REG1) & (~ACTIVE_MASK));
		if ((i2c0_rd(FXOS_ADDR, CTRL_REG1) & ACTIVE_MASK) == ACTIVE_MASK)
		{
			uart_print_string(UART0, "\r\nFXOS CTRL_REG1 write error");
			error_signal();
		}
		else uart_print_string(UART0, "\r\nFXOS CTRL_REG1 write OK");
		i2c0_wr(FXOS_ADDR, F_SETUP_REG, F_MODE_DISABLED);
		i2c0_wr(FXOS_ADDR, CTRL_REG2, MOD_HIGH_RES);
		i2c0_wr(FXOS_ADDR, M_CTRL_REG1, (M_RST_MASK | M_OSR_MASK | M_HMS_MASK));
		i2c0_wr(FXOS_ADDR, M_CTRL_REG2, M_HYB_AUTOINC_MASK);
		i2c0_wr(FXOS_ADDR, XYZ_DATA_CFG_REG, FULL_SCALE_4G);
		i2c0_wr(FXOS_ADDR, CTRL_REG1, (HYB_DATA_RATE_200HZ | ACTIVE_MASK));
		if ((i2c0_rd(FXOS_ADDR, CTRL_REG1) & ACTIVE_MASK) != ACTIVE_MASK)
		{
			uart_print_string(UART0, "\r\nFXOS CTRL_REG1 write error");
			error_signal();
		}
		else uart_print_string(UART0, "\n\rFXOS init completed");
	}
	else
	{
		uart_print_string("\n\rFXOS I2C init failed");
		error_signal();
	}
#endif
#if _RTC
	rtc_init();
	time.day = 1;
	time.month = 1;
	time.year = 2016;
#endif
#if _RNGA
	rnga_init();
#endif
#if _DS18B20
	//E24 pin
	PORTE->PCR[24] = 0x100;
	PTE->PDDR |= (1 << 24);
	PTE->PSOR = (1 << 24);
	uart_print_string(UART0, "\n\rDS18B20");
	one_wire_start();
	one_wire_write_byte(WRITE_SCRATCHPAD);
	one_wire_write_byte(00);
	one_wire_write_byte(00);
	one_wire_write_byte(RESOLUTION_10_BIT);
	print_temp();
#endif
#if _MMCAU
	MMCAU_AES_SetKey(&lr, 16, &lg);
#endif
	while(1)
	{
#if _PWM_RGB_LED
		if(state == 0)
		{
			uart_print_string(UART0, "\r\nChoose color (r,g,b): ");
			uart_scan_string(&recieve_char, 1);
			uart_print_char(UART0, recieve_char);
			switch(recieve_char)
			{
				case 0x72:
					uart_print_string(UART0, "\r\nSet RED color (0-9): ");
					state = 1;
				break;
				case 0x67:
					uart_print_string(UART0, "\r\nSet GREEN color (0-9): ");
					state = 2;
				break;
				case 0x62:
					uart_print_string(UART0, "\r\nSet BLUE color (0-9): ");
					state = 3;
				break;
				default:
					uart_print_string(UART0, "\r\nundef");
				break;
			}
		}
		else
		{
			uart_scan_string(&recieve_char, 1);
			uart_print_char(UART0, recieve_char);
			recieve_char -= 0x2f;
			if(recieve_char<11)
			{
				if(state == 1) color.r = (recieve_char*10);
				if(state == 2) color.g = (recieve_char*10);
				if(state == 3) color.b = (recieve_char*10);
			}
			pwm_rgb_led_init(&color);
			state = 0;
		}
#endif
		if ((PTC->PDIR & (1 << 6)) == 0) switcher1 = 1;
		if ((PTA->PDIR & (1 << 4)) == 0) switcher2 = 1;
		delay(3200000);
		if (switcher1)
		{
	#if _RTC
			print_time();
	#endif
			frdm_led_rgb(green, toggle);
			switcher1 = 0;
		}
		if (switcher2)
		{
	#if _RTC
			uart_print_string("Hour:\n\r");
			time.hour = get_data();
			uart_print_string("Minute:\n\r");
			time.minute = get_data();
			uart_print_string("Second:\n\r");
			time.second = get_data();
			rtc_set_time(RTC_ConvertDatetimeToSeconds(&time););
			print_time();
	#endif
			frdm_led_rgb(blue, toggle);
			switcher2 = 0;
		}
#if _RNGA
		while((RNG->SR & (1<<8)) != (1<<8)){};//OREG_LVL
		switch(RNG->OR & 7)
		{
			case 0: FRDM_LED_RGB(all, off); break;
			case 1: FRDM_LED_RGB(green, on); FRDM_LED_RGB(red|blue, off); break;
			case 2: FRDM_LED_RGB(red, on); FRDM_LED_RGB(green|blue, off); break;
			case 3: FRDM_LED_RGB(blue, on); FRDM_LED_RGB(red|green, off); break;
			case 4: FRDM_LED_RGB(green|red, on); FRDM_LED_RGB(blue, off); break;
			case 5: FRDM_LED_RGB(green|blue, on); FRDM_LED_RGB(red, off); break;
			case 6: FRDM_LED_RGB(red|blue, on); FRDM_LED_RGB(green, off); break;
			case 7: FRDM_LED_RGB(all, on); break;
		}
		delay(12000000);
#endif
#if _ADC_DMA_TEMP
		if(mutex)
		{
			float32_t temp4;
			uint16_t* temp3;
			uint16_t temp2;
			temper[9] = "0000.000\0";
			adc_ch_string[10] = pointer + 0x30;
			temp3 = &adc_buf[pointer*16];
			uart_print_string(UART0, adc_ch_string);
			for(uint8_t i=0; i<16; i++)
			{
				temp4 = 25.0f-(((3.3f*temp3[i]/65535.0f) - 0.716f)/0.00162f);
				temp2 = temp4;
				uart_print_string(UART0, "\r\nTemPerature: ");
				temper[0] = (((temp2/1000) % 10) + 0x30);
				temper[1] = (((temp2/100) % 10) + 0x30);
				temper[2] = (((temp2/10) % 10) + 0x30);
				temper[3] = ((temp2 % 10) + 0x30);
				temp2 = temp4 * 1000.0f;
				temper[5] = (((temp2/100) % 10) + 0x30);
				temper[6] = (((temp2/10) % 10) + 0x30);
				temper[7] = ((temp2 % 10) + 0x30);
				uart_print_string(UARt0, temper);
			}
			mutex = 0;
		}
#endif
#if _DS18B20
		uart_scan_string(&onechar, 1);
		print_temp();
#endif
	}
}
uint8_t get_data(void)
{
	uint8_t temp_string[3] = "00\0";
	uart_scan_string(UART0, temp_string, 2);
	uart_print_string(UART0, temp_string);
	temp_string[0] = temp_string[0] - 0x30;
	if(temp_string[0] > 9) temp_string[0] = 0;
	temp_string[1] = temp_string[1] - 0x30;
	if(temp_string[1] > 9) temp_string[1] = 0;
	return temp_string[0]*10 + temp_string[1];
}
void print_time(void)
{
	datetime_t dtime;
	uint8_t output[11] = "\r\n00:00:00\0";
	uint32_t temp1t = RTC->TSR;
	rtc_seconds_to_datetime(temp1t, &dtime);
	output[2] = (dtime.time.hour/10) + 0x30;
	output[3] = (dtime.time.hour%10) + 0x30;
	output[5] = (dtime.time.minute/10) + 0x30;
	output[6] = (dtime.time.minute%10) + 0x30;
	output[8] = (dtime.time.second/10) + 0x30;
	output[9] = (dtime.time.second%10) + 0x30;
	uart_print_string(UART0, output);
}
uint8_t i2c0_rd(uint8_t address, uint8_t reg)
{
	uint8_t loc_temp;
	I2C0->C1 |= I2C_C1_TX_MASK|I2C_C1_MST_MASK;
	while((I2C0->S & I2C_S_BUSY_MASK) == 0){};
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = (address << 1);
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = reg;
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	I2C0->C1 |= I2C_C1_RSTA_MASK;
	while((I2C0->S & I2C_S_BUSY_MASK) == 0){};
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = (address << 1)|1;//READ
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->C1 &= ~I2C_C1_TX_MASK;
	I2C0->C1 |= I2C_C1_TXAK_MASK;
	loc_temp = I2C0->D;
  	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
  	I2C0->S |= I2C_S_IICIF_MASK;
  	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
  	loc_temp = I2C0->D;
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	I2C0->C1 &= ~I2C_C1_MST_MASK;
	while(I2C0->S & I2C_S_BUSY_MASK){};
	return loc_temp;
}
void i2c0_wr(uint8_t address, uint8_t reg, uint8_t data)
{
	I2C0->C1 |= I2C_C1_TX_MASK|I2C_C1_MST_MASK;
	while((I2C0->S & I2C_S_BUSY_MASK) == 0){};
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = (address << 1);
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = reg;
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	while((I2C0->S & I2C_S_TCF_MASK) == 0){};
	I2C0->D = data;
	while((I2C0->S & I2C_S_IICIF_MASK) == 0){};
	I2C0->S |= I2C_S_IICIF_MASK;
	I2C0->C1 &= ~I2C_C1_MST_MASK;
	while(I2C0->S & I2C_S_BUSY_MASK){};
}

void DMA0_IRQHandler(void)
{
	mutex = 1;
	pointer ^= 1;
	DMA0->INT = DMA_INT_INT0_MASK;
}
void DMA1_IRQHandler(void)
{
	//DMA0->INT = DMA_INT_INT1_MASK;
}
void pdb_init(void)
{
	SIM->SCGC6 |= SIM_SCGC6_PDB_MASK;
	PDB0->SC = PDB_SC_TRGSEL(0xF)|PDB_SC_PDBEN_MASK|PDB_SC_MULT(3)|PDB_SC_PRESCALER(5)|PDB_SC_CONT_MASK|PDB_SC_LDOK_MASK;
	PDB0->MOD = 0xB71A;
	PDB0->CH[0].C1 |= PDB_C1_EN(1)|PDB_C1_TOS(1);
	PDB0->CH[0].DLY[0] = 0;
	PDB0->SC |= PDB_SC_PDBEN_MASK|PDB_SC_SWTRIG_MASK|PDB_SC_LDOK_MASK;
}
void pwm_rgb_led_set_color(rgb_888_t* color)
{
	FTM0->CONTROLS[0].CnV = color->r;
	FTM0->CONTROLS[1].CnV = color->g;
	FTM0->CONTROLS[2].CnV = color->b;
}
void ow_send_presence()
{
	PTE->PCOR = (1 << 24);
	delay(5800);//480
	PTE->PSOR = (1 << 24);
	delay(6000);//480
}
void ow_wr_bit(uint8_t bit)
{
	delay(12);//1
	PTE->PCOR = (1 << 24);
	delay(120);//15
	if(bit)
	{
		PTE->PSOR = (1 << 24);
		delay(600);//45
	}
	else
	{
		delay(600);//45
		PTE->PSOR = (1 << 24);
	}
}
uint8_t ow_rd_bit()
{
	uint8_t bit = 0;
	delay(12);//1
	PTE->PCOR = (1 << 24);
	delay(14);//1
	PTE->PSOR = (1 << 24);
	PTE->PDDR &= ~(1 << 24);
	PORTE->PCR[24] = 0x120;
	delay(140);//13
	bit = (PTE->PDIR & (1 << 24))>>24;
	PTE->PDDR |= (1 << 24);
	delay(710);//46
	return bit;
}
void print_temp(void)
{
	uint32_t data = 0;
	ow_start();
	ow_wr_byte(CONVERT_TEMPERATURE);
	delay(2250000);//186000
	ow_start();
	ow_wr_byte(READ_SCRATCHPAD);
	for(uint8_t i = 0; i<11; i++) data |= (ow_rd_bit() << i);
	uart_print_string(UART0, "\n\r");
	uart_print_char(UART0, ((data >> 4)/10 + 0x30));
	uart_print_char(UART0, (((data >> 4) % 10) + 0x30));
	uart_print_char(UART0, (((data >> 3) & 1) + 0x30));
	uart_print_char(UART0, (((data >> 2) & 1) + 0x30));
	uart_print_char(UART0, (((data >> 1) & 1) + 0x30));
	uart_print_char(UART0, (((data) & 1) + 0x30));
}
void ow_start(void)
{
	ow_send_presence();
	ow_wr_byte(SKIP_ROM_COMMAND);
}
void ow_wr_byte(uint8_t data)
{
	for(uint8_t i = 0; i<8; i++) ow_wr_bit(data>>i & 1);
}
