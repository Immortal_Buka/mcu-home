//ver170328
#include <project.h>

#define SKIP_ROM_COMMAND		0xCC
#define WRITE_SCRATCHPAD		0x4E
#define COPY_SCRATCHPAD			0x48
#define RESOLUTION_10_BIT		0x3F
#define CONVERT_TEMPERATURE		0x44
#define READ_SCRATCHPAD			0xBE
#define FRAM_ADDRESS            0x50

uint16 result;
uint8 volatile dataReady;
 
void one_wire_write_bit(uint8_t bit)
{
	CyDelayUs(1);//1
	temp_Write(0);
	CyDelayUs(15);//15
	if(bit)
	{
		temp_Write(1);
		CyDelayUs(45);//45
	}
	else
	{
		CyDelayUs(45);//45
		temp_Write(1);
	}
}
void one_wire_write_byte(uint8_t data)
{
    uint8_t i=0;
	for(i = 0; i<8; i++) one_wire_write_bit(data>>i & 1);
}
void start_1wire(void)
{
    temp_Write(0);
	CyDelayUs(480);//480
	temp_Write(1);
	CyDelayUs(480);//480
	one_wire_write_byte(SKIP_ROM_COMMAND);   
}
uint8_t one_wire_read_bit()
{
	uint8_t bit = 0;
	CyDelayUs(1);//1
	temp_Write(0);
	CyDelayUs(1);//1
	temp_Write(1);
	CyDelayUs(13);//13
	bit = temp_Read();
	CyDelayUs(46);//46
	return bit;
}
void print_temp()
{
	uint32_t data = 0;
    uint8_t i=0;
	start_1wire();
	one_wire_write_byte(CONVERT_TEMPERATURE);
	CyDelay(186);
	start_1wire();
	one_wire_write_byte(READ_SCRATCHPAD);
	for(i = 0; i<11; i++) data |= (one_wire_read_bit() << i);
    uart_debug_UartPutString("\r\nTemperature: ");
	uart_debug_UartPutChar((data >> 4)/10 + 0x30);
	uart_debug_UartPutChar(((data >> 4) % 10) + 0x30);
	uart_debug_UartPutChar(0x2e);
    uart_debug_UartPutChar(((data >> 3) & 1) + 0x30);
    uart_debug_UartPutChar(((data >> 2) & 1) + 0x30);
    uart_debug_UartPutChar(((data >> 1) & 1) + 0x30);
    uart_debug_UartPutChar((data & 1) + 0x30);
}
void fram_write(uint8 data, uint32 address)
{
    uint8 page = (address & 0x10000) >> 15;
    i2c_fram_I2CMasterSendStart(FRAM_ADDRESS|page, i2c_fram_I2C_WRITE_XFER_MODE);
    i2c_fram_I2CMasterWriteByte((address&0xff00)>>8);
	i2c_fram_I2CMasterWriteByte(address&0x00ff);
    i2c_fram_I2CMasterWriteByte(data);
    i2c_fram_I2CMasterSendStop();
}
uint8 fram_read(uint32 address)
{
    uint8 page = (address & 0x10000) >> 15;
    i2c_fram_I2CMasterSendStart(FRAM_ADDRESS|page, i2c_fram_I2C_WRITE_XFER_MODE);
    i2c_fram_I2CMasterWriteByte((address&0xff00)>>8);
	i2c_fram_I2CMasterWriteByte(address&0x00ff);
    i2c_fram_I2CMasterSendRestart(0x50, i2c_fram_I2C_READ_XFER_MODE);
    page = i2c_fram_I2CMasterReadByte(1);
    i2c_fram_I2CMasterSendStop();
    return page;
}
int main()
{
    uint8 temp1, temp2, temp3, temp4;
    CyGlobalIntEnable;
    uart_debug_Start();
    led_green_Write(1);
    led_blue_Write(1);
    led_red_Write(1);
    adc_photo_Start();
    i2c_fram_Start();
	start_1wire();
	one_wire_write_byte(WRITE_SCRATCHPAD);
	one_wire_write_byte(00);
	one_wire_write_byte(00);
	one_wire_write_byte(RESOLUTION_10_BIT);
    uart_debug_UartPutString("Init done");
    print_temp();
    uart_debug_UartPutString("\n\r");
    temp1 = 0;
    temp2 = 0;
    temp3 = 0;
    temp4 = 0;
    dataReady = 0;
    while(1)
    {
        uart_debug_UartPutString("Input command: ");
        while(temp1 == 0) temp1 = uart_debug_UartGetChar();
        uart_debug_UartPutChar(temp1);
        switch(temp1)  
        {
            case 's':
                uart_debug_UartPutString("\n\rAdc result: ");
                adc_photo_StartConvert();
                adc_photo_IsEndConversion(adc_photo_WAIT_FOR_RESULT);
                result = adc_photo_GetResult16(0);
                uart_debug_UartPutChar(result/1000+0x30);
                uart_debug_UartPutChar(((result/100)%10)+0x30);
                uart_debug_UartPutChar(((result/10)%10)+0x30);
                uart_debug_UartPutChar((result%10)+0x30);
                uart_debug_UartPutString(" units");
                dataReady = 0;
                break;
            case 'r':
                red_Write(1);
                break;
            case 'g':
                green_Write(1);
                break; 
            case 'b':
                blue_Write(1);
                break;
            case 'c':
                red_Write(0);
                break;
            case 'd':
                green_Write(0);
                break;
            case 'e':
                blue_Write(0);
                break;
            case 't':
                print_temp();
                break;
            case 'w':  
                uart_debug_UartPutString("\n\rFRAM write\r\nInput first address char: ");
                while(temp2 == 0) temp2 = uart_debug_UartGetChar();
                uart_debug_UartPutChar(temp2);
                uart_debug_UartPutString("\r\nInput second address char: ");
                while(temp3 == 0) temp3 = uart_debug_UartGetChar();
                uart_debug_UartPutChar(temp3);
                uart_debug_UartPutString("\r\nInput data char: ");
                while(temp4 == 0) temp4 = uart_debug_UartGetChar();
                uart_debug_UartPutChar(temp4);
                fram_write(temp4, (temp2<<8)|temp3);
                uart_debug_UartPutString("\r\nWrite done");
                break;
            case 'f':
                uart_debug_UartPutString("\n\rFRAM read\r\nInput first address char: ");
                while(temp2 == 0) temp2 = uart_debug_UartGetChar();
                uart_debug_UartPutChar(temp2);
                uart_debug_UartPutString("\r\nInput second address char: ");
                while(temp3 == 0) temp3 = uart_debug_UartGetChar();
                uart_debug_UartPutChar(temp3);
                temp4 = fram_read((temp2<<8)|temp3);
                uart_debug_UartPutString("\r\nData char: "); 
                uart_debug_UartPutChar(temp4);
                uart_debug_UartPutString("\r\nRead done");
                break;
            default: break;    
        }
        uart_debug_UartPutString("\r\n");
        temp1 = 0;
        temp2 = 0;
        temp3 = 0;
        temp4 = 0;
    }
}
