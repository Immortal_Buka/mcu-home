#include "system.h"
#include "dsp.h"
#include "color.h"
//
void main(void);
void uart_print_char(void* uart, uint8_t data);
void UART0_Handler(void);
//
volatile const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x20,
	.month = 0x03,
	.year = 0x20,
};
volatile uint16_t color_resuts[4][400] = {0};
//
void main(void)
{
	PMC->PMC_PCER0 |= PMC_PCER0_PID8|//uart0
		PMC_PCER0_PID11;//pioa
	//a9-rx, a10-tx, a13-mosi, a16-led
	PIOA->PIO_PDR = (1<<9)|(1<<10);
	PIOA->PIO_PER = (1<<16);
	PIOA->PIO_OER = (1<<16);
	//uart0
	UART0->UART_CR = UART_CR_RSTRX|UART_CR_RSTTX|UART_CR_RXDIS|UART_CR_TXDIS;
	UART0->UART_BRGR = 26;
	UART0->UART_MR = (4<<9);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);
	UART0->UART_IER = UART_IER_RXRDY;
	UART0->UART_CR = UART_CR_RXEN|UART_CR_TXEN;
	while(1)
	{
		delay(1000000);
		PIOA->PIO_SODR = (1<<16);
		uart_print_char(UART0, 0x55);
		delay(1000000);
		PIOA->PIO_CODR = (1<<16);
		uart_print_char(UART0, 0xaa);
	}
}
void uart_print_char(void* uart, uint8_t data)
{
	Uart* periph = (Uart*)uart;
	while((periph->UART_SR & UART_SR_TXRDY) == 0){};
	periph->UART_THR = data;
}
void UART0_Handler(void)
{
	uint8_t loc_temp = UART0->UART_RHR;
	uart_print_char(UART0, loc_temp);
}
