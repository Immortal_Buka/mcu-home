#include "system.h"
#include "lcd.h"
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "queue.h"
//
typedef struct TaskParam_t
{
	uint8_t* string;
	uint32_t period;
	uint8_t priority;
} TaskParam;
//
void main(void);
void vTaskFunction_1(void *pvParameters);
void vTaskFunction_2(void *pvParameters);
void vTaskFunction_3(void *pvParameters);
void vTaskFunction_4(void *pvParameters);
void vTaskFunction_5(void *pvParameters);
//
volatile uint8_t chk = 0;
TaskParam tp1, tp2;
uint8_t string1[] = "Task 1 is running\0";
TaskHandle_t task1h;
QueueHandle_t qh;
//
void main(void)
{
	FPU_ON;
	init_gpio();
	lcd_init(C_Black);
//encoder
	//B4-CH1
	GPIOB->MODER |= (2<<(4*2));
	GPIOB->AFR[0] |= (2<<16);
	//A7-CH2
	GPIOA->MODER |= (2<<(7*2));
	GPIOA->AFR[0] |= (2<<28);
	//TIM3
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->ARR = 1999;
	TIM3->PSC = 41999;
	TIM3->DIER = 1;
	TIM3->CR1 = (TIM_CR1_CEN|TIM_CR1_ARPE);
//params for task
	tp1.string = string1;
	tp1.period = 899;
	tp1.priority = 1;
	tp2.priority = 94;
//
	qh = xQueueCreate(5, 1);
	xTaskCreate(vTaskFunction_1, "test task #1", configMINIMAL_STACK_SIZE, (void*)&tp1, 0, task1h);
	xTaskCreate(vTaskFunction_2, "test task #2", configMINIMAL_STACK_SIZE, (void*)&tp2, 1, NULL);
	xTaskCreate(vTaskFunction_3, "test task #3", configMINIMAL_STACK_SIZE, NULL, 0, NULL);
	xTaskCreate(vTaskFunction_4, "test task #4", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
	xTaskCreate(vTaskFunction_5, "test task #5", configMINIMAL_STACK_SIZE, NULL, 0, NULL);
	vTaskStartScheduler();
	while(1)
	{
	}
}
void vTaskFunction_1(void *pvParameters)
{
	volatile TaskParam* pxTaskParam = (TaskParam*)pvParameters;
	while(1)
	{
		lcd_print_string(pxTaskParam->string, C_White);
		vTaskPrioritySet(task1h, pxTaskParam->priority);
		vTaskDelay(pxTaskParam->period);
	}
	vTaskDelete(NULL);
}
void vTaskFunction_2(void *pvParameters)
{
	volatile TaskParam* pxTaskParam = (TaskParam*)pvParameters;
	xQueueSend(qh, &(pxTaskParam->period), 0);
	while(1)
	{
		lcd_print_string("\r\nTask 2 is running", C_Blue);
		vTaskDelay(1200);
	}
	vTaskDelete(NULL);
}
void vTaskFunction_3(void *pvParameters)
{
	while(1)
	{
		lcd_print_string("\r\nTask 3 is running", C_Yellow);
		vTaskDelay(2500);
	}
	vTaskDelete(NULL);
}
void vTaskFunction_4(void *pvParameters)
{
	while(1)
	{
		lcd_print_string("\r\nTask 4 is running", C_Green);
		vTaskDelay(1300);
	}
	vTaskDelete(NULL);
}
void vTaskFunction_5(void *pvParameters)
{
	while(1)
	{
		lcd_print_string("\r\nTask 5 is running", C_Red);
		vTaskDelay(1900);
	}
	vTaskDelete(NULL);
}
