#include "std_system.hpp"
#include "XMC1100.h"
#include "XMC1000_RomFunctionTable.h"
//
void main(void);
void sys_init(void);
void project_def_handler(void);
void project_systick_handler(void);
extern "C"
{
	void SCU_0_IRQHandler(void);
}
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x02,
	.month = 0x12,
	.year = 0x20,
};
//
void main(void)
{
	PORT1->IOCR0 = 0x80;
	SysTick_Config(4000000);
	while(1)
	{
		//delay(500000);
		//project_systick_handler();
	}
}
void sys_init(void)
{
	SCU_GENERAL->PASSWD = 0x000000C0;
	SCU_CLK->CLKCR = 0x3FF10100;
	while(SCU_CLK->CLKCR & SCU_CLK_CLKCR_VDDC2LOW_Msk){};
	SCU_GENERAL->PASSWD = 0x000000C3;
}
void project_def_handler(void)
{
}
void project_systick_handler(void)
{
	static uint32_t counter = 0;
	if(counter & 1) PORT1->OMR = 1;
	else PORT1->OMR = 1<<16;
	counter++;
}
void SCU_0_IRQHandler(void)
{
	extern void Default_Handler(void);
	Default_Handler();
}
