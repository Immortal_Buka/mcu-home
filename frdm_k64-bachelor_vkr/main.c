#include "system.h"
#include "ff.h"
#include "arm_math.h"
#include "print.h"
#include "time.h"
//
#define FFT_SIZE 			1024
#define IIR_ORDER			1
#define DELTA_CRIT			800
#define FILT_COL			2
#define _USD				0
#define SIN_TABLE 			2000
#define uSD_DELAY			240000000
#define PRINT(x)			uart_print_string(UART0, x)
//
void main (void);
void dma_init(void);
void adc_init(void);
void DMA0_IRQHandler(void);
void pit_init(void);
uint8_t ADC_Calibration(void);
void dac_init(void);
uint8_t get_data(void);
void print_times(void);
void uart_scan_string_chk(uint8_t *data);
//
uint8_t temp1;
uint32_t temp2 = 0;
uint8_t usd_exist = 0;
uint8_t uart_exist = 0;
int16_t adc_data[FFT_SIZE*2] = {0};
uint16_t sinetable[SIN_TABLE] = {0};
uint8_t dac_use = 0;
uint8_t string[11] = {0};
uint8_t temp3[7] = "000000@";
uint8_t using_filter_number = 0;
datetime_t time = {0};
static FATFS g_fileSystem;
const TCHAR driverNumberBuffer[3] = {1 + '0', ':', '/'};//1!!
static FIL g_fileObject[3];
uint8_t file_name[3][7] =
{
	"fsa.txt",
	"mpa.txt",
	"mfa.txt",
};
static FIL g_fileObjectS;
FRESULT status;
arm_biquad_casd_df1_inst_q15 iir_inst[4];
float32_t fc[FILT_COL][IIR_ORDER*6] = //{b10, 0, b11, b12, a11, a12}
{
	{0.18405151367f, 0.0f, 0.36810302734f, 0.18405151367f, -0.47564697266f, 0.21185302734f},
	{0.17932128906f, 0.0f, 0.35864257813f, 0.17932128906f, -0.49859619141f, 0.21585083008f}
};
q15_t fcq[IIR_ORDER*6] = {0};
q15_t fsb[4][IIR_ORDER*4] = {0};
int16_t spectrum[FFT_SIZE] = {0};
arm_rfft_instance_q15 fi;
int16_t mean[4] = {0};
uint16_t counter1 = 0, counter2 = 0, fft_col = 1;
uint8_t mutex = 0, pointer = 1, file_col = 1, switcher = 1;
uint16_t tempf = 0;
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x02,
	.day = 0x31,
	.month = 0x01,
	.year = 0x19
};
//
void main (void)
{
	fpu_on();
	NVIC_SetPriority(DMA0_IRQn, 1);
	NVIC_SetPriority(SDHC_IRQn, 3);
	NVIC_SetPriority(SysTick_IRQn, 2);
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK|SIM_SCGC5_PORTC_MASK|SIM_SCGC5_PORTE_MASK;
	//sw2
	PORTC->PCR[6] = 0x103;
	//uart
	PORTB->PCR[16] = 0x300;//dbg UART
	PORTB->PCR[17] = 0x300;//dbg UART
	sda_uart_init();
	//uSD
	PORTE->PCR[0] = 0x443;
	PORTE->PCR[1] = 0x443;
	PORTE->PCR[2] = 0x443;
	PORTE->PCR[3] = 0x443;
	PORTE->PCR[4] = 0x443;
	PORTE->PCR[5] = 0x443;
	PORTE->PCR[6] = 0x102;
	//rgb led
	PORTB->PCR[21] = 0x100;
	PORTB->PCR[22] = 0x100;
	PORTE->PCR[26] = 0x100;
	PTB->PDDR |= ((1 << 21)|(1 << 22));
	PTE->PDDR |= (1 << 26);
	frdm_led_rgb(all, off);
	rtc_init();
	print_times();
	PRINT("\r\nSet time? ");
	uart_scan_string_chk(&temp1);
	uart_print_char(UART0, temp1);
	if(temp1 == 'y')
	{
		PRINT("\n\rYear(20xx): ");
		time.date.year = 2000 + get_data();
		PRINT("\n\rMonth: ");
		time.date.month = get_data();
		PRINT("\r\nDay: ");
		time.date.day = get_data();
		PRINT("\n\rHour: ");
		time.time.hour = get_data();
		PRINT("\n\rMinute: ");
		time.time.minute = get_data();
		PRINT("\n\rSecond:");
		time.time.second = get_data();
		rtc_set_time(rtc_datetime_to_seconds(&time));
		print_times();
	}
	PRINT("\n\rWait uSD card...");
	while (!(PTE->PDIR & (1 << 6)))//wait uSD conection
	{
		temp2++;
		if(temp2 > uSD_DELAY) break;
	}
	if(temp2 < uSD_DELAY) usd_exist = 1;
	delay(100000);
	if(usd_exist)
	{
		if(f_mount(&g_fileSystem, driverNumberBuffer, 0))
		{
			PRINT("\n\rf_mount error");
			error_signal();
		}
		else PRINT("\n\rf_mount OK");
		if (f_chdrive((char const *)&driverNumberBuffer[0]))
		{
			PRINT("\n\rf_chdrive error");
			error_signal();
		}
		else PRINT("\n\rf_chdrive OK");
		if(uart_exist)
		{
			PRINT("\n\rDo you want use DAC? ");
			uart_scan_string(UART0, &temp1, 1);
			uart_print_char(UART0, temp1);
			if(temp1 == 's')
			{
				dac_use = 1;
				status = f_open(&g_fileObjectS, _T("SIN.CSV"), FA_READ);
				if(status)
				{
					uint8_to_string((uint8_t)status, string);
					PRINT("\r\nsin.csv oPen error ");
					PRINT(string);
				}
				else
				{
					PRINT("\n\rsin.csv oPen OK");
					for(uint16_t i=0;i<SIN_TABLE;i++)
					{
						if (f_lseek(&g_fileObjectS, i*5)) PRINT("\r\nSet file pointer position failed");
						if (f_read(&g_fileObjectS, temp3, 4, (unsigned int*)&temp2)) PRINT("\r\nread file error");
						sinetable[i] = (temp3[0] - 0x30)*1000 + (temp3[1] - 0x30)*100 + (temp3[2] - 0x30)*10 + (temp3[4] - 0x30);
					}
					if (f_close(&g_fileObjectS))
					{
						PRINT("\r\nClose file failed");
						error_signal();
					}
					else PRINT ("\n\rf_close OK");
				}
			}
		}
	}
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK|SIM_SCGC6_DMAMUX_MASK|SIM_SCGC6_ADC0_MASK;
	SIM->SCGC7 |= SIM_SCGC7_DMA_MASK;
	dma_init();
	adc_init();
	if(dac_use) dac_init();
	arm_rfft_init_q15(&fi, FFT_SIZE/2, 0, 1);
	pit_init();
	while(1)
	{
		if(switcher)
		{
			counter2 = 0;
			if(uart_exist)
			{
				PRINT("\n\rSend filter number: ");
				uart_scan_string(UART0, &temp1, 1);
				uart_print_char(UART0, temp1);
				using_filter_number = temp1 - 0x31;
				if (!(using_filter_number < FILT_COL))
				{
					PRINT("\n\rerror, set filter #1");
					using_filter_number = 0;
				}
				arm_float_to_q15(fc[using_filter_number], fcq, IIR_ORDER*6);
				for(uint8_t i=0; i < 4; i++) arm_biquad_cascade_df1_init_q15(&iir_inst[i], IIR_ORDER, fcq, fsb[i], 0);
				PRINT("\n\rSend files length: ");
				uart_scan_string(UART0, &temp1, 1);
				uart_print_char(UART0, temp1);
				switch(temp1)
				{
					case '1': fft_col = 1; break;//4sec
					case '2': fft_col = 2; break;//8sec
					case '5': fft_col = 5; break;//20sec
					case 't': fft_col = 30; break;//2min
					case 'y': fft_col = 75; break;//5min
					case 'd': fft_col = 150; break;//10min
					case 'q': fft_col = 225; break;//15min
					case 'v': fft_col = 300; break;//20min
					case 'f': fft_col = 450; break;//30min
					case 'h': fft_col = 900; break;//1hour
					default: fft_col = 15; break;//1min
				}
				PRINT("\n\rSend files count: ");
				uart_scan_string(UART0, &temp1, 1);
				uart_print_char(UART0, temp1);
				file_col = temp1 - 0x30;
			}
		}
		while (counter2 < file_col)
		{
			if(usd_exist) for(uint8_t i=0; i<3; i++)
			{
				status = f_open(&g_fileObject[i], _T(file_name[i]), (FA_CREATE_ALWAYS|FA_WRITE));
				if ((status)&&(status != FR_EXIST))
				{
					PRINT("\n\r f_oPen ");
					PRINT(file_name[i]);
					PRINT(" error ");
					uint8_to_string((uint8_t)status, string);
					PRINT(string);
					error_signal();
				}
				else
				{
					PRINT("\n\r f_oPen ");
					PRINT(file_name[i]);
					PRINT(" OK");
				}
			}
			counter1 = 0;
			while (counter1 < fft_col)
			{
				if(mutex)
				{
					arm_rfft_q15(&fi, &adc_data[pointer*FFT_SIZE], spectrum);
					arm_abs_q15(spectrum, spectrum, FFT_SIZE/4);
					arm_mean_q15(&spectrum[4], 12, &mean[0]);
					arm_mean_q15(&spectrum[16], 16, &mean[1]);
					arm_mean_q15(&spectrum[24], 20, &mean[2]);
					arm_mean_q15(&spectrum[52], 58, &mean[3]);
					if(usd_exist)
					{
						for(uint16_t i=1; i < FFT_SIZE/4; i++)
						{
							temp3[0] = spectrum[i]/100000 + 0x30;
							temp3[1] = ((spectrum[i]/10000) % 10) + 0x30;
							temp3[2] = ((spectrum[i]/1000) % 10) + 0x30;
							temp3[3] = ((spectrum[i]/100) % 10) + 0x30;
							temp3[4] = ((spectrum[i]/10) % 10) + 0x30;
							temp3[5] = (spectrum[i] % 10) + 0x30;
							if(f_write(&g_fileObject[0], temp3, sizeof(temp3), (unsigned int*)&temp2)) PRINT("\n\rf_write_1 error");
						}
						PRINT("\n\rf_write_1 OK");
						for(uint8_t i=0; i < 4; i++)
						{
							temp3[0] = mean[i]/100000 + 0x30;
							temp3[1] = ((mean[i]/10000) % 10) + 0x30;
							temp3[2] = ((mean[i]/1000) % 10) + 0x30;
							temp3[3] = ((mean[i]/100) % 10) + 0x30;
							temp3[4] = ((mean[i]/10) % 10) + 0x30;
							temp3[5] = (mean[i] % 10) + 0x30;
							if(f_write(&g_fileObject[1], temp3, sizeof(temp3), (unsigned int*)&temp2)) PRINT("\n\rf_write_2 error");
						}
						PRINT("\n\rf_write_2 OK");
						for(uint8_t i=0; i < 4; i++)
						{
							arm_biquad_cascade_df1_q15 (&iir_inst[i], &mean[i], &tempf, 1);
							if(tempf > DELTA_CRIT) frdm_led_rgb(red, on);
							temp3[0] = tempf/100000 + 0x30;
							temp3[1] = ((tempf/10000) % 10) + 0x30;
							temp3[2] = ((tempf/1000) % 10) + 0x30;
							temp3[3] = ((tempf/100) % 10) + 0x30;
							temp3[4] = ((tempf/10) % 10) + 0x30;
							temp3[5] = (tempf % 10) + 0x30;
							if(f_write(&g_fileObject[2], temp3, sizeof(temp3), (unsigned int*)&temp2)) PRINT("\n\rf_write_3 error");
						}
						PRINT("\n\rf_write_3 OK");
					}
					else
					{
						arm_biquad_cascade_df1_q15 (&iir_inst[0], &mean[0], &tempf, 1);
						if(tempf > DELTA_CRIT) frdm_led_rgb(red, on);
					}
					counter1++;
					mutex = 0;
				}
			}
			if(usd_exist)
			{
				for(uint8_t i=0; i<3; i++)
				{
					if (f_close(&g_fileObject[i]))
					{
						PRINT("\n\rf_close error");
						error_signal();
					}
					else PRINT ("\n\rf_close OK");
					if (file_name[i][2] < 'z') file_name[i][2]++;
					else file_name[i][2] = 'a';
				}
			}
			print_times();
			counter2++;
		}
		switcher = 0;
		if((PTC->PDIR & (1 << 6)) == 0) switcher = 1;
	}
}
void dma_init(void)
{
	if(dac_use)
	{
		DMAMUX->CHCFG[1] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_TRIG_MASK|DMAMUX_CHCFG_SOURCE(63);
		DMA0->TCD[1].SADDR = (uint32_t)sinetable;
		DMA0->TCD[1].DADDR = (uint32_t)&(DAC0->DAT[0].DATL);
		DMA0->TCD[1].DLAST_SGA = 0;
		DMA0->TCD[1].SOFF = 2;
		DMA0->TCD[1].DOFF = 0;
		DMA0->TCD[1].SLAST = -(SIN_TABLE*2);
		DMA0->TCD[1].NBYTES_MLNO = 2;
		DMA0->TCD[1].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
		DMA0->TCD[1].CITER_ELINKNO = SIN_TABLE;
		DMA0->TCD[1].BITER_ELINKNO = SIN_TABLE;
	}
	DMAMUX->CHCFG[0] = DMAMUX_CHCFG_ENBL_MASK|DMAMUX_CHCFG_SOURCE(40);
	DMA0->ERQ = DMA_ERQ_ERQ0_MASK|DMA_ERQ_ERQ1_MASK;
	DMA0->TCD[0].SADDR = (uint32_t) &ADC0->R[0];
	DMA0->TCD[0].DADDR = (uint32_t)adc_data;
	DMA0->TCD[0].DOFF = 2;
	DMA0->TCD[0].DLAST_SGA = -(FFT_SIZE*4);
	DMA0->TCD[0].NBYTES_MLNO = 2;
	DMA0->TCD[0].SOFF = 0;
	DMA0->TCD[0].SLAST = 0;
	DMA0->TCD[0].ATTR = DMA_ATTR_SSIZE(1)|DMA_ATTR_DSIZE(1);
	DMA0->TCD[0].CITER_ELINKNO = FFT_SIZE*2;
	DMA0->TCD[0].BITER_ELINKNO = FFT_SIZE*2;
	DMA0->TCD[0].CSR = DMA_CSR_INTMAJOR_MASK|DMA_CSR_INTHALF_MASK;
	NVIC_EnableIRQ(DMA0_IRQn);
}
void adc_init(void)
{
	ADC0->CFG1 |= ADC_CFG1_MODE(3)|ADC_CFG1_ADIV(3)|ADC_CFG1_ADLSMP_MASK;
	ADC0->SC1[0] = ADC_SC1_DIFF_MASK;//DAD0
	ADC0->SC3 = ADC_SC3_AVGE_MASK|ADC_SC3_AVGS(3);
	if(ADC_Calibration() == 1)
	{
		PRINT("\n\rADC_Calibration error");
		error_signal();
	}
	ADC0->SC2 = ADC_SC2_ADTRG_MASK|ADC_SC2_DMAEN_MASK;
}
void DMA0_IRQHandler(void)
{
	mutex = 1;
	pointer ^= 1;
	DMA0->INT = DMA_INT_INT0_MASK;
}
void pit_init(void)
{
	SIM->SOPT7 = SIM_SOPT7_ADC0TRGSEL(4)|SIM_SOPT7_ADC0ALTTRGEN_MASK;
	PIT->MCR = 0;
	if(dac_use)
	{
		PIT->CHANNEL[1].LDVAL = 0x0000EA5F;//1kHz
		PIT->CHANNEL[1].TCTRL = PIT_TCTRL_TEN_MASK;
	}
	PIT->CHANNEL[0].LDVAL = 0x00039386;//256Hz
	PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TEN_MASK;
}
uint8_t ADC_Calibration(void)
{
    ADC0->SC3 |= ADC_SC3_CAL_MASK;
    while ((ADC0->SC1[0] & ADC_SC1_COCO_MASK) != ADC_SC1_COCO_MASK)
    {
        if (ADC0->SC3 & ADC_SC3_CALF_MASK) return 1;
    }
    if (ADC0->SC3 & ADC_SC3_CALF_MASK) return 1;
    ADC0->PG = (0x8000|((ADC0->CLP0 + ADC0->CLP1 + ADC0->CLP2 + ADC0->CLP3 + ADC0->CLP4 + ADC0->CLPS) >> 1));
    ADC0->MG = (0x8000|((ADC0->CLM0 + ADC0->CLM1 + ADC0->CLM2 + ADC0->CLM3 + ADC0->CLM4 + ADC0->CLMS) >> 1));
    return 0;
}
void dac_init(void)
{
	SIM->SCGC2 |= SIM_SCGC2_DAC0_MASK;
	DAC0->C0 = DAC_C0_DACEN_MASK|DAC_C0_DACRFS_MASK;
}
uint8_t get_data(void)
{
	uint8_t recieve_char_t[4] = "00";
	uart_scan_string(UART0, recieve_char_t, 2);
	PRINT(recieve_char_t);
	recieve_char_t[0] = recieve_char_t[0] - 0x30;
	if(recieve_char_t[0] > 9) recieve_char_t[0] = 0;
	recieve_char_t[1] = recieve_char_t[1] - 0x30;
	if(recieve_char_t[1] > 9) recieve_char_t[1] = 0;
	return recieve_char_t[0]*10 + recieve_char_t[1];
}
void print_times(void)
{
	uint8_t output[11] = "\r\n00:00:00\0";
	uint32_t temptime = RTC->TSR;
	rtc_seconds_to_datetime(temptime, &time);
	output[2] = (time.time.hour/10) + 0x30;
	output[2] = (time.time.hour%10) + 0x30;
	output[5] = (time.time.minute/10) + 0x30;
	output[6] = (time.time.minute%10) + 0x30;
	output[8] = (time.time.second/10) + 0x30;
	output[9] = (time.time.second%10) + 0x30;
	PRINT(output);
}
void uart_scan_string_chk(uint8_t* data)
{
	uint32_t loc_temp = 0;
    while (!(UART0->S1 & UART_S1_RDRF_MASK))
    {
    	loc_temp++;
    	if(loc_temp > uSD_DELAY) break;
    }
    if(loc_temp < uSD_DELAY) uart_exist = 1;
    *data = UART0->D;
}
