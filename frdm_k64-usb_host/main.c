#include "system.h"
#include "print.h"
#include "busby.h"
//
#define USB_TIMEOUT		0x10000
//
typedef union
{
	uint8_t raw;
	struct
	{
		uint8_t usbrst : 1;
		uint8_t error : 1;
		uint8_t softok : 1;
		uint8_t tokdne : 1;
		uint8_t sleep : 1;
		uint8_t resume : 1;
		uint8_t attach : 1;
		uint8_t stall : 1;
	} bits;
} usb_irq_signal_t;
typedef struct
{
	union
	{
		struct
		{
			uint32_t reserved0_0 : 2;
			uint32_t tok_pid : 4;
			uint32_t data1 : 1;
			uint32_t own : 1;
			uint32_t reserved1_0 : 8;
			uint32_t bc : 10;
			uint32_t reserved2_0 : 6;
		} fields;
		uint32_t raw;
	} buf_descriptor;
	uint32_t buf_address;
} bdt_one_ep_struct;
typedef struct
{
	struct
	{
		bdt_one_ep_struct rx[2];
		bdt_one_ep_struct tx[2];
	} bdt_ep[16];
} bdt_t;
typedef struct
{
	usb_dev_state_t state;
	uint8_t address;
	uint8_t max_size_ep0;
} usb_ddevice_t;
//
void main(void);
void usb_host_init(void);
void USB_Delay(uint16_t ms);
void usb_reset_dev(void);
void usb_attach_dev(void);
void setup_req_stage(usb_setup_req_t* req);
void setup_status_stage_out(void);
void setup_data_stage(uint16_t size);
void setup_status_stage_in(void);
void print_langid(uint16_t langid);
void print_rx_descriptors(void);
void func_1(bdt_one_ep_struct* ptr);
void print_token(bdt_one_ep_struct* ptr);
//
__attribute__((aligned(4))) usb_setup_req_t tx_data;
volatile usb_irq_signal_t signal;
__attribute__((aligned(512))) static bdt_t bdt;
usb_ddevice_t dev0 =
{
	.state = dev_unconnected,
	.address = 0,
	.max_size_ep0 = 8,
};
uint8_t rx_data[1024] = {0}, tx_odd_even = 0, rx_odd_even = 0, onechar, string[11] = {0};
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0x02,
	.day = 0x29,
	.month = 0x10,
	.year = 0x19,
};
//
void main (void)
{
	signal.raw = 0;
	for(uint16_t i=0; i<512; i++) ((uint8_t*)&bdt)[i] = 0;
	init_common_gpio();
	sda_uart_init();
	frdm_led_rgb(all, off);//RGB LED
	while(1)
	{
		uart_print_string(UART0, "\n\rSet ON USB Host?(y/*): ");
		uart_scan_string(UART0, &onechar, 1);
		uart_print_char(UART0, onechar);
		if(onechar == 'y')
		{
			dev0.state = dev_unconnected;
			usb_host_init();
			break;
		}
	}
	while(1)
	{
		if(dev0.state != dev_unconnected)
		{
			if(signal.bits.usbrst) usb_reset_dev();
			uart_print_string(UART0, "\r\nDevice\r\n\tstate: ");
			switch(dev0.state)
			{
				case dev_attached: uart_print_string(UART0, "attached"); break;
				case dev_addressed: uart_print_string(UART0, "addressed"); break;
				case dev_configured: uart_print_string(UART0, "configured"); break;
				default: break;
			}
			uart_print_string(UART0, "\r\n\tEP0 size: ");
			uint8_to_hex_string(dev0.max_size_ep0, string);
			uart_print_string(UART0, string);
			uart_print_string(UART0, "\r\n\taddress: ");
			uint8_to_hex_string(dev0.address, string);
			uart_print_string(UART0, string);
			uart_print_string(UART0, "\r\nSelect req: "
					"\r\n\ta - set address"
					"\r\n\tb - set config"
					"\r\n\tc - get config"
					"\r\n\td - get descr"
					"\r\n\te - get interf"
					"\r\n\tf - set interf"
					"\r\n\tr - reset"
					"\r\n\ts - get status"
					"\r\n\t");
			uart_scan_string(UART0, &onechar, 1);
			uart_print_char(UART0, onechar);
			switch(onechar)
			{
				case 'a':
					uart_print_string(UART0, "\r\nSet address\r\nInput address(1-9): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					onechar -= 0x30;
					if(onechar > 10) onechar = 0;
					tx_data.bmRequestType.raw = 0;
					tx_data.bmRequest = SET_ADDRESS;
					tx_data.wValue = onechar;
					tx_data.wIndex = 0;
					tx_data.wLength = 0;
					setup_req_stage(&tx_data);
					setup_status_stage_in();
					USB0->ADDR &= ~USB_ADDR_ADDR_MASK;
					USB0->ADDR |= onechar;
					dev0.address = onechar;
					dev0.state = dev_addressed;
				break;
				case 'b'://SET_CONFIGURATION
					uart_print_string(UART0, "\r\nSet config\r\nInput number(1-9): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.bmRequestType.raw = 0;
					tx_data.bmRequest = SET_CONFIGURATION;
					tx_data.wValue = onechar - 0x30;;
					tx_data.wIndex = 0;
					tx_data.wLength = 0;
					setup_req_stage(&tx_data);
					setup_status_stage_in();
				break;
				case 'c'://GET_CONFIGURATION
					uart_print_string(UART0, "\r\nGet config");
					tx_data.bmRequestType.raw = 0x80;
					tx_data.bmRequest = GET_CONFIGURATION;
					tx_data.wValue = 0;
					tx_data.wIndex = 0;
					tx_data.wLength = 1;
					setup_req_stage(&tx_data);
					setup_data_stage(1);
					setup_status_stage_out();
					uart_print_string(UART0, "\r\nConfig. #");
					onechar = rx_data[0] + 0x30;
					uart_print_char(UART0, onechar);
				break;
				case 'd'://GET_DESCRIPTOR
					uart_print_string(UART0, "\r\nGet descriptor\r\nTyPe(d,c,s): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.bmRequestType.raw = 0x80;
					tx_data.bmRequest = GET_DESCRIPTOR;
					switch(onechar)
					{
						case 'd'://device
							tx_data.wValue = 0x0100;
							tx_data.wIndex = 0;
							tx_data.wLength = 0x12;
							setup_req_stage(&tx_data);
							setup_data_stage(0x12);
							setup_status_stage_out();
							if(rx_data[0])	dev0.max_size_ep0 = rx_data[7];
							print_rx_descriptors();
						break;
						case 'c'://configuration
							tx_data.wValue = 0x0200;
							tx_data.wIndex = 0;
							tx_data.wLength = 9;
							setup_req_stage(&tx_data);
							setup_data_stage(9);
							setup_status_stage_out();
							uint16_t lenght = rx_data[2]|(rx_data[3]<<8);
							print_rx_descriptors();
							uart_print_string(UART0, "\r\nGet full configuration descriptor? (y/*): ");
							uart_scan_string(UART0, &onechar, 1);
							uart_print_char(UART0, onechar);
							if(onechar == 0x79)
							{
								tx_data.wLength = lenght;
								setup_req_stage(&tx_data);
								setup_data_stage(lenght);
								setup_status_stage_out();
								print_rx_descriptors();
							}
							break;
						case 's'://string
							uart_print_string(UART0, "\r\nString descriptor count: ");
							uart_scan_string(UART0, &onechar, 1);
							uart_print_char(UART0, onechar);
							onechar -= 0x30;
							tx_data.wValue = 0x0300;
							tx_data.wIndex = 0;
							tx_data.wLength = 1;
							setup_req_stage(&tx_data);
							setup_data_stage(1);
							setup_status_stage_out();
							uart_print_string(UART0, "\r\nstring #0 descriptor length: ");
							uart_print_char(UART0, rx_data[0]/10 + 0x30);
							uart_print_char(UART0, rx_data[0]%10 + 0x30);
							tx_data.wLength = rx_data[0];
							setup_req_stage(&tx_data);
							setup_data_stage(tx_data.wLength);
							setup_status_stage_out();
							uart_print_string(UART0, "\r\nLang IDs: ");
							uint16_t lang_id;
							for(uint8_t i=0; i < (tx_data.wLength-2); i+=2)
							{
								lang_id = rx_data[i+2]|(rx_data[i+3]<<8);
								uart_print_char(UART0, onechar);
								print_langid(lang_id);
							}
							uart_print_string(UART0, "\r\nStrings: ");
							for(uint8_t i=1; i < onechar+1; i++)
							{
								tx_data.wValue = 0x0300|i;
								tx_data.wIndex = lang_id;
								tx_data.wLength = 1;
								setup_req_stage(&tx_data);
								setup_data_stage(1);
								setup_status_stage_out();
								tx_data.wLength = rx_data[0];
								setup_req_stage(&tx_data);
								setup_data_stage(tx_data.wLength);
								setup_status_stage_out();
								uart_print_string(UART0, "\n\r");
								for(uint8_t i = 0; i < rx_data[0]-2; i++) uart_print_char(UART0, rx_data[2+i]);
							}
						break;
						default: break;
					}
				break;
				case 'e'://GET_INTERFACE
					uart_print_string(UART0, "\r\nGet interface\r\nInterface number(1-9): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.bmRequestType.raw = 0x81;
					tx_data.bmRequest = GET_INTERFACE;
					tx_data.wValue = 0;
					tx_data.wIndex = onechar - 0x30;
					tx_data.wLength = 0;
					setup_req_stage(&tx_data);
					setup_data_stage(1);
					setup_status_stage_out();
					uart_print_string(UART0, "\r\nAlt. inter.: ");
					uart_print_char(UART0, rx_data[0] + 0x30);
				break;
				case 'f'://SET_INTERFACE
					uart_print_string(UART0, "\r\nSet interface\r\nInterface number(1-9): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.wIndex = onechar-0x30;
					uart_print_string(UART0, "\r\nAlt. interface number(1-9): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.wValue = onechar-0x30;
					tx_data.bmRequestType.raw = 0x01;
					tx_data.bmRequest = SET_INTERFACE;
					tx_data.wLength = 0;
					setup_req_stage(&tx_data);
					setup_status_stage_in();
				break;
				case 'r'://RESET_DEVICE
					usb_reset_dev();
				break;
				case 's'://GET_STATUS
					uart_print_string(UART0, "\r\nGet status\r\nTyPe(d,i,e): ");
					uart_scan_string(UART0, &onechar, 1);
					uart_print_char(UART0, onechar);
					tx_data.bmRequest = GET_STATUS;
					tx_data.wValue = 0;
					tx_data.wLength = 2;
					switch(onechar)
					{
						case 'd':
							tx_data.bmRequestType.raw = 0x80;
							tx_data.wIndex = 0;
							setup_req_stage(&tx_data);
							setup_data_stage(2);
							setup_status_stage_out();
							if(rx_data[0] & 1) uart_print_string(UART0, "\r\nDevice self-powered");
							else uart_print_string(UART0, "\r\nDevice not self-powered");
							if(rx_data[0] & 2) uart_print_string(UART0, "\r\nDevice can remote wakeup");
							else uart_print_string(UART0, "\r\nDevice can't remote wakeup");
						break;
						case 'i':
							uart_print_string(UART0, "\r\nInterface number(1-9): ");
							uart_scan_string(UART0, &onechar, 1);
							uart_print_char(UART0, onechar);
							tx_data.bmRequestType.raw = 0x81;
							tx_data.wIndex = onechar - 0x30;
							setup_req_stage(&tx_data);
							setup_data_stage(2);
							setup_status_stage_out();
							if(rx_data[0] == 0) uart_print_string(UART0, "\n\rInterface OK");
							else uart_print_string(UART0, "\n\rInterface not OK");
						break;
						case 'e':
							uart_print_string(UART0, "\r\nEP number(1-9): ");
							uart_scan_string(UART0, &onechar, 1);
							uart_print_char(UART0, onechar);
							tx_data.wIndex = onechar - 0x30;
							uart_print_string(UART0, "\r\nEP direction(i/o): ");
							uart_scan_string(UART0, &onechar, 1);
							uart_print_char(UART0, onechar);
							if(onechar == 'i') tx_data.wIndex |= 0x80;
							tx_data.bmRequestType.raw = 0x82;
							setup_req_stage(&tx_data);
							setup_data_stage(2);
							setup_status_stage_out();
							if(rx_data[0] == 0) uart_print_string(UART0, "\n\rEP OK");
							else uart_print_string(UART0, "\n\rEP Halt");
						break;
						default:
						break;
					}
				break;
				default:
				break;
			}
		}
		else
		{
			if(signal.bits.attach)
			{
				usb_attach_dev();
				signal.bits.attach = 0;
				uart_print_string(UART0, "\r\nUSB IRQ: \e[35mattach\e[39m");
			}
		}
	}
}
void usb_host_init (void)
{
	uint32_t temp;
	mpu_disabled();
	SIM->CLKDIV2 = SIM_CLKDIV2_USBDIV(4)|SIM_CLKDIV2_USBFRAC(1);//0xB
	SIM->SOPT2 |= SIM_SOPT2_USBSRC(1)|SIM_SOPT2_PLLFLLSEL(1);
	SIM->SCGC4 |= SIM_SCGC4_USBOTG_MASK;
	USB0->CTL = USB_CTL_SE0_MASK;
	USB0->CTL = 0;
	USB0->ISTAT = 0xFF;
	USB0->USBCTRL |= USB_USBCTRL_PDE_MASK;
	USB0->USBCTRL &= ~USB_USBCTRL_SUSP_MASK;
	USB0->CTL |= USB_CTL_ODDRST_MASK;
	temp = (uint32_t)&bdt;
	USB0->BDTPAGE1 = (uint8_t)(temp >> 8);
	USB0->BDTPAGE2 = (uint8_t)(temp >> 16);
	USB0->BDTPAGE3 = (uint8_t)(temp >> 24);
	USB0->SOFTHLD = 255;
	USB0->CTL = USB_CTL_HOSTMODEEN_MASK;
	USB0->INTEN = 0xfb;
	uart_print_string(UART0, "\r\nUSB host has set up");
	NVIC_EnableIRQ(USB0_IRQn);
	USB0->ENDPOINT[0].ENDPT = USB_ENDPT_HOSTWOHUB_MASK|USB_ENDPT_EPRXEN_MASK|USB_ENDPT_EPTXEN_MASK|
			USB_ENDPT_EPHSHK_MASK;//|USB_ENDPT_RETRYDIS_MASK;
}
void usb_reset(void)
{
	USB0->CTL |= (USB_CTL_RESET_MASK|USB_CTL_USBENSOFEN_MASK);
}
void USB0_IRQHandler(void)
{
	if(USB0->ISTAT & USB_ISTAT_ATTACH_MASK)
	{
		USB0->INTEN &= ~USB_INTEN_ATTACHEN_MASK;
		USB0->ISTAT = USB_ISTAT_ATTACH_MASK;
		signal.bits.attach = 1;
	}
	if(USB0->ISTAT & USB_ISTAT_TOKDNE_MASK)
	{
		USB0->ISTAT = USB_ISTAT_TOKDNE_MASK;
		signal.bits.tokdne = 1;
	}
	if(USB0->ISTAT & USB_ISTAT_USBRST_MASK)
	{
		USB0->ISTAT = USB_ISTAT_USBRST_MASK;
		signal.bits.usbrst = 1;
    }
	if(USB0->ISTAT & USB_ISTAT_ERROR_MASK)
	{
		USB0->ISTAT = USB_ISTAT_ERROR_MASK;
		signal.bits.error = 1;
    }
	if(USB0->ISTAT & USB_ISTAT_SOFTOK_MASK)
	{
		USB0->ISTAT = USB_ISTAT_SOFTOK_MASK;
		signal.bits.softok = 1;
    }
	if(USB0->ISTAT & USB_ISTAT_SLEEP_MASK)
	{
		USB0->ISTAT = USB_ISTAT_SLEEP_MASK;
		signal.bits.sleep = 1;
    }
	if(USB0->ISTAT & USB_ISTAT_RESUME_MASK)
	{
		USB0->ISTAT = USB_ISTAT_RESUME_MASK;
		signal.bits.resume = 1;
    }
	if(USB0->ISTAT & USB_ISTAT_STALL_MASK)
	{
		USB0->ISTAT = USB_ISTAT_STALL_MASK;
		signal.bits.stall = 1;
    }
}
void USB_Delay(uint16_t ms)
{
	uint16_t sof_start;
	uint16_t sof_end;
    sof_start = (USB0->FRMNUMH << 8)|(USB0->FRMNUML);
    do
    {
        sof_end = (USB0->FRMNUMH << 8)|(USB0->FRMNUML);
    }
    while (((uint16_t)(sof_end - sof_start)) < ms);
}
void usb_reset_dev(void)
{
	USB0->ADDR &= ~USB_ADDR_ADDR_MASK;
	USB0->CTL |= USB_CTL_RESET_MASK;
	USB_Delay(30);
	USB0->CTL &= ~USB_CTL_RESET_MASK;
	USB_Delay(100);
	signal.bits.usbrst = 0;
	uart_print_string(UART0, "\r\nUSB device have been reseted");
	dev0.max_size_ep0 = 8;
	dev0.address = 0;
}
void usb_attach_dev(void)
{
	uint8_t temp_loc, speed;
	uart_print_string(UART0, "\r\nUSB device is connected - ");
	USB0->CTL |= USB_CTL_ODDRST_MASK;
	USB0->CTL = USB_CTL_HOSTMODEEN_MASK;
	USB0->ADDR = 0;
	delay(2000000);
	for(uint8_t index = 0; index < 3; index++)
	{
		temp_loc = USB0->CTL & USB_CTL_JSTATE_MASK;
		delay(100000);
		speed = USB0->CTL & USB_CTL_JSTATE_MASK;
		if(temp_loc == speed) break;
	}
	if(temp_loc != speed)
	{
		uart_print_string(UART0, "unknown speed");
		USB0->INTEN |= USB_INTEN_ATTACHEN_MASK;
		return;
	}
	if ((USB0->CTL & USB_CTL_SE0_MASK) == USB_CTL_SE0_MASK)
	{
		USB0->INTEN |= USB_INTEN_ATTACHEN_MASK;
		return;
	}
	if(speed) uart_print_string(UART0, "FS");
	else
	{
		uart_print_string(UART0, "LS");
		USB0->ENDPOINT[0U].ENDPT = USB_ENDPT_HOSTWOHUB_MASK;
		USB0->ADDR |= USB_ADDR_LSEN_MASK;
	}
	USB0->CTL |= USB_CTL_USBENSOFEN_MASK;
	USB0->ISTAT = 0xFF;
	USB0->INTEN &= ~(USB_INTEN_TOKDNEEN_MASK|USB_INTEN_USBRSTEN_MASK);
	USB0->CTL |= USB_CTL_RESET_MASK;
	USB_Delay(30);
	USB0->CTL &= ~USB_CTL_RESET_MASK;
	USB_Delay(100);
	USB0->CONTROL &= ~USB_CONTROL_DPPULLUPNONOTG_MASK;
	USB0->INTEN |= (USB_INTEN_TOKDNEEN_MASK|USB_INTEN_USBRSTEN_MASK);
	dev0.state = dev_attached;
	signal.bits.attach = 0;
}
void setup_req_stage(usb_setup_req_t* req)
{
	bdt.bdt_ep[0].tx[tx_odd_even].buf_address = (uint32_t)req;
	bdt.bdt_ep[0].tx[tx_odd_even].buf_descriptor.raw = 0x00080080;
	USB0->TOKEN = 0xd0;
	uart_print_string(UART0, "\r\nSetup request stage: ");
	func_1(&bdt.bdt_ep[0].tx[tx_odd_even]);
}
void setup_status_stage_out(void)
{
	bdt.bdt_ep[0].tx[tx_odd_even].buf_descriptor.raw = 0x000000C0;
	USB0->TOKEN = 0x10;
	uart_print_string(UART0, "\r\nSetup status stage out: ");
	func_1(&bdt.bdt_ep[0].tx[tx_odd_even]);
}
void func_1(bdt_one_ep_struct* ptr)
{
	uint8_t error = 0;
	for(uint32_t i=USB_TIMEOUT; i>0;i--)
	{
		if(signal.bits.tokdne) break;
		if(i == 10) error = 1;
	}
	signal.bits.tokdne = 0;
	if(error) uart_print_string(UART0, "\e[31mFAIL\e[39m");
	else uart_print_string(UART0, "\e[32mOK\e[39m");
	print_token(ptr);
	tx_odd_even ^= 1;
}
void setup_data_stage(uint16_t size)
{
	uint32_t ptr = (uint32_t)rx_data;
	uint8_t data1 = 1;
	rx_data[size] = 0;
	uint8_t error = 0;
	while(size)
	{
		if(size > dev0.max_size_ep0)
		{
			bdt.bdt_ep[0].rx[rx_odd_even].buf_descriptor.raw = 0x00000080|(dev0.max_size_ep0<<16)|(data1<<6);
			bdt.bdt_ep[0].rx[rx_odd_even].buf_address = ptr;
			size = size - dev0.max_size_ep0;
			ptr += dev0.max_size_ep0;
		}
		else
		{
			bdt.bdt_ep[0].rx[rx_odd_even].buf_descriptor.raw = 0x00000080|(size<<16)|(data1<<6);
			bdt.bdt_ep[0].rx[rx_odd_even].buf_address = ptr;
			size = 0;
		}
		data1 ^= 1;
		USB0->TOKEN = 0x90;
		for(uint32_t i=USB_TIMEOUT; i>0;i--)
		{
			if(signal.bits.tokdne) break;
			if(i == 10) error = 1;
		}
		signal.bits.tokdne = 0;
		print_token(&bdt.bdt_ep[0].rx[rx_odd_even]);
		rx_odd_even ^= 1;;
	}
	uart_print_string(UART0, "\r\nSetup data stage: ");
	if(error) uart_print_string(UART0, "\e[31mFAIL\e[39m");
	else uart_print_string(UART0, "\e[32mOK\e[39m");
}
void setup_status_stage_in(void)
{
	uint8_t error = 0;
	bdt.bdt_ep[0].rx[rx_odd_even].buf_descriptor.raw = 0x00000080;
	USB0->TOKEN = 0x90;
	for(uint32_t i=USB_TIMEOUT; i>0;i--)
	{
		if(signal.bits.tokdne) break;
		if(i == 10) error = 1;
	}
	signal.bits.tokdne = 0;
	print_token(&bdt.bdt_ep[0].rx[rx_odd_even]);
	rx_odd_even ^= 1;
	uart_print_string(UART0, "\r\nSetup status stage in: ");
	if(error) uart_print_string(UART0, "\e[31mFAIL\e[39m");
	else uart_print_string(UART0, "\e[32mOK\e[39m");
}
void print_langid(uint16_t langid)
{
	switch(langid)
	{
		case 0x0409: uart_print_string(UART0, "\n\r\tEnglish (United States)"); break;
		default: uart_print_string(UART0, "unknown lang"); break;
	}
}
void print_rx_descriptors(void)
{
	uint8_t* length = rx_data;
	while(length[0])
	{
		switch(length[1])
		{
			case USB_DESC_TYPE_DEVICE:
				uart_print_string(UART0, "\n\rDevice descriptor\n\r\tbcdUSB: ");
				uint8_to_string(length[3], string);
				uart_print_string(UART0, string);
				uart_print_char(UART0, '.');
				uint8_to_string(length[2], string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbDeviceClass: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->bDeviceClass, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbDeviceSubClass: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->bDeviceSubClass, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbDeviceProtocol: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->bDeviceProtocol, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbMaxPacketSize0: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->bMaxPacketSize, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tidVendor: ");
				uint16_to_hex_string(((usb_device_descriptor_t*)length)->idVendor, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tidProduct: ");
				uint16_to_hex_string(((usb_device_descriptor_t*)length)->idProduct, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbcdDevice: ");
				uint8_to_string(length[13], string);
				uart_print_string(UART0, string);
				uart_print_char(UART0, '.');
				uint8_to_string(length[12], string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tiManufacturer: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->iManufacturer, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tiProduct: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->iProduct, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tiSerialNumber: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->iSerialNumber, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbNumConfigurations: ");
				uint8_to_hex_string(((usb_device_descriptor_t*)length)->bNumConfigurations, string);
				uart_print_string(UART0, string);
			break;
			case USB_DESC_TYPE_CONFIGURATION:
				uart_print_string(UART0, "\n\rConfiguration descriptor\n\r\twTotalLength: ");
				uint16_to_hex_string(length[2]|(length[3]<<8), string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbNumInterfaces: ");
				uint8_to_hex_string(((usb_configuration_descriptor_t*)length)->bNumInterfaces, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbConfigurationValue: ");
				uint8_to_hex_string(((usb_configuration_descriptor_t*)length)->bConfigurationValue, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tiConfiguration: ");
				uint8_to_hex_string(((usb_configuration_descriptor_t*)length)->iConfiguration, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbmAttributes: ");
				uint8_to_hex_string(length[7], string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbMaxPower: ");
				uint16_to_string(((usb_configuration_descriptor_t*)length)->bMaxPower * 2, string);
				uart_print_string(UART0, string);
			break;
			case USB_DESC_TYPE_STRING:
			break;
			case USB_DESC_TYPE_INTERFACE:
				uart_print_string(UART0, "\n\rInterface descriptor:\n\r\tbInterfaceNumber: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bInterfaceNumber, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbAlternateSetting: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bAlternateSetting, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbNumEndpoints: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bNumEndpoints, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbInterfaceClass: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bInterfaceClass, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbInterfaceSubClass: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bInterfaceSubClass, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbInterfaceProtocol: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->bInterfaceProtocol, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tiInterface: ");
				uint8_to_hex_string(((usb_interface_descriptor_t*)length)->iInterface, string);
				uart_print_string(UART0, string);
			break;
			case USB_DESC_TYPE_EP:
				uart_print_string(UART0, "\n\rEP descriptor:\n\r\tbEndpointAddress: ");
				uint8_to_hex_string(length[2], string);
				uart_print_string(UART0, string);
				if(length[2] & 0x80) uart_print_string(UART0, "(In)");
				else uart_print_string(UART0, "(Out)");
				uart_print_string(UART0, "\n\r\tbmAttributes: ");
				uint8_to_hex_string(length[3], string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\twMaxPacketSize: ");
				uint16_to_hex_string(((usb_ep_descriptor_t*)length)->wMaxPacketSize, string);
				uart_print_string(UART0, string);
				uart_print_string(UART0, "\n\r\tbInterval: ");
				uint8_to_hex_string(((usb_ep_descriptor_t*)length)->bInterval, string);
				uart_print_string(UART0, string);
			break;
			case USB_DESC_TYPE_HID_DEV:
			break;
			case USB_DESC_TYPE_HID_REPORT:
			break;
			default: break;
		}
		length = length + length[0];
	}
	uart_print_string(UART0, "\r\nend descriptor buffer");
	for(uint16_t i=0; i<1024; i++) rx_data[i] = 0;
}
void print_token(bdt_one_ep_struct* ptr)
{
	uart_print_string(UART0, "\r\nToken: \e[36m");
	switch(ptr->buf_descriptor.fields.tok_pid)
	{
		case 0: uart_print_string(UART0, "bus timeout"); break;
		case 1: uart_print_string(UART0, "OUT"); break;
		case 2: uart_print_string(UART0, "ACK"); break;
		case 3: uart_print_string(UART0, "DATA0"); break;
		case 9: uart_print_string(UART0, "IN"); break;
		case 0xa: uart_print_string(UART0, "NAK"); break;
		case 0xb: uart_print_string(UART0, "DATA1"); break;
		case 0xd: uart_print_string(UART0, "SETUP"); break;
		case 0xe: uart_print_string(UART0, "STALL"); break;
		case 0xf: uart_print_string(UART0, "data error"); break;
		default: uart_print_string(UART0, "STALL"); break;
	}
	uart_print_string(UART0, "\e[39m");
}
