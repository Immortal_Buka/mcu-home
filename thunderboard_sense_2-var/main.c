#include "system.h"
#include "uart_log.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "native_gecko.h"
//
void main (void);
void uart_print_char(void* uart, uint8_t data);
void init_hw(void);
void init_bt(void);
void SLEEP_SleepBlockBegin(void);
void SLEEP_Init(void);
void SLEEP_LowestEnergyModeGet(void);
void SLEEP_Sleep(void);
void SLEEP_SleepBlockEnd(void);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x20,
	.month = 0x09,
	.year = 0x18
};
uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(1)];
uint32_t end = (uint32_t)(bluetooth_stack_heap + sizeof(bluetooth_stack_heap));
uint8_t string_1[11] = {0};
struct gecko_cmd_packet* evt;
//
void main(void)
{
	fpu_on();
	init_hw();
	CMU->HFBUSCLKEN0 = CMU_HFBUSCLKEN0_GPIO;
	CMU->HFPERCLKEN0 = CMU_HFPERCLKEN0_WTIMER0|CMU_HFPERCLKEN0_USART0;
	//pd8,9 - leds, pa0,1 - uart0
	GPIO->P[3].MODEH = GPIO_P_MODEH_MODE9_PUSHPULLALT|GPIO_P_MODEH_MODE8_PUSHPULLALT;
	GPIO->P[0].MODEL = GPIO_P_MODEL_MODE0_PUSHPULLALT|GPIO_P_MODEL_MODE1_INPUT;
	//wtimer 0
	WTIMER0->ROUTEPEN = 6;
	WTIMER0->ROUTELOC0 = (30<<8)|(29<<16);
	WTIMER0->TOP = 37500-1;//38400000/1024 = 37500
	WTIMER0->CTRL = (10<<24);
	WTIMER0->CC[1].CTRL = (1<<8)|2;
	WTIMER0->CC[1].CCV = 37000;
	WTIMER0->CC[2].CTRL = (1<<8)|2;
	WTIMER0->CC[2].CCV = 18500;
	WTIMER0->CMD = 1;
	//usart0
	USART0->CLKDIV = 5077 & _USART_CLKDIV_DIV_MASK;
	USART0->ROUTEPEN = USART_ROUTEPEN_TXPEN;//|USART_ROUTEPEN_RXPEN;
	USART0->CMD = USART_CMD_TXEN;//|USART_CMD_RXEN;
	uart_log_init(USART0);
	uart_log_add(t_info, __DATE__, 0);
	uart_log_add(t_info, __TIME__, 0);
	init_bt();
	while(1)
	{
	    evt = gecko_wait_event();
	    switch((evt->header))// & 0xffff00f8)
	    {
	    	default:
	    		uint32_to_hex_string(evt->header, string_1);
	    		uart_log_add(t_evt, "Unknown", string_1, 0);
	    	break;
	    }
	}
}
void uart_print_char(void* uart, uint8_t data)
{
	while((((USART_TypeDef*)uart)->STATUS & USART_STATUS_TXBL) == 0){};
	((USART_TypeDef*)uart)->TXDATA = data;
}
void init_hw(void)
{
	EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;
	CMU_HFXOInit_TypeDef hfxoInit = CMU_HFXOINIT_DEFAULT;
	CMU_LFXOInit_TypeDef lfxoInit = CMU_LFXOINIT_DEFAULT;
	//
	CHIP_Init();
	dcdcInit.powerConfig = emuPowerConfig_DcdcToDvdd;
	dcdcInit.dcdcMode = emuDcdcMode_LowNoise;
	dcdcInit.mVout = 1800;
	dcdcInit.em01LoadCurrent_mA = 5;
	dcdcInit.em234LoadCurrent_uA = 10;
	dcdcInit.maxCurrent_mA = 160;
	dcdcInit.anaPeripheralPower = emuDcdcAnaPeripheralPower_AVDD;
	dcdcInit.reverseCurrentControl = emuDcdcLnFastTransient;
	EMU_DCDCInit(&dcdcInit);
	hfxoInit.autoStartEm01 = 1;
	hfxoInit.ctuneSteadyState = 344;
	CMU_HFXOInit(&hfxoInit);
	SystemHFXOClockSet(38400000);
	CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
	CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);
	CMU_ClockEnable(cmuClock_CORELE, true);
	CMU_LFXOInit(&lfxoInit);
	CMU_OscillatorEnable(cmuOsc_LFXO, true, true);
	CMU_ClockSelectSet(cmuClock_LFE, cmuSelect_LFXO);
	CMU_ClockEnable(cmuClock_GPCRC, true);
	CMU_ClockEnable(cmuClock_LDMA, true);
	CMU_ClockEnable(cmuClock_PRS, true);
	CMU_ClockEnable(cmuClock_RTCC, true);
}
void init_bt(void)
{
	static gecko_configuration_t config;
	config.gattdb = 0;
	config.sleep.flags = 0;
	config.bluetooth.max_connections = 1;
	config.bluetooth.heap = bluetooth_stack_heap;
	config.bluetooth.heap_size = sizeof(bluetooth_stack_heap);
	gecko_stack_init(&config);
}
void SLEEP_SleepBlockBegin(void)
{
	uart_log_add(t_info, "SLEEP_SleepBlockBegin", 0);
}
void SLEEP_Init(void)
{
	uart_log_add(t_info, "SLEEP_Init", 0);
}
void SLEEP_LowestEnergyModeGet(void)
{
	uart_log_add(t_info, "SLEEP_LowestEnergyModeGet", 0);
}
void SLEEP_Sleep(void)
{
	uart_log_add(t_info, "SLEEP_Sleep", 0);
}
void SLEEP_SleepBlockEnd(void)
{
	uart_log_add(t_info, "SLEEP_SleepBlockEnd", 0);
}
