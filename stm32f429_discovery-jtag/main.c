#include "system.h"
#include "lcd.h"
#include "print.h"
#include "jtag.h"
//
#define CHAIN_SIZE		5
//
void main(void);
void USART1_IRQHandler(void);
void TMS(uint8_t tms);
void TCK(uint8_t tck);
void TDI(uint8_t tdi);
uint8_t TDO(void);
//
volatile uint8_t uart_rx = 0;
uint8_t string[11] = {0}, size;
extern jtag_chain_t jtag;
//
void main(void)
{
	size = jtag_init_chain(CHAIN_SIZE);
	init_gpio();
	lcd_init(C_Black);
	//PA9-TX, PA10-RX
	GPIOA->MODER |= (2<<(9*2))|(2<<(10*2));
	GPIOA->OSPEEDR |= (3<<(9*2))|(3<<(10*2));
	GPIOA->AFR[1] |= (7<<4)|(7<<8);
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->BRR = (45<<4)|9;//115200
	USART1->CR1 = USART_CR1_UE|USART_CR1_TE|USART_CR1_RE|USART_CR1_RXNEIE;
	NVIC_EnableIRQ(USART1_IRQn);
	//c11 - tck, c12 - tms, c13 - tdi, c14 - tdo
	GPIOC->MODER |= (1<<(11*2))|(1<<(12*2))|(1<<(13*2));
	GPIOC->PUPDR |= (2<<(14*2));
	while(1)
	{
		/*switch(state)
		{
			case 2:
				uart_print_string(USART1, "\r\nchain position: ");
				state = 3;
				uart_rx = 0;
			break;
			case 3:
				while(uart_rx == 0){};
				if(char_to_dec_dig(uart_rx, &temp_glob) == 0)
				{
					if(temp_glob >= jtag.chain_size)
					{
						uart_print_string(USART1, "\r\nwrong position!!!");
						state = 2;
					}
					else
					{
						position = temp_glob;
						state = 4;
					}
				}
				else state = 2;
			break;
			case 4:
				uart_print_string(USART1, "\r\nIR(i)/DR(d)?: ");
				state = 5;
				uart_rx = 0;
			break;
			case 5:
				while(uart_rx == 0){};
				switch(uart_rx)
				{
					case 'i': state = 6; break;
					case 'd': state = 7; break;
					default: state = 4; break;
				}
			break;
			case 6:
				jtag_state_reset(&jtag);
				jtag_state_idle(&jtag);
				//jtag_ir(&jtag, position, 0x0f);
				state = 2;
			break;
			case 7:
				jtag_state_reset(&jtag);
				jtag_state_idle(&jtag);
				//jtag_dr(&jtag, position, 0x17fffffff, 33);
				state = 2;
			break;
		}*/
		uart_print_string(USART1, "\n\rprint IDs? (y/*)");
		while(uart_rx == 0){};
		if(uart_rx == 'y')
		{
			for(uint8_t i=0; i<CHAIN_SIZE; i++)
			{
				uart_print_string(USART1, "\n\r");
				uint8_to_hex_string(i, string);
				uart_print_string(USART1, string);
				uart_print_char(USART1, ' ');
				uint32_to_hex_string(jtag.dr_rx_data[i], string);
				uart_print_string(USART1, string);
			}
			uart_print_string(USART1, "\n\rchain size: ");
			uint8_to_string(size, string);
			uart_print_string(USART1, string);
		}
		uart_rx = 0;
	}
}
void USART1_IRQHandler(void)
{
	uart_rx = USART1->DR;
	USART1->DR = uart_rx;
}
void TMS(uint8_t tms)
{
	if (tms) GPIOC->BSRR = 1<<12;
	else GPIOC->BSRR = 1<<(12+16);
}
void TCK(uint8_t tck)
{
	if (tck) GPIOC->BSRR = 1<<11;
	else GPIOC->BSRR = 1<<(11+16);
}
void TDI(uint8_t tdi)
{
	if (tdi) GPIOC->BSRR = 1<<13;
	else GPIOC->BSRR = 1<<(13+16);
}
uint8_t TDO(void)
{
	return ((GPIOC->IDR >> 14) & 1);
}
