#include <project.h>
#include <stdio.h>
//
#define WS_Q		32
#define HID
//
typedef union
{
	struct
	{
		uint8_t g;
		uint8_t r;
		uint8_t b;
	} color;
	uint8_t raw[3];
} rgb_888_t;
typedef union
{
	struct
	{
		uint16_t h;//hue       : 0..359
		uint8_t s;//saturation : 0..255 (white - color)
		uint8_t v;//value      : 0..255 (black - color)
	} color;
	uint32_t raw;
} hsv_t;
typedef struct
{
    uint16_t* h_value;
    uint8_t* h_delta;
    uint8_t* s_value;
    uint8_t* s_delta;
    uint8_t* v_value;
    uint8_t* v_delta;
    uint16_t h_size;
    uint16_t s_size;
    uint16_t v_size;
} hsv_change_t;
//
void hsv_to_ws(hsv_t* hsv, uint8_t* ws);
void HSV_to_RGB_int(hsv_t* hsv, rgb_888_t* rgb);
void rgb_to_ws(rgb_888_t* rgb, uint8_t* ws);
CY_ISR(timer_irq_handler);
//
uint8_t to_ws[WS_Q][9] = {0}, txChannel, usb_buffer[64], IN_Data_Buffer[8], OUT_Data_Buffer[8], active_led = 0, change_flag = 0;
hsv_t led_hsv[WS_Q] = {0};
uint16_t count;
uint8_t version[16] = {0x0E,0x03,'1',0,'9',0,'0',0,'3',0,'2',0,'2',0};
//
int main(void)
{
    CyGlobalIntEnable;
    EEPROM_1_Start();
    for(uint8_t i=0; i<WS_Q; i++)
	{
		led_hsv[i].color.s = 255;
		led_hsv[i].color.v = 60;
		led_hsv[i].color.h = 360*i/WS_Q;
	}
    txChannel = DMA_1_DmaInitialize(1, 1, HI16(CYDEV_SRAM_BASE), HI16(CYDEV_PERIPH_BASE));
    uint8_t txTD = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(txTD, 9*WS_Q, CY_DMA_DISABLE_TD, TD_INC_SRC_ADR);
    CyDmaTdSetAddress(txTD, LO16((uint32)to_ws), LO16((uint32)SPIM_1_TXDATA_PTR));
    CyDmaChSetInitialTd(txChannel, txTD); 
    SPIM_1_Start();
    isr_timer_1_StartEx(timer_irq_handler);
#ifdef HID
    USBHID_1_Start(0, USBHID_1_5V_OPERATION);
    USBHID_1_SerialNumString(version);
#else
    USBUART_1_Start(0, USBUART_1_5V_OPERATION);
#endif
    Timer_1_Start();
    LED1_Write(1);
    IN_Data_Buffer[0] = WS_Q;
    while(1)
    {
#ifdef HID
	    if(USBHID_1_GetEPState(1) == USBHID_1_IN_BUFFER_EMPTY)
		{
			USBHID_1_LoadEP(1, IN_Data_Buffer, 8);
			USBHID_1_EnableOutEP(2);
		}	
		if(USBHID_1_GetEPState(2) == USBHID_1_OUT_BUFFER_FULL)
		{
			count = USBHID_1_GetEPCount(2);
			USBHID_1_ReadOutEP(2, OUT_Data_Buffer, count);
			USBHID_1_EnableOutEP(2);
            active_led = OUT_Data_Buffer[1];
            IN_Data_Buffer[1] = active_led;
            IN_Data_Buffer[2] = led_hsv[active_led].color.h & 0xff;
            IN_Data_Buffer[3] = led_hsv[active_led].color.h >> 8;
            IN_Data_Buffer[4] = led_hsv[active_led].color.s;
            IN_Data_Buffer[5] = led_hsv[active_led].color.v;
            switch(OUT_Data_Buffer[0])
            {
                case 0x16:
                    led_hsv[active_led].color.h = OUT_Data_Buffer[2]|(OUT_Data_Buffer[3] << 8);
                    led_hsv[active_led].color.s = OUT_Data_Buffer[4];
                    led_hsv[active_led].color.v = OUT_Data_Buffer[5];
                break;
                case 0x21:
                    EEPROM_1_WriteByte(OUT_Data_Buffer[4], OUT_Data_Buffer[2]|(OUT_Data_Buffer[3] << 8));
                break;
                case 0x22:
                    IN_Data_Buffer[6] = EEPROM_1_ReadByte(OUT_Data_Buffer[2]|(OUT_Data_Buffer[3] << 8));
                break;
                case 0x31: change_flag = 1; break;
                case 0x32: change_flag = 0; break;    
                default: break;
            }
        }
#else    
        if(USBUART_1_IsConfigurationChanged())
        {
            if(USBUART_1_GetConfiguration()) USBUART_1_CDC_Init();
        }
        if(USBUART_1_GetConfiguration())
        {
            if(USBUART_1_DataIsReady())
            {
                count = USBUART_1_GetAll(usb_buffer);
                if(count)
                {
                    while(USBUART_1_CDCIsReady() == 0){};
                    USBUART_1_PutData(usb_buffer, count);
                }
            }
        }
#endif        
    }    
}
void hsv_to_ws(hsv_t* hsv, uint8_t* ws)
{
	rgb_888_t temp_loc;
	HSV_to_RGB_int(hsv, &temp_loc);
	rgb_to_ws(&temp_loc, ws);
}
void HSV_to_RGB_int(hsv_t* hsv, rgb_888_t* rgb)
{
	const uint8_t dim_curve[256] =
	{
		0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6,
		6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8,
		8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 11, 11, 11,
		11, 11, 12, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15,
		15, 15, 16, 16, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20,
		20, 20, 21, 21, 22, 22, 22, 23, 23, 24, 24, 25, 25, 25, 26, 26,
		27, 27, 28, 28, 29, 29, 30, 30, 31, 32, 32, 33, 33, 34, 35, 35,
		36, 36, 37, 38, 38, 39, 40, 40, 41, 42, 43, 43, 44, 45, 46, 47,
		48, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
		63, 64, 65, 66, 68, 69, 70, 71, 73, 74, 75, 76, 78, 79, 81, 82,
		83, 85, 86, 88, 90, 91, 93, 94, 96, 98, 99, 101, 103, 105, 107, 109,
		110, 112, 114, 116, 118, 121, 123, 125, 127, 129, 132, 134, 136, 139, 141, 144,
		146, 149, 151, 154, 157, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 190,
		193, 196, 200, 203, 207, 211, 214, 218, 222, 226, 230, 234, 238, 242, 248, 255,
	};
	if(hsv->color.h > 359) hsv->color.h = 0;
	uint32_t base;
	uint8_t temp_val = dim_curve[hsv->color.v];
	uint8_t temp_sat = 255 - dim_curve[255 - hsv->color.s];
	if(temp_sat == 0)//Achromatic color (gray). Hue doesn't mind.
	{
		rgb->color.r = temp_val;
		rgb->color.g = temp_val;
		rgb->color.b = temp_val;
	}
	else
	{
		base = ((255 - temp_sat) * temp_val) >> 8;
		switch (hsv->color.h / 60)
		{
			case 0:
				rgb->color.r = temp_val;
				rgb->color.g = (((temp_val - base) * hsv->color.h) / 60) + base;
				rgb->color.b = base;
			break;
			case 1:
				rgb->color.r = (((temp_val - base) * (60 - (hsv->color.h % 60))) / 60) + base;
				rgb->color.g = temp_val;
				rgb->color.b = base;
			break;
			case 2:
				rgb->color.r = base;
				rgb->color.g = temp_val;
				rgb->color.b = (((temp_val - base) * (hsv->color.h % 60)) / 60) + base;
			break;
			case 3:
				rgb->color.r = base;
				rgb->color.g = (((temp_val - base) * (60 - (hsv->color.h % 60))) / 60) + base;
				rgb->color.b = temp_val;
			break;
			case 4:
				rgb->color.r = (((temp_val - base) * (hsv->color.h % 60)) / 60) + base;
				rgb->color.g = base;
				rgb->color.b = temp_val;
			break;
			case 5:
				rgb->color.r = temp_val;
				rgb->color.g = base;
				rgb->color.b = (((temp_val - base) * (60 - (hsv->color.h % 60))) / 60) + base;
			break;
		}
	}
}
void rgb_to_ws(rgb_888_t* rgb, uint8_t* ws)
{
	for(uint8_t i=0; i<3; i++)
	{
		ws[0+3*i] = 0x24;
		if((rgb->raw[i] & 0x80) == 0) ws[0+3*i] |= 0x40;
		if((rgb->raw[i] & 0x40) == 0) ws[0+3*i] |= 8;
		if((rgb->raw[i] & 0x20) == 0) ws[0+3*i] |= 1;
		ws[1+3*i] = 0x92;
		if((rgb->raw[i] & 0x10) == 0) ws[1+3*i] |= 0x20;
		if((rgb->raw[i] & 8) == 0) ws[1+3*i] |= 4;
		ws[2+3*i] = 0x49;
		if((rgb->raw[i] & 4) == 0) ws[2+3*i] |= 0x80;
		if((rgb->raw[i] & 2) == 0) ws[2+3*i] |= 0x10;
		if((rgb->raw[i] & 1) == 0) ws[2+3*i] |= 2;
        for(uint8_t j=0; j<3; j++) ws[i*3+j] = ~ws[i*3+j];
	}
}
CY_ISR(timer_irq_handler)
{
    Timer_1_STATUS;
    for(uint8_t i=0; i<WS_Q; i++) 
    {
        if(change_flag) led_hsv[i].color.h++; 
        hsv_to_ws(&led_hsv[i], to_ws[i]);
    }
    CyDmaChEnable(txChannel, 1);
}
