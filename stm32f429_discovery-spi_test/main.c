#include "system.h"
#include "lcd.h"
//
void main(void);
void SPI4_IRQHandler(void);
//
void main(void)
{
	init_gpio();
	sdram_init();
	lcd_init(C_Black);
	//e2 - clk, e4 - nss, e5 - miso, e6 - mosi
	GPIOE->AFR[0] |= 0x05550500;
	GPIOE->MODER |= (2<<(2*2))|(2<<(4*2))|(2<<(5*2))|(2<<(6*2));
	RCC->APB2ENR |= RCC_APB2ENR_SPI4EN;
	SPI4->CR2 = SPI_CR2_RXNEIE|SPI_CR2_ERRIE;
	SPI4->CR1 = SPI_CR1_DFF|SPI_CR1_SPE|SPI_CR1_CPOL|SPI_CR1_CPHA;
	NVIC_EnableIRQ(SPI4_IRQn);
	board_LED(green, on);
	while(1)
	{
	}
}
void SPI4_IRQHandler(void)
{
	uint16_t loc_temp;
	uint8_t string[11] = {0};
	//static uint16_t rx_cnt = 0;
	while(SPI4->SR & SPI_SR_RXNE)
	{
		loc_temp = SPI4->DR;
		uint16_to_hex_string(loc_temp, string);
		lcd_print_string(string, C_White);
		lcd_print_string("\r\n", C_White);
	}
}
