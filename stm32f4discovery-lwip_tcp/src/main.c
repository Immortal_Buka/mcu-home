#include "system.h"
#include "my_lwip.h"
#include <string.h>
#include "print.h"
#include "circular_buffer.h"
#include "binh.h"
//
#define LED_ON							0
#define LED_OFF							1
#define LED_ORANGE						3
#define LED_GREEN						4
#define LED_RED							5
#define LED_BLUE						6
#define PKT_SIZE						TCP_MSS*10
#define LAN8720_ADDR					0
#define ETH_MAX_PACKET_SIZE				1524
#define PHY_RESET_DELAY					0xFF
#define ETH_TXBUFNB						10
#define ETH_RXBUFNB						1
#define PHY_BCR							0
#define PHY_BSR							1
#define PHY_Reset						((uint16_t)0x8000)
#define PHY_Linked_Status				((uint16_t)0x0004)
#define PHY_AutoNegotiation				((uint16_t)0x1000)
#define PHY_AutoNego_Complete			((uint16_t)0x0020)
#define ETH_DMARXDESC_FRAMELENGTHSHIFT	16
#define ETH_DMATXDESC_TCH				0x00100000U
#define ETH_DMARXDESC_RCH				0x00004000U
#define ETH_DMARXDESC_OWN				0x80000000U
#define ETH_DMATXDESC_OWN				0x80000000U
#define ETH_DMATXDESC_LS				0x20000000U
#define ETH_DMATXDESC_FS				0x10000000U
#define ETH_DMATXDESC_TBS1				0x00001FFFU
#define ETH_DMARXDESC_FS				0x00000200U
#define ETH_DMARXDESC_LS				0x00000100U
#define ETH_DMARXDESC_FL				0x3FFF0000U
//
typedef enum
{
	speed_unknown,
    speed_10M,
	speed_100M
} enet_speed_t;
typedef enum
{
	duplex_unknown,
	duplex_half,
	duplex_full
} enet_duplex_t;
typedef struct
{
	uint8_t* hw_addr;
	enet_speed_t speed;
	enet_duplex_t duplex;
} conn_prop_t;
typedef struct
{
	volatile uint32_t   Status;
	uint32_t   ControlBufferSize;
	uint32_t   Buffer1Addr;
	uint32_t   Buffer2NextDescAddr;
	uint32_t   ExtendedStatus;
	uint32_t   Reserved1;
	uint32_t   TimeStampLow;
	uint32_t   TimeStampHigh;
} ETH_DMADESCTypeDef;
typedef struct
{
	ETH_DMADESCTypeDef* FSRxDesc;          /*!< First Segment Rx Desc */
	ETH_DMADESCTypeDef* LSRxDesc;          /*!< Last Segment Rx Desc */
	uint32_t  SegCount;                    /*!< Segment count */
	uint32_t length;                       /*!< Frame length */
	uint32_t buffer;                       /*!< Frame buffer */
} ETH_DMARxFrameInfos;
//
void main(void);
void project_def_handler(void);
void sys_init(void);
void SysTick_Handler(void);
void LED(uint8_t number, uint8_t state);
void low_level_debug_msg(uint8_t* string);
void low_level_error(uint8_t* string);
void low_level_init(struct netif *);
err_t low_level_output(struct netif *netif, struct pbuf *p);
struct pbuf* low_level_input(void);
void HAL_ETH_WritePHYRegister(uint8_t phy_addr, uint16_t PHYReg, uint16_t RegValue);
uint16_t HAL_ETH_ReadPHYRegister(uint8_t phy_addr, uint16_t PHYReg);
void ETH_Delay(uint32_t mdelay);
uint32_t ETH_Prepare_Transmit_Descriptors(uint16_t FrameLength);
uint8_t HAL_ETH_GetReceivedFrame(void);
void uart_print_char(void* uart, uint8_t data);
void HAL_ETH_TransmitFrame(uint32_t FrameLength);
uint8_t* low_level_get_mac_ptr(void);
void low_level_data_in(uint16_t size, uint8_t* data);
void cb_error_signal(circle_buffer_t* buffer, cb_error_t error);
void proj_print(uint8_t* ptr);
//
const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x30,
	.month = 0x08,
	.year = 0x19,
};
conn_prop_t props;
uint8_t pkt[PKT_SIZE] = {0};
ETH_DMADESCTypeDef DMARxDscrTab[ETH_RXBUFNB] __attribute__ ((aligned (4)));
ETH_DMADESCTypeDef DMATxDscrTab[ETH_TXBUFNB] __attribute__ ((aligned (4)));
uint8_t Rx_Buff[ETH_RXBUFNB][ETH_MAX_PACKET_SIZE] __attribute__ ((aligned (4)));
uint8_t Tx_Buff[ETH_TXBUFNB][ETH_MAX_PACKET_SIZE] __attribute__ ((aligned (4)));
ETH_DMADESCTypeDef* RxDesc;
ETH_DMADESCTypeDef* TxDesc;
ETH_DMARxFrameInfos RxFrameInfos;
uint8_t hw_addr[6] = {0, 0x80, 0xe1}, cb_buffer[0x1000];
circle_buffer_t cb_tcp_rx;
volatile memory_buffer_t binh_data_buf __attribute__ ((aligned (4), section (".ccram")));
//
void main(void)
{
	time_ms = 0;
	SysTick_Config(168000);//1ms
	NVIC_EnableIRQ(SysTick_IRQn);
	cb_init(&cb_tcp_rx, cb_buffer, sizeof(cb_buffer));
	for(uint8_t i=0; i<3; i++) hw_addr[i+3] = ((uint8_t*)UID_BASE)[i];
	//for(uint16_t i=0; i<PKT_SIZE; i++) pkt[i] = i&0xff;
	//clk
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIOCEN|RCC_AHB1ENR_GPIODEN|RCC_AHB1ENR_GPIOEEN;
	//gpio
	//a1-nint/refclko, 2-mdio, 7-crs_dv/mode2, 13,14-swd
	GPIOA->AFR[0] = (11<<(1*4))|(11<<(2*4))|(11<<(7*4));
	GPIOA->MODER = (2<<2)|(2<<4)|(2<<14)|(2<<26)|(2<<28);
	GPIOA->OSPEEDR = 0xffffffff;
	//b11-txen, 12-txd0, 13-txd1,
	GPIOB->AFR[1] = (11<<((11-8)*4))|(11<<((12-8)*4))|(11<<((13-8)*4));
	GPIOB->MODER = (2<<22)|(2<<24)|(2<<26);
	GPIOB->OSPEEDR = 0xffffffff;
	//c1-mdc, 4-rxd0, 5-rxd1, 6,7 - uart6
	GPIOC->AFR[0] = (11<<(1*4))|(11<<(4*4))|(11<<(5*4))|(8<<(7*4))|(8<<(6*4));
	GPIOC->MODER = (2<<2)|(2<<8)|(2<<10)|(2<<12)|(2<<14);
	GPIOC->OSPEEDR = 0xffffffff;
	//d4-0, 12-15-LEDs
	GPIOD->MODER = 0x55000100;
	GPIOD->OSPEEDR = 0xffffffff;
	//e2-nrst
	GPIOE->MODER = 0x00000010;
	GPIOE->OSPEEDR = 0xffffffff;
	GPIOE->OTYPER = 1<<2;
	RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
	USART6->BRR = 182;//460800
	USART6->CR1 = USART_CR1_UE|USART_CR1_TE;
	proj_print("\r\nuart init done\r\nbuild date: ");
	proj_print(__DATE__);
	GPIOE->BSRR = 1<<18;
	delay(200000);
	GPIOE->BSRR = 1<<2;
	my_lwip_init();
	LED(LED_BLUE, LED_ON);
	while(1)
	{
		my_lwip_loop();
		/*if((tcp_pcb_inst->state == ESTABLISHED))
		{
			tcp_write(tcp_pcb_inst, pkt, PKT_SIZE, 0);
			tcp_output(tcp_pcb_inst);
		}*/
		uint16_t temp_loc = cb_calc_unread(&cb_tcp_rx);
		if(temp_loc)
		{
			for(uint16_t i=0; i<temp_loc; i++)
			{
				uint8_t temp_loc_2, temp_loc_3[5];
				cb_read(&cb_tcp_rx, &temp_loc_2, 1);
				uint8_to_hex_string(temp_loc_2, temp_loc_3);
				proj_print(temp_loc_3);
				binh_rx(temp_loc_2);
			}
		}
	}
}
void project_def_handler(void)
{
}
void sys_init(void)
{
	FLASH->ACR = FLASH_ACR_ICEN|FLASH_ACR_DCEN|FLASH_ACR_PRFTEN|FLASH_ACR_LATENCY_5WS;
	RCC->CR |= RCC_CR_HSEON|RCC_CR_CSSON;
	RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC_HSE|(7<<24)|4|(168<<6);
	while((RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY){};
	RCC->CR |= RCC_CR_PLLON;
	while ((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY){};
	RCC->CFGR = RCC_CFGR_SW_PLL|RCC_CFGR_PPRE1_DIV4|RCC_CFGR_PPRE2_DIV2;
	//APB1 42MHz (PCLK1)
	//APB2 84MHz (PCLK2)
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL){};
}
void SysTick_Handler(void)
{
	/*extern uint32_t picture_size;
	extern uint16_t picture[];
	extern uint32_t _binary_A__mine_download_data_png_start;
	extern uint32_t _binary_A__mine_download_data_png_size;
	static uint32_t size = 0;
	static uint8_t en = 1;
	uint32_t temp_loc_1;
	uint8_t* temp_loc_2;*/
	time_ms++;
	/*if((tcp_pcb_inst->state == ESTABLISHED)&&(en))
	{
		if((size + PKT_SIZE) < (uint32_t)&_binary_A__mine_download_data_png_size)
		{
			temp_loc_2 = (uint8_t*)((uint32_t)&_binary_A__mine_download_data_png_start + size);
			tcp_write(tcp_pcb_inst, temp_loc_2, PKT_SIZE, 0);
			size += PKT_SIZE;
		}
		else
		{
			temp_loc_1 = (uint32_t)&_binary_A__mine_download_data_png_size - size;
			temp_loc_2 = (uint8_t*)((uint32_t)&_binary_A__mine_download_data_png_start + size);
			tcp_write(tcp_pcb_inst, temp_loc_2, temp_loc_1, 0);
			en = 0;
		}
		tcp_output(tcp_pcb_inst);
	}*/
}
void LED(uint8_t number, uint8_t state)
{
	number += 9;
	GPIOD->BSRR = 1<<(number+(state*16));
}
void low_level_debug_msg(uint8_t* string)
{
	proj_print(string);
}
void low_level_error(uint8_t* string)
{
	LED(LED_RED, LED_ON);
	proj_print(string);
	while(1){};
}
void low_level_init(struct netif* netif)
{
	uint16_t temp_loc;
	RCC->AHB1ENR |= RCC_AHB1ENR_ETHMACEN|RCC_AHB1ENR_ETHMACRXEN|RCC_AHB1ENR_ETHMACTXEN;
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	SYSCFG->PMC = SYSCFG_PMC_MII_RMII_SEL;
	ETH->DMABMR |= ETH_DMABMR_SR;
	while ((ETH->DMABMR & ETH_DMABMR_SR) != 0){};
	ETH->MACMIIAR = ETH_MACMIIAR_CR_Div102;
	temp_loc = HAL_ETH_ReadPHYRegister(LAN8720_ADDR, 2);
	if(temp_loc != 7) low_level_error("\r\nPHY error!");
	HAL_ETH_WritePHYRegister(LAN8720_ADDR, PHY_BCR, PHY_Reset);
	ETH_Delay(PHY_RESET_DELAY);
	do
	{
		temp_loc = HAL_ETH_ReadPHYRegister(LAN8720_ADDR, PHY_BSR);
	}
	while (((temp_loc & PHY_Linked_Status) != PHY_Linked_Status));
	HAL_ETH_WritePHYRegister(LAN8720_ADDR, PHY_BCR, PHY_AutoNegotiation);
	do
	{
		temp_loc = HAL_ETH_ReadPHYRegister(LAN8720_ADDR, PHY_BSR);
	}
	while (((temp_loc & PHY_AutoNego_Complete) != PHY_AutoNego_Complete));
	temp_loc = HAL_ETH_ReadPHYRegister(LAN8720_ADDR, 31);
	temp_loc = (temp_loc >> 2) & 7;
	switch(temp_loc)
	{
		case 1:
			props.duplex = duplex_half;
			props.speed = speed_10M;
		break;
		case 2:
			props.duplex = duplex_half;
			props.speed = speed_100M;
		break;
		case 5:
			props.duplex = duplex_full;
			props.speed = speed_10M;
		break;
		case 6:
			props.duplex = duplex_full;
			props.speed = speed_100M;
		break;
		default:
			low_level_error("\r\nAutoNegotiation error!");
		break;
	}
	ETH->MACCR |= ETH_MACCR_FES|ETH_MACCR_RD|ETH_MACCR_DM;//|ETH_MACCR_LM|ETH_MACCR_IPCO;
	ETH->MACFFR = 1<<6;
	ETH->MACFCR = ETH_MACFCR_ZQPD;
	ETH->DMAOMR = ETH_DMAOMR_RSF|ETH_DMAOMR_TSF|ETH_DMAOMR_OSF;
    ETH->DMABMR = ETH_DMABMR_AAB|ETH_DMABMR_FB|0x400000|(0x20<<8)|ETH_DMABMR_USP|ETH_DMABMR_EDE|(0x20<<17)|ETH_DMABMR_USP;
    //if((heth->Init).RxMode == ETH_RXINTERRUPT_MODE) ETH->DMAIER |= ETH_DMA_IT_NIS|ETH_DMA_IT_R;
	ETH->MACA0HR = (hw_addr[5] << 8)|hw_addr[4];
	ETH->MACA0LR = (hw_addr[3] << 24)|(hw_addr[2] << 16)|(hw_addr[1] << 8)|hw_addr[0];
	TxDesc = DMATxDscrTab;
	for(uint8_t i=0; i < ETH_TXBUFNB; i++)
	{
		DMATxDscrTab[i].Status = ETH_DMATXDESC_TCH;
		DMATxDscrTab[i].Buffer1Addr = (uint32_t)Tx_Buff[i];
		//if ((heth->Init).ChecksumMode == ETH_CHECKSUM_BY_HARDWARE) DMATxDscrTab[i].Status |= ETH_DMATXDESC_CHECKSUMTCPUDPICMPFULL;
		if(i < (ETH_TXBUFNB-1)) DMATxDscrTab[i].Buffer2NextDescAddr = (uint32_t)(&DMATxDscrTab[i+1]);
		else DMATxDscrTab[i].Buffer2NextDescAddr = (uint32_t)DMATxDscrTab;
	}
	ETH->DMATDLAR = (uint32_t)DMATxDscrTab;
	RxDesc = DMARxDscrTab;
	for(uint8_t i=0; i < ETH_RXBUFNB; i++)
	{
		DMARxDscrTab[i].Status = ETH_DMARXDESC_OWN;
		DMARxDscrTab[i].ControlBufferSize = ETH_DMARXDESC_RCH | ETH_MAX_PACKET_SIZE;
		DMARxDscrTab[i].Buffer1Addr = (uint32_t)Rx_Buff[i];
		//if((heth->Init).RxMode == ETH_RXINTERRUPT_MODE) DMARxDscrTab[i].ControlBufferSize &= ~ETH_DMARXDESC_DIC;
		if(i < (ETH_RXBUFNB-1)) DMARxDscrTab[i].Buffer2NextDescAddr = (uint32_t)&DMARxDscrTab[i+1];
		else DMARxDscrTab[i].Buffer2NextDescAddr = (uint32_t)DMARxDscrTab;
	}
	ETH->DMARDLAR = (uint32_t) DMARxDscrTab;
	ETH->MACCR |= ETH_MACCR_TE|ETH_MACCR_RE;
	ETH->DMAOMR |= ETH_DMAOMR_FTF;
	ETH->DMAOMR |= ETH_DMAOMR_ST|ETH_DMAOMR_SR;
	/*heth.Init.AutoNegotiation = ETH_AUTONEGOTIATION_ENABLE;
	heth.Init.PhyAddress = PHY_USER_NAME_PHY_ADDRESS;
	heth.Init.RxMode = ETH_RXPOLLING_MODE;
	heth.Init.ChecksumMode = ETH_CHECKSUM_BY_HARDWARE;
	heth.Init.MediaInterface = ETH_MEDIA_INTERFACE_RMII;
	(heth->Init).DuplexMode;
	(heth->Init).Speed;
	if(heth->Init.ChecksumMode == ETH_CHECKSUM_BY_HARDWARE) macinit.ChecksumOffload = ETH_CHECKSUMOFFLAOD_ENABLE;
	else macinit.ChecksumOffload = ETH_CHECKSUMOFFLAOD_DISABLE;*/
}
err_t low_level_output(struct netif *netif, struct pbuf *p)
{
	/*struct pbuf* q;
	uint16_t framelength = 0;
	uint8_t* buffer = (uint8_t*)(DMATxDescToSet->Buffer1Addr);
	for(q = p; q != 0; q = q->next)
	{
		memcpy((u8_t*)&buffer[framelength], q->payload, q->len);
		framelength = framelength + q->len;
	}
	ETH_Prepare_Transmit_Descriptors(framelength);
	return ERR_OK;*/
	err_t errval;
	struct pbuf* q;
	uint8_t* buffer = (uint8_t *)(TxDesc->Buffer1Addr);
	uint32_t framelength = 0;
	uint32_t bufferoffset = 0;
	uint32_t byteslefttocopy = 0;
	uint32_t payloadoffset = 0;
	bufferoffset = 0;
	for(q = p; q != 0; q = q->next)
	{
		if((TxDesc->Status & ETH_DMATXDESC_OWN) != 0)
		{
			errval = ERR_USE;
	        goto error;
		}
		byteslefttocopy = q->len;
	    payloadoffset = 0;
	    while( (byteslefttocopy + bufferoffset) > ETH_MAX_PACKET_SIZE )
	    {
	    	memcpy((uint8_t*)((uint8_t*)buffer + bufferoffset), (uint8_t*)((uint8_t*)q->payload + payloadoffset), (ETH_MAX_PACKET_SIZE - bufferoffset) );
	    	TxDesc = (ETH_DMADESCTypeDef*)(TxDesc->Buffer2NextDescAddr);
	    	if((TxDesc->Status & ETH_DMATXDESC_OWN) != 0)
	        {
	    		errval = ERR_USE;
	    		goto error;
	        }
	        buffer = (uint8_t *)(TxDesc->Buffer1Addr);
	        byteslefttocopy = byteslefttocopy - (ETH_MAX_PACKET_SIZE - bufferoffset);
	        payloadoffset = payloadoffset + (ETH_MAX_PACKET_SIZE - bufferoffset);
	        framelength = framelength + (ETH_MAX_PACKET_SIZE - bufferoffset);
	        bufferoffset = 0;
	    }
	    memcpy( (uint8_t*)((uint8_t*)buffer + bufferoffset), (uint8_t*)((uint8_t*)q->payload + payloadoffset), byteslefttocopy );
	    bufferoffset = bufferoffset + byteslefttocopy;
	    framelength = framelength + byteslefttocopy;
	}
	HAL_ETH_TransmitFrame(framelength);
	errval = ERR_OK;
error:
	if((ETH->DMASR & ETH_DMASR_TUS) != 0)
	{
		ETH->DMASR = ETH_DMASR_TUS;
		ETH->DMATPDR = 0;
	}
	return errval;
}
struct pbuf* low_level_input(void)
{
	/*struct pbuf *p, *q;
	uint16_t len;
	uint32_t l =0;
	FrameTypeDef* frame;
	uint8_t* buffer;
	volatile ETH_DMADESCTypeDef *DMARxNextDesc;
	p = 0;
	frame = ETH_Get_Received_Frame();
	len = frame->length;
	buffer = (uint8_t*)(frame->buffer);
	p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
	if (p != 0)
	{
		for (q = p; q != 0; q = q->next)
		{
			memcpy((u8_t*)q->payload, (u8_t*)&buffer[l], q->len);
			l = l + q->len;
		}
	}
	if (ETH_DMA_Rx_Frame_infos.Seg_Count > 1) DMARxNextDesc = ETH_DMA_Rx_Frame_infos.FS_Rx_Desc;
	else DMARxNextDesc = frame->descriptor;
	for (uint32_t i=0; i<ETH_DMA_Rx_Frame_infos.Seg_Count; i++)
	{
		DMARxNextDesc->Status = ETH_DMARxDesc_OWN;
		DMARxNextDesc = (ETH_DMADESCTypeDef *)(DMARxNextDesc->Buffer2NextDescAddr);
	}
	ETH_DMA_Rx_Frame_infos.Seg_Count = 0;
	if ((ETH->DMASR & ETH_DMASR_RBUS) != 0)
	{
		ETH->DMASR = ETH_DMASR_RBUS;
		ETH->DMARPDR = 0;
	}
	return p;*/
	struct pbuf* p = 0;
	struct pbuf* q = 0;
	uint16_t len = 0;
	uint8_t* buffer;
	__IO ETH_DMADESCTypeDef* dmarxdesc;
	uint32_t bufferoffset = 0;
	uint32_t payloadoffset = 0;
	uint32_t byteslefttocopy = 0;
	uint32_t i=0;
	if(HAL_ETH_GetReceivedFrame()) return 0;
	len = RxFrameInfos.length;
	buffer = (uint8_t *)RxFrameInfos.buffer;
	if (len > 0) p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
	if (p != 0)
	{
		dmarxdesc = RxFrameInfos.FSRxDesc;
	    bufferoffset = 0;
	    for(q = p; q != 0; q = q->next)
	    {
	    	byteslefttocopy = q->len;
	    	payloadoffset = 0;
	    	while( (byteslefttocopy + bufferoffset) > ETH_MAX_PACKET_SIZE )
	    	{
	    		memcpy( (uint8_t*)((uint8_t*)q->payload + payloadoffset), (uint8_t*)((uint8_t*)buffer + bufferoffset), (ETH_MAX_PACKET_SIZE - bufferoffset));
	    		dmarxdesc = (ETH_DMADESCTypeDef*)(dmarxdesc->Buffer2NextDescAddr);
	    		buffer = (uint8_t *)(dmarxdesc->Buffer1Addr);
	    		byteslefttocopy = byteslefttocopy - (ETH_MAX_PACKET_SIZE - bufferoffset);
	    		payloadoffset = payloadoffset + (ETH_MAX_PACKET_SIZE - bufferoffset);
	    		bufferoffset = 0;
	    	}
	    	memcpy( (uint8_t*)((uint8_t*)q->payload + payloadoffset), (uint8_t*)((uint8_t*)buffer + bufferoffset), byteslefttocopy);
	    	bufferoffset = bufferoffset + byteslefttocopy;
	    }
	}
	dmarxdesc = RxFrameInfos.FSRxDesc;
	for (i=0; i< RxFrameInfos.SegCount; i++)
	{
		dmarxdesc->Status |= ETH_DMARXDESC_OWN;
	    dmarxdesc = (ETH_DMADESCTypeDef*)(dmarxdesc->Buffer2NextDescAddr);
	}
	RxFrameInfos.SegCount =0;
	if ((ETH->DMASR & ETH_DMASR_RBUS) != 0)
	{
		ETH->DMASR = ETH_DMASR_RBUS;
		ETH->DMARPDR = 0;
	}
	return p;
}
void HAL_ETH_WritePHYRegister(uint8_t phy_addr, uint16_t PHYReg, uint16_t RegValue)
{
	uint32_t temp_loc = ETH->MACMIIAR & ETH_MACMIIAR_CR_Msk;
	ETH->MACMIIDR = RegValue;
	temp_loc |= (phy_addr<<11)|(PHYReg<<6)|ETH_MACMIIAR_MB|ETH_MACMIIAR_MW;
	ETH->MACMIIAR = temp_loc;
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB) == ETH_MACMIIAR_MB){};
}
uint16_t HAL_ETH_ReadPHYRegister(uint8_t phy_addr, uint16_t PHYReg)
{
	uint32_t temp_loc = ETH->MACMIIAR & ETH_MACMIIAR_CR_Msk;
	temp_loc |= (phy_addr<<11)|(PHYReg<<6)|ETH_MACMIIAR_MB;
	ETH->MACMIIAR = temp_loc;
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB) == ETH_MACMIIAR_MB){};
	return (ETH->MACMIIDR);
}
void ETH_Delay(uint32_t mdelay)
{
	uint32_t temp_loc = time_ms;
	while((time_ms - temp_loc) < mdelay)
	/*__IO uint32_t Delay = mdelay * (168000000 / 8U / 1000U);
	do*/
	{
		__NOP();
	}
	//while (Delay --);
}
/*uint32_t ETH_Prepare_Transmit_Descriptors(uint16_t FrameLength)
{
	uint32_t buf_count =0, size=0;
	__IO ETH_DMADESCTypeDef *DMATxNextDesc;
	if((DMATxDescToSet->Status & ETH_DMATxDesc_OWN) != 0) return 0;
	DMATxNextDesc = DMATxDescToSet;
	if (FrameLength > ETH_MAX_PACKET_SIZE)
	{
		buf_count = FrameLength/ETH_MAX_PACKET_SIZE;
		if (FrameLength%ETH_MAX_PACKET_SIZE) buf_count++;
	}
	else buf_count =1;
	if (buf_count ==1)
	{
		DMATxDescToSet->Status |=ETH_DMATxDesc_FS|ETH_DMATxDesc_LS;
		DMATxDescToSet->ControlBufferSize = (FrameLength & ETH_DMATxDesc_TBS1);
		DMATxDescToSet->Status |= ETH_DMATxDesc_OWN;
		DMATxDescToSet= (ETH_DMADESCTypeDef *)(DMATxDescToSet->Buffer2NextDescAddr);
	}
	else
	{
		for (uint32_t i=0; i< buf_count; i++)
		{
			if(i==0) DMATxDescToSet->Status |= ETH_DMATxDesc_FS;
			DMATxNextDesc->ControlBufferSize = (ETH_MAX_PACKET_SIZE & ETH_DMATxDesc_TBS1);
			if (i== (buf_count-1))
			{
				DMATxNextDesc->Status |= ETH_DMATxDesc_LS;
				size = FrameLength - (buf_count-1)*ETH_MAX_PACKET_SIZE;
				DMATxNextDesc->ControlBufferSize = (size & ETH_DMATxDesc_TBS1);
			}
			DMATxNextDesc->Status |= ETH_DMATxDesc_OWN;
			DMATxNextDesc = (ETH_DMADESCTypeDef *)(DMATxNextDesc->Buffer2NextDescAddr);
		}
		DMATxDescToSet = DMATxNextDesc ;
	}
	if ((ETH->DMASR & ETH_DMASR_TBUS) != 0)
	{
		ETH->DMASR = ETH_DMASR_TBUS;
		ETH->DMATPDR = 0;
	}
	return 1;
}
FrameTypeDef* ETH_Get_Received_Frame(void)
{
	glob_frame.length = ((DMARxDescToGet->Status & ETH_DMARxDesc_FL) >> ETH_DMARxDesc_FrameLengthShift) - 4;
	if (ETH_DMA_Rx_Frame_infos.Seg_Count >1) glob_frame.buffer = (ETH_DMA_Rx_Frame_infos.FS_Rx_Desc)->Buffer1Addr;
	else glob_frame.buffer = DMARxDescToGet->Buffer1Addr;
	glob_frame.descriptor = DMARxDescToGet;
	DMARxDescToGet = (ETH_DMADESCTypeDef*) (DMARxDescToGet->Buffer2NextDescAddr);
	return (&glob_frame);
}*/
void uart_print_char(void* uart, uint8_t data)
{
	while(!(((USART_TypeDef*)uart)->SR & USART_SR_TC));
	((USART_TypeDef*)uart)->DR=data;
}
void HAL_ETH_TransmitFrame(uint32_t FrameLength)
{
	uint32_t bufcount = 0, size = 0;
	if(FrameLength == 0) return;
	if((TxDesc->Status & ETH_DMATXDESC_OWN) != 0) return;
	if(FrameLength > ETH_MAX_PACKET_SIZE)
	{
		bufcount = FrameLength/ETH_MAX_PACKET_SIZE;
		if(FrameLength % ETH_MAX_PACKET_SIZE) bufcount++;
	}
	else bufcount = 1;
	if(bufcount == 1)
	{
		TxDesc->Status |=ETH_DMATXDESC_FS|ETH_DMATXDESC_LS;
		TxDesc->ControlBufferSize = (FrameLength & ETH_DMATXDESC_TBS1);
		TxDesc->Status |= ETH_DMATXDESC_OWN;
		TxDesc= (ETH_DMADESCTypeDef*)(TxDesc->Buffer2NextDescAddr);
	}
	else
	{
		for (uint32_t i=0; i< bufcount; i++)
		{
			TxDesc->Status &= ~(ETH_DMATXDESC_FS | ETH_DMATXDESC_LS);
			if (i == 0) TxDesc->Status |= ETH_DMATXDESC_FS;
			TxDesc->ControlBufferSize = (ETH_MAX_PACKET_SIZE & ETH_DMATXDESC_TBS1);
			if (i == (bufcount-1))
			{
				TxDesc->Status |= ETH_DMATXDESC_LS;
				size = FrameLength - (bufcount-1U)*ETH_MAX_PACKET_SIZE;
				TxDesc->ControlBufferSize = (size & ETH_DMATXDESC_TBS1);
			}
			TxDesc->Status |= ETH_DMATXDESC_OWN;
			TxDesc = (ETH_DMADESCTypeDef*)(TxDesc->Buffer2NextDescAddr);
		}
	}
	if ((ETH->DMASR & ETH_DMASR_TBUS) != 0)
	{
		ETH->DMASR = ETH_DMASR_TBUS;
		ETH->DMATPDR = 0;
	}
	return;
}
uint8_t HAL_ETH_GetReceivedFrame(void)
{
	uint32_t framelength = 0U;
	if(((RxDesc->Status & ETH_DMARXDESC_OWN) == 0))
	{
		if(((RxDesc->Status & ETH_DMARXDESC_LS) != 0))
		{
			RxFrameInfos.SegCount++;
			if(RxFrameInfos.SegCount == 1) RxFrameInfos.FSRxDesc = RxDesc;
			RxFrameInfos.LSRxDesc = RxDesc;
			framelength = ((RxDesc->Status & ETH_DMARXDESC_FL) >> ETH_DMARXDESC_FRAMELENGTHSHIFT) - 4;
			RxFrameInfos.length = framelength;
			RxFrameInfos.buffer = RxFrameInfos.FSRxDesc->Buffer1Addr;
			RxDesc = (ETH_DMADESCTypeDef*)RxDesc->Buffer2NextDescAddr;
			return 0;
		}
		else
		{
			if((RxDesc->Status & ETH_DMARXDESC_FS) != 0)
			{
				RxFrameInfos.FSRxDesc = RxDesc;
				RxFrameInfos.LSRxDesc = 0;
				RxFrameInfos.SegCount = 1;
				RxDesc = (ETH_DMADESCTypeDef*)RxDesc->Buffer2NextDescAddr;
			}
			else
			{
				RxFrameInfos.SegCount++;
				RxDesc = (ETH_DMADESCTypeDef*)RxDesc->Buffer2NextDescAddr;
			}
		}
	}
	return 1;
}
uint8_t* low_level_get_mac_ptr(void)
{
	return hw_addr;
}
void low_level_data_in(uint16_t size, uint8_t* data)
{
	cb_write(&cb_tcp_rx, data, size);
}
void cb_error_signal(circle_buffer_t* buffer, cb_error_t error)
{
	proj_print("\r\ncb_error_signal");
}
void proj_print(uint8_t* ptr)
{
	uart_print_string(USART6, ptr);
}
void binh_tx(uint8_t* tx_buf, uint16_t tx_len)
{
	tcp_write(tcp_pcb_inst, tx_buf, tx_len, 0);
	tcp_output(tcp_pcb_inst);
}
