#ifndef __DAP_CONFIG_H__
#define __DAP_CONFIG_H__
//
#include <project.h>
#include <stdio.h>
//
#define CPU_CLOCK               78000000
#define IO_PORT_WRITE_CYCLES    2U              ///< I/O Cycles: 2=default, 1=Cortex-M0+ fast I/0.
#define DAP_SWD                 1               ///< SWD Mode:  1 = available, 0 = not available.
#define DAP_JTAG                0               ///< JTAG Mode: 1 = available, 0 = not available.
#define DAP_JTAG_DEV_CNT        8U              ///< Maximum number of JTAG devices on scan chain.
#define DAP_DEFAULT_PORT        1U              ///< Default JTAG/SWJ Port Mode: 1 = SWD, 2 = JTAG.
#define DAP_DEFAULT_SWJ_CLOCK   1000000U        ///< Default SWD/JTAG clock frequency in Hz.
#define DAP_PACKET_SIZE         1024U           ///< Specifies Packet Size in bytes.
#define DAP_PACKET_COUNT        4U              ///< Specifies number of packets buffered.
#define SWO_UART                0               ///< SWO UART:  1 = available, 0 = not available.
#define SWO_UART_MAX_BAUDRATE   1000000U       ///< SWO UART Maximum Baudrate in Hz.
#define SWO_MANCHESTER          0               ///< SWO Manchester:  1 = available, 0 = not available.
#define SWO_BUFFER_SIZE         8192U           ///< SWO Trace Buffer Size in bytes (must be 2^n).
#define SWO_STREAM              0               ///< SWO Streaming Trace: 1 = available, 0 = not available.
#define TIMESTAMP_CLOCK         78000000      ///< Timestamp clock in Hz (0 = timestamps not supported).
#define TARGET_DEVICE_FIXED     0               ///< Target Device: 1 = known, 0 = unknown;
#if TARGET_DEVICE_FIXED
    #define TARGET_DEVICE_VENDOR    ""              ///< String indicating the Silicon Vendor
    #define TARGET_DEVICE_NAME      ""              ///< String indicating the Target Device
#endif
//
__STATIC_INLINE uint8_t DAP_GetVendorString (char *str) 
{
  (void)str;
  return (0U);
}
__STATIC_INLINE uint8_t DAP_GetProductString (char *str) 
{
  (void)str;
  return (0U);
}
__STATIC_INLINE uint8_t DAP_GetSerNumString (char *str) 
{
  (void)str;
  return (0U);
}
/*
Standard I/O Pins of the CMSIS-DAP Hardware Debug Port support standard JTAG mode
and Serial Wire Debug (SWD) mode. In SWD mode only 2 pins are required to implement the debug 
interface of a device. The following I/O Pins are provided:

JTAG I/O Pin                 | SWD I/O Pin          | CMSIS-DAP Hardware pin mode
---------------------------- | -------------------- | ---------------------------------------------
TCK: Test Clock              | SWCLK: Clock         | Output Push/Pull
TMS: Test Mode Select        | SWDIO: Data I/O      | Output Push/Pull; Input (for receiving data)
TDI: Test Data Input         |                      | Output Push/Pull
TDO: Test Data Output        |                      | Input             
nTRST: Test Reset (optional) |                      | Output Open Drain with pull-up resistor
nRESET: Device Reset         | nRESET: Device Reset | Output Open Drain with pull-up resistor
*/

/** Setup JTAG I/O pins: TCK, TMS, TDI, TDO, nTRST, and nRESET.
Configures the DAP Hardware I/O pins for JTAG mode:
 - TCK, TMS, TDI, nTRST, nRESET to output mode and set to high level.
 - TDO to input mode.
*/ 
__STATIC_INLINE void PORT_JTAG_SETUP (void) 
{
    nRST_Write(1);
}
/** Setup SWD I/O pins: SWCLK, SWDIO, and nRESET.
Configures the DAP Hardware I/O pins for Serial Wire Debug (SWD) mode:
 - SWCLK, SWDIO, nRESET to output mode and set to default high level.
 - TDI, nTRST to HighZ mode (pins are unused in SWD mode).
*/ 
__STATIC_INLINE void PORT_SWD_SETUP (void) 
{
    nRST_Write(1);
    SWDCLK_SetDriveMode(CY_PINS_DM_STRONG);
    SWDCLK_Write(1);
    SWDIO_SetDriveMode(CY_PINS_DM_STRONG);
    SWDIO_Write(1);
}
/** Disable JTAG/SWD I/O Pins.
Disables the DAP Hardware I/O pins which configures:
 - TCK/SWCLK, TMS/SWDIO, TDI, TDO, nTRST, nRESET to High-Z mode.
*/
__STATIC_INLINE void PORT_OFF (void) 
{
    nRST_SetDriveMode(CY_PINS_DM_DIG_HIZ);
    SWDCLK_SetDriveMode(CY_PINS_DM_DIG_HIZ);
    SWDIO_SetDriveMode(CY_PINS_DM_DIG_HIZ);
}
/** SWCLK/TCK I/O pin: Get Input.
\return Current status of the SWCLK/TCK DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_SWCLK_TCK_IN(void) 
{
    return SWDCLK_Read();
}
/** SWCLK/TCK I/O pin: Set Output to High.
Set the SWCLK/TCK DAP hardware I/O pin to high level.
*/
__STATIC_FORCEINLINE void PIN_SWCLK_TCK_SET(void) 
{
    SWDCLK_Write(1);
}
/** SWCLK/TCK I/O pin: Set Output to Low.
Set the SWCLK/TCK DAP hardware I/O pin to low level.
*/
__STATIC_FORCEINLINE void PIN_SWCLK_TCK_CLR(void) 
{
    SWDCLK_Write(0);
}
/** SWDIO/TMS I/O pin: Get Input.
\return Current status of the SWDIO/TMS DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_SWDIO_TMS_IN(void) 
{
    return SWDIO_Read();
}
/** SWDIO/TMS I/O pin: Set Output to High.
Set the SWDIO/TMS DAP hardware I/O pin to high level.
*/
__STATIC_FORCEINLINE void PIN_SWDIO_TMS_SET(void) 
{
    SWDIO_Write(1);
}
/** SWDIO/TMS I/O pin: Set Output to Low.
Set the SWDIO/TMS DAP hardware I/O pin to low level.
*/
__STATIC_FORCEINLINE void PIN_SWDIO_TMS_CLR(void)
{
    SWDIO_Write(0);
}
/** SWDIO I/O pin: Get Input (used in SWD mode only).
\return Current status of the SWDIO DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_SWDIO_IN(void) 
{
    return SWDIO_Read();
}
/** SWDIO I/O pin: Set Output (used in SWD mode only).
\param bit Output value for the SWDIO DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE void PIN_SWDIO_OUT(uint32_t bit) 
{
    SWDIO_Write(bit);
}
/** SWDIO I/O pin: Switch to Output mode (used in SWD mode only).
Configure the SWDIO DAP hardware I/O pin to output mode. This function is
called prior \ref PIN_SWDIO_OUT function calls.
*/
__STATIC_FORCEINLINE void PIN_SWDIO_OUT_ENABLE(void)
{
    SWDIO_SetDriveMode(CY_PINS_DM_STRONG);
}
/** SWDIO I/O pin: Switch to Input mode (used in SWD mode only).
Configure the SWDIO DAP hardware I/O pin to input mode. This function is
called prior \ref PIN_SWDIO_IN function calls.
*/
__STATIC_FORCEINLINE void PIN_SWDIO_OUT_DISABLE (void) 
{
    SWDIO_SetDriveMode(CY_PINS_DM_DIG_HIZ);
}
/** TDI I/O pin: Get Input.
\return Current status of the TDI DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_TDI_IN  (void) 
{
    return 0;
}
/** TDI I/O pin: Set Output.
\param bit Output value for the TDI DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE void PIN_TDI_OUT(uint32_t bit) 
{
    (void)bit;
}
/** TDO I/O pin: Get Input.
\return Current status of the TDO DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_TDO_IN(void) 
{
    return 0;
}
__STATIC_FORCEINLINE uint32_t PIN_nTRST_IN(void) 
{
    return 0;
}
/** nTRST I/O pin: Set Output.
\param bit JTAG TRST Test Reset pin status:
           - 0: issue a JTAG TRST Test Reset.
           - 1: release JTAG TRST Test Reset.
*/
__STATIC_FORCEINLINE void PIN_nTRST_OUT(uint32_t bit) 
{
    (void)bit;
}
/** nRESET I/O pin: Get Input.
\return Current status of the nRESET DAP hardware I/O pin.
*/
__STATIC_FORCEINLINE uint32_t PIN_nRESET_IN(void) 
{
    return nRST_Read();
}
/** nRESET I/O pin: Set Output.
\param bit target device hardware reset pin status:
           - 0: issue a device hardware reset.
           - 1: release device hardware reset.
*/
__STATIC_FORCEINLINE void PIN_nRESET_OUT (uint32_t bit) 
{
    nRST_Write(bit);
}
__STATIC_INLINE void LED_CONNECTED_OUT (uint32_t bit) 
{
    LED_Write(bit);
}
__STATIC_INLINE void LED_RUNNING_OUT (uint32_t bit) 
{
    (void)bit;
}
__STATIC_INLINE uint32_t TIMESTAMP_GET (void) 
{
    return (DWT->CYCCNT);
}
__STATIC_INLINE void DAP_SETUP (void) 
{
}
__STATIC_INLINE uint8_t RESET_TARGET(void) 
{
    return (0U);             // change to '1' when a device reset sequence is implemented
}

#endif /* __DAP_CONFIG_H__ */
